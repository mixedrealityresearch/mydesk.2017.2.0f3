﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.Schema.TypedObject/DecimalStruct
struct DecimalStruct_t715828147;
// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1656058977;
// System.Xml.XmlQualifiedName/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_t3565421161;
// System.Xml.Schema.KeySequence
struct KeySequence_t746093258;
// System.Xml.Schema.LocatedActiveAxis[]
struct LocatedActiveAxisU5BU5D_t2201939280;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Decimal[]
struct DecimalU5BU5D_t624008824;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t2064808786;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Xml.Schema.TypedObject[]
struct TypedObjectU5BU5D_t948546190;
// System.Xml.Schema.DoubleLinkAxis
struct DoubleLinkAxis_t3164907012;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t2971213394;
// System.Xml.Schema.ForwardAxis
struct ForwardAxis_t3876904870;
// System.Xml.Schema.ActiveAxis
struct ActiveAxis_t439376929;
// System.Xml.Schema.Asttree
struct Asttree_t3451058494;
// System.Xml.Schema.CompiledIdentityConstraint
struct CompiledIdentityConstraint_t964629540;
// System.Xml.Schema.SelectorActiveAxis
struct SelectorActiveAxis_t789423304;
// MS.Internal.Xml.Cache.XPathNode[]
struct XPathNodeU5BU5D_t339325318;
// System.Collections.Generic.Dictionary`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef>
struct Dictionary_2_t3726397491;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t3518500204;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_t482152337;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Uri
struct Uri_t19570940;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t1507412803;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.ValidationState
struct ValidationState_t3143048826;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Text.DecoderFallback
struct DecoderFallback_t1715117820;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t4206371382;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2022420531;
// System.Text.EncoderFallback
struct EncoderFallback_t1756452756;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t1975884510;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t2217612696;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Threading.Tasks.Task`1<System.Net.WebResponse>
struct Task_1_t1015255058;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t2745753060;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t461808439;
// System.Xml.Schema.SyntaxTreeNode
struct SyntaxTreeNode_t2397191729;
// System.Xml.Schema.SequenceNode
struct SequenceNode_t4039907291;
// System.Xml.Schema.BitSet
struct BitSet_t1062448123;
// System.Xml.Schema.NamespaceList
struct NamespaceList_t848177191;
// System.Threading.Tasks.Task`1<System.IO.Stream>
struct Task_1_t2375465813;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Xml.Schema.ConstraintStruct
struct ConstraintStruct_t2462842120;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;
// System.Xml.XPath.XPathNavigatorKeyComparer
struct XPathNavigatorKeyComparer_t3055722314;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_t3152578875;
// System.Action
struct Action_t3226471752;
// System.Void
struct Void_t1841601450;
// System.Xml.Ucs4Decoder
struct Ucs4Decoder_t2645160027;
// System.Type
struct Type_t;
// System.IO.Stream
struct Stream_t3255436806;
// System.Xml.XmlDownloadManager
struct XmlDownloadManager_t830495394;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_t1809478302;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t1191906455;
// System.String[]
struct StringU5BU5D_t1642385972;
// MS.Internal.Xml.XPath.AstNode
struct AstNode_t2002670936;
// System.Xml.XmlUrlResolver
struct XmlUrlResolver_t896669594;
// System.Xml.IDtdParserAdapter
struct IDtdParserAdapter_t2279635749;
// System.Xml.IDtdParserAdapterWithValidation
struct IDtdParserAdapterWithValidation_t636382188;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.DtdParser/UndeclaredNotation>
struct Dictionary_2_t3981174159;
// System.Xml.Schema.Asttree[]
struct AsttreeU5BU5D_t1751151627;
// System.Net.WebRequest
struct WebRequest_t1365124353;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.Xml.Schema.BitSet[]
struct BitSetU5BU5D_t2256991674;
// System.Xml.Schema.SymbolsDictionary
struct SymbolsDictionary_t1753655453;
// System.Xml.Schema.Positions
struct Positions_t3593914952;
// MS.Internal.Xml.XPath.Axis
struct Axis_t1660922251;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;
struct Decimal_t724701077 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TYPEDOBJECT_T1797374135_H
#define TYPEDOBJECT_T1797374135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject
struct  TypedObject_t1797374135  : public RuntimeObject
{
public:
	// System.Xml.Schema.TypedObject/DecimalStruct System.Xml.Schema.TypedObject::dstruct
	DecimalStruct_t715828147 * ___dstruct_0;
	// System.Object System.Xml.Schema.TypedObject::ovalue
	RuntimeObject * ___ovalue_1;
	// System.String System.Xml.Schema.TypedObject::svalue
	String_t* ___svalue_2;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.TypedObject::xsdtype
	XmlSchemaDatatype_t1195946242 * ___xsdtype_3;
	// System.Int32 System.Xml.Schema.TypedObject::dim
	int32_t ___dim_4;
	// System.Boolean System.Xml.Schema.TypedObject::isList
	bool ___isList_5;

public:
	inline static int32_t get_offset_of_dstruct_0() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___dstruct_0)); }
	inline DecimalStruct_t715828147 * get_dstruct_0() const { return ___dstruct_0; }
	inline DecimalStruct_t715828147 ** get_address_of_dstruct_0() { return &___dstruct_0; }
	inline void set_dstruct_0(DecimalStruct_t715828147 * value)
	{
		___dstruct_0 = value;
		Il2CppCodeGenWriteBarrier((&___dstruct_0), value);
	}

	inline static int32_t get_offset_of_ovalue_1() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___ovalue_1)); }
	inline RuntimeObject * get_ovalue_1() const { return ___ovalue_1; }
	inline RuntimeObject ** get_address_of_ovalue_1() { return &___ovalue_1; }
	inline void set_ovalue_1(RuntimeObject * value)
	{
		___ovalue_1 = value;
		Il2CppCodeGenWriteBarrier((&___ovalue_1), value);
	}

	inline static int32_t get_offset_of_svalue_2() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___svalue_2)); }
	inline String_t* get_svalue_2() const { return ___svalue_2; }
	inline String_t** get_address_of_svalue_2() { return &___svalue_2; }
	inline void set_svalue_2(String_t* value)
	{
		___svalue_2 = value;
		Il2CppCodeGenWriteBarrier((&___svalue_2), value);
	}

	inline static int32_t get_offset_of_xsdtype_3() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___xsdtype_3)); }
	inline XmlSchemaDatatype_t1195946242 * get_xsdtype_3() const { return ___xsdtype_3; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_xsdtype_3() { return &___xsdtype_3; }
	inline void set_xsdtype_3(XmlSchemaDatatype_t1195946242 * value)
	{
		___xsdtype_3 = value;
		Il2CppCodeGenWriteBarrier((&___xsdtype_3), value);
	}

	inline static int32_t get_offset_of_dim_4() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___dim_4)); }
	inline int32_t get_dim_4() const { return ___dim_4; }
	inline int32_t* get_address_of_dim_4() { return &___dim_4; }
	inline void set_dim_4(int32_t value)
	{
		___dim_4 = value;
	}

	inline static int32_t get_offset_of_isList_5() { return static_cast<int32_t>(offsetof(TypedObject_t1797374135, ___isList_5)); }
	inline bool get_isList_5() const { return ___isList_5; }
	inline bool* get_address_of_isList_5() { return &___isList_5; }
	inline void set_isList_5(bool value)
	{
		___isList_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDOBJECT_T1797374135_H
#ifndef XMLNAMETABLE_T1345805268_H
#define XMLNAMETABLE_T1345805268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t1345805268  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T1345805268_H
#ifndef MARSHALBYREFOBJECT_T1285298191_H
#define MARSHALBYREFOBJECT_T1285298191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1285298191  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1656058977 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1285298191, ____identity_0)); }
	inline ServerIdentity_t1656058977 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1656058977 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1656058977 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_pinvoke
{
	ServerIdentity_t1656058977 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_com
{
	ServerIdentity_t1656058977 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T1285298191_H
#ifndef XMLQUALIFIEDNAME_T1944712516_H
#define XMLQUALIFIEDNAME_T1944712516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t1944712516  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

struct XmlQualifiedName_t1944712516_StaticFields
{
public:
	// System.Xml.XmlQualifiedName/HashCodeOfStringDelegate System.Xml.XmlQualifiedName::hashCodeDelegate
	HashCodeOfStringDelegate_t3565421161 * ___hashCodeDelegate_0;
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t1944712516 * ___Empty_4;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_t3565421161 * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_t3565421161 ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_t3565421161 * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashCodeDelegate_0), value);
	}

	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516_StaticFields, ___Empty_4)); }
	inline XmlQualifiedName_t1944712516 * get_Empty_4() const { return ___Empty_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(XmlQualifiedName_t1944712516 * value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLQUALIFIEDNAME_T1944712516_H
#ifndef KSSTRUCT_T704598349_H
#define KSSTRUCT_T704598349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KSStruct
struct  KSStruct_t704598349  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.KSStruct::depth
	int32_t ___depth_0;
	// System.Xml.Schema.KeySequence System.Xml.Schema.KSStruct::ks
	KeySequence_t746093258 * ___ks_1;
	// System.Xml.Schema.LocatedActiveAxis[] System.Xml.Schema.KSStruct::fields
	LocatedActiveAxisU5BU5D_t2201939280* ___fields_2;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(KSStruct_t704598349, ___depth_0)); }
	inline int32_t get_depth_0() const { return ___depth_0; }
	inline int32_t* get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(int32_t value)
	{
		___depth_0 = value;
	}

	inline static int32_t get_offset_of_ks_1() { return static_cast<int32_t>(offsetof(KSStruct_t704598349, ___ks_1)); }
	inline KeySequence_t746093258 * get_ks_1() const { return ___ks_1; }
	inline KeySequence_t746093258 ** get_address_of_ks_1() { return &___ks_1; }
	inline void set_ks_1(KeySequence_t746093258 * value)
	{
		___ks_1 = value;
		Il2CppCodeGenWriteBarrier((&___ks_1), value);
	}

	inline static int32_t get_offset_of_fields_2() { return static_cast<int32_t>(offsetof(KSStruct_t704598349, ___fields_2)); }
	inline LocatedActiveAxisU5BU5D_t2201939280* get_fields_2() const { return ___fields_2; }
	inline LocatedActiveAxisU5BU5D_t2201939280** get_address_of_fields_2() { return &___fields_2; }
	inline void set_fields_2(LocatedActiveAxisU5BU5D_t2201939280* value)
	{
		___fields_2 = value;
		Il2CppCodeGenWriteBarrier((&___fields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KSSTRUCT_T704598349_H
#ifndef XMLSERIALIZERNAMESPACES_T3063656491_H
#define XMLSERIALIZERNAMESPACES_T3063656491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t3063656491  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	Hashtable_t909839986 * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t3063656491, ___namespaces_0)); }
	inline Hashtable_t909839986 * get_namespaces_0() const { return ___namespaces_0; }
	inline Hashtable_t909839986 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(Hashtable_t909839986 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_T3063656491_H
#ifndef POSITIONS_T3593914952_H
#define POSITIONS_T3593914952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Positions
struct  Positions_t3593914952  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Positions::positions
	ArrayList_t4252133567 * ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(Positions_t3593914952, ___positions_0)); }
	inline ArrayList_t4252133567 * get_positions_0() const { return ___positions_0; }
	inline ArrayList_t4252133567 ** get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(ArrayList_t4252133567 * value)
	{
		___positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___positions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONS_T3593914952_H
#ifndef SYNTAXTREENODE_T2397191729_H
#define SYNTAXTREENODE_T2397191729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SyntaxTreeNode
struct  SyntaxTreeNode_t2397191729  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXTREENODE_T2397191729_H
#ifndef DECIMALSTRUCT_T715828147_H
#define DECIMALSTRUCT_T715828147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.TypedObject/DecimalStruct
struct  DecimalStruct_t715828147  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.TypedObject/DecimalStruct::isDecimal
	bool ___isDecimal_0;
	// System.Decimal[] System.Xml.Schema.TypedObject/DecimalStruct::dvalue
	DecimalU5BU5D_t624008824* ___dvalue_1;

public:
	inline static int32_t get_offset_of_isDecimal_0() { return static_cast<int32_t>(offsetof(DecimalStruct_t715828147, ___isDecimal_0)); }
	inline bool get_isDecimal_0() const { return ___isDecimal_0; }
	inline bool* get_address_of_isDecimal_0() { return &___isDecimal_0; }
	inline void set_isDecimal_0(bool value)
	{
		___isDecimal_0 = value;
	}

	inline static int32_t get_offset_of_dvalue_1() { return static_cast<int32_t>(offsetof(DecimalStruct_t715828147, ___dvalue_1)); }
	inline DecimalU5BU5D_t624008824* get_dvalue_1() const { return ___dvalue_1; }
	inline DecimalU5BU5D_t624008824** get_address_of_dvalue_1() { return &___dvalue_1; }
	inline void set_dvalue_1(DecimalU5BU5D_t624008824* value)
	{
		___dvalue_1 = value;
		Il2CppCodeGenWriteBarrier((&___dvalue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALSTRUCT_T715828147_H
#ifndef XMLNAMESPACEMANAGER_T486731501_H
#define XMLNAMESPACEMANAGER_T486731501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t486731501  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t2064808786* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_t3986656710 * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t2064808786* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t2064808786** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t2064808786* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_0), value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___hashTable_4)); }
	inline Dictionary_2_t3986656710 * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_t3986656710 ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_t3986656710 * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashTable_4), value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((&___xml_6), value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlNs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T486731501_H
#ifndef KEYSEQUENCE_T746093258_H
#define KEYSEQUENCE_T746093258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.KeySequence
struct  KeySequence_t746093258  : public RuntimeObject
{
public:
	// System.Xml.Schema.TypedObject[] System.Xml.Schema.KeySequence::ks
	TypedObjectU5BU5D_t948546190* ___ks_0;
	// System.Int32 System.Xml.Schema.KeySequence::dim
	int32_t ___dim_1;
	// System.Int32 System.Xml.Schema.KeySequence::hashcode
	int32_t ___hashcode_2;
	// System.Int32 System.Xml.Schema.KeySequence::posline
	int32_t ___posline_3;
	// System.Int32 System.Xml.Schema.KeySequence::poscol
	int32_t ___poscol_4;

public:
	inline static int32_t get_offset_of_ks_0() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___ks_0)); }
	inline TypedObjectU5BU5D_t948546190* get_ks_0() const { return ___ks_0; }
	inline TypedObjectU5BU5D_t948546190** get_address_of_ks_0() { return &___ks_0; }
	inline void set_ks_0(TypedObjectU5BU5D_t948546190* value)
	{
		___ks_0 = value;
		Il2CppCodeGenWriteBarrier((&___ks_0), value);
	}

	inline static int32_t get_offset_of_dim_1() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___dim_1)); }
	inline int32_t get_dim_1() const { return ___dim_1; }
	inline int32_t* get_address_of_dim_1() { return &___dim_1; }
	inline void set_dim_1(int32_t value)
	{
		___dim_1 = value;
	}

	inline static int32_t get_offset_of_hashcode_2() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___hashcode_2)); }
	inline int32_t get_hashcode_2() const { return ___hashcode_2; }
	inline int32_t* get_address_of_hashcode_2() { return &___hashcode_2; }
	inline void set_hashcode_2(int32_t value)
	{
		___hashcode_2 = value;
	}

	inline static int32_t get_offset_of_posline_3() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___posline_3)); }
	inline int32_t get_posline_3() const { return ___posline_3; }
	inline int32_t* get_address_of_posline_3() { return &___posline_3; }
	inline void set_posline_3(int32_t value)
	{
		___posline_3 = value;
	}

	inline static int32_t get_offset_of_poscol_4() { return static_cast<int32_t>(offsetof(KeySequence_t746093258, ___poscol_4)); }
	inline int32_t get_poscol_4() const { return ___poscol_4; }
	inline int32_t* get_address_of_poscol_4() { return &___poscol_4; }
	inline void set_poscol_4(int32_t value)
	{
		___poscol_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYSEQUENCE_T746093258_H
#ifndef XMLRESOLVER_T2024571559_H
#define XMLRESOLVER_T2024571559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlResolver
struct  XmlResolver_t2024571559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESOLVER_T2024571559_H
#ifndef FORWARDAXIS_T3876904870_H
#define FORWARDAXIS_T3876904870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ForwardAxis
struct  ForwardAxis_t3876904870  : public RuntimeObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::topNode
	DoubleLinkAxis_t3164907012 * ___topNode_0;
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.ForwardAxis::rootNode
	DoubleLinkAxis_t3164907012 * ___rootNode_1;
	// System.Boolean System.Xml.Schema.ForwardAxis::isAttribute
	bool ___isAttribute_2;
	// System.Boolean System.Xml.Schema.ForwardAxis::isDss
	bool ___isDss_3;
	// System.Boolean System.Xml.Schema.ForwardAxis::isSelfAxis
	bool ___isSelfAxis_4;

public:
	inline static int32_t get_offset_of_topNode_0() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___topNode_0)); }
	inline DoubleLinkAxis_t3164907012 * get_topNode_0() const { return ___topNode_0; }
	inline DoubleLinkAxis_t3164907012 ** get_address_of_topNode_0() { return &___topNode_0; }
	inline void set_topNode_0(DoubleLinkAxis_t3164907012 * value)
	{
		___topNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___topNode_0), value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___rootNode_1)); }
	inline DoubleLinkAxis_t3164907012 * get_rootNode_1() const { return ___rootNode_1; }
	inline DoubleLinkAxis_t3164907012 ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(DoubleLinkAxis_t3164907012 * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootNode_1), value);
	}

	inline static int32_t get_offset_of_isAttribute_2() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___isAttribute_2)); }
	inline bool get_isAttribute_2() const { return ___isAttribute_2; }
	inline bool* get_address_of_isAttribute_2() { return &___isAttribute_2; }
	inline void set_isAttribute_2(bool value)
	{
		___isAttribute_2 = value;
	}

	inline static int32_t get_offset_of_isDss_3() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___isDss_3)); }
	inline bool get_isDss_3() const { return ___isDss_3; }
	inline bool* get_address_of_isDss_3() { return &___isDss_3; }
	inline void set_isDss_3(bool value)
	{
		___isDss_3 = value;
	}

	inline static int32_t get_offset_of_isSelfAxis_4() { return static_cast<int32_t>(offsetof(ForwardAxis_t3876904870, ___isSelfAxis_4)); }
	inline bool get_isSelfAxis_4() const { return ___isSelfAxis_4; }
	inline bool* get_address_of_isSelfAxis_4() { return &___isSelfAxis_4; }
	inline void set_isSelfAxis_4(bool value)
	{
		___isSelfAxis_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDAXIS_T3876904870_H
#ifndef ASTTREE_T3451058494_H
#define ASTTREE_T3451058494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Asttree
struct  Asttree_t3451058494  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.Asttree::fAxisArray
	ArrayList_t4252133567 * ___fAxisArray_0;
	// System.String System.Xml.Schema.Asttree::xpathexpr
	String_t* ___xpathexpr_1;
	// System.Boolean System.Xml.Schema.Asttree::isField
	bool ___isField_2;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Asttree::nsmgr
	XmlNamespaceManager_t486731501 * ___nsmgr_3;

public:
	inline static int32_t get_offset_of_fAxisArray_0() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___fAxisArray_0)); }
	inline ArrayList_t4252133567 * get_fAxisArray_0() const { return ___fAxisArray_0; }
	inline ArrayList_t4252133567 ** get_address_of_fAxisArray_0() { return &___fAxisArray_0; }
	inline void set_fAxisArray_0(ArrayList_t4252133567 * value)
	{
		___fAxisArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___fAxisArray_0), value);
	}

	inline static int32_t get_offset_of_xpathexpr_1() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___xpathexpr_1)); }
	inline String_t* get_xpathexpr_1() const { return ___xpathexpr_1; }
	inline String_t** get_address_of_xpathexpr_1() { return &___xpathexpr_1; }
	inline void set_xpathexpr_1(String_t* value)
	{
		___xpathexpr_1 = value;
		Il2CppCodeGenWriteBarrier((&___xpathexpr_1), value);
	}

	inline static int32_t get_offset_of_isField_2() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___isField_2)); }
	inline bool get_isField_2() const { return ___isField_2; }
	inline bool* get_address_of_isField_2() { return &___isField_2; }
	inline void set_isField_2(bool value)
	{
		___isField_2 = value;
	}

	inline static int32_t get_offset_of_nsmgr_3() { return static_cast<int32_t>(offsetof(Asttree_t3451058494, ___nsmgr_3)); }
	inline XmlNamespaceManager_t486731501 * get_nsmgr_3() const { return ___nsmgr_3; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsmgr_3() { return &___nsmgr_3; }
	inline void set_nsmgr_3(XmlNamespaceManager_t486731501 * value)
	{
		___nsmgr_3 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTTREE_T3451058494_H
#ifndef BASEPROCESSOR_T2373158431_H
#define BASEPROCESSOR_T2373158431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseProcessor
struct  BaseProcessor_t2373158431  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.BaseProcessor::nameTable
	XmlNameTable_t1345805268 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseProcessor::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.BaseProcessor::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.BaseProcessor::compilationSettings
	XmlSchemaCompilationSettings_t2971213394 * ___compilationSettings_3;
	// System.Int32 System.Xml.Schema.BaseProcessor::errorCount
	int32_t ___errorCount_4;
	// System.String System.Xml.Schema.BaseProcessor::NsXml
	String_t* ___NsXml_5;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___nameTable_0)); }
	inline XmlNameTable_t1345805268 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t1345805268 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___schemaNames_1)); }
	inline SchemaNames_t1619962557 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t1619962557 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_1), value);
	}

	inline static int32_t get_offset_of_eventHandler_2() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___eventHandler_2)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_2() const { return ___eventHandler_2; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_2() { return &___eventHandler_2; }
	inline void set_eventHandler_2(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_2), value);
	}

	inline static int32_t get_offset_of_compilationSettings_3() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___compilationSettings_3)); }
	inline XmlSchemaCompilationSettings_t2971213394 * get_compilationSettings_3() const { return ___compilationSettings_3; }
	inline XmlSchemaCompilationSettings_t2971213394 ** get_address_of_compilationSettings_3() { return &___compilationSettings_3; }
	inline void set_compilationSettings_3(XmlSchemaCompilationSettings_t2971213394 * value)
	{
		___compilationSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___compilationSettings_3), value);
	}

	inline static int32_t get_offset_of_errorCount_4() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___errorCount_4)); }
	inline int32_t get_errorCount_4() const { return ___errorCount_4; }
	inline int32_t* get_address_of_errorCount_4() { return &___errorCount_4; }
	inline void set_errorCount_4(int32_t value)
	{
		___errorCount_4 = value;
	}

	inline static int32_t get_offset_of_NsXml_5() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___NsXml_5)); }
	inline String_t* get_NsXml_5() const { return ___NsXml_5; }
	inline String_t** get_address_of_NsXml_5() { return &___NsXml_5; }
	inline void set_NsXml_5(String_t* value)
	{
		___NsXml_5 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPROCESSOR_T2373158431_H
#ifndef AXISELEMENT_T941837363_H
#define AXISELEMENT_T941837363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisElement
struct  AxisElement_t941837363  : public RuntimeObject
{
public:
	// System.Xml.Schema.DoubleLinkAxis System.Xml.Schema.AxisElement::curNode
	DoubleLinkAxis_t3164907012 * ___curNode_0;
	// System.Int32 System.Xml.Schema.AxisElement::rootDepth
	int32_t ___rootDepth_1;
	// System.Int32 System.Xml.Schema.AxisElement::curDepth
	int32_t ___curDepth_2;
	// System.Boolean System.Xml.Schema.AxisElement::isMatch
	bool ___isMatch_3;

public:
	inline static int32_t get_offset_of_curNode_0() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___curNode_0)); }
	inline DoubleLinkAxis_t3164907012 * get_curNode_0() const { return ___curNode_0; }
	inline DoubleLinkAxis_t3164907012 ** get_address_of_curNode_0() { return &___curNode_0; }
	inline void set_curNode_0(DoubleLinkAxis_t3164907012 * value)
	{
		___curNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___curNode_0), value);
	}

	inline static int32_t get_offset_of_rootDepth_1() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___rootDepth_1)); }
	inline int32_t get_rootDepth_1() const { return ___rootDepth_1; }
	inline int32_t* get_address_of_rootDepth_1() { return &___rootDepth_1; }
	inline void set_rootDepth_1(int32_t value)
	{
		___rootDepth_1 = value;
	}

	inline static int32_t get_offset_of_curDepth_2() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___curDepth_2)); }
	inline int32_t get_curDepth_2() const { return ___curDepth_2; }
	inline int32_t* get_address_of_curDepth_2() { return &___curDepth_2; }
	inline void set_curDepth_2(int32_t value)
	{
		___curDepth_2 = value;
	}

	inline static int32_t get_offset_of_isMatch_3() { return static_cast<int32_t>(offsetof(AxisElement_t941837363, ___isMatch_3)); }
	inline bool get_isMatch_3() const { return ___isMatch_3; }
	inline bool* get_address_of_isMatch_3() { return &___isMatch_3; }
	inline void set_isMatch_3(bool value)
	{
		___isMatch_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISELEMENT_T941837363_H
#ifndef AXISSTACK_T3388994403_H
#define AXISSTACK_T3388994403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AxisStack
struct  AxisStack_t3388994403  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.Schema.AxisStack::stack
	ArrayList_t4252133567 * ___stack_0;
	// System.Xml.Schema.ForwardAxis System.Xml.Schema.AxisStack::subtree
	ForwardAxis_t3876904870 * ___subtree_1;
	// System.Xml.Schema.ActiveAxis System.Xml.Schema.AxisStack::parent
	ActiveAxis_t439376929 * ___parent_2;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(AxisStack_t3388994403, ___stack_0)); }
	inline ArrayList_t4252133567 * get_stack_0() const { return ___stack_0; }
	inline ArrayList_t4252133567 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ArrayList_t4252133567 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_subtree_1() { return static_cast<int32_t>(offsetof(AxisStack_t3388994403, ___subtree_1)); }
	inline ForwardAxis_t3876904870 * get_subtree_1() const { return ___subtree_1; }
	inline ForwardAxis_t3876904870 ** get_address_of_subtree_1() { return &___subtree_1; }
	inline void set_subtree_1(ForwardAxis_t3876904870 * value)
	{
		___subtree_1 = value;
		Il2CppCodeGenWriteBarrier((&___subtree_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(AxisStack_t3388994403, ___parent_2)); }
	inline ActiveAxis_t439376929 * get_parent_2() const { return ___parent_2; }
	inline ActiveAxis_t439376929 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ActiveAxis_t439376929 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSTACK_T3388994403_H
#ifndef ACTIVEAXIS_T439376929_H
#define ACTIVEAXIS_T439376929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ActiveAxis
struct  ActiveAxis_t439376929  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.ActiveAxis::currentDepth
	int32_t ___currentDepth_0;
	// System.Boolean System.Xml.Schema.ActiveAxis::isActive
	bool ___isActive_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.ActiveAxis::axisTree
	Asttree_t3451058494 * ___axisTree_2;
	// System.Collections.ArrayList System.Xml.Schema.ActiveAxis::axisStack
	ArrayList_t4252133567 * ___axisStack_3;

public:
	inline static int32_t get_offset_of_currentDepth_0() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___currentDepth_0)); }
	inline int32_t get_currentDepth_0() const { return ___currentDepth_0; }
	inline int32_t* get_address_of_currentDepth_0() { return &___currentDepth_0; }
	inline void set_currentDepth_0(int32_t value)
	{
		___currentDepth_0 = value;
	}

	inline static int32_t get_offset_of_isActive_1() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___isActive_1)); }
	inline bool get_isActive_1() const { return ___isActive_1; }
	inline bool* get_address_of_isActive_1() { return &___isActive_1; }
	inline void set_isActive_1(bool value)
	{
		___isActive_1 = value;
	}

	inline static int32_t get_offset_of_axisTree_2() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___axisTree_2)); }
	inline Asttree_t3451058494 * get_axisTree_2() const { return ___axisTree_2; }
	inline Asttree_t3451058494 ** get_address_of_axisTree_2() { return &___axisTree_2; }
	inline void set_axisTree_2(Asttree_t3451058494 * value)
	{
		___axisTree_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisTree_2), value);
	}

	inline static int32_t get_offset_of_axisStack_3() { return static_cast<int32_t>(offsetof(ActiveAxis_t439376929, ___axisStack_3)); }
	inline ArrayList_t4252133567 * get_axisStack_3() const { return ___axisStack_3; }
	inline ArrayList_t4252133567 ** get_address_of_axisStack_3() { return &___axisStack_3; }
	inline void set_axisStack_3(ArrayList_t4252133567 * value)
	{
		___axisStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___axisStack_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEAXIS_T439376929_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef CONSTRAINTSTRUCT_T2462842120_H
#define CONSTRAINTSTRUCT_T2462842120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ConstraintStruct
struct  ConstraintStruct_t2462842120  : public RuntimeObject
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.ConstraintStruct::constraint
	CompiledIdentityConstraint_t964629540 * ___constraint_0;
	// System.Xml.Schema.SelectorActiveAxis System.Xml.Schema.ConstraintStruct::axisSelector
	SelectorActiveAxis_t789423304 * ___axisSelector_1;
	// System.Collections.ArrayList System.Xml.Schema.ConstraintStruct::axisFields
	ArrayList_t4252133567 * ___axisFields_2;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::qualifiedTable
	Hashtable_t909839986 * ___qualifiedTable_3;
	// System.Collections.Hashtable System.Xml.Schema.ConstraintStruct::keyrefTable
	Hashtable_t909839986 * ___keyrefTable_4;
	// System.Int32 System.Xml.Schema.ConstraintStruct::tableDim
	int32_t ___tableDim_5;

public:
	inline static int32_t get_offset_of_constraint_0() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___constraint_0)); }
	inline CompiledIdentityConstraint_t964629540 * get_constraint_0() const { return ___constraint_0; }
	inline CompiledIdentityConstraint_t964629540 ** get_address_of_constraint_0() { return &___constraint_0; }
	inline void set_constraint_0(CompiledIdentityConstraint_t964629540 * value)
	{
		___constraint_0 = value;
		Il2CppCodeGenWriteBarrier((&___constraint_0), value);
	}

	inline static int32_t get_offset_of_axisSelector_1() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___axisSelector_1)); }
	inline SelectorActiveAxis_t789423304 * get_axisSelector_1() const { return ___axisSelector_1; }
	inline SelectorActiveAxis_t789423304 ** get_address_of_axisSelector_1() { return &___axisSelector_1; }
	inline void set_axisSelector_1(SelectorActiveAxis_t789423304 * value)
	{
		___axisSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisSelector_1), value);
	}

	inline static int32_t get_offset_of_axisFields_2() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___axisFields_2)); }
	inline ArrayList_t4252133567 * get_axisFields_2() const { return ___axisFields_2; }
	inline ArrayList_t4252133567 ** get_address_of_axisFields_2() { return &___axisFields_2; }
	inline void set_axisFields_2(ArrayList_t4252133567 * value)
	{
		___axisFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisFields_2), value);
	}

	inline static int32_t get_offset_of_qualifiedTable_3() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___qualifiedTable_3)); }
	inline Hashtable_t909839986 * get_qualifiedTable_3() const { return ___qualifiedTable_3; }
	inline Hashtable_t909839986 ** get_address_of_qualifiedTable_3() { return &___qualifiedTable_3; }
	inline void set_qualifiedTable_3(Hashtable_t909839986 * value)
	{
		___qualifiedTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedTable_3), value);
	}

	inline static int32_t get_offset_of_keyrefTable_4() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___keyrefTable_4)); }
	inline Hashtable_t909839986 * get_keyrefTable_4() const { return ___keyrefTable_4; }
	inline Hashtable_t909839986 ** get_address_of_keyrefTable_4() { return &___keyrefTable_4; }
	inline void set_keyrefTable_4(Hashtable_t909839986 * value)
	{
		___keyrefTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyrefTable_4), value);
	}

	inline static int32_t get_offset_of_tableDim_5() { return static_cast<int32_t>(offsetof(ConstraintStruct_t2462842120, ___tableDim_5)); }
	inline int32_t get_tableDim_5() const { return ___tableDim_5; }
	inline int32_t* get_address_of_tableDim_5() { return &___tableDim_5; }
	inline void set_tableDim_5(int32_t value)
	{
		___tableDim_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTSTRUCT_T2462842120_H
#ifndef XPATHDOCUMENT_T1328191420_H
#define XPATHDOCUMENT_T1328191420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathDocument
struct  XPathDocument_t1328191420  : public RuntimeObject
{
public:
	// MS.Internal.Xml.Cache.XPathNode[] System.Xml.XPath.XPathDocument::pageXmlNmsp
	XPathNodeU5BU5D_t339325318* ___pageXmlNmsp_0;
	// System.Int32 System.Xml.XPath.XPathDocument::idxXmlNmsp
	int32_t ___idxXmlNmsp_1;
	// System.Xml.XmlNameTable System.Xml.XPath.XPathDocument::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Collections.Generic.Dictionary`2<MS.Internal.Xml.Cache.XPathNodeRef,MS.Internal.Xml.Cache.XPathNodeRef> System.Xml.XPath.XPathDocument::mapNmsp
	Dictionary_2_t3726397491 * ___mapNmsp_3;

public:
	inline static int32_t get_offset_of_pageXmlNmsp_0() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___pageXmlNmsp_0)); }
	inline XPathNodeU5BU5D_t339325318* get_pageXmlNmsp_0() const { return ___pageXmlNmsp_0; }
	inline XPathNodeU5BU5D_t339325318** get_address_of_pageXmlNmsp_0() { return &___pageXmlNmsp_0; }
	inline void set_pageXmlNmsp_0(XPathNodeU5BU5D_t339325318* value)
	{
		___pageXmlNmsp_0 = value;
		Il2CppCodeGenWriteBarrier((&___pageXmlNmsp_0), value);
	}

	inline static int32_t get_offset_of_idxXmlNmsp_1() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___idxXmlNmsp_1)); }
	inline int32_t get_idxXmlNmsp_1() const { return ___idxXmlNmsp_1; }
	inline int32_t* get_address_of_idxXmlNmsp_1() { return &___idxXmlNmsp_1; }
	inline void set_idxXmlNmsp_1(int32_t value)
	{
		___idxXmlNmsp_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_mapNmsp_3() { return static_cast<int32_t>(offsetof(XPathDocument_t1328191420, ___mapNmsp_3)); }
	inline Dictionary_2_t3726397491 * get_mapNmsp_3() const { return ___mapNmsp_3; }
	inline Dictionary_2_t3726397491 ** get_address_of_mapNmsp_3() { return &___mapNmsp_3; }
	inline void set_mapNmsp_3(Dictionary_2_t3726397491 * value)
	{
		___mapNmsp_3 = value;
		Il2CppCodeGenWriteBarrier((&___mapNmsp_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHDOCUMENT_T1328191420_H
#ifndef XPATHNAVIGATORKEYCOMPARER_T3055722314_H
#define XPATHNAVIGATORKEYCOMPARER_T3055722314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigatorKeyComparer
struct  XPathNavigatorKeyComparer_t3055722314  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATORKEYCOMPARER_T3055722314_H
#ifndef BASEVALIDATOR_T3557140249_H
#define BASEVALIDATOR_T3557140249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_t3557140249  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_t3518500204 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t19570940 * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t1507412803 * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_t1944712516 * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t3143048826 * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t1221177846 * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaCollection_0)); }
	inline XmlSchemaCollection_t3518500204 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_t3518500204 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_t3518500204 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaNames_3)); }
	inline SchemaNames_t1619962557 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t1619962557 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___positionInfo_4)); }
	inline PositionInfo_t3273236083 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_t3273236083 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___xmlResolver_5)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___baseUri_6)); }
	inline Uri_t19570940 * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t19570940 ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t19570940 * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaInfo_7)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___reader_8)); }
	inline XmlValidatingReaderImpl_t1507412803 * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t1507412803 ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t1507412803 * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___elementName_9)); }
	inline XmlQualifiedName_t1944712516 * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_t1944712516 * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___context_10)); }
	inline ValidationState_t3143048826 * get_context_10() const { return ___context_10; }
	inline ValidationState_t3143048826 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t3143048826 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textValue_11)); }
	inline StringBuilder_t1221177846 * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t1221177846 ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t1221177846 * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_T3557140249_H
#ifndef BITSET_T1062448123_H
#define BITSET_T1062448123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BitSet
struct  BitSet_t1062448123  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.BitSet::count
	int32_t ___count_0;
	// System.UInt32[] System.Xml.Schema.BitSet::bits
	UInt32U5BU5D_t59386216* ___bits_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(BitSet_t1062448123, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_bits_1() { return static_cast<int32_t>(offsetof(BitSet_t1062448123, ___bits_1)); }
	inline UInt32U5BU5D_t59386216* get_bits_1() const { return ___bits_1; }
	inline UInt32U5BU5D_t59386216** get_address_of_bits_1() { return &___bits_1; }
	inline void set_bits_1(UInt32U5BU5D_t59386216* value)
	{
		___bits_1 = value;
		Il2CppCodeGenWriteBarrier((&___bits_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSET_T1062448123_H
#ifndef OPENEDHOST_T3322155821_H
#define OPENEDHOST_T3322155821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.OpenedHost
struct  OpenedHost_t3322155821  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.OpenedHost::nonCachedConnectionsCount
	int32_t ___nonCachedConnectionsCount_0;

public:
	inline static int32_t get_offset_of_nonCachedConnectionsCount_0() { return static_cast<int32_t>(offsetof(OpenedHost_t3322155821, ___nonCachedConnectionsCount_0)); }
	inline int32_t get_nonCachedConnectionsCount_0() const { return ___nonCachedConnectionsCount_0; }
	inline int32_t* get_address_of_nonCachedConnectionsCount_0() { return &___nonCachedConnectionsCount_0; }
	inline void set_nonCachedConnectionsCount_0(int32_t value)
	{
		___nonCachedConnectionsCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENEDHOST_T3322155821_H
#ifndef UNDECLAREDNOTATION_T2066394897_H
#define UNDECLAREDNOTATION_T2066394897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/UndeclaredNotation
struct  UndeclaredNotation_t2066394897  : public RuntimeObject
{
public:
	// System.String System.Xml.DtdParser/UndeclaredNotation::name
	String_t* ___name_0;
	// System.Int32 System.Xml.DtdParser/UndeclaredNotation::lineNo
	int32_t ___lineNo_1;
	// System.Int32 System.Xml.DtdParser/UndeclaredNotation::linePos
	int32_t ___linePos_2;
	// System.Xml.DtdParser/UndeclaredNotation System.Xml.DtdParser/UndeclaredNotation::next
	UndeclaredNotation_t2066394897 * ___next_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_lineNo_1() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___lineNo_1)); }
	inline int32_t get_lineNo_1() const { return ___lineNo_1; }
	inline int32_t* get_address_of_lineNo_1() { return &___lineNo_1; }
	inline void set_lineNo_1(int32_t value)
	{
		___lineNo_1 = value;
	}

	inline static int32_t get_offset_of_linePos_2() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___linePos_2)); }
	inline int32_t get_linePos_2() const { return ___linePos_2; }
	inline int32_t* get_address_of_linePos_2() { return &___linePos_2; }
	inline void set_linePos_2(int32_t value)
	{
		___linePos_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(UndeclaredNotation_t2066394897, ___next_3)); }
	inline UndeclaredNotation_t2066394897 * get_next_3() const { return ___next_3; }
	inline UndeclaredNotation_t2066394897 ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(UndeclaredNotation_t2066394897 * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier((&___next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNDECLAREDNOTATION_T2066394897_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef ASTNODE_T2002670936_H
#define ASTNODE_T2002670936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.AstNode
struct  AstNode_t2002670936  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTNODE_T2002670936_H
#ifndef DECODER_T3792697818_H
#define DECODER_T3792697818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Decoder
struct  Decoder_t3792697818  : public RuntimeObject
{
public:
	// System.Text.DecoderFallback System.Text.Decoder::m_fallback
	DecoderFallback_t1715117820 * ___m_fallback_0;
	// System.Text.DecoderFallbackBuffer System.Text.Decoder::m_fallbackBuffer
	DecoderFallbackBuffer_t4206371382 * ___m_fallbackBuffer_1;

public:
	inline static int32_t get_offset_of_m_fallback_0() { return static_cast<int32_t>(offsetof(Decoder_t3792697818, ___m_fallback_0)); }
	inline DecoderFallback_t1715117820 * get_m_fallback_0() const { return ___m_fallback_0; }
	inline DecoderFallback_t1715117820 ** get_address_of_m_fallback_0() { return &___m_fallback_0; }
	inline void set_m_fallback_0(DecoderFallback_t1715117820 * value)
	{
		___m_fallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallback_0), value);
	}

	inline static int32_t get_offset_of_m_fallbackBuffer_1() { return static_cast<int32_t>(offsetof(Decoder_t3792697818, ___m_fallbackBuffer_1)); }
	inline DecoderFallbackBuffer_t4206371382 * get_m_fallbackBuffer_1() const { return ___m_fallbackBuffer_1; }
	inline DecoderFallbackBuffer_t4206371382 ** get_address_of_m_fallbackBuffer_1() { return &___m_fallbackBuffer_1; }
	inline void set_m_fallbackBuffer_1(DecoderFallbackBuffer_t4206371382 * value)
	{
		___m_fallbackBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackBuffer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODER_T3792697818_H
#ifndef XMLDOWNLOADMANAGER_T830495394_H
#define XMLDOWNLOADMANAGER_T830495394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDownloadManager
struct  XmlDownloadManager_t830495394  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.XmlDownloadManager::connections
	Hashtable_t909839986 * ___connections_0;

public:
	inline static int32_t get_offset_of_connections_0() { return static_cast<int32_t>(offsetof(XmlDownloadManager_t830495394, ___connections_0)); }
	inline Hashtable_t909839986 * get_connections_0() const { return ___connections_0; }
	inline Hashtable_t909839986 ** get_address_of_connections_0() { return &___connections_0; }
	inline void set_connections_0(Hashtable_t909839986 * value)
	{
		___connections_0 = value;
		Il2CppCodeGenWriteBarrier((&___connections_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOWNLOADMANAGER_T830495394_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T3246720517_H
#define U3CU3EC__DISPLAYCLASS0_0_T3246720517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDownloadManager/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t3246720517  : public RuntimeObject
{
public:
	// System.Uri System.Xml.XmlDownloadManager/<>c__DisplayClass0_0::uri
	Uri_t19570940 * ___uri_0;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3246720517, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T3246720517_H
#ifndef XMLCOMPLIANCEUTIL_T976628708_H
#define XMLCOMPLIANCEUTIL_T976628708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComplianceUtil
struct  XmlComplianceUtil_t976628708  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMPLIANCEUTIL_T976628708_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef SYMBOLSDICTIONARY_T1753655453_H
#define SYMBOLSDICTIONARY_T1753655453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SymbolsDictionary
struct  SymbolsDictionary_t1753655453  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.SymbolsDictionary::last
	int32_t ___last_0;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::names
	Hashtable_t909839986 * ___names_1;
	// System.Collections.Hashtable System.Xml.Schema.SymbolsDictionary::wildcards
	Hashtable_t909839986 * ___wildcards_2;
	// System.Collections.ArrayList System.Xml.Schema.SymbolsDictionary::particles
	ArrayList_t4252133567 * ___particles_3;
	// System.Object System.Xml.Schema.SymbolsDictionary::particleLast
	RuntimeObject * ___particleLast_4;
	// System.Boolean System.Xml.Schema.SymbolsDictionary::isUpaEnforced
	bool ___isUpaEnforced_5;

public:
	inline static int32_t get_offset_of_last_0() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___last_0)); }
	inline int32_t get_last_0() const { return ___last_0; }
	inline int32_t* get_address_of_last_0() { return &___last_0; }
	inline void set_last_0(int32_t value)
	{
		___last_0 = value;
	}

	inline static int32_t get_offset_of_names_1() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___names_1)); }
	inline Hashtable_t909839986 * get_names_1() const { return ___names_1; }
	inline Hashtable_t909839986 ** get_address_of_names_1() { return &___names_1; }
	inline void set_names_1(Hashtable_t909839986 * value)
	{
		___names_1 = value;
		Il2CppCodeGenWriteBarrier((&___names_1), value);
	}

	inline static int32_t get_offset_of_wildcards_2() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___wildcards_2)); }
	inline Hashtable_t909839986 * get_wildcards_2() const { return ___wildcards_2; }
	inline Hashtable_t909839986 ** get_address_of_wildcards_2() { return &___wildcards_2; }
	inline void set_wildcards_2(Hashtable_t909839986 * value)
	{
		___wildcards_2 = value;
		Il2CppCodeGenWriteBarrier((&___wildcards_2), value);
	}

	inline static int32_t get_offset_of_particles_3() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___particles_3)); }
	inline ArrayList_t4252133567 * get_particles_3() const { return ___particles_3; }
	inline ArrayList_t4252133567 ** get_address_of_particles_3() { return &___particles_3; }
	inline void set_particles_3(ArrayList_t4252133567 * value)
	{
		___particles_3 = value;
		Il2CppCodeGenWriteBarrier((&___particles_3), value);
	}

	inline static int32_t get_offset_of_particleLast_4() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___particleLast_4)); }
	inline RuntimeObject * get_particleLast_4() const { return ___particleLast_4; }
	inline RuntimeObject ** get_address_of_particleLast_4() { return &___particleLast_4; }
	inline void set_particleLast_4(RuntimeObject * value)
	{
		___particleLast_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleLast_4), value);
	}

	inline static int32_t get_offset_of_isUpaEnforced_5() { return static_cast<int32_t>(offsetof(SymbolsDictionary_t1753655453, ___isUpaEnforced_5)); }
	inline bool get_isUpaEnforced_5() const { return ___isUpaEnforced_5; }
	inline bool* get_address_of_isUpaEnforced_5() { return &___isUpaEnforced_5; }
	inline void set_isUpaEnforced_5(bool value)
	{
		___isUpaEnforced_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSDICTIONARY_T1753655453_H
#ifndef ENCODING_T663144255_H
#define ENCODING_T663144255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t663144255  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2022420531 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1756452756 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t1715117820 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___dataItem_10)); }
	inline CodePageDataItem_t2022420531 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2022420531 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2022420531 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoderFallback_13)); }
	inline EncoderFallback_t1756452756 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t1756452756 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t1756452756 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___decoderFallback_14)); }
	inline DecoderFallback_t1715117820 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t1715117820 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t1715117820 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t663144255_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t663144255 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t663144255 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t663144255 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t663144255 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t663144255 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t663144255 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t663144255 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t663144255 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t909839986 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t663144255 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t663144255 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t663144255 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t663144255 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t663144255 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t663144255 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t663144255 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t663144255 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t663144255 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t663144255 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t663144255 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t663144255 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t663144255 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t663144255 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t663144255 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t663144255 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t663144255 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t663144255 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t663144255 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t663144255 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t663144255 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t663144255 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t663144255 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t663144255 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___encodings_8)); }
	inline Hashtable_t909839986 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t909839986 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t909839986 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T663144255_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1927440687 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t169632028* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____innerException_4)); }
	inline Exception_t1927440687 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1927440687 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1927440687 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t1975884510 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t1975884510 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t1975884510 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t2217612696* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t2217612696** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t2217612696* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t169632028* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t169632028** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t169632028* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1927440687_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1927440687_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1927440687_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1927440687_H
#ifndef CONFIGUREDTASKAWAITER_T2811256899_H
#define CONFIGUREDTASKAWAITER_T2811256899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.WebResponse>
struct  ConfiguredTaskAwaiter_t2811256899 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t1015255058 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t2811256899, ___m_task_0)); }
	inline Task_1_t1015255058 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t1015255058 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t1015255058 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t2811256899, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T2811256899_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public MarshalByRefObject_t1285298191
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t2745753060 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t461808439 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t2745753060 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t2745753060 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t2745753060 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t461808439 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t461808439 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t461808439 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_1)); }
	inline Stream_t3255436806 * get_Null_1() const { return ___Null_1; }
	inline Stream_t3255436806 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t3255436806 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef INT64_T909078037_H
#define INT64_T909078037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t909078037 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t909078037, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T909078037_H
#ifndef XMLTEXTATTRIBUTE_T3321178844_H
#define XMLTEXTATTRIBUTE_T3321178844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t3321178844  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T3321178844_H
#ifndef AUTOVALIDATOR_T190363951_H
#define AUTOVALIDATOR_T190363951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AutoValidator
struct  AutoValidator_t190363951  : public BaseValidator_t3557140249
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOVALIDATOR_T190363951_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef INTERIORNODE_T2716368958_H
#define INTERIORNODE_T2716368958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.InteriorNode
struct  InteriorNode_t2716368958  : public SyntaxTreeNode_t2397191729
{
public:
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.InteriorNode::leftChild
	SyntaxTreeNode_t2397191729 * ___leftChild_0;
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.InteriorNode::rightChild
	SyntaxTreeNode_t2397191729 * ___rightChild_1;

public:
	inline static int32_t get_offset_of_leftChild_0() { return static_cast<int32_t>(offsetof(InteriorNode_t2716368958, ___leftChild_0)); }
	inline SyntaxTreeNode_t2397191729 * get_leftChild_0() const { return ___leftChild_0; }
	inline SyntaxTreeNode_t2397191729 ** get_address_of_leftChild_0() { return &___leftChild_0; }
	inline void set_leftChild_0(SyntaxTreeNode_t2397191729 * value)
	{
		___leftChild_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftChild_0), value);
	}

	inline static int32_t get_offset_of_rightChild_1() { return static_cast<int32_t>(offsetof(InteriorNode_t2716368958, ___rightChild_1)); }
	inline SyntaxTreeNode_t2397191729 * get_rightChild_1() const { return ___rightChild_1; }
	inline SyntaxTreeNode_t2397191729 ** get_address_of_rightChild_1() { return &___rightChild_1; }
	inline void set_rightChild_1(SyntaxTreeNode_t2397191729 * value)
	{
		___rightChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___rightChild_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERIORNODE_T2716368958_H
#ifndef UPAEXCEPTION_T656169215_H
#define UPAEXCEPTION_T656169215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UpaException
struct  UpaException_t656169215  : public Exception_t1927440687
{
public:
	// System.Object System.Xml.Schema.UpaException::particle1
	RuntimeObject * ___particle1_16;
	// System.Object System.Xml.Schema.UpaException::particle2
	RuntimeObject * ___particle2_17;

public:
	inline static int32_t get_offset_of_particle1_16() { return static_cast<int32_t>(offsetof(UpaException_t656169215, ___particle1_16)); }
	inline RuntimeObject * get_particle1_16() const { return ___particle1_16; }
	inline RuntimeObject ** get_address_of_particle1_16() { return &___particle1_16; }
	inline void set_particle1_16(RuntimeObject * value)
	{
		___particle1_16 = value;
		Il2CppCodeGenWriteBarrier((&___particle1_16), value);
	}

	inline static int32_t get_offset_of_particle2_17() { return static_cast<int32_t>(offsetof(UpaException_t656169215, ___particle2_17)); }
	inline RuntimeObject * get_particle2_17() const { return ___particle2_17; }
	inline RuntimeObject ** get_address_of_particle2_17() { return &___particle2_17; }
	inline void set_particle2_17(RuntimeObject * value)
	{
		___particle2_17 = value;
		Il2CppCodeGenWriteBarrier((&___particle2_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPAEXCEPTION_T656169215_H
#ifndef SEQUENCECONSTRUCTPOSCONTEXT_T3853454650_H
#define SEQUENCECONSTRUCTPOSCONTEXT_T3853454650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct  SequenceConstructPosContext_t3853454650 
{
public:
	// System.Xml.Schema.SequenceNode System.Xml.Schema.SequenceNode/SequenceConstructPosContext::this_
	SequenceNode_t4039907291 * ___this__0;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::firstpos
	BitSet_t1062448123 * ___firstpos_1;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::lastpos
	BitSet_t1062448123 * ___lastpos_2;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::lastposLeft
	BitSet_t1062448123 * ___lastposLeft_3;
	// System.Xml.Schema.BitSet System.Xml.Schema.SequenceNode/SequenceConstructPosContext::firstposRight
	BitSet_t1062448123 * ___firstposRight_4;

public:
	inline static int32_t get_offset_of_this__0() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___this__0)); }
	inline SequenceNode_t4039907291 * get_this__0() const { return ___this__0; }
	inline SequenceNode_t4039907291 ** get_address_of_this__0() { return &___this__0; }
	inline void set_this__0(SequenceNode_t4039907291 * value)
	{
		___this__0 = value;
		Il2CppCodeGenWriteBarrier((&___this__0), value);
	}

	inline static int32_t get_offset_of_firstpos_1() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___firstpos_1)); }
	inline BitSet_t1062448123 * get_firstpos_1() const { return ___firstpos_1; }
	inline BitSet_t1062448123 ** get_address_of_firstpos_1() { return &___firstpos_1; }
	inline void set_firstpos_1(BitSet_t1062448123 * value)
	{
		___firstpos_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstpos_1), value);
	}

	inline static int32_t get_offset_of_lastpos_2() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___lastpos_2)); }
	inline BitSet_t1062448123 * get_lastpos_2() const { return ___lastpos_2; }
	inline BitSet_t1062448123 ** get_address_of_lastpos_2() { return &___lastpos_2; }
	inline void set_lastpos_2(BitSet_t1062448123 * value)
	{
		___lastpos_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastpos_2), value);
	}

	inline static int32_t get_offset_of_lastposLeft_3() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___lastposLeft_3)); }
	inline BitSet_t1062448123 * get_lastposLeft_3() const { return ___lastposLeft_3; }
	inline BitSet_t1062448123 ** get_address_of_lastposLeft_3() { return &___lastposLeft_3; }
	inline void set_lastposLeft_3(BitSet_t1062448123 * value)
	{
		___lastposLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___lastposLeft_3), value);
	}

	inline static int32_t get_offset_of_firstposRight_4() { return static_cast<int32_t>(offsetof(SequenceConstructPosContext_t3853454650, ___firstposRight_4)); }
	inline BitSet_t1062448123 * get_firstposRight_4() const { return ___firstposRight_4; }
	inline BitSet_t1062448123 ** get_address_of_firstposRight_4() { return &___firstposRight_4; }
	inline void set_firstposRight_4(BitSet_t1062448123 * value)
	{
		___firstposRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___firstposRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct SequenceConstructPosContext_t3853454650_marshaled_pinvoke
{
	SequenceNode_t4039907291 * ___this__0;
	BitSet_t1062448123 * ___firstpos_1;
	BitSet_t1062448123 * ___lastpos_2;
	BitSet_t1062448123 * ___lastposLeft_3;
	BitSet_t1062448123 * ___firstposRight_4;
};
// Native definition for COM marshalling of System.Xml.Schema.SequenceNode/SequenceConstructPosContext
struct SequenceConstructPosContext_t3853454650_marshaled_com
{
	SequenceNode_t4039907291 * ___this__0;
	BitSet_t1062448123 * ___firstpos_1;
	BitSet_t1062448123 * ___lastpos_2;
	BitSet_t1062448123 * ___lastposLeft_3;
	BitSet_t1062448123 * ___firstposRight_4;
};
#endif // SEQUENCECONSTRUCTPOSCONTEXT_T3853454650_H
#ifndef LEAFNODE_T3748718316_H
#define LEAFNODE_T3748718316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LeafNode
struct  LeafNode_t3748718316  : public SyntaxTreeNode_t2397191729
{
public:
	// System.Int32 System.Xml.Schema.LeafNode::pos
	int32_t ___pos_0;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(LeafNode_t3748718316, ___pos_0)); }
	inline int32_t get_pos_0() const { return ___pos_0; }
	inline int32_t* get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(int32_t value)
	{
		___pos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAFNODE_T3748718316_H
#ifndef NAMESPACELISTNODE_T2509262495_H
#define NAMESPACELISTNODE_T2509262495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceListNode
struct  NamespaceListNode_t2509262495  : public SyntaxTreeNode_t2397191729
{
public:
	// System.Xml.Schema.NamespaceList System.Xml.Schema.NamespaceListNode::namespaceList
	NamespaceList_t848177191 * ___namespaceList_0;
	// System.Object System.Xml.Schema.NamespaceListNode::particle
	RuntimeObject * ___particle_1;

public:
	inline static int32_t get_offset_of_namespaceList_0() { return static_cast<int32_t>(offsetof(NamespaceListNode_t2509262495, ___namespaceList_0)); }
	inline NamespaceList_t848177191 * get_namespaceList_0() const { return ___namespaceList_0; }
	inline NamespaceList_t848177191 ** get_address_of_namespaceList_0() { return &___namespaceList_0; }
	inline void set_namespaceList_0(NamespaceList_t848177191 * value)
	{
		___namespaceList_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceList_0), value);
	}

	inline static int32_t get_offset_of_particle_1() { return static_cast<int32_t>(offsetof(NamespaceListNode_t2509262495, ___particle_1)); }
	inline RuntimeObject * get_particle_1() const { return ___particle_1; }
	inline RuntimeObject ** get_address_of_particle_1() { return &___particle_1; }
	inline void set_particle_1(RuntimeObject * value)
	{
		___particle_1 = value;
		Il2CppCodeGenWriteBarrier((&___particle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACELISTNODE_T2509262495_H
#ifndef POSITION_T1796812729_H
#define POSITION_T1796812729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Position
struct  Position_t1796812729 
{
public:
	// System.Int32 System.Xml.Schema.Position::symbol
	int32_t ___symbol_0;
	// System.Object System.Xml.Schema.Position::particle
	RuntimeObject * ___particle_1;

public:
	inline static int32_t get_offset_of_symbol_0() { return static_cast<int32_t>(offsetof(Position_t1796812729, ___symbol_0)); }
	inline int32_t get_symbol_0() const { return ___symbol_0; }
	inline int32_t* get_address_of_symbol_0() { return &___symbol_0; }
	inline void set_symbol_0(int32_t value)
	{
		___symbol_0 = value;
	}

	inline static int32_t get_offset_of_particle_1() { return static_cast<int32_t>(offsetof(Position_t1796812729, ___particle_1)); }
	inline RuntimeObject * get_particle_1() const { return ___particle_1; }
	inline RuntimeObject ** get_address_of_particle_1() { return &___particle_1; }
	inline void set_particle_1(RuntimeObject * value)
	{
		___particle_1 = value;
		Il2CppCodeGenWriteBarrier((&___particle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.Position
struct Position_t1796812729_marshaled_pinvoke
{
	int32_t ___symbol_0;
	Il2CppIUnknown* ___particle_1;
};
// Native definition for COM marshalling of System.Xml.Schema.Position
struct Position_t1796812729_marshaled_com
{
	int32_t ___symbol_0;
	Il2CppIUnknown* ___particle_1;
};
#endif // POSITION_T1796812729_H
#ifndef CONFIGUREDTASKAWAITER_T4171467654_H
#define CONFIGUREDTASKAWAITER_T4171467654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.IO.Stream>
struct  ConfiguredTaskAwaiter_t4171467654 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t2375465813 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t4171467654, ___m_task_0)); }
	inline Task_1_t2375465813 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t2375465813 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t2375465813 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t4171467654, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T4171467654_H
#ifndef LINEINFO_T1429635508_H
#define LINEINFO_T1429635508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.LineInfo
struct  LineInfo_t1429635508 
{
public:
	// System.Int32 System.Xml.LineInfo::lineNo
	int32_t ___lineNo_0;
	// System.Int32 System.Xml.LineInfo::linePos
	int32_t ___linePos_1;

public:
	inline static int32_t get_offset_of_lineNo_0() { return static_cast<int32_t>(offsetof(LineInfo_t1429635508, ___lineNo_0)); }
	inline int32_t get_lineNo_0() const { return ___lineNo_0; }
	inline int32_t* get_address_of_lineNo_0() { return &___lineNo_0; }
	inline void set_lineNo_0(int32_t value)
	{
		___lineNo_0 = value;
	}

	inline static int32_t get_offset_of_linePos_1() { return static_cast<int32_t>(offsetof(LineInfo_t1429635508, ___linePos_1)); }
	inline int32_t get_linePos_1() const { return ___linePos_1; }
	inline int32_t* get_address_of_linePos_1() { return &___linePos_1; }
	inline void set_linePos_1(int32_t value)
	{
		___linePos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFO_T1429635508_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef DECIMAL_T724701077_H
#define DECIMAL_T724701077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t724701077 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t724701077_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t59386216* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t724701077  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t724701077  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t724701077  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t724701077  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t724701077  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t724701077  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t724701077  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t59386216* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t59386216** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t59386216* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___Zero_7)); }
	inline Decimal_t724701077  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t724701077 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t724701077  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___One_8)); }
	inline Decimal_t724701077  get_One_8() const { return ___One_8; }
	inline Decimal_t724701077 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t724701077  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinusOne_9)); }
	inline Decimal_t724701077  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t724701077 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t724701077  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MaxValue_10)); }
	inline Decimal_t724701077  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t724701077 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t724701077  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinValue_11)); }
	inline Decimal_t724701077  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t724701077 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t724701077  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t724701077  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t724701077 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t724701077  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t724701077  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t724701077 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t724701077  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T724701077_H
#ifndef RANGEPOSITIONINFO_T2780802922_H
#define RANGEPOSITIONINFO_T2780802922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RangePositionInfo
struct  RangePositionInfo_t2780802922 
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.RangePositionInfo::curpos
	BitSet_t1062448123 * ___curpos_0;
	// System.Decimal[] System.Xml.Schema.RangePositionInfo::rangeCounters
	DecimalU5BU5D_t624008824* ___rangeCounters_1;

public:
	inline static int32_t get_offset_of_curpos_0() { return static_cast<int32_t>(offsetof(RangePositionInfo_t2780802922, ___curpos_0)); }
	inline BitSet_t1062448123 * get_curpos_0() const { return ___curpos_0; }
	inline BitSet_t1062448123 ** get_address_of_curpos_0() { return &___curpos_0; }
	inline void set_curpos_0(BitSet_t1062448123 * value)
	{
		___curpos_0 = value;
		Il2CppCodeGenWriteBarrier((&___curpos_0), value);
	}

	inline static int32_t get_offset_of_rangeCounters_1() { return static_cast<int32_t>(offsetof(RangePositionInfo_t2780802922, ___rangeCounters_1)); }
	inline DecimalU5BU5D_t624008824* get_rangeCounters_1() const { return ___rangeCounters_1; }
	inline DecimalU5BU5D_t624008824** get_address_of_rangeCounters_1() { return &___rangeCounters_1; }
	inline void set_rangeCounters_1(DecimalU5BU5D_t624008824* value)
	{
		___rangeCounters_1 = value;
		Il2CppCodeGenWriteBarrier((&___rangeCounters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.RangePositionInfo
struct RangePositionInfo_t2780802922_marshaled_pinvoke
{
	BitSet_t1062448123 * ___curpos_0;
	Decimal_t724701077 * ___rangeCounters_1;
};
// Native definition for COM marshalling of System.Xml.Schema.RangePositionInfo
struct RangePositionInfo_t2780802922_marshaled_com
{
	BitSet_t1062448123 * ___curpos_0;
	Decimal_t724701077 * ___rangeCounters_1;
};
#endif // RANGEPOSITIONINFO_T2780802922_H
#ifndef SELECTORACTIVEAXIS_T789423304_H
#define SELECTORACTIVEAXIS_T789423304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SelectorActiveAxis
struct  SelectorActiveAxis_t789423304  : public ActiveAxis_t439376929
{
public:
	// System.Xml.Schema.ConstraintStruct System.Xml.Schema.SelectorActiveAxis::cs
	ConstraintStruct_t2462842120 * ___cs_4;
	// System.Collections.ArrayList System.Xml.Schema.SelectorActiveAxis::KSs
	ArrayList_t4252133567 * ___KSs_5;
	// System.Int32 System.Xml.Schema.SelectorActiveAxis::KSpointer
	int32_t ___KSpointer_6;

public:
	inline static int32_t get_offset_of_cs_4() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t789423304, ___cs_4)); }
	inline ConstraintStruct_t2462842120 * get_cs_4() const { return ___cs_4; }
	inline ConstraintStruct_t2462842120 ** get_address_of_cs_4() { return &___cs_4; }
	inline void set_cs_4(ConstraintStruct_t2462842120 * value)
	{
		___cs_4 = value;
		Il2CppCodeGenWriteBarrier((&___cs_4), value);
	}

	inline static int32_t get_offset_of_KSs_5() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t789423304, ___KSs_5)); }
	inline ArrayList_t4252133567 * get_KSs_5() const { return ___KSs_5; }
	inline ArrayList_t4252133567 ** get_address_of_KSs_5() { return &___KSs_5; }
	inline void set_KSs_5(ArrayList_t4252133567 * value)
	{
		___KSs_5 = value;
		Il2CppCodeGenWriteBarrier((&___KSs_5), value);
	}

	inline static int32_t get_offset_of_KSpointer_6() { return static_cast<int32_t>(offsetof(SelectorActiveAxis_t789423304, ___KSpointer_6)); }
	inline int32_t get_KSpointer_6() const { return ___KSpointer_6; }
	inline int32_t* get_address_of_KSpointer_6() { return &___KSpointer_6; }
	inline void set_KSpointer_6(int32_t value)
	{
		___KSpointer_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTORACTIVEAXIS_T789423304_H
#ifndef LOCATEDACTIVEAXIS_T90453917_H
#define LOCATEDACTIVEAXIS_T90453917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LocatedActiveAxis
struct  LocatedActiveAxis_t90453917  : public ActiveAxis_t439376929
{
public:
	// System.Int32 System.Xml.Schema.LocatedActiveAxis::column
	int32_t ___column_4;
	// System.Boolean System.Xml.Schema.LocatedActiveAxis::isMatched
	bool ___isMatched_5;
	// System.Xml.Schema.KeySequence System.Xml.Schema.LocatedActiveAxis::Ks
	KeySequence_t746093258 * ___Ks_6;

public:
	inline static int32_t get_offset_of_column_4() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_t90453917, ___column_4)); }
	inline int32_t get_column_4() const { return ___column_4; }
	inline int32_t* get_address_of_column_4() { return &___column_4; }
	inline void set_column_4(int32_t value)
	{
		___column_4 = value;
	}

	inline static int32_t get_offset_of_isMatched_5() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_t90453917, ___isMatched_5)); }
	inline bool get_isMatched_5() const { return ___isMatched_5; }
	inline bool* get_address_of_isMatched_5() { return &___isMatched_5; }
	inline void set_isMatched_5(bool value)
	{
		___isMatched_5 = value;
	}

	inline static int32_t get_offset_of_Ks_6() { return static_cast<int32_t>(offsetof(LocatedActiveAxis_t90453917, ___Ks_6)); }
	inline KeySequence_t746093258 * get_Ks_6() const { return ___Ks_6; }
	inline KeySequence_t746093258 ** get_address_of_Ks_6() { return &___Ks_6; }
	inline void set_Ks_6(KeySequence_t746093258 * value)
	{
		___Ks_6 = value;
		Il2CppCodeGenWriteBarrier((&___Ks_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATEDACTIVEAXIS_T90453917_H
#ifndef NAMESPACEDECLARATION_T3577631811_H
#define NAMESPACEDECLARATION_T3577631811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager/NamespaceDeclaration
struct  NamespaceDeclaration_t3577631811 
{
public:
	// System.String System.Xml.XmlNamespaceManager/NamespaceDeclaration::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlNamespaceManager/NamespaceDeclaration::uri
	String_t* ___uri_1;
	// System.Int32 System.Xml.XmlNamespaceManager/NamespaceDeclaration::scopeId
	int32_t ___scopeId_2;
	// System.Int32 System.Xml.XmlNamespaceManager/NamespaceDeclaration::previousNsIndex
	int32_t ___previousNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___scopeId_2)); }
	inline int32_t get_scopeId_2() const { return ___scopeId_2; }
	inline int32_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int32_t value)
	{
		___scopeId_2 = value;
	}

	inline static int32_t get_offset_of_previousNsIndex_3() { return static_cast<int32_t>(offsetof(NamespaceDeclaration_t3577631811, ___previousNsIndex_3)); }
	inline int32_t get_previousNsIndex_3() const { return ___previousNsIndex_3; }
	inline int32_t* get_address_of_previousNsIndex_3() { return &___previousNsIndex_3; }
	inline void set_previousNsIndex_3(int32_t value)
	{
		___previousNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NamespaceDeclaration
struct NamespaceDeclaration_t3577631811_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___uri_1;
	int32_t ___scopeId_2;
	int32_t ___previousNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NamespaceDeclaration
struct NamespaceDeclaration_t3577631811_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___uri_1;
	int32_t ___scopeId_2;
	int32_t ___previousNsIndex_3;
};
#endif // NAMESPACEDECLARATION_T3577631811_H
#ifndef UCS4DECODER_T2645160027_H
#define UCS4DECODER_T2645160027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Decoder
struct  Ucs4Decoder_t2645160027  : public Decoder_t3792697818
{
public:
	// System.Byte[] System.Xml.Ucs4Decoder::lastBytes
	ByteU5BU5D_t3397334013* ___lastBytes_2;
	// System.Int32 System.Xml.Ucs4Decoder::lastBytesCount
	int32_t ___lastBytesCount_3;

public:
	inline static int32_t get_offset_of_lastBytes_2() { return static_cast<int32_t>(offsetof(Ucs4Decoder_t2645160027, ___lastBytes_2)); }
	inline ByteU5BU5D_t3397334013* get_lastBytes_2() const { return ___lastBytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_lastBytes_2() { return &___lastBytes_2; }
	inline void set_lastBytes_2(ByteU5BU5D_t3397334013* value)
	{
		___lastBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastBytes_2), value);
	}

	inline static int32_t get_offset_of_lastBytesCount_3() { return static_cast<int32_t>(offsetof(Ucs4Decoder_t2645160027, ___lastBytesCount_3)); }
	inline int32_t get_lastBytesCount_3() const { return ___lastBytesCount_3; }
	inline int32_t* get_address_of_lastBytesCount_3() { return &___lastBytesCount_3; }
	inline void set_lastBytesCount_3(int32_t value)
	{
		___lastBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4DECODER_T2645160027_H
#ifndef XMLURLRESOLVER_T896669594_H
#define XMLURLRESOLVER_T896669594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_t896669594  : public XmlResolver_t2024571559
{
public:
	// System.Net.ICredentials System.Xml.XmlUrlResolver::_credentials
	RuntimeObject* ____credentials_1;
	// System.Net.IWebProxy System.Xml.XmlUrlResolver::_proxy
	RuntimeObject* ____proxy_2;
	// System.Net.Cache.RequestCachePolicy System.Xml.XmlUrlResolver::_cachePolicy
	RequestCachePolicy_t2663429579 * ____cachePolicy_3;

public:
	inline static int32_t get_offset_of__credentials_1() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594, ____credentials_1)); }
	inline RuntimeObject* get__credentials_1() const { return ____credentials_1; }
	inline RuntimeObject** get_address_of__credentials_1() { return &____credentials_1; }
	inline void set__credentials_1(RuntimeObject* value)
	{
		____credentials_1 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_1), value);
	}

	inline static int32_t get_offset_of__proxy_2() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594, ____proxy_2)); }
	inline RuntimeObject* get__proxy_2() const { return ____proxy_2; }
	inline RuntimeObject** get_address_of__proxy_2() { return &____proxy_2; }
	inline void set__proxy_2(RuntimeObject* value)
	{
		____proxy_2 = value;
		Il2CppCodeGenWriteBarrier((&____proxy_2), value);
	}

	inline static int32_t get_offset_of__cachePolicy_3() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594, ____cachePolicy_3)); }
	inline RequestCachePolicy_t2663429579 * get__cachePolicy_3() const { return ____cachePolicy_3; }
	inline RequestCachePolicy_t2663429579 ** get_address_of__cachePolicy_3() { return &____cachePolicy_3; }
	inline void set__cachePolicy_3(RequestCachePolicy_t2663429579 * value)
	{
		____cachePolicy_3 = value;
		Il2CppCodeGenWriteBarrier((&____cachePolicy_3), value);
	}
};

struct XmlUrlResolver_t896669594_StaticFields
{
public:
	// System.Object System.Xml.XmlUrlResolver::s_DownloadManager
	RuntimeObject * ___s_DownloadManager_0;

public:
	inline static int32_t get_offset_of_s_DownloadManager_0() { return static_cast<int32_t>(offsetof(XmlUrlResolver_t896669594_StaticFields, ___s_DownloadManager_0)); }
	inline RuntimeObject * get_s_DownloadManager_0() const { return ___s_DownloadManager_0; }
	inline RuntimeObject ** get_address_of_s_DownloadManager_0() { return &___s_DownloadManager_0; }
	inline void set_s_DownloadManager_0(RuntimeObject * value)
	{
		___s_DownloadManager_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_DownloadManager_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLURLRESOLVER_T896669594_H
#ifndef XPATHNAVIGATOR_T3981235968_H
#define XPATHNAVIGATOR_T3981235968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t3981235968  : public XPathItem_t3130801258
{
public:

public:
};

struct XPathNavigator_t3981235968_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorKeyComparer System.Xml.XPath.XPathNavigator::comparer
	XPathNavigatorKeyComparer_t3055722314 * ___comparer_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::NodeTypeLetter
	CharU5BU5D_t1328083999* ___NodeTypeLetter_1;
	// System.Char[] System.Xml.XPath.XPathNavigator::UniqueIdTbl
	CharU5BU5D_t1328083999* ___UniqueIdTbl_2;
	// System.Int32[] System.Xml.XPath.XPathNavigator::ContentKindMasks
	Int32U5BU5D_t3030399641* ___ContentKindMasks_3;

public:
	inline static int32_t get_offset_of_comparer_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___comparer_0)); }
	inline XPathNavigatorKeyComparer_t3055722314 * get_comparer_0() const { return ___comparer_0; }
	inline XPathNavigatorKeyComparer_t3055722314 ** get_address_of_comparer_0() { return &___comparer_0; }
	inline void set_comparer_0(XPathNavigatorKeyComparer_t3055722314 * value)
	{
		___comparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_0), value);
	}

	inline static int32_t get_offset_of_NodeTypeLetter_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___NodeTypeLetter_1)); }
	inline CharU5BU5D_t1328083999* get_NodeTypeLetter_1() const { return ___NodeTypeLetter_1; }
	inline CharU5BU5D_t1328083999** get_address_of_NodeTypeLetter_1() { return &___NodeTypeLetter_1; }
	inline void set_NodeTypeLetter_1(CharU5BU5D_t1328083999* value)
	{
		___NodeTypeLetter_1 = value;
		Il2CppCodeGenWriteBarrier((&___NodeTypeLetter_1), value);
	}

	inline static int32_t get_offset_of_UniqueIdTbl_2() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___UniqueIdTbl_2)); }
	inline CharU5BU5D_t1328083999* get_UniqueIdTbl_2() const { return ___UniqueIdTbl_2; }
	inline CharU5BU5D_t1328083999** get_address_of_UniqueIdTbl_2() { return &___UniqueIdTbl_2; }
	inline void set_UniqueIdTbl_2(CharU5BU5D_t1328083999* value)
	{
		___UniqueIdTbl_2 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueIdTbl_2), value);
	}

	inline static int32_t get_offset_of_ContentKindMasks_3() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___ContentKindMasks_3)); }
	inline Int32U5BU5D_t3030399641* get_ContentKindMasks_3() const { return ___ContentKindMasks_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_ContentKindMasks_3() { return &___ContentKindMasks_3; }
	inline void set_ContentKindMasks_3(Int32U5BU5D_t3030399641* value)
	{
		___ContentKindMasks_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentKindMasks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T3981235968_H
#ifndef ASYNCMETHODBUILDERCORE_T2485284745_H
#define ASYNCMETHODBUILDERCORE_T2485284745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2485284745 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t3226471752 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2485284745, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2485284745, ___m_defaultContextAction_1)); }
	inline Action_t3226471752 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t3226471752 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t3226471752 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2485284745_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2485284745_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T2485284745_H
#ifndef UTF16DECODER_T2333661544_H
#define UTF16DECODER_T2333661544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.UTF16Decoder
struct  UTF16Decoder_t2333661544  : public Decoder_t3792697818
{
public:
	// System.Boolean System.Xml.UTF16Decoder::bigEndian
	bool ___bigEndian_2;
	// System.Int32 System.Xml.UTF16Decoder::lastByte
	int32_t ___lastByte_3;

public:
	inline static int32_t get_offset_of_bigEndian_2() { return static_cast<int32_t>(offsetof(UTF16Decoder_t2333661544, ___bigEndian_2)); }
	inline bool get_bigEndian_2() const { return ___bigEndian_2; }
	inline bool* get_address_of_bigEndian_2() { return &___bigEndian_2; }
	inline void set_bigEndian_2(bool value)
	{
		___bigEndian_2 = value;
	}

	inline static int32_t get_offset_of_lastByte_3() { return static_cast<int32_t>(offsetof(UTF16Decoder_t2333661544, ___lastByte_3)); }
	inline int32_t get_lastByte_3() const { return ___lastByte_3; }
	inline int32_t* get_address_of_lastByte_3() { return &___lastByte_3; }
	inline void set_lastByte_3(int32_t value)
	{
		___lastByte_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTF16DECODER_T2333661544_H
#ifndef XMLCHARTYPE_T1050521405_H
#define XMLCHARTYPE_T1050521405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t1050521405 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t3397334013* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405, ___charProperties_2)); }
	inline ByteU5BU5D_t3397334013* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t3397334013* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t1050521405_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t3397334013* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t3397334013* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t3397334013* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t1050521405_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t1050521405_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T1050521405_H
#ifndef SAFEASCIIDECODER_T3220120650_H
#define SAFEASCIIDECODER_T3220120650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SafeAsciiDecoder
struct  SafeAsciiDecoder_t3220120650  : public Decoder_t3792697818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEASCIIDECODER_T3220120650_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UCS4ENCODING_T650080996_H
#define UCS4ENCODING_T650080996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Encoding
struct  Ucs4Encoding_t650080996  : public Encoding_t663144255
{
public:
	// System.Xml.Ucs4Decoder System.Xml.Ucs4Encoding::ucs4Decoder
	Ucs4Decoder_t2645160027 * ___ucs4Decoder_16;

public:
	inline static int32_t get_offset_of_ucs4Decoder_16() { return static_cast<int32_t>(offsetof(Ucs4Encoding_t650080996, ___ucs4Decoder_16)); }
	inline Ucs4Decoder_t2645160027 * get_ucs4Decoder_16() const { return ___ucs4Decoder_16; }
	inline Ucs4Decoder_t2645160027 ** get_address_of_ucs4Decoder_16() { return &___ucs4Decoder_16; }
	inline void set_ucs4Decoder_16(Ucs4Decoder_t2645160027 * value)
	{
		___ucs4Decoder_16 = value;
		Il2CppCodeGenWriteBarrier((&___ucs4Decoder_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4ENCODING_T650080996_H
#ifndef XMLROOTATTRIBUTE_T3527426713_H
#define XMLROOTATTRIBUTE_T3527426713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlRootAttribute
struct  XmlRootAttribute_t3527426713  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_1;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::nullable
	bool ___nullable_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t3527426713, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t3527426713, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_nullable_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t3527426713, ___nullable_2)); }
	inline bool get_nullable_2() const { return ___nullable_2; }
	inline bool* get_address_of_nullable_2() { return &___nullable_2; }
	inline void set_nullable_2(bool value)
	{
		___nullable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLROOTATTRIBUTE_T3527426713_H
#ifndef XMLANYATTRIBUTEATTRIBUTE_T713947513_H
#define XMLANYATTRIBUTEATTRIBUTE_T713947513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyAttributeAttribute
struct  XmlAnyAttributeAttribute_t713947513  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYATTRIBUTEATTRIBUTE_T713947513_H
#ifndef XMLATTRIBUTEATTRIBUTE_T850813783_H
#define XMLATTRIBUTEATTRIBUTE_T850813783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_t850813783  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t850813783, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t850813783, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_T850813783_H
#ifndef XMLELEMENTATTRIBUTE_T2182839281_H
#define XMLELEMENTATTRIBUTE_T2182839281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_t2182839281  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_0;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_1;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t2182839281, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t2182839281, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t2182839281, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_T2182839281_H
#ifndef XMLNAMESPACEDECLARATIONSATTRIBUTE_T2069522403_H
#define XMLNAMESPACEDECLARATIONSATTRIBUTE_T2069522403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlNamespaceDeclarationsAttribute
struct  XmlNamespaceDeclarationsAttribute_t2069522403  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEDECLARATIONSATTRIBUTE_T2069522403_H
#ifndef XMLANYELEMENTATTRIBUTE_T2502375235_H
#define XMLANYELEMENTATTRIBUTE_T2502375235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyElementAttribute
struct  XmlAnyElementAttribute_t2502375235  : public Attribute_t542643598
{
public:
	// System.Int32 System.Xml.Serialization.XmlAnyElementAttribute::order
	int32_t ___order_0;

public:
	inline static int32_t get_offset_of_order_0() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_t2502375235, ___order_0)); }
	inline int32_t get_order_0() const { return ___order_0; }
	inline int32_t* get_address_of_order_0() { return &___order_0; }
	inline void set_order_0(int32_t value)
	{
		___order_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYELEMENTATTRIBUTE_T2502375235_H
#ifndef XMLENUMATTRIBUTE_T919400678_H
#define XMLENUMATTRIBUTE_T919400678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t919400678  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_t919400678, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_T919400678_H
#ifndef XMLIGNOREATTRIBUTE_T2333915871_H
#define XMLIGNOREATTRIBUTE_T2333915871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t2333915871  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_T2333915871_H
#ifndef XMLREGISTEREDNONCACHEDSTREAM_T1825156356_H
#define XMLREGISTEREDNONCACHEDSTREAM_T1825156356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRegisteredNonCachedStream
struct  XmlRegisteredNonCachedStream_t1825156356  : public Stream_t3255436806
{
public:
	// System.IO.Stream System.Xml.XmlRegisteredNonCachedStream::stream
	Stream_t3255436806 * ___stream_4;
	// System.Xml.XmlDownloadManager System.Xml.XmlRegisteredNonCachedStream::downloadManager
	XmlDownloadManager_t830495394 * ___downloadManager_5;
	// System.String System.Xml.XmlRegisteredNonCachedStream::host
	String_t* ___host_6;

public:
	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(XmlRegisteredNonCachedStream_t1825156356, ___stream_4)); }
	inline Stream_t3255436806 * get_stream_4() const { return ___stream_4; }
	inline Stream_t3255436806 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_t3255436806 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_downloadManager_5() { return static_cast<int32_t>(offsetof(XmlRegisteredNonCachedStream_t1825156356, ___downloadManager_5)); }
	inline XmlDownloadManager_t830495394 * get_downloadManager_5() const { return ___downloadManager_5; }
	inline XmlDownloadManager_t830495394 ** get_address_of_downloadManager_5() { return &___downloadManager_5; }
	inline void set_downloadManager_5(XmlDownloadManager_t830495394 * value)
	{
		___downloadManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___downloadManager_5), value);
	}

	inline static int32_t get_offset_of_host_6() { return static_cast<int32_t>(offsetof(XmlRegisteredNonCachedStream_t1825156356, ___host_6)); }
	inline String_t* get_host_6() const { return ___host_6; }
	inline String_t** get_address_of_host_6() { return &___host_6; }
	inline void set_host_6(String_t* value)
	{
		___host_6 = value;
		Il2CppCodeGenWriteBarrier((&___host_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREGISTEREDNONCACHEDSTREAM_T1825156356_H
#ifndef XMLCONVERT_T1936105738_H
#define XMLCONVERT_T1936105738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1936105738  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t1936105738_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.XmlConvert::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_0;
	// System.Char[] System.Xml.XmlConvert::crt
	CharU5BU5D_t1328083999* ___crt_1;
	// System.Int32 System.Xml.XmlConvert::c_EncodedCharLength
	int32_t ___c_EncodedCharLength_2;
	// System.Char[] System.Xml.XmlConvert::WhitespaceChars
	CharU5BU5D_t1328083999* ___WhitespaceChars_3;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t1050521405  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t1050521405  value)
	{
		___xmlCharType_0 = value;
	}

	inline static int32_t get_offset_of_crt_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___crt_1)); }
	inline CharU5BU5D_t1328083999* get_crt_1() const { return ___crt_1; }
	inline CharU5BU5D_t1328083999** get_address_of_crt_1() { return &___crt_1; }
	inline void set_crt_1(CharU5BU5D_t1328083999* value)
	{
		___crt_1 = value;
		Il2CppCodeGenWriteBarrier((&___crt_1), value);
	}

	inline static int32_t get_offset_of_c_EncodedCharLength_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___c_EncodedCharLength_2)); }
	inline int32_t get_c_EncodedCharLength_2() const { return ___c_EncodedCharLength_2; }
	inline int32_t* get_address_of_c_EncodedCharLength_2() { return &___c_EncodedCharLength_2; }
	inline void set_c_EncodedCharLength_2(int32_t value)
	{
		___c_EncodedCharLength_2 = value;
	}

	inline static int32_t get_offset_of_WhitespaceChars_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___WhitespaceChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhitespaceChars_3() const { return ___WhitespaceChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhitespaceChars_3() { return &___WhitespaceChars_3; }
	inline void set_WhitespaceChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhitespaceChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T1936105738_H
#ifndef ASYNCTASKMETHODBUILDER_1_T1272420930_H
#define ASYNCTASKMETHODBUILDER_1_T1272420930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object>
struct  AsyncTaskMethodBuilder_1_t1272420930 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2485284745  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1809478302 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1272420930, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2485284745  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2485284745 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2485284745  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1272420930, ___m_task_2)); }
	inline Task_1_t1809478302 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1809478302 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1809478302 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t1272420930_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1809478302 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1272420930_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1809478302 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1809478302 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1809478302 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T1272420930_H
#ifndef LEAFRANGENODE_T2572019409_H
#define LEAFRANGENODE_T2572019409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.LeafRangeNode
struct  LeafRangeNode_t2572019409  : public LeafNode_t3748718316
{
public:
	// System.Decimal System.Xml.Schema.LeafRangeNode::min
	Decimal_t724701077  ___min_1;
	// System.Decimal System.Xml.Schema.LeafRangeNode::max
	Decimal_t724701077  ___max_2;
	// System.Xml.Schema.BitSet System.Xml.Schema.LeafRangeNode::nextIteration
	BitSet_t1062448123 * ___nextIteration_3;

public:
	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(LeafRangeNode_t2572019409, ___min_1)); }
	inline Decimal_t724701077  get_min_1() const { return ___min_1; }
	inline Decimal_t724701077 * get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(Decimal_t724701077  value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(LeafRangeNode_t2572019409, ___max_2)); }
	inline Decimal_t724701077  get_max_2() const { return ___max_2; }
	inline Decimal_t724701077 * get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(Decimal_t724701077  value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_nextIteration_3() { return static_cast<int32_t>(offsetof(LeafRangeNode_t2572019409, ___nextIteration_3)); }
	inline BitSet_t1062448123 * get_nextIteration_3() const { return ___nextIteration_3; }
	inline BitSet_t1062448123 ** get_address_of_nextIteration_3() { return &___nextIteration_3; }
	inline void set_nextIteration_3(BitSet_t1062448123 * value)
	{
		___nextIteration_3 = value;
		Il2CppCodeGenWriteBarrier((&___nextIteration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAFRANGENODE_T2572019409_H
#ifndef STARNODE_T2416964522_H
#define STARNODE_T2416964522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StarNode
struct  StarNode_t2416964522  : public InteriorNode_t2716368958
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARNODE_T2416964522_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef LITERALTYPE_T3592345881_H
#define LITERALTYPE_T3592345881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/LiteralType
struct  LiteralType_t3592345881 
{
public:
	// System.Int32 System.Xml.DtdParser/LiteralType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LiteralType_t3592345881, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITERALTYPE_T3592345881_H
#ifndef AXISTYPE_T301805468_H
#define AXISTYPE_T301805468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis/AxisType
struct  AxisType_t301805468 
{
public:
	// System.Int32 MS.Internal.Xml.XPath.Axis/AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_t301805468, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTYPE_T301805468_H
#ifndef SCANNINGFUNCTION_T2556434333_H
#define SCANNINGFUNCTION_T2556434333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/ScanningFunction
struct  ScanningFunction_t2556434333 
{
public:
	// System.Int32 System.Xml.DtdParser/ScanningFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScanningFunction_t2556434333, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANNINGFUNCTION_T2556434333_H
#ifndef UCS4ENCODING2143_T576439440_H
#define UCS4ENCODING2143_T576439440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Encoding2143
struct  Ucs4Encoding2143_t576439440  : public Ucs4Encoding_t650080996
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4ENCODING2143_T576439440_H
#ifndef TOKEN_T322298853_H
#define TOKEN_T322298853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/Token
struct  Token_t322298853 
{
public:
	// System.Int32 System.Xml.DtdParser/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t322298853, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T322298853_H
#ifndef ASYNCTASKMETHODBUILDER_1_T1838408441_H
#define ASYNCTASKMETHODBUILDER_1_T1838408441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.IO.Stream>
struct  AsyncTaskMethodBuilder_1_t1838408441 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2485284745  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t2375465813 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1838408441, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2485284745  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2485284745 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2485284745  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1838408441, ___m_task_2)); }
	inline Task_1_t2375465813 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t2375465813 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t2375465813 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t1838408441_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t2375465813 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t1838408441_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t2375465813 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t2375465813 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t2375465813 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T1838408441_H
#ifndef EXCEPTIONTYPE_T1261777571_H
#define EXCEPTIONTYPE_T1261777571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ExceptionType
struct  ExceptionType_t1261777571 
{
public:
	// System.Int32 System.Xml.ExceptionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionType_t1261777571, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONTYPE_T1261777571_H
#ifndef VALIDATENAMES_T208250372_H
#define VALIDATENAMES_T208250372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidateNames
struct  ValidateNames_t208250372  : public RuntimeObject
{
public:

public:
};

struct ValidateNames_t208250372_StaticFields
{
public:
	// System.Xml.XmlCharType System.Xml.ValidateNames::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_0;

public:
	inline static int32_t get_offset_of_xmlCharType_0() { return static_cast<int32_t>(offsetof(ValidateNames_t208250372_StaticFields, ___xmlCharType_0)); }
	inline XmlCharType_t1050521405  get_xmlCharType_0() const { return ___xmlCharType_0; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_0() { return &___xmlCharType_0; }
	inline void set_xmlCharType_0(XmlCharType_t1050521405  value)
	{
		___xmlCharType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATENAMES_T208250372_H
#ifndef MEMORYSTREAM_T743994179_H
#define MEMORYSTREAM_T743994179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t743994179  : public Stream_t3255436806
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_t3397334013* ____buffer_4;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_5;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_6;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_7;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_8;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_9;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_10;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_11;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t1191906455 * ____lastReadTask_13;

public:
	inline static int32_t get_offset_of__buffer_4() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____buffer_4)); }
	inline ByteU5BU5D_t3397334013* get__buffer_4() const { return ____buffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_4() { return &____buffer_4; }
	inline void set__buffer_4(ByteU5BU5D_t3397334013* value)
	{
		____buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_4), value);
	}

	inline static int32_t get_offset_of__origin_5() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____origin_5)); }
	inline int32_t get__origin_5() const { return ____origin_5; }
	inline int32_t* get_address_of__origin_5() { return &____origin_5; }
	inline void set__origin_5(int32_t value)
	{
		____origin_5 = value;
	}

	inline static int32_t get_offset_of__position_6() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____position_6)); }
	inline int32_t get__position_6() const { return ____position_6; }
	inline int32_t* get_address_of__position_6() { return &____position_6; }
	inline void set__position_6(int32_t value)
	{
		____position_6 = value;
	}

	inline static int32_t get_offset_of__length_7() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____length_7)); }
	inline int32_t get__length_7() const { return ____length_7; }
	inline int32_t* get_address_of__length_7() { return &____length_7; }
	inline void set__length_7(int32_t value)
	{
		____length_7 = value;
	}

	inline static int32_t get_offset_of__capacity_8() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____capacity_8)); }
	inline int32_t get__capacity_8() const { return ____capacity_8; }
	inline int32_t* get_address_of__capacity_8() { return &____capacity_8; }
	inline void set__capacity_8(int32_t value)
	{
		____capacity_8 = value;
	}

	inline static int32_t get_offset_of__expandable_9() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____expandable_9)); }
	inline bool get__expandable_9() const { return ____expandable_9; }
	inline bool* get_address_of__expandable_9() { return &____expandable_9; }
	inline void set__expandable_9(bool value)
	{
		____expandable_9 = value;
	}

	inline static int32_t get_offset_of__writable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____writable_10)); }
	inline bool get__writable_10() const { return ____writable_10; }
	inline bool* get_address_of__writable_10() { return &____writable_10; }
	inline void set__writable_10(bool value)
	{
		____writable_10 = value;
	}

	inline static int32_t get_offset_of__exposable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____exposable_11)); }
	inline bool get__exposable_11() const { return ____exposable_11; }
	inline bool* get_address_of__exposable_11() { return &____exposable_11; }
	inline void set__exposable_11(bool value)
	{
		____exposable_11 = value;
	}

	inline static int32_t get_offset_of__isOpen_12() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____isOpen_12)); }
	inline bool get__isOpen_12() const { return ____isOpen_12; }
	inline bool* get_address_of__isOpen_12() { return &____isOpen_12; }
	inline void set__isOpen_12(bool value)
	{
		____isOpen_12 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_13() { return static_cast<int32_t>(offsetof(MemoryStream_t743994179, ____lastReadTask_13)); }
	inline Task_1_t1191906455 * get__lastReadTask_13() const { return ____lastReadTask_13; }
	inline Task_1_t1191906455 ** get_address_of__lastReadTask_13() { return &____lastReadTask_13; }
	inline void set__lastReadTask_13(Task_1_t1191906455 * value)
	{
		____lastReadTask_13 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T743994179_H
#ifndef XMLTOKENIZEDTYPE_T1619571710_H
#define XMLTOKENIZEDTYPE_T1619571710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTokenizedType
struct  XmlTokenizedType_t1619571710 
{
public:
	// System.Int32 System.Xml.XmlTokenizedType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlTokenizedType_t1619571710, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENIZEDTYPE_T1619571710_H
#ifndef XMLNAMESPACESCOPE_T1384947590_H
#define XMLNAMESPACESCOPE_T1384947590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceScope
struct  XmlNamespaceScope_t1384947590 
{
public:
	// System.Int32 System.Xml.XmlNamespaceScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNamespaceScope_t1384947590, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACESCOPE_T1384947590_H
#ifndef XMLNODETYPE_T739504597_H
#define XMLNODETYPE_T739504597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t739504597 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeType_t739504597, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T739504597_H
#ifndef XMLEXCEPTION_T4188277960_H
#define XMLEXCEPTION_T4188277960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_t4188277960  : public SystemException_t3877406272
{
public:
	// System.String System.Xml.XmlException::res
	String_t* ___res_16;
	// System.String[] System.Xml.XmlException::args
	StringU5BU5D_t1642385972* ___args_17;
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_18;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_19;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_20;
	// System.String System.Xml.XmlException::message
	String_t* ___message_21;

public:
	inline static int32_t get_offset_of_res_16() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___res_16)); }
	inline String_t* get_res_16() const { return ___res_16; }
	inline String_t** get_address_of_res_16() { return &___res_16; }
	inline void set_res_16(String_t* value)
	{
		___res_16 = value;
		Il2CppCodeGenWriteBarrier((&___res_16), value);
	}

	inline static int32_t get_offset_of_args_17() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___args_17)); }
	inline StringU5BU5D_t1642385972* get_args_17() const { return ___args_17; }
	inline StringU5BU5D_t1642385972** get_address_of_args_17() { return &___args_17; }
	inline void set_args_17(StringU5BU5D_t1642385972* value)
	{
		___args_17 = value;
		Il2CppCodeGenWriteBarrier((&___args_17), value);
	}

	inline static int32_t get_offset_of_lineNumber_18() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___lineNumber_18)); }
	inline int32_t get_lineNumber_18() const { return ___lineNumber_18; }
	inline int32_t* get_address_of_lineNumber_18() { return &___lineNumber_18; }
	inline void set_lineNumber_18(int32_t value)
	{
		___lineNumber_18 = value;
	}

	inline static int32_t get_offset_of_linePosition_19() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___linePosition_19)); }
	inline int32_t get_linePosition_19() const { return ___linePosition_19; }
	inline int32_t* get_address_of_linePosition_19() { return &___linePosition_19; }
	inline void set_linePosition_19(int32_t value)
	{
		___linePosition_19 = value;
	}

	inline static int32_t get_offset_of_sourceUri_20() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___sourceUri_20)); }
	inline String_t* get_sourceUri_20() const { return ___sourceUri_20; }
	inline String_t** get_address_of_sourceUri_20() { return &___sourceUri_20; }
	inline void set_sourceUri_20(String_t* value)
	{
		___sourceUri_20 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_20), value);
	}

	inline static int32_t get_offset_of_message_21() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___message_21)); }
	inline String_t* get_message_21() const { return ___message_21; }
	inline String_t** get_address_of_message_21() { return &___message_21; }
	inline void set_message_21(String_t* value)
	{
		___message_21 = value;
		Il2CppCodeGenWriteBarrier((&___message_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEXCEPTION_T4188277960_H
#ifndef UCS4DECODER2143_T3957010607_H
#define UCS4DECODER2143_T3957010607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Decoder2143
struct  Ucs4Decoder2143_t3957010607  : public Ucs4Decoder_t2645160027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4DECODER2143_T3957010607_H
#ifndef UCS4DECODER3412_T3372157535_H
#define UCS4DECODER3412_T3372157535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Decoder3412
struct  Ucs4Decoder3412_t3372157535  : public Ucs4Decoder_t2645160027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4DECODER3412_T3372157535_H
#ifndef XPATHNAMESPACESCOPE_T3601604274_H
#define XPATHNAMESPACESCOPE_T3601604274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNamespaceScope
struct  XPathNamespaceScope_t3601604274 
{
public:
	// System.Int32 System.Xml.XPath.XPathNamespaceScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNamespaceScope_t3601604274, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAMESPACESCOPE_T3601604274_H
#ifndef XPATHNODETYPE_T817388867_H
#define XPATHNODETYPE_T817388867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_t817388867 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathNodeType_t817388867, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_T817388867_H
#ifndef XPATHRESULTTYPE_T1521569578_H
#define XPATHRESULTTYPE_T1521569578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathResultType
struct  XPathResultType_t1521569578 
{
public:
	// System.Int32 System.Xml.XPath.XPathResultType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XPathResultType_t1521569578, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHRESULTTYPE_T1521569578_H
#ifndef XPATHEXCEPTION_T1503722168_H
#define XPATHEXCEPTION_T1503722168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathException
struct  XPathException_t1503722168  : public SystemException_t3877406272
{
public:
	// System.String System.Xml.XPath.XPathException::res
	String_t* ___res_16;
	// System.String[] System.Xml.XPath.XPathException::args
	StringU5BU5D_t1642385972* ___args_17;
	// System.String System.Xml.XPath.XPathException::message
	String_t* ___message_18;

public:
	inline static int32_t get_offset_of_res_16() { return static_cast<int32_t>(offsetof(XPathException_t1503722168, ___res_16)); }
	inline String_t* get_res_16() const { return ___res_16; }
	inline String_t** get_address_of_res_16() { return &___res_16; }
	inline void set_res_16(String_t* value)
	{
		___res_16 = value;
		Il2CppCodeGenWriteBarrier((&___res_16), value);
	}

	inline static int32_t get_offset_of_args_17() { return static_cast<int32_t>(offsetof(XPathException_t1503722168, ___args_17)); }
	inline StringU5BU5D_t1642385972* get_args_17() const { return ___args_17; }
	inline StringU5BU5D_t1642385972** get_address_of_args_17() { return &___args_17; }
	inline void set_args_17(StringU5BU5D_t1642385972* value)
	{
		___args_17 = value;
		Il2CppCodeGenWriteBarrier((&___args_17), value);
	}

	inline static int32_t get_offset_of_message_18() { return static_cast<int32_t>(offsetof(XPathException_t1503722168, ___message_18)); }
	inline String_t* get_message_18() const { return ___message_18; }
	inline String_t** get_address_of_message_18() { return &___message_18; }
	inline void set_message_18(String_t* value)
	{
		___message_18 = value;
		Il2CppCodeGenWriteBarrier((&___message_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHEXCEPTION_T1503722168_H
#ifndef CONSTRAINTROLE_T3811561833_H
#define CONSTRAINTROLE_T3811561833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole
struct  ConstraintRole_t3811561833 
{
public:
	// System.Int32 System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstraintRole_t3811561833, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTROLE_T3811561833_H
#ifndef UCS4DECODER1234_T522314591_H
#define UCS4DECODER1234_T522314591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Decoder1234
struct  Ucs4Decoder1234_t522314591  : public Ucs4Decoder_t2645160027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4DECODER1234_T522314591_H
#ifndef CHOICENODE_T3123692209_H
#define CHOICENODE_T3123692209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ChoiceNode
struct  ChoiceNode_t3123692209  : public InteriorNode_t2716368958
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHOICENODE_T3123692209_H
#ifndef UCS4ENCODING1234_T31992440_H
#define UCS4ENCODING1234_T31992440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Encoding1234
struct  Ucs4Encoding1234_t31992440  : public Ucs4Encoding_t650080996
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4ENCODING1234_T31992440_H
#ifndef QMARKNODE_T902109702_H
#define QMARKNODE_T902109702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QmarkNode
struct  QmarkNode_t902109702  : public InteriorNode_t2716368958
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QMARKNODE_T902109702_H
#ifndef PLUSNODE_T3494526928_H
#define PLUSNODE_T3494526928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.PlusNode
struct  PlusNode_t3494526928  : public InteriorNode_t2716368958
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUSNODE_T3494526928_H
#ifndef SEQUENCENODE_T4039907291_H
#define SEQUENCENODE_T4039907291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SequenceNode
struct  SequenceNode_t4039907291  : public InteriorNode_t2716368958
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCENODE_T4039907291_H
#ifndef XMLSCHEMACONTENTTYPE_T2874429441_H
#define XMLSCHEMACONTENTTYPE_T2874429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentType
struct  XmlSchemaContentType_t2874429441 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentType_t2874429441, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTTYPE_T2874429441_H
#ifndef UCS4DECODER4321_T2511885999_H
#define UCS4DECODER4321_T2511885999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Decoder4321
struct  Ucs4Decoder4321_t2511885999  : public Ucs4Decoder_t2645160027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4DECODER4321_T2511885999_H
#ifndef UCS4ENCODING3412_T2881835128_H
#define UCS4ENCODING3412_T2881835128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Encoding3412
struct  Ucs4Encoding3412_t2881835128  : public Ucs4Encoding_t650080996
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4ENCODING3412_T2881835128_H
#ifndef UCS4ENCODING4321_T616845456_H
#define UCS4ENCODING4321_T616845456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ucs4Encoding4321
struct  Ucs4Encoding4321_t616845456  : public Ucs4Encoding_t650080996
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UCS4ENCODING4321_T616845456_H
#ifndef AXIS_T1660922251_H
#define AXIS_T1660922251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MS.Internal.Xml.XPath.Axis
struct  Axis_t1660922251  : public AstNode_t2002670936
{
public:
	// MS.Internal.Xml.XPath.Axis/AxisType MS.Internal.Xml.XPath.Axis::axisType
	int32_t ___axisType_0;
	// MS.Internal.Xml.XPath.AstNode MS.Internal.Xml.XPath.Axis::input
	AstNode_t2002670936 * ___input_1;
	// System.String MS.Internal.Xml.XPath.Axis::prefix
	String_t* ___prefix_2;
	// System.String MS.Internal.Xml.XPath.Axis::name
	String_t* ___name_3;
	// System.Xml.XPath.XPathNodeType MS.Internal.Xml.XPath.Axis::nodeType
	int32_t ___nodeType_4;
	// System.Boolean MS.Internal.Xml.XPath.Axis::abbrAxis
	bool ___abbrAxis_5;
	// System.String MS.Internal.Xml.XPath.Axis::urn
	String_t* ___urn_6;

public:
	inline static int32_t get_offset_of_axisType_0() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___axisType_0)); }
	inline int32_t get_axisType_0() const { return ___axisType_0; }
	inline int32_t* get_address_of_axisType_0() { return &___axisType_0; }
	inline void set_axisType_0(int32_t value)
	{
		___axisType_0 = value;
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___input_1)); }
	inline AstNode_t2002670936 * get_input_1() const { return ___input_1; }
	inline AstNode_t2002670936 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(AstNode_t2002670936 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_prefix_2() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___prefix_2)); }
	inline String_t* get_prefix_2() const { return ___prefix_2; }
	inline String_t** get_address_of_prefix_2() { return &___prefix_2; }
	inline void set_prefix_2(String_t* value)
	{
		___prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_abbrAxis_5() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___abbrAxis_5)); }
	inline bool get_abbrAxis_5() const { return ___abbrAxis_5; }
	inline bool* get_address_of_abbrAxis_5() { return &___abbrAxis_5; }
	inline void set_abbrAxis_5(bool value)
	{
		___abbrAxis_5 = value;
	}

	inline static int32_t get_offset_of_urn_6() { return static_cast<int32_t>(offsetof(Axis_t1660922251, ___urn_6)); }
	inline String_t* get_urn_6() const { return ___urn_6; }
	inline String_t** get_address_of_urn_6() { return &___urn_6; }
	inline void set_urn_6(String_t* value)
	{
		___urn_6 = value;
		Il2CppCodeGenWriteBarrier((&___urn_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1660922251_H
#ifndef U3CGETENTITYASYNCU3ED__0_T385453514_H
#define U3CGETENTITYASYNCU3ED__0_T385453514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver/<GetEntityAsync>d__0
struct  U3CGetEntityAsyncU3Ed__0_t385453514 
{
public:
	// System.Int32 System.Xml.XmlUrlResolver/<GetEntityAsync>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Object> System.Xml.XmlUrlResolver/<GetEntityAsync>d__0::<>t__builder
	AsyncTaskMethodBuilder_1_t1272420930  ___U3CU3Et__builder_1;
	// System.Type System.Xml.XmlUrlResolver/<GetEntityAsync>d__0::ofObjectToReturn
	Type_t * ___ofObjectToReturn_2;
	// System.Uri System.Xml.XmlUrlResolver/<GetEntityAsync>d__0::absoluteUri
	Uri_t19570940 * ___absoluteUri_3;
	// System.Xml.XmlUrlResolver System.Xml.XmlUrlResolver/<GetEntityAsync>d__0::<>4__this
	XmlUrlResolver_t896669594 * ___U3CU3E4__this_4;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.IO.Stream> System.Xml.XmlUrlResolver/<GetEntityAsync>d__0::<>u__1
	ConfiguredTaskAwaiter_t4171467654  ___U3CU3Eu__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__0_t385453514, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__0_t385453514, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t1272420930  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t1272420930 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t1272420930  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_ofObjectToReturn_2() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__0_t385453514, ___ofObjectToReturn_2)); }
	inline Type_t * get_ofObjectToReturn_2() const { return ___ofObjectToReturn_2; }
	inline Type_t ** get_address_of_ofObjectToReturn_2() { return &___ofObjectToReturn_2; }
	inline void set_ofObjectToReturn_2(Type_t * value)
	{
		___ofObjectToReturn_2 = value;
		Il2CppCodeGenWriteBarrier((&___ofObjectToReturn_2), value);
	}

	inline static int32_t get_offset_of_absoluteUri_3() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__0_t385453514, ___absoluteUri_3)); }
	inline Uri_t19570940 * get_absoluteUri_3() const { return ___absoluteUri_3; }
	inline Uri_t19570940 ** get_address_of_absoluteUri_3() { return &___absoluteUri_3; }
	inline void set_absoluteUri_3(Uri_t19570940 * value)
	{
		___absoluteUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___absoluteUri_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__0_t385453514, ___U3CU3E4__this_4)); }
	inline XmlUrlResolver_t896669594 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline XmlUrlResolver_t896669594 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(XmlUrlResolver_t896669594 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CGetEntityAsyncU3Ed__0_t385453514, ___U3CU3Eu__1_5)); }
	inline ConfiguredTaskAwaiter_t4171467654  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline ConfiguredTaskAwaiter_t4171467654 * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(ConfiguredTaskAwaiter_t4171467654  value)
	{
		___U3CU3Eu__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENTITYASYNCU3ED__0_T385453514_H
#ifndef DTDPARSER_T821190747_H
#define DTDPARSER_T821190747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser
struct  DtdParser_t821190747  : public RuntimeObject
{
public:
	// System.Xml.IDtdParserAdapter System.Xml.DtdParser::readerAdapter
	RuntimeObject* ___readerAdapter_0;
	// System.Xml.IDtdParserAdapterWithValidation System.Xml.DtdParser::readerAdapterWithValidation
	RuntimeObject* ___readerAdapterWithValidation_1;
	// System.Xml.XmlNameTable System.Xml.DtdParser::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Xml.Schema.SchemaInfo System.Xml.DtdParser::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_3;
	// System.Xml.XmlCharType System.Xml.DtdParser::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_4;
	// System.String System.Xml.DtdParser::systemId
	String_t* ___systemId_5;
	// System.String System.Xml.DtdParser::publicId
	String_t* ___publicId_6;
	// System.Boolean System.Xml.DtdParser::normalize
	bool ___normalize_7;
	// System.Boolean System.Xml.DtdParser::validate
	bool ___validate_8;
	// System.Boolean System.Xml.DtdParser::supportNamespaces
	bool ___supportNamespaces_9;
	// System.Boolean System.Xml.DtdParser::v1Compat
	bool ___v1Compat_10;
	// System.Char[] System.Xml.DtdParser::chars
	CharU5BU5D_t1328083999* ___chars_11;
	// System.Int32 System.Xml.DtdParser::charsUsed
	int32_t ___charsUsed_12;
	// System.Int32 System.Xml.DtdParser::curPos
	int32_t ___curPos_13;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::scanningFunction
	int32_t ___scanningFunction_14;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::nextScaningFunction
	int32_t ___nextScaningFunction_15;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::savedScanningFunction
	int32_t ___savedScanningFunction_16;
	// System.Boolean System.Xml.DtdParser::whitespaceSeen
	bool ___whitespaceSeen_17;
	// System.Int32 System.Xml.DtdParser::tokenStartPos
	int32_t ___tokenStartPos_18;
	// System.Int32 System.Xml.DtdParser::colonPos
	int32_t ___colonPos_19;
	// System.Text.StringBuilder System.Xml.DtdParser::internalSubsetValueSb
	StringBuilder_t1221177846 * ___internalSubsetValueSb_20;
	// System.Int32 System.Xml.DtdParser::externalEntitiesDepth
	int32_t ___externalEntitiesDepth_21;
	// System.Int32 System.Xml.DtdParser::currentEntityId
	int32_t ___currentEntityId_22;
	// System.Boolean System.Xml.DtdParser::freeFloatingDtd
	bool ___freeFloatingDtd_23;
	// System.Boolean System.Xml.DtdParser::hasFreeFloatingInternalSubset
	bool ___hasFreeFloatingInternalSubset_24;
	// System.Text.StringBuilder System.Xml.DtdParser::stringBuilder
	StringBuilder_t1221177846 * ___stringBuilder_25;
	// System.Int32 System.Xml.DtdParser::condSectionDepth
	int32_t ___condSectionDepth_26;
	// System.Xml.LineInfo System.Xml.DtdParser::literalLineInfo
	LineInfo_t1429635508  ___literalLineInfo_27;
	// System.Char System.Xml.DtdParser::literalQuoteChar
	Il2CppChar ___literalQuoteChar_28;
	// System.String System.Xml.DtdParser::documentBaseUri
	String_t* ___documentBaseUri_29;
	// System.String System.Xml.DtdParser::externalDtdBaseUri
	String_t* ___externalDtdBaseUri_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.DtdParser/UndeclaredNotation> System.Xml.DtdParser::undeclaredNotations
	Dictionary_2_t3981174159 * ___undeclaredNotations_31;
	// System.Int32[] System.Xml.DtdParser::condSectionEntityIds
	Int32U5BU5D_t3030399641* ___condSectionEntityIds_32;

public:
	inline static int32_t get_offset_of_readerAdapter_0() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___readerAdapter_0)); }
	inline RuntimeObject* get_readerAdapter_0() const { return ___readerAdapter_0; }
	inline RuntimeObject** get_address_of_readerAdapter_0() { return &___readerAdapter_0; }
	inline void set_readerAdapter_0(RuntimeObject* value)
	{
		___readerAdapter_0 = value;
		Il2CppCodeGenWriteBarrier((&___readerAdapter_0), value);
	}

	inline static int32_t get_offset_of_readerAdapterWithValidation_1() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___readerAdapterWithValidation_1)); }
	inline RuntimeObject* get_readerAdapterWithValidation_1() const { return ___readerAdapterWithValidation_1; }
	inline RuntimeObject** get_address_of_readerAdapterWithValidation_1() { return &___readerAdapterWithValidation_1; }
	inline void set_readerAdapterWithValidation_1(RuntimeObject* value)
	{
		___readerAdapterWithValidation_1 = value;
		Il2CppCodeGenWriteBarrier((&___readerAdapterWithValidation_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaInfo_3() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___schemaInfo_3)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_3() const { return ___schemaInfo_3; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_3() { return &___schemaInfo_3; }
	inline void set_schemaInfo_3(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_3), value);
	}

	inline static int32_t get_offset_of_xmlCharType_4() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___xmlCharType_4)); }
	inline XmlCharType_t1050521405  get_xmlCharType_4() const { return ___xmlCharType_4; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_4() { return &___xmlCharType_4; }
	inline void set_xmlCharType_4(XmlCharType_t1050521405  value)
	{
		___xmlCharType_4 = value;
	}

	inline static int32_t get_offset_of_systemId_5() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___systemId_5)); }
	inline String_t* get_systemId_5() const { return ___systemId_5; }
	inline String_t** get_address_of_systemId_5() { return &___systemId_5; }
	inline void set_systemId_5(String_t* value)
	{
		___systemId_5 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_normalize_7() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___normalize_7)); }
	inline bool get_normalize_7() const { return ___normalize_7; }
	inline bool* get_address_of_normalize_7() { return &___normalize_7; }
	inline void set_normalize_7(bool value)
	{
		___normalize_7 = value;
	}

	inline static int32_t get_offset_of_validate_8() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___validate_8)); }
	inline bool get_validate_8() const { return ___validate_8; }
	inline bool* get_address_of_validate_8() { return &___validate_8; }
	inline void set_validate_8(bool value)
	{
		___validate_8 = value;
	}

	inline static int32_t get_offset_of_supportNamespaces_9() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___supportNamespaces_9)); }
	inline bool get_supportNamespaces_9() const { return ___supportNamespaces_9; }
	inline bool* get_address_of_supportNamespaces_9() { return &___supportNamespaces_9; }
	inline void set_supportNamespaces_9(bool value)
	{
		___supportNamespaces_9 = value;
	}

	inline static int32_t get_offset_of_v1Compat_10() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___v1Compat_10)); }
	inline bool get_v1Compat_10() const { return ___v1Compat_10; }
	inline bool* get_address_of_v1Compat_10() { return &___v1Compat_10; }
	inline void set_v1Compat_10(bool value)
	{
		___v1Compat_10 = value;
	}

	inline static int32_t get_offset_of_chars_11() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___chars_11)); }
	inline CharU5BU5D_t1328083999* get_chars_11() const { return ___chars_11; }
	inline CharU5BU5D_t1328083999** get_address_of_chars_11() { return &___chars_11; }
	inline void set_chars_11(CharU5BU5D_t1328083999* value)
	{
		___chars_11 = value;
		Il2CppCodeGenWriteBarrier((&___chars_11), value);
	}

	inline static int32_t get_offset_of_charsUsed_12() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___charsUsed_12)); }
	inline int32_t get_charsUsed_12() const { return ___charsUsed_12; }
	inline int32_t* get_address_of_charsUsed_12() { return &___charsUsed_12; }
	inline void set_charsUsed_12(int32_t value)
	{
		___charsUsed_12 = value;
	}

	inline static int32_t get_offset_of_curPos_13() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___curPos_13)); }
	inline int32_t get_curPos_13() const { return ___curPos_13; }
	inline int32_t* get_address_of_curPos_13() { return &___curPos_13; }
	inline void set_curPos_13(int32_t value)
	{
		___curPos_13 = value;
	}

	inline static int32_t get_offset_of_scanningFunction_14() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___scanningFunction_14)); }
	inline int32_t get_scanningFunction_14() const { return ___scanningFunction_14; }
	inline int32_t* get_address_of_scanningFunction_14() { return &___scanningFunction_14; }
	inline void set_scanningFunction_14(int32_t value)
	{
		___scanningFunction_14 = value;
	}

	inline static int32_t get_offset_of_nextScaningFunction_15() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___nextScaningFunction_15)); }
	inline int32_t get_nextScaningFunction_15() const { return ___nextScaningFunction_15; }
	inline int32_t* get_address_of_nextScaningFunction_15() { return &___nextScaningFunction_15; }
	inline void set_nextScaningFunction_15(int32_t value)
	{
		___nextScaningFunction_15 = value;
	}

	inline static int32_t get_offset_of_savedScanningFunction_16() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___savedScanningFunction_16)); }
	inline int32_t get_savedScanningFunction_16() const { return ___savedScanningFunction_16; }
	inline int32_t* get_address_of_savedScanningFunction_16() { return &___savedScanningFunction_16; }
	inline void set_savedScanningFunction_16(int32_t value)
	{
		___savedScanningFunction_16 = value;
	}

	inline static int32_t get_offset_of_whitespaceSeen_17() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___whitespaceSeen_17)); }
	inline bool get_whitespaceSeen_17() const { return ___whitespaceSeen_17; }
	inline bool* get_address_of_whitespaceSeen_17() { return &___whitespaceSeen_17; }
	inline void set_whitespaceSeen_17(bool value)
	{
		___whitespaceSeen_17 = value;
	}

	inline static int32_t get_offset_of_tokenStartPos_18() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___tokenStartPos_18)); }
	inline int32_t get_tokenStartPos_18() const { return ___tokenStartPos_18; }
	inline int32_t* get_address_of_tokenStartPos_18() { return &___tokenStartPos_18; }
	inline void set_tokenStartPos_18(int32_t value)
	{
		___tokenStartPos_18 = value;
	}

	inline static int32_t get_offset_of_colonPos_19() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___colonPos_19)); }
	inline int32_t get_colonPos_19() const { return ___colonPos_19; }
	inline int32_t* get_address_of_colonPos_19() { return &___colonPos_19; }
	inline void set_colonPos_19(int32_t value)
	{
		___colonPos_19 = value;
	}

	inline static int32_t get_offset_of_internalSubsetValueSb_20() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___internalSubsetValueSb_20)); }
	inline StringBuilder_t1221177846 * get_internalSubsetValueSb_20() const { return ___internalSubsetValueSb_20; }
	inline StringBuilder_t1221177846 ** get_address_of_internalSubsetValueSb_20() { return &___internalSubsetValueSb_20; }
	inline void set_internalSubsetValueSb_20(StringBuilder_t1221177846 * value)
	{
		___internalSubsetValueSb_20 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubsetValueSb_20), value);
	}

	inline static int32_t get_offset_of_externalEntitiesDepth_21() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___externalEntitiesDepth_21)); }
	inline int32_t get_externalEntitiesDepth_21() const { return ___externalEntitiesDepth_21; }
	inline int32_t* get_address_of_externalEntitiesDepth_21() { return &___externalEntitiesDepth_21; }
	inline void set_externalEntitiesDepth_21(int32_t value)
	{
		___externalEntitiesDepth_21 = value;
	}

	inline static int32_t get_offset_of_currentEntityId_22() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___currentEntityId_22)); }
	inline int32_t get_currentEntityId_22() const { return ___currentEntityId_22; }
	inline int32_t* get_address_of_currentEntityId_22() { return &___currentEntityId_22; }
	inline void set_currentEntityId_22(int32_t value)
	{
		___currentEntityId_22 = value;
	}

	inline static int32_t get_offset_of_freeFloatingDtd_23() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___freeFloatingDtd_23)); }
	inline bool get_freeFloatingDtd_23() const { return ___freeFloatingDtd_23; }
	inline bool* get_address_of_freeFloatingDtd_23() { return &___freeFloatingDtd_23; }
	inline void set_freeFloatingDtd_23(bool value)
	{
		___freeFloatingDtd_23 = value;
	}

	inline static int32_t get_offset_of_hasFreeFloatingInternalSubset_24() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___hasFreeFloatingInternalSubset_24)); }
	inline bool get_hasFreeFloatingInternalSubset_24() const { return ___hasFreeFloatingInternalSubset_24; }
	inline bool* get_address_of_hasFreeFloatingInternalSubset_24() { return &___hasFreeFloatingInternalSubset_24; }
	inline void set_hasFreeFloatingInternalSubset_24(bool value)
	{
		___hasFreeFloatingInternalSubset_24 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_25() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___stringBuilder_25)); }
	inline StringBuilder_t1221177846 * get_stringBuilder_25() const { return ___stringBuilder_25; }
	inline StringBuilder_t1221177846 ** get_address_of_stringBuilder_25() { return &___stringBuilder_25; }
	inline void set_stringBuilder_25(StringBuilder_t1221177846 * value)
	{
		___stringBuilder_25 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuilder_25), value);
	}

	inline static int32_t get_offset_of_condSectionDepth_26() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___condSectionDepth_26)); }
	inline int32_t get_condSectionDepth_26() const { return ___condSectionDepth_26; }
	inline int32_t* get_address_of_condSectionDepth_26() { return &___condSectionDepth_26; }
	inline void set_condSectionDepth_26(int32_t value)
	{
		___condSectionDepth_26 = value;
	}

	inline static int32_t get_offset_of_literalLineInfo_27() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___literalLineInfo_27)); }
	inline LineInfo_t1429635508  get_literalLineInfo_27() const { return ___literalLineInfo_27; }
	inline LineInfo_t1429635508 * get_address_of_literalLineInfo_27() { return &___literalLineInfo_27; }
	inline void set_literalLineInfo_27(LineInfo_t1429635508  value)
	{
		___literalLineInfo_27 = value;
	}

	inline static int32_t get_offset_of_literalQuoteChar_28() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___literalQuoteChar_28)); }
	inline Il2CppChar get_literalQuoteChar_28() const { return ___literalQuoteChar_28; }
	inline Il2CppChar* get_address_of_literalQuoteChar_28() { return &___literalQuoteChar_28; }
	inline void set_literalQuoteChar_28(Il2CppChar value)
	{
		___literalQuoteChar_28 = value;
	}

	inline static int32_t get_offset_of_documentBaseUri_29() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___documentBaseUri_29)); }
	inline String_t* get_documentBaseUri_29() const { return ___documentBaseUri_29; }
	inline String_t** get_address_of_documentBaseUri_29() { return &___documentBaseUri_29; }
	inline void set_documentBaseUri_29(String_t* value)
	{
		___documentBaseUri_29 = value;
		Il2CppCodeGenWriteBarrier((&___documentBaseUri_29), value);
	}

	inline static int32_t get_offset_of_externalDtdBaseUri_30() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___externalDtdBaseUri_30)); }
	inline String_t* get_externalDtdBaseUri_30() const { return ___externalDtdBaseUri_30; }
	inline String_t** get_address_of_externalDtdBaseUri_30() { return &___externalDtdBaseUri_30; }
	inline void set_externalDtdBaseUri_30(String_t* value)
	{
		___externalDtdBaseUri_30 = value;
		Il2CppCodeGenWriteBarrier((&___externalDtdBaseUri_30), value);
	}

	inline static int32_t get_offset_of_undeclaredNotations_31() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___undeclaredNotations_31)); }
	inline Dictionary_2_t3981174159 * get_undeclaredNotations_31() const { return ___undeclaredNotations_31; }
	inline Dictionary_2_t3981174159 ** get_address_of_undeclaredNotations_31() { return &___undeclaredNotations_31; }
	inline void set_undeclaredNotations_31(Dictionary_2_t3981174159 * value)
	{
		___undeclaredNotations_31 = value;
		Il2CppCodeGenWriteBarrier((&___undeclaredNotations_31), value);
	}

	inline static int32_t get_offset_of_condSectionEntityIds_32() { return static_cast<int32_t>(offsetof(DtdParser_t821190747, ___condSectionEntityIds_32)); }
	inline Int32U5BU5D_t3030399641* get_condSectionEntityIds_32() const { return ___condSectionEntityIds_32; }
	inline Int32U5BU5D_t3030399641** get_address_of_condSectionEntityIds_32() { return &___condSectionEntityIds_32; }
	inline void set_condSectionEntityIds_32(Int32U5BU5D_t3030399641* value)
	{
		___condSectionEntityIds_32 = value;
		Il2CppCodeGenWriteBarrier((&___condSectionEntityIds_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARSER_T821190747_H
#ifndef COMPILEDIDENTITYCONSTRAINT_T964629540_H
#define COMPILEDIDENTITYCONSTRAINT_T964629540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.CompiledIdentityConstraint
struct  CompiledIdentityConstraint_t964629540  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::name
	XmlQualifiedName_t1944712516 * ___name_0;
	// System.Xml.Schema.CompiledIdentityConstraint/ConstraintRole System.Xml.Schema.CompiledIdentityConstraint::role
	int32_t ___role_1;
	// System.Xml.Schema.Asttree System.Xml.Schema.CompiledIdentityConstraint::selector
	Asttree_t3451058494 * ___selector_2;
	// System.Xml.Schema.Asttree[] System.Xml.Schema.CompiledIdentityConstraint::fields
	AsttreeU5BU5D_t1751151627* ___fields_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.CompiledIdentityConstraint::refer
	XmlQualifiedName_t1944712516 * ___refer_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___name_0)); }
	inline XmlQualifiedName_t1944712516 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t1944712516 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_role_1() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___role_1)); }
	inline int32_t get_role_1() const { return ___role_1; }
	inline int32_t* get_address_of_role_1() { return &___role_1; }
	inline void set_role_1(int32_t value)
	{
		___role_1 = value;
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___selector_2)); }
	inline Asttree_t3451058494 * get_selector_2() const { return ___selector_2; }
	inline Asttree_t3451058494 ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Asttree_t3451058494 * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier((&___selector_2), value);
	}

	inline static int32_t get_offset_of_fields_3() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___fields_3)); }
	inline AsttreeU5BU5D_t1751151627* get_fields_3() const { return ___fields_3; }
	inline AsttreeU5BU5D_t1751151627** get_address_of_fields_3() { return &___fields_3; }
	inline void set_fields_3(AsttreeU5BU5D_t1751151627* value)
	{
		___fields_3 = value;
		Il2CppCodeGenWriteBarrier((&___fields_3), value);
	}

	inline static int32_t get_offset_of_refer_4() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540, ___refer_4)); }
	inline XmlQualifiedName_t1944712516 * get_refer_4() const { return ___refer_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refer_4() { return &___refer_4; }
	inline void set_refer_4(XmlQualifiedName_t1944712516 * value)
	{
		___refer_4 = value;
		Il2CppCodeGenWriteBarrier((&___refer_4), value);
	}
};

struct CompiledIdentityConstraint_t964629540_StaticFields
{
public:
	// System.Xml.Schema.CompiledIdentityConstraint System.Xml.Schema.CompiledIdentityConstraint::Empty
	CompiledIdentityConstraint_t964629540 * ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(CompiledIdentityConstraint_t964629540_StaticFields, ___Empty_5)); }
	inline CompiledIdentityConstraint_t964629540 * get_Empty_5() const { return ___Empty_5; }
	inline CompiledIdentityConstraint_t964629540 ** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(CompiledIdentityConstraint_t964629540 * value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDIDENTITYCONSTRAINT_T964629540_H
#ifndef PARSEELEMENTONLYCONTENT_LOCALFRAME_T225387055_H
#define PARSEELEMENTONLYCONTENT_LOCALFRAME_T225387055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame
struct  ParseElementOnlyContent_LocalFrame_t225387055  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame::startParenEntityId
	int32_t ___startParenEntityId_0;
	// System.Xml.DtdParser/Token System.Xml.DtdParser/ParseElementOnlyContent_LocalFrame::parsingSchema
	int32_t ___parsingSchema_1;

public:
	inline static int32_t get_offset_of_startParenEntityId_0() { return static_cast<int32_t>(offsetof(ParseElementOnlyContent_LocalFrame_t225387055, ___startParenEntityId_0)); }
	inline int32_t get_startParenEntityId_0() const { return ___startParenEntityId_0; }
	inline int32_t* get_address_of_startParenEntityId_0() { return &___startParenEntityId_0; }
	inline void set_startParenEntityId_0(int32_t value)
	{
		___startParenEntityId_0 = value;
	}

	inline static int32_t get_offset_of_parsingSchema_1() { return static_cast<int32_t>(offsetof(ParseElementOnlyContent_LocalFrame_t225387055, ___parsingSchema_1)); }
	inline int32_t get_parsingSchema_1() const { return ___parsingSchema_1; }
	inline int32_t* get_address_of_parsingSchema_1() { return &___parsingSchema_1; }
	inline void set_parsingSchema_1(int32_t value)
	{
		___parsingSchema_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEELEMENTONLYCONTENT_LOCALFRAME_T225387055_H
#ifndef U3CGETNONFILESTREAMASYNCU3ED__1_T2196027765_H
#define U3CGETNONFILESTREAMASYNCU3ED__1_T2196027765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1
struct  U3CGetNonFileStreamAsyncU3Ed__1_t2196027765 
{
public:
	// System.Int32 System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.IO.Stream> System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::<>t__builder
	AsyncTaskMethodBuilder_1_t1838408441  ___U3CU3Et__builder_1;
	// System.Uri System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::uri
	Uri_t19570940 * ___uri_2;
	// System.Net.ICredentials System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::credentials
	RuntimeObject* ___credentials_3;
	// System.Net.IWebProxy System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::proxy
	RuntimeObject* ___proxy_4;
	// System.Net.Cache.RequestCachePolicy System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::cachePolicy
	RequestCachePolicy_t2663429579 * ___cachePolicy_5;
	// System.Net.WebRequest System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::<req>5__1
	WebRequest_t1365124353 * ___U3CreqU3E5__1_6;
	// System.Xml.XmlDownloadManager System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::<>4__this
	XmlDownloadManager_t830495394 * ___U3CU3E4__this_7;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.WebResponse> System.Xml.XmlDownloadManager/<GetNonFileStreamAsync>d__1::<>u__1
	ConfiguredTaskAwaiter_t2811256899  ___U3CU3Eu__1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t1838408441  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t1838408441 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t1838408441  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___uri_2)); }
	inline Uri_t19570940 * get_uri_2() const { return ___uri_2; }
	inline Uri_t19570940 ** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(Uri_t19570940 * value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_credentials_3() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___credentials_3)); }
	inline RuntimeObject* get_credentials_3() const { return ___credentials_3; }
	inline RuntimeObject** get_address_of_credentials_3() { return &___credentials_3; }
	inline void set_credentials_3(RuntimeObject* value)
	{
		___credentials_3 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_3), value);
	}

	inline static int32_t get_offset_of_proxy_4() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___proxy_4)); }
	inline RuntimeObject* get_proxy_4() const { return ___proxy_4; }
	inline RuntimeObject** get_address_of_proxy_4() { return &___proxy_4; }
	inline void set_proxy_4(RuntimeObject* value)
	{
		___proxy_4 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_4), value);
	}

	inline static int32_t get_offset_of_cachePolicy_5() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___cachePolicy_5)); }
	inline RequestCachePolicy_t2663429579 * get_cachePolicy_5() const { return ___cachePolicy_5; }
	inline RequestCachePolicy_t2663429579 ** get_address_of_cachePolicy_5() { return &___cachePolicy_5; }
	inline void set_cachePolicy_5(RequestCachePolicy_t2663429579 * value)
	{
		___cachePolicy_5 = value;
		Il2CppCodeGenWriteBarrier((&___cachePolicy_5), value);
	}

	inline static int32_t get_offset_of_U3CreqU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___U3CreqU3E5__1_6)); }
	inline WebRequest_t1365124353 * get_U3CreqU3E5__1_6() const { return ___U3CreqU3E5__1_6; }
	inline WebRequest_t1365124353 ** get_address_of_U3CreqU3E5__1_6() { return &___U3CreqU3E5__1_6; }
	inline void set_U3CreqU3E5__1_6(WebRequest_t1365124353 * value)
	{
		___U3CreqU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreqU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___U3CU3E4__this_7)); }
	inline XmlDownloadManager_t830495394 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline XmlDownloadManager_t830495394 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(XmlDownloadManager_t830495394 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CGetNonFileStreamAsyncU3Ed__1_t2196027765, ___U3CU3Eu__1_8)); }
	inline ConfiguredTaskAwaiter_t2811256899  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline ConfiguredTaskAwaiter_t2811256899 * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(ConfiguredTaskAwaiter_t2811256899  value)
	{
		___U3CU3Eu__1_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETNONFILESTREAMASYNCU3ED__1_T2196027765_H
#ifndef XMLCACHEDSTREAM_T235166513_H
#define XMLCACHEDSTREAM_T235166513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCachedStream
struct  XmlCachedStream_t235166513  : public MemoryStream_t743994179
{
public:
	// System.Uri System.Xml.XmlCachedStream::uri
	Uri_t19570940 * ___uri_14;

public:
	inline static int32_t get_offset_of_uri_14() { return static_cast<int32_t>(offsetof(XmlCachedStream_t235166513, ___uri_14)); }
	inline Uri_t19570940 * get_uri_14() const { return ___uri_14; }
	inline Uri_t19570940 ** get_address_of_uri_14() { return &___uri_14; }
	inline void set_uri_14(Uri_t19570940 * value)
	{
		___uri_14 = value;
		Il2CppCodeGenWriteBarrier((&___uri_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCACHEDSTREAM_T235166513_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef CONTENTVALIDATOR_T2510151843_H
#define CONTENTVALIDATOR_T2510151843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ContentValidator
struct  ContentValidator_t2510151843  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.ContentValidator::contentType
	int32_t ___contentType_0;
	// System.Boolean System.Xml.Schema.ContentValidator::isOpen
	bool ___isOpen_1;
	// System.Boolean System.Xml.Schema.ContentValidator::isEmptiable
	bool ___isEmptiable_2;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___contentType_0)); }
	inline int32_t get_contentType_0() const { return ___contentType_0; }
	inline int32_t* get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(int32_t value)
	{
		___contentType_0 = value;
	}

	inline static int32_t get_offset_of_isOpen_1() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___isOpen_1)); }
	inline bool get_isOpen_1() const { return ___isOpen_1; }
	inline bool* get_address_of_isOpen_1() { return &___isOpen_1; }
	inline void set_isOpen_1(bool value)
	{
		___isOpen_1 = value;
	}

	inline static int32_t get_offset_of_isEmptiable_2() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___isEmptiable_2)); }
	inline bool get_isEmptiable_2() const { return ___isEmptiable_2; }
	inline bool* get_address_of_isEmptiable_2() { return &___isEmptiable_2; }
	inline void set_isEmptiable_2(bool value)
	{
		___isEmptiable_2 = value;
	}
};

struct ContentValidator_t2510151843_StaticFields
{
public:
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Empty
	ContentValidator_t2510151843 * ___Empty_3;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::TextOnly
	ContentValidator_t2510151843 * ___TextOnly_4;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Mixed
	ContentValidator_t2510151843 * ___Mixed_5;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Any
	ContentValidator_t2510151843 * ___Any_6;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Empty_3)); }
	inline ContentValidator_t2510151843 * get_Empty_3() const { return ___Empty_3; }
	inline ContentValidator_t2510151843 ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(ContentValidator_t2510151843 * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}

	inline static int32_t get_offset_of_TextOnly_4() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___TextOnly_4)); }
	inline ContentValidator_t2510151843 * get_TextOnly_4() const { return ___TextOnly_4; }
	inline ContentValidator_t2510151843 ** get_address_of_TextOnly_4() { return &___TextOnly_4; }
	inline void set_TextOnly_4(ContentValidator_t2510151843 * value)
	{
		___TextOnly_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextOnly_4), value);
	}

	inline static int32_t get_offset_of_Mixed_5() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Mixed_5)); }
	inline ContentValidator_t2510151843 * get_Mixed_5() const { return ___Mixed_5; }
	inline ContentValidator_t2510151843 ** get_address_of_Mixed_5() { return &___Mixed_5; }
	inline void set_Mixed_5(ContentValidator_t2510151843 * value)
	{
		___Mixed_5 = value;
		Il2CppCodeGenWriteBarrier((&___Mixed_5), value);
	}

	inline static int32_t get_offset_of_Any_6() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Any_6)); }
	inline ContentValidator_t2510151843 * get_Any_6() const { return ___Any_6; }
	inline ContentValidator_t2510151843 ** get_address_of_Any_6() { return &___Any_6; }
	inline void set_Any_6(ContentValidator_t2510151843 * value)
	{
		___Any_6 = value;
		Il2CppCodeGenWriteBarrier((&___Any_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTVALIDATOR_T2510151843_H
#ifndef RANGECONTENTVALIDATOR_T857885766_H
#define RANGECONTENTVALIDATOR_T857885766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RangeContentValidator
struct  RangeContentValidator_t857885766  : public ContentValidator_t2510151843
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.RangeContentValidator::firstpos
	BitSet_t1062448123 * ___firstpos_7;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.RangeContentValidator::followpos
	BitSetU5BU5D_t2256991674* ___followpos_8;
	// System.Xml.Schema.BitSet System.Xml.Schema.RangeContentValidator::positionsWithRangeTerminals
	BitSet_t1062448123 * ___positionsWithRangeTerminals_9;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.RangeContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_10;
	// System.Xml.Schema.Positions System.Xml.Schema.RangeContentValidator::positions
	Positions_t3593914952 * ___positions_11;
	// System.Int32 System.Xml.Schema.RangeContentValidator::minMaxNodesCount
	int32_t ___minMaxNodesCount_12;
	// System.Int32 System.Xml.Schema.RangeContentValidator::endMarkerPos
	int32_t ___endMarkerPos_13;

public:
	inline static int32_t get_offset_of_firstpos_7() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___firstpos_7)); }
	inline BitSet_t1062448123 * get_firstpos_7() const { return ___firstpos_7; }
	inline BitSet_t1062448123 ** get_address_of_firstpos_7() { return &___firstpos_7; }
	inline void set_firstpos_7(BitSet_t1062448123 * value)
	{
		___firstpos_7 = value;
		Il2CppCodeGenWriteBarrier((&___firstpos_7), value);
	}

	inline static int32_t get_offset_of_followpos_8() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___followpos_8)); }
	inline BitSetU5BU5D_t2256991674* get_followpos_8() const { return ___followpos_8; }
	inline BitSetU5BU5D_t2256991674** get_address_of_followpos_8() { return &___followpos_8; }
	inline void set_followpos_8(BitSetU5BU5D_t2256991674* value)
	{
		___followpos_8 = value;
		Il2CppCodeGenWriteBarrier((&___followpos_8), value);
	}

	inline static int32_t get_offset_of_positionsWithRangeTerminals_9() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___positionsWithRangeTerminals_9)); }
	inline BitSet_t1062448123 * get_positionsWithRangeTerminals_9() const { return ___positionsWithRangeTerminals_9; }
	inline BitSet_t1062448123 ** get_address_of_positionsWithRangeTerminals_9() { return &___positionsWithRangeTerminals_9; }
	inline void set_positionsWithRangeTerminals_9(BitSet_t1062448123 * value)
	{
		___positionsWithRangeTerminals_9 = value;
		Il2CppCodeGenWriteBarrier((&___positionsWithRangeTerminals_9), value);
	}

	inline static int32_t get_offset_of_symbols_10() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___symbols_10)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_10() const { return ___symbols_10; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_10() { return &___symbols_10; }
	inline void set_symbols_10(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_10 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_10), value);
	}

	inline static int32_t get_offset_of_positions_11() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___positions_11)); }
	inline Positions_t3593914952 * get_positions_11() const { return ___positions_11; }
	inline Positions_t3593914952 ** get_address_of_positions_11() { return &___positions_11; }
	inline void set_positions_11(Positions_t3593914952 * value)
	{
		___positions_11 = value;
		Il2CppCodeGenWriteBarrier((&___positions_11), value);
	}

	inline static int32_t get_offset_of_minMaxNodesCount_12() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___minMaxNodesCount_12)); }
	inline int32_t get_minMaxNodesCount_12() const { return ___minMaxNodesCount_12; }
	inline int32_t* get_address_of_minMaxNodesCount_12() { return &___minMaxNodesCount_12; }
	inline void set_minMaxNodesCount_12(int32_t value)
	{
		___minMaxNodesCount_12 = value;
	}

	inline static int32_t get_offset_of_endMarkerPos_13() { return static_cast<int32_t>(offsetof(RangeContentValidator_t857885766, ___endMarkerPos_13)); }
	inline int32_t get_endMarkerPos_13() const { return ___endMarkerPos_13; }
	inline int32_t* get_address_of_endMarkerPos_13() { return &___endMarkerPos_13; }
	inline void set_endMarkerPos_13(int32_t value)
	{
		___endMarkerPos_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGECONTENTVALIDATOR_T857885766_H
#ifndef DOUBLELINKAXIS_T3164907012_H
#define DOUBLELINKAXIS_T3164907012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DoubleLinkAxis
struct  DoubleLinkAxis_t3164907012  : public Axis_t1660922251
{
public:
	// MS.Internal.Xml.XPath.Axis System.Xml.Schema.DoubleLinkAxis::next
	Axis_t1660922251 * ___next_7;

public:
	inline static int32_t get_offset_of_next_7() { return static_cast<int32_t>(offsetof(DoubleLinkAxis_t3164907012, ___next_7)); }
	inline Axis_t1660922251 * get_next_7() const { return ___next_7; }
	inline Axis_t1660922251 ** get_address_of_next_7() { return &___next_7; }
	inline void set_next_7(Axis_t1660922251 * value)
	{
		___next_7 = value;
		Il2CppCodeGenWriteBarrier((&___next_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELINKAXIS_T3164907012_H
#ifndef NFACONTENTVALIDATOR_T1842871216_H
#define NFACONTENTVALIDATOR_T1842871216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NfaContentValidator
struct  NfaContentValidator_t1842871216  : public ContentValidator_t2510151843
{
public:
	// System.Xml.Schema.BitSet System.Xml.Schema.NfaContentValidator::firstpos
	BitSet_t1062448123 * ___firstpos_7;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.NfaContentValidator::followpos
	BitSetU5BU5D_t2256991674* ___followpos_8;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.NfaContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_9;
	// System.Xml.Schema.Positions System.Xml.Schema.NfaContentValidator::positions
	Positions_t3593914952 * ___positions_10;
	// System.Int32 System.Xml.Schema.NfaContentValidator::endMarkerPos
	int32_t ___endMarkerPos_11;

public:
	inline static int32_t get_offset_of_firstpos_7() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___firstpos_7)); }
	inline BitSet_t1062448123 * get_firstpos_7() const { return ___firstpos_7; }
	inline BitSet_t1062448123 ** get_address_of_firstpos_7() { return &___firstpos_7; }
	inline void set_firstpos_7(BitSet_t1062448123 * value)
	{
		___firstpos_7 = value;
		Il2CppCodeGenWriteBarrier((&___firstpos_7), value);
	}

	inline static int32_t get_offset_of_followpos_8() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___followpos_8)); }
	inline BitSetU5BU5D_t2256991674* get_followpos_8() const { return ___followpos_8; }
	inline BitSetU5BU5D_t2256991674** get_address_of_followpos_8() { return &___followpos_8; }
	inline void set_followpos_8(BitSetU5BU5D_t2256991674* value)
	{
		___followpos_8 = value;
		Il2CppCodeGenWriteBarrier((&___followpos_8), value);
	}

	inline static int32_t get_offset_of_symbols_9() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___symbols_9)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_9() const { return ___symbols_9; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_9() { return &___symbols_9; }
	inline void set_symbols_9(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_9 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_9), value);
	}

	inline static int32_t get_offset_of_positions_10() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___positions_10)); }
	inline Positions_t3593914952 * get_positions_10() const { return ___positions_10; }
	inline Positions_t3593914952 ** get_address_of_positions_10() { return &___positions_10; }
	inline void set_positions_10(Positions_t3593914952 * value)
	{
		___positions_10 = value;
		Il2CppCodeGenWriteBarrier((&___positions_10), value);
	}

	inline static int32_t get_offset_of_endMarkerPos_11() { return static_cast<int32_t>(offsetof(NfaContentValidator_t1842871216, ___endMarkerPos_11)); }
	inline int32_t get_endMarkerPos_11() const { return ___endMarkerPos_11; }
	inline int32_t* get_address_of_endMarkerPos_11() { return &___endMarkerPos_11; }
	inline void set_endMarkerPos_11(int32_t value)
	{
		___endMarkerPos_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFACONTENTVALIDATOR_T1842871216_H
#ifndef PARTICLECONTENTVALIDATOR_T1341047977_H
#define PARTICLECONTENTVALIDATOR_T1341047977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ParticleContentValidator
struct  ParticleContentValidator_t1341047977  : public ContentValidator_t2510151843
{
public:
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.ParticleContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_7;
	// System.Xml.Schema.Positions System.Xml.Schema.ParticleContentValidator::positions
	Positions_t3593914952 * ___positions_8;
	// System.Collections.Stack System.Xml.Schema.ParticleContentValidator::stack
	Stack_t1043988394 * ___stack_9;
	// System.Xml.Schema.SyntaxTreeNode System.Xml.Schema.ParticleContentValidator::contentNode
	SyntaxTreeNode_t2397191729 * ___contentNode_10;
	// System.Boolean System.Xml.Schema.ParticleContentValidator::isPartial
	bool ___isPartial_11;
	// System.Int32 System.Xml.Schema.ParticleContentValidator::minMaxNodesCount
	int32_t ___minMaxNodesCount_12;
	// System.Boolean System.Xml.Schema.ParticleContentValidator::enableUpaCheck
	bool ___enableUpaCheck_13;

public:
	inline static int32_t get_offset_of_symbols_7() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___symbols_7)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_7() const { return ___symbols_7; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_7() { return &___symbols_7; }
	inline void set_symbols_7(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_7 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_7), value);
	}

	inline static int32_t get_offset_of_positions_8() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___positions_8)); }
	inline Positions_t3593914952 * get_positions_8() const { return ___positions_8; }
	inline Positions_t3593914952 ** get_address_of_positions_8() { return &___positions_8; }
	inline void set_positions_8(Positions_t3593914952 * value)
	{
		___positions_8 = value;
		Il2CppCodeGenWriteBarrier((&___positions_8), value);
	}

	inline static int32_t get_offset_of_stack_9() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___stack_9)); }
	inline Stack_t1043988394 * get_stack_9() const { return ___stack_9; }
	inline Stack_t1043988394 ** get_address_of_stack_9() { return &___stack_9; }
	inline void set_stack_9(Stack_t1043988394 * value)
	{
		___stack_9 = value;
		Il2CppCodeGenWriteBarrier((&___stack_9), value);
	}

	inline static int32_t get_offset_of_contentNode_10() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___contentNode_10)); }
	inline SyntaxTreeNode_t2397191729 * get_contentNode_10() const { return ___contentNode_10; }
	inline SyntaxTreeNode_t2397191729 ** get_address_of_contentNode_10() { return &___contentNode_10; }
	inline void set_contentNode_10(SyntaxTreeNode_t2397191729 * value)
	{
		___contentNode_10 = value;
		Il2CppCodeGenWriteBarrier((&___contentNode_10), value);
	}

	inline static int32_t get_offset_of_isPartial_11() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___isPartial_11)); }
	inline bool get_isPartial_11() const { return ___isPartial_11; }
	inline bool* get_address_of_isPartial_11() { return &___isPartial_11; }
	inline void set_isPartial_11(bool value)
	{
		___isPartial_11 = value;
	}

	inline static int32_t get_offset_of_minMaxNodesCount_12() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___minMaxNodesCount_12)); }
	inline int32_t get_minMaxNodesCount_12() const { return ___minMaxNodesCount_12; }
	inline int32_t* get_address_of_minMaxNodesCount_12() { return &___minMaxNodesCount_12; }
	inline void set_minMaxNodesCount_12(int32_t value)
	{
		___minMaxNodesCount_12 = value;
	}

	inline static int32_t get_offset_of_enableUpaCheck_13() { return static_cast<int32_t>(offsetof(ParticleContentValidator_t1341047977, ___enableUpaCheck_13)); }
	inline bool get_enableUpaCheck_13() const { return ___enableUpaCheck_13; }
	inline bool* get_address_of_enableUpaCheck_13() { return &___enableUpaCheck_13; }
	inline void set_enableUpaCheck_13(bool value)
	{
		___enableUpaCheck_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECONTENTVALIDATOR_T1341047977_H
#ifndef DFACONTENTVALIDATOR_T429624170_H
#define DFACONTENTVALIDATOR_T429624170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DfaContentValidator
struct  DfaContentValidator_t429624170  : public ContentValidator_t2510151843
{
public:
	// System.Int32[][] System.Xml.Schema.DfaContentValidator::transitionTable
	Int32U5BU5DU5BU5D_t3750818532* ___transitionTable_7;
	// System.Xml.Schema.SymbolsDictionary System.Xml.Schema.DfaContentValidator::symbols
	SymbolsDictionary_t1753655453 * ___symbols_8;

public:
	inline static int32_t get_offset_of_transitionTable_7() { return static_cast<int32_t>(offsetof(DfaContentValidator_t429624170, ___transitionTable_7)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get_transitionTable_7() const { return ___transitionTable_7; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of_transitionTable_7() { return &___transitionTable_7; }
	inline void set_transitionTable_7(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		___transitionTable_7 = value;
		Il2CppCodeGenWriteBarrier((&___transitionTable_7), value);
	}

	inline static int32_t get_offset_of_symbols_8() { return static_cast<int32_t>(offsetof(DfaContentValidator_t429624170, ___symbols_8)); }
	inline SymbolsDictionary_t1753655453 * get_symbols_8() const { return ___symbols_8; }
	inline SymbolsDictionary_t1753655453 ** get_address_of_symbols_8() { return &___symbols_8; }
	inline void set_symbols_8(SymbolsDictionary_t1753655453 * value)
	{
		___symbols_8 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DFACONTENTVALIDATOR_T429624170_H
#ifndef HASHCODEOFSTRINGDELEGATE_T3565421161_H
#define HASHCODEOFSTRINGDELEGATE_T3565421161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName/HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_t3565421161  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODEOFSTRINGDELEGATE_T3565421161_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (DtdParser_t821190747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[33] = 
{
	DtdParser_t821190747::get_offset_of_readerAdapter_0(),
	DtdParser_t821190747::get_offset_of_readerAdapterWithValidation_1(),
	DtdParser_t821190747::get_offset_of_nameTable_2(),
	DtdParser_t821190747::get_offset_of_schemaInfo_3(),
	DtdParser_t821190747::get_offset_of_xmlCharType_4(),
	DtdParser_t821190747::get_offset_of_systemId_5(),
	DtdParser_t821190747::get_offset_of_publicId_6(),
	DtdParser_t821190747::get_offset_of_normalize_7(),
	DtdParser_t821190747::get_offset_of_validate_8(),
	DtdParser_t821190747::get_offset_of_supportNamespaces_9(),
	DtdParser_t821190747::get_offset_of_v1Compat_10(),
	DtdParser_t821190747::get_offset_of_chars_11(),
	DtdParser_t821190747::get_offset_of_charsUsed_12(),
	DtdParser_t821190747::get_offset_of_curPos_13(),
	DtdParser_t821190747::get_offset_of_scanningFunction_14(),
	DtdParser_t821190747::get_offset_of_nextScaningFunction_15(),
	DtdParser_t821190747::get_offset_of_savedScanningFunction_16(),
	DtdParser_t821190747::get_offset_of_whitespaceSeen_17(),
	DtdParser_t821190747::get_offset_of_tokenStartPos_18(),
	DtdParser_t821190747::get_offset_of_colonPos_19(),
	DtdParser_t821190747::get_offset_of_internalSubsetValueSb_20(),
	DtdParser_t821190747::get_offset_of_externalEntitiesDepth_21(),
	DtdParser_t821190747::get_offset_of_currentEntityId_22(),
	DtdParser_t821190747::get_offset_of_freeFloatingDtd_23(),
	DtdParser_t821190747::get_offset_of_hasFreeFloatingInternalSubset_24(),
	DtdParser_t821190747::get_offset_of_stringBuilder_25(),
	DtdParser_t821190747::get_offset_of_condSectionDepth_26(),
	DtdParser_t821190747::get_offset_of_literalLineInfo_27(),
	DtdParser_t821190747::get_offset_of_literalQuoteChar_28(),
	DtdParser_t821190747::get_offset_of_documentBaseUri_29(),
	DtdParser_t821190747::get_offset_of_externalDtdBaseUri_30(),
	DtdParser_t821190747::get_offset_of_undeclaredNotations_31(),
	DtdParser_t821190747::get_offset_of_condSectionEntityIds_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (Token_t322298853)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1901[49] = 
{
	Token_t322298853::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (ScanningFunction_t2556434333)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[35] = 
{
	ScanningFunction_t2556434333::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (LiteralType_t3592345881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[4] = 
{
	LiteralType_t3592345881::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (UndeclaredNotation_t2066394897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[4] = 
{
	UndeclaredNotation_t2066394897::get_offset_of_name_0(),
	UndeclaredNotation_t2066394897::get_offset_of_lineNo_1(),
	UndeclaredNotation_t2066394897::get_offset_of_linePos_2(),
	UndeclaredNotation_t2066394897::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (ParseElementOnlyContent_LocalFrame_t225387055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[2] = 
{
	ParseElementOnlyContent_LocalFrame_t225387055::get_offset_of_startParenEntityId_0(),
	ParseElementOnlyContent_LocalFrame_t225387055::get_offset_of_parsingSchema_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (ValidateNames_t208250372), -1, sizeof(ValidateNames_t208250372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1907[1] = 
{
	ValidateNames_t208250372_StaticFields::get_offset_of_xmlCharType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (XmlCharType_t1050521405)+ sizeof (RuntimeObject), sizeof(XmlCharType_t1050521405_marshaled_pinvoke), sizeof(XmlCharType_t1050521405_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1908[3] = 
{
	XmlCharType_t1050521405_StaticFields::get_offset_of_s_Lock_0(),
	XmlCharType_t1050521405_StaticFields::get_offset_of_s_CharProperties_1(),
	XmlCharType_t1050521405::get_offset_of_charProperties_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (XmlComplianceUtil_t976628708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (ExceptionType_t1261777571)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[3] = 
{
	ExceptionType_t1261777571::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1911[4] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_xmlCharType_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_crt_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_c_EncodedCharLength_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_WhitespaceChars_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (XmlDownloadManager_t830495394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[1] = 
{
	XmlDownloadManager_t830495394::get_offset_of_connections_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (U3CU3Ec__DisplayClass0_0_t3246720517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[1] = 
{
	U3CU3Ec__DisplayClass0_0_t3246720517::get_offset_of_uri_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (U3CGetNonFileStreamAsyncU3Ed__1_t2196027765)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[9] = 
{
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_uri_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_credentials_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_proxy_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_cachePolicy_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_U3CreqU3E5__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_U3CU3E4__this_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetNonFileStreamAsyncU3Ed__1_t2196027765::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (OpenedHost_t3322155821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[1] = 
{
	OpenedHost_t3322155821::get_offset_of_nonCachedConnectionsCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (XmlRegisteredNonCachedStream_t1825156356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[3] = 
{
	XmlRegisteredNonCachedStream_t1825156356::get_offset_of_stream_4(),
	XmlRegisteredNonCachedStream_t1825156356::get_offset_of_downloadManager_5(),
	XmlRegisteredNonCachedStream_t1825156356::get_offset_of_host_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (XmlCachedStream_t235166513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[1] = 
{
	XmlCachedStream_t235166513::get_offset_of_uri_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (UTF16Decoder_t2333661544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[2] = 
{
	UTF16Decoder_t2333661544::get_offset_of_bigEndian_2(),
	UTF16Decoder_t2333661544::get_offset_of_lastByte_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (SafeAsciiDecoder_t3220120650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (Ucs4Encoding_t650080996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[1] = 
{
	Ucs4Encoding_t650080996::get_offset_of_ucs4Decoder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (Ucs4Encoding1234_t31992440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Ucs4Encoding4321_t616845456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (Ucs4Encoding2143_t576439440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (Ucs4Encoding3412_t2881835128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (Ucs4Decoder_t2645160027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[2] = 
{
	Ucs4Decoder_t2645160027::get_offset_of_lastBytes_2(),
	Ucs4Decoder_t2645160027::get_offset_of_lastBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Ucs4Decoder4321_t2511885999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (Ucs4Decoder1234_t522314591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Ucs4Decoder2143_t3957010607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (Ucs4Decoder3412_t3372157535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[6] = 
{
	XmlException_t4188277960::get_offset_of_res_16(),
	XmlException_t4188277960::get_offset_of_args_17(),
	XmlException_t4188277960::get_offset_of_lineNumber_18(),
	XmlException_t4188277960::get_offset_of_linePosition_19(),
	XmlException_t4188277960::get_offset_of_sourceUri_20(),
	XmlException_t4188277960::get_offset_of_message_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (XmlNamespaceManager_t486731501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[8] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_nsdecls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_lastDecl_1(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopeId_3(),
	XmlNamespaceManager_t486731501::get_offset_of_hashTable_4(),
	XmlNamespaceManager_t486731501::get_offset_of_useHashtable_5(),
	XmlNamespaceManager_t486731501::get_offset_of_xml_6(),
	XmlNamespaceManager_t486731501::get_offset_of_xmlNs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (NamespaceDeclaration_t3577631811)+ sizeof (RuntimeObject), sizeof(NamespaceDeclaration_t3577631811_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1932[4] = 
{
	NamespaceDeclaration_t3577631811::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NamespaceDeclaration_t3577631811::get_offset_of_uri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NamespaceDeclaration_t3577631811::get_offset_of_scopeId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NamespaceDeclaration_t3577631811::get_offset_of_previousNsIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (XmlNamespaceScope_t1384947590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1933[4] = 
{
	XmlNamespaceScope_t1384947590::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (XmlNodeType_t739504597)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1935[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1936[5] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_hashCodeDelegate_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (HashCodeOfStringDelegate_t3565421161), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (XmlUrlResolver_t896669594), -1, sizeof(XmlUrlResolver_t896669594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1939[4] = 
{
	XmlUrlResolver_t896669594_StaticFields::get_offset_of_s_DownloadManager_0(),
	XmlUrlResolver_t896669594::get_offset_of__credentials_1(),
	XmlUrlResolver_t896669594::get_offset_of__proxy_2(),
	XmlUrlResolver_t896669594::get_offset_of__cachePolicy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (U3CGetEntityAsyncU3Ed__0_t385453514)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[6] = 
{
	U3CGetEntityAsyncU3Ed__0_t385453514::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__0_t385453514::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__0_t385453514::get_offset_of_ofObjectToReturn_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__0_t385453514::get_offset_of_absoluteUri_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__0_t385453514::get_offset_of_U3CU3E4__this_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetEntityAsyncU3Ed__0_t385453514::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (XPathDocument_t1328191420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[4] = 
{
	XPathDocument_t1328191420::get_offset_of_pageXmlNmsp_0(),
	XPathDocument_t1328191420::get_offset_of_idxXmlNmsp_1(),
	XPathDocument_t1328191420::get_offset_of_nameTable_2(),
	XPathDocument_t1328191420::get_offset_of_mapNmsp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (XPathException_t1503722168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[3] = 
{
	XPathException_t1503722168::get_offset_of_res_16(),
	XPathException_t1503722168::get_offset_of_args_17(),
	XPathException_t1503722168::get_offset_of_message_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (XPathResultType_t1521569578)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1944[8] = 
{
	XPathResultType_t1521569578::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (XPathItem_t3130801258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (XPathNamespaceScope_t3601604274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1946[4] = 
{
	XPathNamespaceScope_t3601604274::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (XPathNavigator_t3981235968), -1, sizeof(XPathNavigator_t3981235968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1947[4] = 
{
	XPathNavigator_t3981235968_StaticFields::get_offset_of_comparer_0(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_NodeTypeLetter_1(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_UniqueIdTbl_2(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_ContentKindMasks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (XPathNavigatorKeyComparer_t3055722314), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (XPathNodeType_t817388867)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[11] = 
{
	XPathNodeType_t817388867::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (XmlAnyAttributeAttribute_t713947513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (XmlAnyElementAttribute_t2502375235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[1] = 
{
	XmlAnyElementAttribute_t2502375235::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[2] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t850813783::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (XmlNamespaceDeclarationsAttribute_t2069522403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (XmlRootAttribute_t3527426713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[3] = 
{
	XmlRootAttribute_t3527426713::get_offset_of_elementName_0(),
	XmlRootAttribute_t3527426713::get_offset_of_ns_1(),
	XmlRootAttribute_t3527426713::get_offset_of_nullable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (XmlTextAttribute_t3321178844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (AxisElement_t941837363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[4] = 
{
	AxisElement_t941837363::get_offset_of_curNode_0(),
	AxisElement_t941837363::get_offset_of_rootDepth_1(),
	AxisElement_t941837363::get_offset_of_curDepth_2(),
	AxisElement_t941837363::get_offset_of_isMatch_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (AxisStack_t3388994403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[3] = 
{
	AxisStack_t3388994403::get_offset_of_stack_0(),
	AxisStack_t3388994403::get_offset_of_subtree_1(),
	AxisStack_t3388994403::get_offset_of_parent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (ActiveAxis_t439376929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[4] = 
{
	ActiveAxis_t439376929::get_offset_of_currentDepth_0(),
	ActiveAxis_t439376929::get_offset_of_isActive_1(),
	ActiveAxis_t439376929::get_offset_of_axisTree_2(),
	ActiveAxis_t439376929::get_offset_of_axisStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (DoubleLinkAxis_t3164907012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[1] = 
{
	DoubleLinkAxis_t3164907012::get_offset_of_next_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (ForwardAxis_t3876904870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[5] = 
{
	ForwardAxis_t3876904870::get_offset_of_topNode_0(),
	ForwardAxis_t3876904870::get_offset_of_rootNode_1(),
	ForwardAxis_t3876904870::get_offset_of_isAttribute_2(),
	ForwardAxis_t3876904870::get_offset_of_isDss_3(),
	ForwardAxis_t3876904870::get_offset_of_isSelfAxis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (Asttree_t3451058494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[4] = 
{
	Asttree_t3451058494::get_offset_of_fAxisArray_0(),
	Asttree_t3451058494::get_offset_of_xpathexpr_1(),
	Asttree_t3451058494::get_offset_of_isField_2(),
	Asttree_t3451058494::get_offset_of_nsmgr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (AutoValidator_t190363951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (BaseProcessor_t2373158431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[6] = 
{
	BaseProcessor_t2373158431::get_offset_of_nameTable_0(),
	BaseProcessor_t2373158431::get_offset_of_schemaNames_1(),
	BaseProcessor_t2373158431::get_offset_of_eventHandler_2(),
	BaseProcessor_t2373158431::get_offset_of_compilationSettings_3(),
	BaseProcessor_t2373158431::get_offset_of_errorCount_4(),
	BaseProcessor_t2373158431::get_offset_of_NsXml_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (BaseValidator_t3557140249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[15] = 
{
	BaseValidator_t3557140249::get_offset_of_schemaCollection_0(),
	BaseValidator_t3557140249::get_offset_of_eventHandling_1(),
	BaseValidator_t3557140249::get_offset_of_nameTable_2(),
	BaseValidator_t3557140249::get_offset_of_schemaNames_3(),
	BaseValidator_t3557140249::get_offset_of_positionInfo_4(),
	BaseValidator_t3557140249::get_offset_of_xmlResolver_5(),
	BaseValidator_t3557140249::get_offset_of_baseUri_6(),
	BaseValidator_t3557140249::get_offset_of_schemaInfo_7(),
	BaseValidator_t3557140249::get_offset_of_reader_8(),
	BaseValidator_t3557140249::get_offset_of_elementName_9(),
	BaseValidator_t3557140249::get_offset_of_context_10(),
	BaseValidator_t3557140249::get_offset_of_textValue_11(),
	BaseValidator_t3557140249::get_offset_of_textString_12(),
	BaseValidator_t3557140249::get_offset_of_hasSibling_13(),
	BaseValidator_t3557140249::get_offset_of_checkDatatype_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (BitSet_t1062448123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[2] = 
{
	BitSet_t1062448123::get_offset_of_count_0(),
	BitSet_t1062448123::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (CompiledIdentityConstraint_t964629540), -1, sizeof(CompiledIdentityConstraint_t964629540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1970[6] = 
{
	CompiledIdentityConstraint_t964629540::get_offset_of_name_0(),
	CompiledIdentityConstraint_t964629540::get_offset_of_role_1(),
	CompiledIdentityConstraint_t964629540::get_offset_of_selector_2(),
	CompiledIdentityConstraint_t964629540::get_offset_of_fields_3(),
	CompiledIdentityConstraint_t964629540::get_offset_of_refer_4(),
	CompiledIdentityConstraint_t964629540_StaticFields::get_offset_of_Empty_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (ConstraintRole_t3811561833)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	ConstraintRole_t3811561833::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (ConstraintStruct_t2462842120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[6] = 
{
	ConstraintStruct_t2462842120::get_offset_of_constraint_0(),
	ConstraintStruct_t2462842120::get_offset_of_axisSelector_1(),
	ConstraintStruct_t2462842120::get_offset_of_axisFields_2(),
	ConstraintStruct_t2462842120::get_offset_of_qualifiedTable_3(),
	ConstraintStruct_t2462842120::get_offset_of_keyrefTable_4(),
	ConstraintStruct_t2462842120::get_offset_of_tableDim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (LocatedActiveAxis_t90453917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[3] = 
{
	LocatedActiveAxis_t90453917::get_offset_of_column_4(),
	LocatedActiveAxis_t90453917::get_offset_of_isMatched_5(),
	LocatedActiveAxis_t90453917::get_offset_of_Ks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (SelectorActiveAxis_t789423304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[3] = 
{
	SelectorActiveAxis_t789423304::get_offset_of_cs_4(),
	SelectorActiveAxis_t789423304::get_offset_of_KSs_5(),
	SelectorActiveAxis_t789423304::get_offset_of_KSpointer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (KSStruct_t704598349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[3] = 
{
	KSStruct_t704598349::get_offset_of_depth_0(),
	KSStruct_t704598349::get_offset_of_ks_1(),
	KSStruct_t704598349::get_offset_of_fields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (TypedObject_t1797374135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[6] = 
{
	TypedObject_t1797374135::get_offset_of_dstruct_0(),
	TypedObject_t1797374135::get_offset_of_ovalue_1(),
	TypedObject_t1797374135::get_offset_of_svalue_2(),
	TypedObject_t1797374135::get_offset_of_xsdtype_3(),
	TypedObject_t1797374135::get_offset_of_dim_4(),
	TypedObject_t1797374135::get_offset_of_isList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (DecimalStruct_t715828147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[2] = 
{
	DecimalStruct_t715828147::get_offset_of_isDecimal_0(),
	DecimalStruct_t715828147::get_offset_of_dvalue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (KeySequence_t746093258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[5] = 
{
	KeySequence_t746093258::get_offset_of_ks_0(),
	KeySequence_t746093258::get_offset_of_dim_1(),
	KeySequence_t746093258::get_offset_of_hashcode_2(),
	KeySequence_t746093258::get_offset_of_posline_3(),
	KeySequence_t746093258::get_offset_of_poscol_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (UpaException_t656169215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[2] = 
{
	UpaException_t656169215::get_offset_of_particle1_16(),
	UpaException_t656169215::get_offset_of_particle2_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (SymbolsDictionary_t1753655453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[6] = 
{
	SymbolsDictionary_t1753655453::get_offset_of_last_0(),
	SymbolsDictionary_t1753655453::get_offset_of_names_1(),
	SymbolsDictionary_t1753655453::get_offset_of_wildcards_2(),
	SymbolsDictionary_t1753655453::get_offset_of_particles_3(),
	SymbolsDictionary_t1753655453::get_offset_of_particleLast_4(),
	SymbolsDictionary_t1753655453::get_offset_of_isUpaEnforced_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (Position_t1796812729)+ sizeof (RuntimeObject), sizeof(Position_t1796812729_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[2] = 
{
	Position_t1796812729::get_offset_of_symbol_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Position_t1796812729::get_offset_of_particle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (Positions_t3593914952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[1] = 
{
	Positions_t3593914952::get_offset_of_positions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (SyntaxTreeNode_t2397191729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (LeafNode_t3748718316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[1] = 
{
	LeafNode_t3748718316::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (NamespaceListNode_t2509262495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[2] = 
{
	NamespaceListNode_t2509262495::get_offset_of_namespaceList_0(),
	NamespaceListNode_t2509262495::get_offset_of_particle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (InteriorNode_t2716368958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[2] = 
{
	InteriorNode_t2716368958::get_offset_of_leftChild_0(),
	InteriorNode_t2716368958::get_offset_of_rightChild_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (SequenceNode_t4039907291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (SequenceConstructPosContext_t3853454650)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[5] = 
{
	SequenceConstructPosContext_t3853454650::get_offset_of_this__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_firstpos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_lastpos_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_lastposLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SequenceConstructPosContext_t3853454650::get_offset_of_firstposRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (ChoiceNode_t3123692209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (PlusNode_t3494526928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (QmarkNode_t902109702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (StarNode_t2416964522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (LeafRangeNode_t2572019409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[3] = 
{
	LeafRangeNode_t2572019409::get_offset_of_min_1(),
	LeafRangeNode_t2572019409::get_offset_of_max_2(),
	LeafRangeNode_t2572019409::get_offset_of_nextIteration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (ContentValidator_t2510151843), -1, sizeof(ContentValidator_t2510151843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1994[7] = 
{
	ContentValidator_t2510151843::get_offset_of_contentType_0(),
	ContentValidator_t2510151843::get_offset_of_isOpen_1(),
	ContentValidator_t2510151843::get_offset_of_isEmptiable_2(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_Empty_3(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_TextOnly_4(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_Mixed_5(),
	ContentValidator_t2510151843_StaticFields::get_offset_of_Any_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (ParticleContentValidator_t1341047977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[7] = 
{
	ParticleContentValidator_t1341047977::get_offset_of_symbols_7(),
	ParticleContentValidator_t1341047977::get_offset_of_positions_8(),
	ParticleContentValidator_t1341047977::get_offset_of_stack_9(),
	ParticleContentValidator_t1341047977::get_offset_of_contentNode_10(),
	ParticleContentValidator_t1341047977::get_offset_of_isPartial_11(),
	ParticleContentValidator_t1341047977::get_offset_of_minMaxNodesCount_12(),
	ParticleContentValidator_t1341047977::get_offset_of_enableUpaCheck_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (DfaContentValidator_t429624170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[2] = 
{
	DfaContentValidator_t429624170::get_offset_of_transitionTable_7(),
	DfaContentValidator_t429624170::get_offset_of_symbols_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (NfaContentValidator_t1842871216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[5] = 
{
	NfaContentValidator_t1842871216::get_offset_of_firstpos_7(),
	NfaContentValidator_t1842871216::get_offset_of_followpos_8(),
	NfaContentValidator_t1842871216::get_offset_of_symbols_9(),
	NfaContentValidator_t1842871216::get_offset_of_positions_10(),
	NfaContentValidator_t1842871216::get_offset_of_endMarkerPos_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (RangePositionInfo_t2780802922)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[2] = 
{
	RangePositionInfo_t2780802922::get_offset_of_curpos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RangePositionInfo_t2780802922::get_offset_of_rangeCounters_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (RangeContentValidator_t857885766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[7] = 
{
	RangeContentValidator_t857885766::get_offset_of_firstpos_7(),
	RangeContentValidator_t857885766::get_offset_of_followpos_8(),
	RangeContentValidator_t857885766::get_offset_of_positionsWithRangeTerminals_9(),
	RangeContentValidator_t857885766::get_offset_of_symbols_10(),
	RangeContentValidator_t857885766::get_offset_of_positions_11(),
	RangeContentValidator_t857885766::get_offset_of_minMaxNodesCount_12(),
	RangeContentValidator_t857885766::get_offset_of_endMarkerPos_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
