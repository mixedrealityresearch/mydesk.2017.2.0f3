﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Security.Cryptography.OidCollection
struct OidCollection_t3790243618;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange>
struct List_1_t3163364420;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String[0...,0...]
struct StringU5B0___U2C0___U5D_t1642385973;
// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping[]
struct LowerCaseMappingU5BU5D_t882374823;
// System.Security.Cryptography.Oid
struct Oid_t3221867120;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Text.RegularExpressions.RegexPrefix
struct RegexPrefix_t1013837165;
// System.Text.RegularExpressions.RegexBoyerMoore
struct RegexBoyerMoore_t2204811018;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Security.Cryptography.X509Certificates.X509ChainImpl
struct X509ChainImpl_t2968295413;
// System.Collections.Generic.List`1<System.Security.Cryptography.X509Certificates.X509CertificateImpl>
struct List_1_t3211185839;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t784058677;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t463456204;
// System.WeakReference
struct WeakReference_t1077405567;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.Text.RegularExpressions.RegexRunner
struct RegexRunner_t3983612747;
// System.Text.RegularExpressions.RegexCode
struct RegexCode_t2469392150;
// System.Text.RegularExpressions.RegexRunnerFactory
struct RegexRunnerFactory_t3902733837;
// System.Text.RegularExpressions.ExclusiveReference
struct ExclusiveReference_t708182869;
// System.Text.RegularExpressions.SharedReference
struct SharedReference_t2137668360;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t1975884510;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t2217612696;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Globalization.CompareInfo
struct CompareInfo_t2310920157;
// System.Security.Cryptography.X509Certificates.X509CertificateImpl
struct X509CertificateImpl_t3842064707;
// System.Uri
struct Uri_t19570940;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Uri/MoreInfo
struct MoreInfo_t2595315311;
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct X509ExtensionCollection_t650873211;
// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t870392;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct X500DistinguishedName_t452415348;
// System.Security.Cryptography.X509Certificates.X509CertificateImplCollection
struct X509CertificateImplCollection_t255811311;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t324051958;
// System.Collections.Generic.Dictionary`2<System.String,System.UriParser>
struct Dictionary_2_t2927290585;
// System.UriParser
struct UriParser_t1012511323;
// System.Uri/UriInfo
struct UriInfo_t4047916940;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t830390908;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t1108969367;
// Mono.Security.X509.X509Store
struct X509Store_t4028973564;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct X509ChainElementCollection_t2081831987;
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct X509ChainPolicy_t3452126517;
// System.Security.Cryptography.X509Certificates.X509ChainElement
struct X509ChainElement_t528874471;
// System.Security.Cryptography.X509Certificates.X509Store
struct X509Store_t1617430119;
// System.IOAsyncCallback
struct IOAsyncCallback_t2427139621;
// System.IOAsyncResult
struct IOAsyncResult_t1276329107;
// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry>
struct LinkedList_1_t3858529280;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

struct Exception_t1927440687_marshaled_pinvoke;
struct Exception_t1927440687_marshaled_com;
struct IOAsyncResult_t1276329107_marshaled_pinvoke;
struct IOAsyncResult_t1276329107_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OIDENUMERATOR_T3674631724_H
#define OIDENUMERATOR_T3674631724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidEnumerator
struct  OidEnumerator_t3674631724  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::m_oids
	OidCollection_t3790243618 * ___m_oids_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::m_current
	int32_t ___m_current_1;

public:
	inline static int32_t get_offset_of_m_oids_0() { return static_cast<int32_t>(offsetof(OidEnumerator_t3674631724, ___m_oids_0)); }
	inline OidCollection_t3790243618 * get_m_oids_0() const { return ___m_oids_0; }
	inline OidCollection_t3790243618 ** get_address_of_m_oids_0() { return &___m_oids_0; }
	inline void set_m_oids_0(OidCollection_t3790243618 * value)
	{
		___m_oids_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oids_0), value);
	}

	inline static int32_t get_offset_of_m_current_1() { return static_cast<int32_t>(offsetof(OidEnumerator_t3674631724, ___m_current_1)); }
	inline int32_t get_m_current_1() const { return ___m_current_1; }
	inline int32_t* get_address_of_m_current_1() { return &___m_current_1; }
	inline void set_m_current_1(int32_t value)
	{
		___m_current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDENUMERATOR_T3674631724_H
#ifndef OIDCOLLECTION_T3790243618_H
#define OIDCOLLECTION_T3790243618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidCollection
struct  OidCollection_t3790243618  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.OidCollection::m_list
	ArrayList_t4252133567 * ___m_list_0;

public:
	inline static int32_t get_offset_of_m_list_0() { return static_cast<int32_t>(offsetof(OidCollection_t3790243618, ___m_list_0)); }
	inline ArrayList_t4252133567 * get_m_list_0() const { return ___m_list_0; }
	inline ArrayList_t4252133567 ** get_address_of_m_list_0() { return &___m_list_0; }
	inline void set_m_list_0(ArrayList_t4252133567 * value)
	{
		___m_list_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDCOLLECTION_T3790243618_H
#ifndef REGEXCHARCLASS_T2441867401_H
#define REGEXCHARCLASS_T2441867401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass
struct  RegexCharClass_t2441867401  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Text.RegularExpressions.RegexCharClass/SingleRange> System.Text.RegularExpressions.RegexCharClass::_rangelist
	List_1_t3163364420 * ____rangelist_0;
	// System.Text.StringBuilder System.Text.RegularExpressions.RegexCharClass::_categories
	StringBuilder_t1221177846 * ____categories_1;
	// System.Boolean System.Text.RegularExpressions.RegexCharClass::_canonical
	bool ____canonical_2;
	// System.Boolean System.Text.RegularExpressions.RegexCharClass::_negate
	bool ____negate_3;
	// System.Text.RegularExpressions.RegexCharClass System.Text.RegularExpressions.RegexCharClass::_subtractor
	RegexCharClass_t2441867401 * ____subtractor_4;

public:
	inline static int32_t get_offset_of__rangelist_0() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____rangelist_0)); }
	inline List_1_t3163364420 * get__rangelist_0() const { return ____rangelist_0; }
	inline List_1_t3163364420 ** get_address_of__rangelist_0() { return &____rangelist_0; }
	inline void set__rangelist_0(List_1_t3163364420 * value)
	{
		____rangelist_0 = value;
		Il2CppCodeGenWriteBarrier((&____rangelist_0), value);
	}

	inline static int32_t get_offset_of__categories_1() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____categories_1)); }
	inline StringBuilder_t1221177846 * get__categories_1() const { return ____categories_1; }
	inline StringBuilder_t1221177846 ** get_address_of__categories_1() { return &____categories_1; }
	inline void set__categories_1(StringBuilder_t1221177846 * value)
	{
		____categories_1 = value;
		Il2CppCodeGenWriteBarrier((&____categories_1), value);
	}

	inline static int32_t get_offset_of__canonical_2() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____canonical_2)); }
	inline bool get__canonical_2() const { return ____canonical_2; }
	inline bool* get_address_of__canonical_2() { return &____canonical_2; }
	inline void set__canonical_2(bool value)
	{
		____canonical_2 = value;
	}

	inline static int32_t get_offset_of__negate_3() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____negate_3)); }
	inline bool get__negate_3() const { return ____negate_3; }
	inline bool* get_address_of__negate_3() { return &____negate_3; }
	inline void set__negate_3(bool value)
	{
		____negate_3 = value;
	}

	inline static int32_t get_offset_of__subtractor_4() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401, ____subtractor_4)); }
	inline RegexCharClass_t2441867401 * get__subtractor_4() const { return ____subtractor_4; }
	inline RegexCharClass_t2441867401 ** get_address_of__subtractor_4() { return &____subtractor_4; }
	inline void set__subtractor_4(RegexCharClass_t2441867401 * value)
	{
		____subtractor_4 = value;
		Il2CppCodeGenWriteBarrier((&____subtractor_4), value);
	}
};

struct RegexCharClass_t2441867401_StaticFields
{
public:
	// System.String System.Text.RegularExpressions.RegexCharClass::InternalRegexIgnoreCase
	String_t* ___InternalRegexIgnoreCase_5;
	// System.String System.Text.RegularExpressions.RegexCharClass::Space
	String_t* ___Space_6;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotSpace
	String_t* ___NotSpace_7;
	// System.String System.Text.RegularExpressions.RegexCharClass::Word
	String_t* ___Word_8;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotWord
	String_t* ___NotWord_9;
	// System.String System.Text.RegularExpressions.RegexCharClass::SpaceClass
	String_t* ___SpaceClass_10;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotSpaceClass
	String_t* ___NotSpaceClass_11;
	// System.String System.Text.RegularExpressions.RegexCharClass::WordClass
	String_t* ___WordClass_12;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotWordClass
	String_t* ___NotWordClass_13;
	// System.String System.Text.RegularExpressions.RegexCharClass::DigitClass
	String_t* ___DigitClass_14;
	// System.String System.Text.RegularExpressions.RegexCharClass::NotDigitClass
	String_t* ___NotDigitClass_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> System.Text.RegularExpressions.RegexCharClass::_definedCategories
	Dictionary_2_t3943999495 * ____definedCategories_16;
	// System.String[0...,0...] System.Text.RegularExpressions.RegexCharClass::_propTable
	StringU5B0___U2C0___U5D_t1642385973* ____propTable_17;
	// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping[] System.Text.RegularExpressions.RegexCharClass::_lcTable
	LowerCaseMappingU5BU5D_t882374823* ____lcTable_18;

public:
	inline static int32_t get_offset_of_InternalRegexIgnoreCase_5() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___InternalRegexIgnoreCase_5)); }
	inline String_t* get_InternalRegexIgnoreCase_5() const { return ___InternalRegexIgnoreCase_5; }
	inline String_t** get_address_of_InternalRegexIgnoreCase_5() { return &___InternalRegexIgnoreCase_5; }
	inline void set_InternalRegexIgnoreCase_5(String_t* value)
	{
		___InternalRegexIgnoreCase_5 = value;
		Il2CppCodeGenWriteBarrier((&___InternalRegexIgnoreCase_5), value);
	}

	inline static int32_t get_offset_of_Space_6() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___Space_6)); }
	inline String_t* get_Space_6() const { return ___Space_6; }
	inline String_t** get_address_of_Space_6() { return &___Space_6; }
	inline void set_Space_6(String_t* value)
	{
		___Space_6 = value;
		Il2CppCodeGenWriteBarrier((&___Space_6), value);
	}

	inline static int32_t get_offset_of_NotSpace_7() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotSpace_7)); }
	inline String_t* get_NotSpace_7() const { return ___NotSpace_7; }
	inline String_t** get_address_of_NotSpace_7() { return &___NotSpace_7; }
	inline void set_NotSpace_7(String_t* value)
	{
		___NotSpace_7 = value;
		Il2CppCodeGenWriteBarrier((&___NotSpace_7), value);
	}

	inline static int32_t get_offset_of_Word_8() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___Word_8)); }
	inline String_t* get_Word_8() const { return ___Word_8; }
	inline String_t** get_address_of_Word_8() { return &___Word_8; }
	inline void set_Word_8(String_t* value)
	{
		___Word_8 = value;
		Il2CppCodeGenWriteBarrier((&___Word_8), value);
	}

	inline static int32_t get_offset_of_NotWord_9() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotWord_9)); }
	inline String_t* get_NotWord_9() const { return ___NotWord_9; }
	inline String_t** get_address_of_NotWord_9() { return &___NotWord_9; }
	inline void set_NotWord_9(String_t* value)
	{
		___NotWord_9 = value;
		Il2CppCodeGenWriteBarrier((&___NotWord_9), value);
	}

	inline static int32_t get_offset_of_SpaceClass_10() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___SpaceClass_10)); }
	inline String_t* get_SpaceClass_10() const { return ___SpaceClass_10; }
	inline String_t** get_address_of_SpaceClass_10() { return &___SpaceClass_10; }
	inline void set_SpaceClass_10(String_t* value)
	{
		___SpaceClass_10 = value;
		Il2CppCodeGenWriteBarrier((&___SpaceClass_10), value);
	}

	inline static int32_t get_offset_of_NotSpaceClass_11() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotSpaceClass_11)); }
	inline String_t* get_NotSpaceClass_11() const { return ___NotSpaceClass_11; }
	inline String_t** get_address_of_NotSpaceClass_11() { return &___NotSpaceClass_11; }
	inline void set_NotSpaceClass_11(String_t* value)
	{
		___NotSpaceClass_11 = value;
		Il2CppCodeGenWriteBarrier((&___NotSpaceClass_11), value);
	}

	inline static int32_t get_offset_of_WordClass_12() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___WordClass_12)); }
	inline String_t* get_WordClass_12() const { return ___WordClass_12; }
	inline String_t** get_address_of_WordClass_12() { return &___WordClass_12; }
	inline void set_WordClass_12(String_t* value)
	{
		___WordClass_12 = value;
		Il2CppCodeGenWriteBarrier((&___WordClass_12), value);
	}

	inline static int32_t get_offset_of_NotWordClass_13() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotWordClass_13)); }
	inline String_t* get_NotWordClass_13() const { return ___NotWordClass_13; }
	inline String_t** get_address_of_NotWordClass_13() { return &___NotWordClass_13; }
	inline void set_NotWordClass_13(String_t* value)
	{
		___NotWordClass_13 = value;
		Il2CppCodeGenWriteBarrier((&___NotWordClass_13), value);
	}

	inline static int32_t get_offset_of_DigitClass_14() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___DigitClass_14)); }
	inline String_t* get_DigitClass_14() const { return ___DigitClass_14; }
	inline String_t** get_address_of_DigitClass_14() { return &___DigitClass_14; }
	inline void set_DigitClass_14(String_t* value)
	{
		___DigitClass_14 = value;
		Il2CppCodeGenWriteBarrier((&___DigitClass_14), value);
	}

	inline static int32_t get_offset_of_NotDigitClass_15() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ___NotDigitClass_15)); }
	inline String_t* get_NotDigitClass_15() const { return ___NotDigitClass_15; }
	inline String_t** get_address_of_NotDigitClass_15() { return &___NotDigitClass_15; }
	inline void set_NotDigitClass_15(String_t* value)
	{
		___NotDigitClass_15 = value;
		Il2CppCodeGenWriteBarrier((&___NotDigitClass_15), value);
	}

	inline static int32_t get_offset_of__definedCategories_16() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ____definedCategories_16)); }
	inline Dictionary_2_t3943999495 * get__definedCategories_16() const { return ____definedCategories_16; }
	inline Dictionary_2_t3943999495 ** get_address_of__definedCategories_16() { return &____definedCategories_16; }
	inline void set__definedCategories_16(Dictionary_2_t3943999495 * value)
	{
		____definedCategories_16 = value;
		Il2CppCodeGenWriteBarrier((&____definedCategories_16), value);
	}

	inline static int32_t get_offset_of__propTable_17() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ____propTable_17)); }
	inline StringU5B0___U2C0___U5D_t1642385973* get__propTable_17() const { return ____propTable_17; }
	inline StringU5B0___U2C0___U5D_t1642385973** get_address_of__propTable_17() { return &____propTable_17; }
	inline void set__propTable_17(StringU5B0___U2C0___U5D_t1642385973* value)
	{
		____propTable_17 = value;
		Il2CppCodeGenWriteBarrier((&____propTable_17), value);
	}

	inline static int32_t get_offset_of__lcTable_18() { return static_cast<int32_t>(offsetof(RegexCharClass_t2441867401_StaticFields, ____lcTable_18)); }
	inline LowerCaseMappingU5BU5D_t882374823* get__lcTable_18() const { return ____lcTable_18; }
	inline LowerCaseMappingU5BU5D_t882374823** get_address_of__lcTable_18() { return &____lcTable_18; }
	inline void set__lcTable_18(LowerCaseMappingU5BU5D_t882374823* value)
	{
		____lcTable_18 = value;
		Il2CppCodeGenWriteBarrier((&____lcTable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCHARCLASS_T2441867401_H
#ifndef CAPI_T3094615495_H
#define CAPI_T3094615495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CAPI
struct  CAPI_t3094615495  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPI_T3094615495_H
#ifndef X509UTILS_T810863315_H
#define X509UTILS_T810863315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Utils
struct  X509Utils_t810863315  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509UTILS_T810863315_H
#ifndef ASNENCODEDDATA_T463456204_H
#define ASNENCODEDDATA_T463456204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t463456204  : public RuntimeObject
{
public:
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t3221867120 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t3397334013* ____raw_1;

public:
	inline static int32_t get_offset_of__oid_0() { return static_cast<int32_t>(offsetof(AsnEncodedData_t463456204, ____oid_0)); }
	inline Oid_t3221867120 * get__oid_0() const { return ____oid_0; }
	inline Oid_t3221867120 ** get_address_of__oid_0() { return &____oid_0; }
	inline void set__oid_0(Oid_t3221867120 * value)
	{
		____oid_0 = value;
		Il2CppCodeGenWriteBarrier((&____oid_0), value);
	}

	inline static int32_t get_offset_of__raw_1() { return static_cast<int32_t>(offsetof(AsnEncodedData_t463456204, ____raw_1)); }
	inline ByteU5BU5D_t3397334013* get__raw_1() const { return ____raw_1; }
	inline ByteU5BU5D_t3397334013** get_address_of__raw_1() { return &____raw_1; }
	inline void set__raw_1(ByteU5BU5D_t3397334013* value)
	{
		____raw_1 = value;
		Il2CppCodeGenWriteBarrier((&____raw_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNENCODEDDATA_T463456204_H
#ifndef CAPTURE_T4157900610_H
#define CAPTURE_T4157900610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t4157900610  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Capture::_text
	String_t* ____text_0;
	// System.Int32 System.Text.RegularExpressions.Capture::_index
	int32_t ____index_1;
	// System.Int32 System.Text.RegularExpressions.Capture::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__text_0() { return static_cast<int32_t>(offsetof(Capture_t4157900610, ____text_0)); }
	inline String_t* get__text_0() const { return ____text_0; }
	inline String_t** get_address_of__text_0() { return &____text_0; }
	inline void set__text_0(String_t* value)
	{
		____text_0 = value;
		Il2CppCodeGenWriteBarrier((&____text_0), value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Capture_t4157900610, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(Capture_t4157900610, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T4157900610_H
#ifndef REGEXCODE_T2469392150_H
#define REGEXCODE_T2469392150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCode
struct  RegexCode_t2469392150  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexCode::_codes
	Int32U5BU5D_t3030399641* ____codes_0;
	// System.String[] System.Text.RegularExpressions.RegexCode::_strings
	StringU5BU5D_t1642385972* ____strings_1;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_trackcount
	int32_t ____trackcount_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.RegexCode::_caps
	Hashtable_t909839986 * ____caps_3;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_capsize
	int32_t ____capsize_4;
	// System.Text.RegularExpressions.RegexPrefix System.Text.RegularExpressions.RegexCode::_fcPrefix
	RegexPrefix_t1013837165 * ____fcPrefix_5;
	// System.Text.RegularExpressions.RegexBoyerMoore System.Text.RegularExpressions.RegexCode::_bmPrefix
	RegexBoyerMoore_t2204811018 * ____bmPrefix_6;
	// System.Int32 System.Text.RegularExpressions.RegexCode::_anchors
	int32_t ____anchors_7;
	// System.Boolean System.Text.RegularExpressions.RegexCode::_rightToLeft
	bool ____rightToLeft_8;

public:
	inline static int32_t get_offset_of__codes_0() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____codes_0)); }
	inline Int32U5BU5D_t3030399641* get__codes_0() const { return ____codes_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__codes_0() { return &____codes_0; }
	inline void set__codes_0(Int32U5BU5D_t3030399641* value)
	{
		____codes_0 = value;
		Il2CppCodeGenWriteBarrier((&____codes_0), value);
	}

	inline static int32_t get_offset_of__strings_1() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____strings_1)); }
	inline StringU5BU5D_t1642385972* get__strings_1() const { return ____strings_1; }
	inline StringU5BU5D_t1642385972** get_address_of__strings_1() { return &____strings_1; }
	inline void set__strings_1(StringU5BU5D_t1642385972* value)
	{
		____strings_1 = value;
		Il2CppCodeGenWriteBarrier((&____strings_1), value);
	}

	inline static int32_t get_offset_of__trackcount_2() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____trackcount_2)); }
	inline int32_t get__trackcount_2() const { return ____trackcount_2; }
	inline int32_t* get_address_of__trackcount_2() { return &____trackcount_2; }
	inline void set__trackcount_2(int32_t value)
	{
		____trackcount_2 = value;
	}

	inline static int32_t get_offset_of__caps_3() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____caps_3)); }
	inline Hashtable_t909839986 * get__caps_3() const { return ____caps_3; }
	inline Hashtable_t909839986 ** get_address_of__caps_3() { return &____caps_3; }
	inline void set__caps_3(Hashtable_t909839986 * value)
	{
		____caps_3 = value;
		Il2CppCodeGenWriteBarrier((&____caps_3), value);
	}

	inline static int32_t get_offset_of__capsize_4() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____capsize_4)); }
	inline int32_t get__capsize_4() const { return ____capsize_4; }
	inline int32_t* get_address_of__capsize_4() { return &____capsize_4; }
	inline void set__capsize_4(int32_t value)
	{
		____capsize_4 = value;
	}

	inline static int32_t get_offset_of__fcPrefix_5() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____fcPrefix_5)); }
	inline RegexPrefix_t1013837165 * get__fcPrefix_5() const { return ____fcPrefix_5; }
	inline RegexPrefix_t1013837165 ** get_address_of__fcPrefix_5() { return &____fcPrefix_5; }
	inline void set__fcPrefix_5(RegexPrefix_t1013837165 * value)
	{
		____fcPrefix_5 = value;
		Il2CppCodeGenWriteBarrier((&____fcPrefix_5), value);
	}

	inline static int32_t get_offset_of__bmPrefix_6() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____bmPrefix_6)); }
	inline RegexBoyerMoore_t2204811018 * get__bmPrefix_6() const { return ____bmPrefix_6; }
	inline RegexBoyerMoore_t2204811018 ** get_address_of__bmPrefix_6() { return &____bmPrefix_6; }
	inline void set__bmPrefix_6(RegexBoyerMoore_t2204811018 * value)
	{
		____bmPrefix_6 = value;
		Il2CppCodeGenWriteBarrier((&____bmPrefix_6), value);
	}

	inline static int32_t get_offset_of__anchors_7() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____anchors_7)); }
	inline int32_t get__anchors_7() const { return ____anchors_7; }
	inline int32_t* get_address_of__anchors_7() { return &____anchors_7; }
	inline void set__anchors_7(int32_t value)
	{
		____anchors_7 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_8() { return static_cast<int32_t>(offsetof(RegexCode_t2469392150, ____rightToLeft_8)); }
	inline bool get__rightToLeft_8() const { return ____rightToLeft_8; }
	inline bool* get_address_of__rightToLeft_8() { return &____rightToLeft_8; }
	inline void set__rightToLeft_8(bool value)
	{
		____rightToLeft_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCODE_T2469392150_H
#ifndef IOASYNCRESULT_T1276329107_H
#define IOASYNCRESULT_T1276329107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOAsyncResult
struct  IOAsyncResult_t1276329107  : public RuntimeObject
{
public:
	// System.AsyncCallback System.IOAsyncResult::async_callback
	AsyncCallback_t163412349 * ___async_callback_0;
	// System.Object System.IOAsyncResult::async_state
	RuntimeObject * ___async_state_1;
	// System.Threading.ManualResetEvent System.IOAsyncResult::wait_handle
	ManualResetEvent_t926074657 * ___wait_handle_2;
	// System.Boolean System.IOAsyncResult::completed_synchronously
	bool ___completed_synchronously_3;
	// System.Boolean System.IOAsyncResult::completed
	bool ___completed_4;

public:
	inline static int32_t get_offset_of_async_callback_0() { return static_cast<int32_t>(offsetof(IOAsyncResult_t1276329107, ___async_callback_0)); }
	inline AsyncCallback_t163412349 * get_async_callback_0() const { return ___async_callback_0; }
	inline AsyncCallback_t163412349 ** get_address_of_async_callback_0() { return &___async_callback_0; }
	inline void set_async_callback_0(AsyncCallback_t163412349 * value)
	{
		___async_callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___async_callback_0), value);
	}

	inline static int32_t get_offset_of_async_state_1() { return static_cast<int32_t>(offsetof(IOAsyncResult_t1276329107, ___async_state_1)); }
	inline RuntimeObject * get_async_state_1() const { return ___async_state_1; }
	inline RuntimeObject ** get_address_of_async_state_1() { return &___async_state_1; }
	inline void set_async_state_1(RuntimeObject * value)
	{
		___async_state_1 = value;
		Il2CppCodeGenWriteBarrier((&___async_state_1), value);
	}

	inline static int32_t get_offset_of_wait_handle_2() { return static_cast<int32_t>(offsetof(IOAsyncResult_t1276329107, ___wait_handle_2)); }
	inline ManualResetEvent_t926074657 * get_wait_handle_2() const { return ___wait_handle_2; }
	inline ManualResetEvent_t926074657 ** get_address_of_wait_handle_2() { return &___wait_handle_2; }
	inline void set_wait_handle_2(ManualResetEvent_t926074657 * value)
	{
		___wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___wait_handle_2), value);
	}

	inline static int32_t get_offset_of_completed_synchronously_3() { return static_cast<int32_t>(offsetof(IOAsyncResult_t1276329107, ___completed_synchronously_3)); }
	inline bool get_completed_synchronously_3() const { return ___completed_synchronously_3; }
	inline bool* get_address_of_completed_synchronously_3() { return &___completed_synchronously_3; }
	inline void set_completed_synchronously_3(bool value)
	{
		___completed_synchronously_3 = value;
	}

	inline static int32_t get_offset_of_completed_4() { return static_cast<int32_t>(offsetof(IOAsyncResult_t1276329107, ___completed_4)); }
	inline bool get_completed_4() const { return ___completed_4; }
	inline bool* get_address_of_completed_4() { return &___completed_4; }
	inline void set_completed_4(bool value)
	{
		___completed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IOAsyncResult
struct IOAsyncResult_t1276329107_marshaled_pinvoke
{
	Il2CppMethodPointer ___async_callback_0;
	Il2CppIUnknown* ___async_state_1;
	ManualResetEvent_t926074657 * ___wait_handle_2;
	int32_t ___completed_synchronously_3;
	int32_t ___completed_4;
};
// Native definition for COM marshalling of System.IOAsyncResult
struct IOAsyncResult_t1276329107_marshaled_com
{
	Il2CppMethodPointer ___async_callback_0;
	Il2CppIUnknown* ___async_state_1;
	ManualResetEvent_t926074657 * ___wait_handle_2;
	int32_t ___completed_synchronously_3;
	int32_t ___completed_4;
};
#endif // IOASYNCRESULT_T1276329107_H
#ifndef X509EXTENSIONCOLLECTION_T650873211_H
#define X509EXTENSIONCOLLECTION_T650873211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct  X509ExtensionCollection_t650873211  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ExtensionCollection::_list
	ArrayList_t4252133567 * ____list_1;

public:
	inline static int32_t get_offset_of__list_1() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t650873211, ____list_1)); }
	inline ArrayList_t4252133567 * get__list_1() const { return ____list_1; }
	inline ArrayList_t4252133567 ** get_address_of__list_1() { return &____list_1; }
	inline void set__list_1(ArrayList_t4252133567 * value)
	{
		____list_1 = value;
		Il2CppCodeGenWriteBarrier((&____list_1), value);
	}
};

struct X509ExtensionCollection_t650873211_StaticFields
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509ExtensionCollection::Empty
	ByteU5BU5D_t3397334013* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t650873211_StaticFields, ___Empty_0)); }
	inline ByteU5BU5D_t3397334013* get_Empty_0() const { return ___Empty_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ByteU5BU5D_t3397334013* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_T650873211_H
#ifndef IOSELECTOR_T2669134661_H
#define IOSELECTOR_T2669134661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOSelector
struct  IOSelector_t2669134661  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSELECTOR_T2669134661_H
#ifndef X509EXTENSIONENUMERATOR_T3763443773_H
#define X509EXTENSIONENUMERATOR_T3763443773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
struct  X509ExtensionEnumerator_t3763443773  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ExtensionEnumerator_t3763443773, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONENUMERATOR_T3763443773_H
#ifndef SINGLERANGE_T3794243288_H
#define SINGLERANGE_T3794243288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/SingleRange
struct  SingleRange_t3794243288  : public RuntimeObject
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/SingleRange::_first
	Il2CppChar ____first_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/SingleRange::_last
	Il2CppChar ____last_1;

public:
	inline static int32_t get_offset_of__first_0() { return static_cast<int32_t>(offsetof(SingleRange_t3794243288, ____first_0)); }
	inline Il2CppChar get__first_0() const { return ____first_0; }
	inline Il2CppChar* get_address_of__first_0() { return &____first_0; }
	inline void set__first_0(Il2CppChar value)
	{
		____first_0 = value;
	}

	inline static int32_t get_offset_of__last_1() { return static_cast<int32_t>(offsetof(SingleRange_t3794243288, ____last_1)); }
	inline Il2CppChar get__last_1() const { return ____last_1; }
	inline Il2CppChar* get_address_of__last_1() { return &____last_1; }
	inline void set__last_1(Il2CppChar value)
	{
		____last_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERANGE_T3794243288_H
#ifndef PLATFORM_T3160142327_H
#define PLATFORM_T3160142327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Platform
struct  Platform_t3160142327  : public RuntimeObject
{
public:

public:
};

struct Platform_t3160142327_StaticFields
{
public:
	// System.Boolean System.Platform::checkedOS
	bool ___checkedOS_0;
	// System.Boolean System.Platform::isMacOS
	bool ___isMacOS_1;
	// System.Boolean System.Platform::isFreeBSD
	bool ___isFreeBSD_2;

public:
	inline static int32_t get_offset_of_checkedOS_0() { return static_cast<int32_t>(offsetof(Platform_t3160142327_StaticFields, ___checkedOS_0)); }
	inline bool get_checkedOS_0() const { return ___checkedOS_0; }
	inline bool* get_address_of_checkedOS_0() { return &___checkedOS_0; }
	inline void set_checkedOS_0(bool value)
	{
		___checkedOS_0 = value;
	}

	inline static int32_t get_offset_of_isMacOS_1() { return static_cast<int32_t>(offsetof(Platform_t3160142327_StaticFields, ___isMacOS_1)); }
	inline bool get_isMacOS_1() const { return ___isMacOS_1; }
	inline bool* get_address_of_isMacOS_1() { return &___isMacOS_1; }
	inline void set_isMacOS_1(bool value)
	{
		___isMacOS_1 = value;
	}

	inline static int32_t get_offset_of_isFreeBSD_2() { return static_cast<int32_t>(offsetof(Platform_t3160142327_StaticFields, ___isFreeBSD_2)); }
	inline bool get_isFreeBSD_2() const { return ___isFreeBSD_2; }
	inline bool* get_address_of_isFreeBSD_2() { return &___isFreeBSD_2; }
	inline void set_isFreeBSD_2(bool value)
	{
		___isFreeBSD_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T3160142327_H
#ifndef OSX509CERTIFICATES_T384932784_H
#define OSX509CERTIFICATES_T384932784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OSX509Certificates
struct  OSX509Certificates_t384932784  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSX509CERTIFICATES_T384932784_H
#ifndef X509CHAINELEMENTCOLLECTION_T2081831987_H
#define X509CHAINELEMENTCOLLECTION_T2081831987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct  X509ChainElementCollection_t2081831987  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ChainElementCollection::_list
	ArrayList_t4252133567 * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ChainElementCollection_t2081831987, ____list_0)); }
	inline ArrayList_t4252133567 * get__list_0() const { return ____list_0; }
	inline ArrayList_t4252133567 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t4252133567 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTCOLLECTION_T2081831987_H
#ifndef X509CHAIN_T777637347_H
#define X509CHAIN_T777637347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Chain
struct  X509Chain_t777637347  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainImpl System.Security.Cryptography.X509Certificates.X509Chain::impl
	X509ChainImpl_t2968295413 * ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(X509Chain_t777637347, ___impl_0)); }
	inline X509ChainImpl_t2968295413 * get_impl_0() const { return ___impl_0; }
	inline X509ChainImpl_t2968295413 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(X509ChainImpl_t2968295413 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T777637347_H
#ifndef X509CERTIFICATEIMPLCOLLECTION_T255811311_H
#define X509CERTIFICATEIMPLCOLLECTION_T255811311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateImplCollection
struct  X509CertificateImplCollection_t255811311  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Security.Cryptography.X509Certificates.X509CertificateImpl> System.Security.Cryptography.X509Certificates.X509CertificateImplCollection::list
	List_1_t3211185839 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(X509CertificateImplCollection_t255811311, ___list_0)); }
	inline List_1_t3211185839 * get_list_0() const { return ___list_0; }
	inline List_1_t3211185839 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3211185839 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEIMPLCOLLECTION_T255811311_H
#ifndef X509CHAINELEMENTENUMERATOR_T3304975821_H
#define X509CHAINELEMENTENUMERATOR_T3304975821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
struct  X509ChainElementEnumerator_t3304975821  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ChainElementEnumerator_t3304975821, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTENUMERATOR_T3304975821_H
#ifndef X509HELPER2_T1000999714_H
#define X509HELPER2_T1000999714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Helper2
struct  X509Helper2_t1000999714  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509HELPER2_T1000999714_H
#ifndef MYNATIVEHELPER_T2161630009_H
#define MYNATIVEHELPER_T2161630009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Helper2/MyNativeHelper
struct  MyNativeHelper_t2161630009  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYNATIVEHELPER_T2161630009_H
#ifndef X509CHAINIMPL_T2968295413_H
#define X509CHAINIMPL_T2968295413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainImpl
struct  X509ChainImpl_t2968295413  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINIMPL_T2968295413_H
#ifndef PUBLICKEY_T870392_H
#define PUBLICKEY_T870392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.PublicKey
struct  PublicKey_t870392  : public RuntimeObject
{
public:
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::_key
	AsymmetricAlgorithm_t784058677 * ____key_0;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_keyValue
	AsnEncodedData_t463456204 * ____keyValue_1;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_params
	AsnEncodedData_t463456204 * ____params_2;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::_oid
	Oid_t3221867120 * ____oid_3;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(PublicKey_t870392, ____key_0)); }
	inline AsymmetricAlgorithm_t784058677 * get__key_0() const { return ____key_0; }
	inline AsymmetricAlgorithm_t784058677 ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(AsymmetricAlgorithm_t784058677 * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__keyValue_1() { return static_cast<int32_t>(offsetof(PublicKey_t870392, ____keyValue_1)); }
	inline AsnEncodedData_t463456204 * get__keyValue_1() const { return ____keyValue_1; }
	inline AsnEncodedData_t463456204 ** get_address_of__keyValue_1() { return &____keyValue_1; }
	inline void set__keyValue_1(AsnEncodedData_t463456204 * value)
	{
		____keyValue_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyValue_1), value);
	}

	inline static int32_t get_offset_of__params_2() { return static_cast<int32_t>(offsetof(PublicKey_t870392, ____params_2)); }
	inline AsnEncodedData_t463456204 * get__params_2() const { return ____params_2; }
	inline AsnEncodedData_t463456204 ** get_address_of__params_2() { return &____params_2; }
	inline void set__params_2(AsnEncodedData_t463456204 * value)
	{
		____params_2 = value;
		Il2CppCodeGenWriteBarrier((&____params_2), value);
	}

	inline static int32_t get_offset_of__oid_3() { return static_cast<int32_t>(offsetof(PublicKey_t870392, ____oid_3)); }
	inline Oid_t3221867120 * get__oid_3() const { return ____oid_3; }
	inline Oid_t3221867120 ** get_address_of__oid_3() { return &____oid_3; }
	inline void set__oid_3(Oid_t3221867120 * value)
	{
		____oid_3 = value;
		Il2CppCodeGenWriteBarrier((&____oid_3), value);
	}
};

struct PublicKey_t870392_StaticFields
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::Empty
	ByteU5BU5D_t3397334013* ___Empty_4;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(PublicKey_t870392_StaticFields, ___Empty_4)); }
	inline ByteU5BU5D_t3397334013* get_Empty_4() const { return ___Empty_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(ByteU5BU5D_t3397334013* value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEY_T870392_H
#ifndef SHAREDREFERENCE_T2137668360_H
#define SHAREDREFERENCE_T2137668360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.SharedReference
struct  SharedReference_t2137668360  : public RuntimeObject
{
public:
	// System.WeakReference System.Text.RegularExpressions.SharedReference::_ref
	WeakReference_t1077405567 * ____ref_0;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(SharedReference_t2137668360, ____ref_0)); }
	inline WeakReference_t1077405567 * get__ref_0() const { return ____ref_0; }
	inline WeakReference_t1077405567 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(WeakReference_t1077405567 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier((&____ref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDREFERENCE_T2137668360_H
#ifndef REGEXBOYERMOORE_T2204811018_H
#define REGEXBOYERMOORE_T2204811018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexBoyerMoore
struct  RegexBoyerMoore_t2204811018  : public RuntimeObject
{
public:
	// System.Int32[] System.Text.RegularExpressions.RegexBoyerMoore::_positive
	Int32U5BU5D_t3030399641* ____positive_0;
	// System.Int32[] System.Text.RegularExpressions.RegexBoyerMoore::_negativeASCII
	Int32U5BU5D_t3030399641* ____negativeASCII_1;
	// System.Int32[][] System.Text.RegularExpressions.RegexBoyerMoore::_negativeUnicode
	Int32U5BU5DU5BU5D_t3750818532* ____negativeUnicode_2;
	// System.String System.Text.RegularExpressions.RegexBoyerMoore::_pattern
	String_t* ____pattern_3;
	// System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::_lowASCII
	int32_t ____lowASCII_4;
	// System.Int32 System.Text.RegularExpressions.RegexBoyerMoore::_highASCII
	int32_t ____highASCII_5;
	// System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::_rightToLeft
	bool ____rightToLeft_6;
	// System.Boolean System.Text.RegularExpressions.RegexBoyerMoore::_caseInsensitive
	bool ____caseInsensitive_7;
	// System.Globalization.CultureInfo System.Text.RegularExpressions.RegexBoyerMoore::_culture
	CultureInfo_t3500843524 * ____culture_8;

public:
	inline static int32_t get_offset_of__positive_0() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____positive_0)); }
	inline Int32U5BU5D_t3030399641* get__positive_0() const { return ____positive_0; }
	inline Int32U5BU5D_t3030399641** get_address_of__positive_0() { return &____positive_0; }
	inline void set__positive_0(Int32U5BU5D_t3030399641* value)
	{
		____positive_0 = value;
		Il2CppCodeGenWriteBarrier((&____positive_0), value);
	}

	inline static int32_t get_offset_of__negativeASCII_1() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____negativeASCII_1)); }
	inline Int32U5BU5D_t3030399641* get__negativeASCII_1() const { return ____negativeASCII_1; }
	inline Int32U5BU5D_t3030399641** get_address_of__negativeASCII_1() { return &____negativeASCII_1; }
	inline void set__negativeASCII_1(Int32U5BU5D_t3030399641* value)
	{
		____negativeASCII_1 = value;
		Il2CppCodeGenWriteBarrier((&____negativeASCII_1), value);
	}

	inline static int32_t get_offset_of__negativeUnicode_2() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____negativeUnicode_2)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__negativeUnicode_2() const { return ____negativeUnicode_2; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__negativeUnicode_2() { return &____negativeUnicode_2; }
	inline void set__negativeUnicode_2(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____negativeUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&____negativeUnicode_2), value);
	}

	inline static int32_t get_offset_of__pattern_3() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____pattern_3)); }
	inline String_t* get__pattern_3() const { return ____pattern_3; }
	inline String_t** get_address_of__pattern_3() { return &____pattern_3; }
	inline void set__pattern_3(String_t* value)
	{
		____pattern_3 = value;
		Il2CppCodeGenWriteBarrier((&____pattern_3), value);
	}

	inline static int32_t get_offset_of__lowASCII_4() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____lowASCII_4)); }
	inline int32_t get__lowASCII_4() const { return ____lowASCII_4; }
	inline int32_t* get_address_of__lowASCII_4() { return &____lowASCII_4; }
	inline void set__lowASCII_4(int32_t value)
	{
		____lowASCII_4 = value;
	}

	inline static int32_t get_offset_of__highASCII_5() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____highASCII_5)); }
	inline int32_t get__highASCII_5() const { return ____highASCII_5; }
	inline int32_t* get_address_of__highASCII_5() { return &____highASCII_5; }
	inline void set__highASCII_5(int32_t value)
	{
		____highASCII_5 = value;
	}

	inline static int32_t get_offset_of__rightToLeft_6() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____rightToLeft_6)); }
	inline bool get__rightToLeft_6() const { return ____rightToLeft_6; }
	inline bool* get_address_of__rightToLeft_6() { return &____rightToLeft_6; }
	inline void set__rightToLeft_6(bool value)
	{
		____rightToLeft_6 = value;
	}

	inline static int32_t get_offset_of__caseInsensitive_7() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____caseInsensitive_7)); }
	inline bool get__caseInsensitive_7() const { return ____caseInsensitive_7; }
	inline bool* get_address_of__caseInsensitive_7() { return &____caseInsensitive_7; }
	inline void set__caseInsensitive_7(bool value)
	{
		____caseInsensitive_7 = value;
	}

	inline static int32_t get_offset_of__culture_8() { return static_cast<int32_t>(offsetof(RegexBoyerMoore_t2204811018, ____culture_8)); }
	inline CultureInfo_t3500843524 * get__culture_8() const { return ____culture_8; }
	inline CultureInfo_t3500843524 ** get_address_of__culture_8() { return &____culture_8; }
	inline void set__culture_8(CultureInfo_t3500843524 * value)
	{
		____culture_8 = value;
		Il2CppCodeGenWriteBarrier((&____culture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXBOYERMOORE_T2204811018_H
#ifndef EXCLUSIVEREFERENCE_T708182869_H
#define EXCLUSIVEREFERENCE_T708182869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.ExclusiveReference
struct  ExclusiveReference_t708182869  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.RegexRunner System.Text.RegularExpressions.ExclusiveReference::_ref
	RegexRunner_t3983612747 * ____ref_0;
	// System.Object System.Text.RegularExpressions.ExclusiveReference::_obj
	RuntimeObject * ____obj_1;
	// System.Int32 System.Text.RegularExpressions.ExclusiveReference::_locked
	int32_t ____locked_2;

public:
	inline static int32_t get_offset_of__ref_0() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____ref_0)); }
	inline RegexRunner_t3983612747 * get__ref_0() const { return ____ref_0; }
	inline RegexRunner_t3983612747 ** get_address_of__ref_0() { return &____ref_0; }
	inline void set__ref_0(RegexRunner_t3983612747 * value)
	{
		____ref_0 = value;
		Il2CppCodeGenWriteBarrier((&____ref_0), value);
	}

	inline static int32_t get_offset_of__obj_1() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____obj_1)); }
	inline RuntimeObject * get__obj_1() const { return ____obj_1; }
	inline RuntimeObject ** get_address_of__obj_1() { return &____obj_1; }
	inline void set__obj_1(RuntimeObject * value)
	{
		____obj_1 = value;
		Il2CppCodeGenWriteBarrier((&____obj_1), value);
	}

	inline static int32_t get_offset_of__locked_2() { return static_cast<int32_t>(offsetof(ExclusiveReference_t708182869, ____locked_2)); }
	inline int32_t get__locked_2() const { return ____locked_2; }
	inline int32_t* get_address_of__locked_2() { return &____locked_2; }
	inline void set__locked_2(int32_t value)
	{
		____locked_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUSIVEREFERENCE_T708182869_H
#ifndef X509CERTIFICATEENUMERATOR_T1208230922_H
#define X509CERTIFICATEENUMERATOR_T1208230922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
struct  X509CertificateEnumerator_t1208230922  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509CertificateEnumerator_t1208230922, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENUMERATOR_T1208230922_H
#ifndef X509CERTIFICATE2ENUMERATOR_T2356134957_H
#define X509CERTIFICATE2ENUMERATOR_T2356134957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct  X509Certificate2Enumerator_t2356134957  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509Certificate2Enumerator_t2356134957, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2ENUMERATOR_T2356134957_H
#ifndef CACHEDCODEENTRY_T3553821051_H
#define CACHEDCODEENTRY_T3553821051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CachedCodeEntry
struct  CachedCodeEntry_t3553821051  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.CachedCodeEntry::_key
	String_t* ____key_0;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.CachedCodeEntry::_code
	RegexCode_t2469392150 * ____code_1;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_caps
	Hashtable_t909839986 * ____caps_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.CachedCodeEntry::_capnames
	Hashtable_t909839986 * ____capnames_3;
	// System.String[] System.Text.RegularExpressions.CachedCodeEntry::_capslist
	StringU5BU5D_t1642385972* ____capslist_4;
	// System.Int32 System.Text.RegularExpressions.CachedCodeEntry::_capsize
	int32_t ____capsize_5;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.CachedCodeEntry::_factory
	RegexRunnerFactory_t3902733837 * ____factory_6;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.CachedCodeEntry::_runnerref
	ExclusiveReference_t708182869 * ____runnerref_7;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.CachedCodeEntry::_replref
	SharedReference_t2137668360 * ____replref_8;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____key_0)); }
	inline String_t* get__key_0() const { return ____key_0; }
	inline String_t** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(String_t* value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__code_1() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____code_1)); }
	inline RegexCode_t2469392150 * get__code_1() const { return ____code_1; }
	inline RegexCode_t2469392150 ** get_address_of__code_1() { return &____code_1; }
	inline void set__code_1(RegexCode_t2469392150 * value)
	{
		____code_1 = value;
		Il2CppCodeGenWriteBarrier((&____code_1), value);
	}

	inline static int32_t get_offset_of__caps_2() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____caps_2)); }
	inline Hashtable_t909839986 * get__caps_2() const { return ____caps_2; }
	inline Hashtable_t909839986 ** get_address_of__caps_2() { return &____caps_2; }
	inline void set__caps_2(Hashtable_t909839986 * value)
	{
		____caps_2 = value;
		Il2CppCodeGenWriteBarrier((&____caps_2), value);
	}

	inline static int32_t get_offset_of__capnames_3() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capnames_3)); }
	inline Hashtable_t909839986 * get__capnames_3() const { return ____capnames_3; }
	inline Hashtable_t909839986 ** get_address_of__capnames_3() { return &____capnames_3; }
	inline void set__capnames_3(Hashtable_t909839986 * value)
	{
		____capnames_3 = value;
		Il2CppCodeGenWriteBarrier((&____capnames_3), value);
	}

	inline static int32_t get_offset_of__capslist_4() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capslist_4)); }
	inline StringU5BU5D_t1642385972* get__capslist_4() const { return ____capslist_4; }
	inline StringU5BU5D_t1642385972** get_address_of__capslist_4() { return &____capslist_4; }
	inline void set__capslist_4(StringU5BU5D_t1642385972* value)
	{
		____capslist_4 = value;
		Il2CppCodeGenWriteBarrier((&____capslist_4), value);
	}

	inline static int32_t get_offset_of__capsize_5() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____capsize_5)); }
	inline int32_t get__capsize_5() const { return ____capsize_5; }
	inline int32_t* get_address_of__capsize_5() { return &____capsize_5; }
	inline void set__capsize_5(int32_t value)
	{
		____capsize_5 = value;
	}

	inline static int32_t get_offset_of__factory_6() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____factory_6)); }
	inline RegexRunnerFactory_t3902733837 * get__factory_6() const { return ____factory_6; }
	inline RegexRunnerFactory_t3902733837 ** get_address_of__factory_6() { return &____factory_6; }
	inline void set__factory_6(RegexRunnerFactory_t3902733837 * value)
	{
		____factory_6 = value;
		Il2CppCodeGenWriteBarrier((&____factory_6), value);
	}

	inline static int32_t get_offset_of__runnerref_7() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____runnerref_7)); }
	inline ExclusiveReference_t708182869 * get__runnerref_7() const { return ____runnerref_7; }
	inline ExclusiveReference_t708182869 ** get_address_of__runnerref_7() { return &____runnerref_7; }
	inline void set__runnerref_7(ExclusiveReference_t708182869 * value)
	{
		____runnerref_7 = value;
		Il2CppCodeGenWriteBarrier((&____runnerref_7), value);
	}

	inline static int32_t get_offset_of__replref_8() { return static_cast<int32_t>(offsetof(CachedCodeEntry_t3553821051, ____replref_8)); }
	inline SharedReference_t2137668360 * get__replref_8() const { return ____replref_8; }
	inline SharedReference_t2137668360 ** get_address_of__replref_8() { return &____replref_8; }
	inline void set__replref_8(SharedReference_t2137668360 * value)
	{
		____replref_8 = value;
		Il2CppCodeGenWriteBarrier((&____replref_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDCODEENTRY_T3553821051_H
#ifndef URIHELPER_T2566857206_H
#define URIHELPER_T2566857206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHelper
struct  UriHelper_t2566857206  : public RuntimeObject
{
public:

public:
};

struct UriHelper_t2566857206_StaticFields
{
public:
	// System.Char[] System.UriHelper::HexUpperChars
	CharU5BU5D_t1328083999* ___HexUpperChars_0;

public:
	inline static int32_t get_offset_of_HexUpperChars_0() { return static_cast<int32_t>(offsetof(UriHelper_t2566857206_StaticFields, ___HexUpperChars_0)); }
	inline CharU5BU5D_t1328083999* get_HexUpperChars_0() const { return ___HexUpperChars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_HexUpperChars_0() { return &___HexUpperChars_0; }
	inline void set_HexUpperChars_0(CharU5BU5D_t1328083999* value)
	{
		___HexUpperChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___HexUpperChars_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHELPER_T2566857206_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t1927440687 * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t169632028* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____innerException_4)); }
	inline Exception_t1927440687 * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t1927440687 ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t1927440687 * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t1975884510 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t1975884510 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t1975884510 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t2217612696* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t2217612696** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t2217612696* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t169632028* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t169632028** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t169632028* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t1927440687_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t1927440687_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t1927440687_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t1927440687_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t1975884510 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t2217612696* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T1927440687_H
#ifndef IRIHELPER_T306005226_H
#define IRIHELPER_T306005226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IriHelper
struct  IriHelper_t306005226  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IRIHELPER_T306005226_H
#ifndef SECURITYUTILS_T1714011141_H
#define SECURITYUTILS_T1714011141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SecurityUtils
struct  SecurityUtils_t1714011141  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYUTILS_T1714011141_H
#ifndef INVARIANTCOMPARER_T3322871449_H
#define INVARIANTCOMPARER_T3322871449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvariantComparer
struct  InvariantComparer_t3322871449  : public RuntimeObject
{
public:
	// System.Globalization.CompareInfo System.InvariantComparer::m_compareInfo
	CompareInfo_t2310920157 * ___m_compareInfo_0;

public:
	inline static int32_t get_offset_of_m_compareInfo_0() { return static_cast<int32_t>(offsetof(InvariantComparer_t3322871449, ___m_compareInfo_0)); }
	inline CompareInfo_t2310920157 * get_m_compareInfo_0() const { return ___m_compareInfo_0; }
	inline CompareInfo_t2310920157 ** get_address_of_m_compareInfo_0() { return &___m_compareInfo_0; }
	inline void set_m_compareInfo_0(CompareInfo_t2310920157 * value)
	{
		___m_compareInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_compareInfo_0), value);
	}
};

struct InvariantComparer_t3322871449_StaticFields
{
public:
	// System.InvariantComparer System.InvariantComparer::Default
	InvariantComparer_t3322871449 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(InvariantComparer_t3322871449_StaticFields, ___Default_1)); }
	inline InvariantComparer_t3322871449 * get_Default_1() const { return ___Default_1; }
	inline InvariantComparer_t3322871449 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(InvariantComparer_t3322871449 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVARIANTCOMPARER_T3322871449_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef MOREINFO_T2595315311_H
#define MOREINFO_T2595315311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/MoreInfo
struct  MoreInfo_t2595315311  : public RuntimeObject
{
public:
	// System.String System.Uri/MoreInfo::Path
	String_t* ___Path_0;
	// System.String System.Uri/MoreInfo::Query
	String_t* ___Query_1;
	// System.String System.Uri/MoreInfo::Fragment
	String_t* ___Fragment_2;
	// System.String System.Uri/MoreInfo::AbsoluteUri
	String_t* ___AbsoluteUri_3;
	// System.Int32 System.Uri/MoreInfo::Hash
	int32_t ___Hash_4;
	// System.String System.Uri/MoreInfo::RemoteUrl
	String_t* ___RemoteUrl_5;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(MoreInfo_t2595315311, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((&___Path_0), value);
	}

	inline static int32_t get_offset_of_Query_1() { return static_cast<int32_t>(offsetof(MoreInfo_t2595315311, ___Query_1)); }
	inline String_t* get_Query_1() const { return ___Query_1; }
	inline String_t** get_address_of_Query_1() { return &___Query_1; }
	inline void set_Query_1(String_t* value)
	{
		___Query_1 = value;
		Il2CppCodeGenWriteBarrier((&___Query_1), value);
	}

	inline static int32_t get_offset_of_Fragment_2() { return static_cast<int32_t>(offsetof(MoreInfo_t2595315311, ___Fragment_2)); }
	inline String_t* get_Fragment_2() const { return ___Fragment_2; }
	inline String_t** get_address_of_Fragment_2() { return &___Fragment_2; }
	inline void set_Fragment_2(String_t* value)
	{
		___Fragment_2 = value;
		Il2CppCodeGenWriteBarrier((&___Fragment_2), value);
	}

	inline static int32_t get_offset_of_AbsoluteUri_3() { return static_cast<int32_t>(offsetof(MoreInfo_t2595315311, ___AbsoluteUri_3)); }
	inline String_t* get_AbsoluteUri_3() const { return ___AbsoluteUri_3; }
	inline String_t** get_address_of_AbsoluteUri_3() { return &___AbsoluteUri_3; }
	inline void set_AbsoluteUri_3(String_t* value)
	{
		___AbsoluteUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___AbsoluteUri_3), value);
	}

	inline static int32_t get_offset_of_Hash_4() { return static_cast<int32_t>(offsetof(MoreInfo_t2595315311, ___Hash_4)); }
	inline int32_t get_Hash_4() const { return ___Hash_4; }
	inline int32_t* get_address_of_Hash_4() { return &___Hash_4; }
	inline void set_Hash_4(int32_t value)
	{
		___Hash_4 = value;
	}

	inline static int32_t get_offset_of_RemoteUrl_5() { return static_cast<int32_t>(offsetof(MoreInfo_t2595315311, ___RemoteUrl_5)); }
	inline String_t* get_RemoteUrl_5() const { return ___RemoteUrl_5; }
	inline String_t** get_address_of_RemoteUrl_5() { return &___RemoteUrl_5; }
	inline void set_RemoteUrl_5(String_t* value)
	{
		___RemoteUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteUrl_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOREINFO_T2595315311_H
#ifndef COLLECTIONBASE_T1101587467_H
#define COLLECTIONBASE_T1101587467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t1101587467  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4252133567 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t1101587467, ___list_0)); }
	inline ArrayList_t4252133567 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4252133567 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4252133567 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T1101587467_H
#ifndef X509CERTIFICATE_T283079845_H
#define X509CERTIFICATE_T283079845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate
struct  X509Certificate_t283079845  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateImpl System.Security.Cryptography.X509Certificates.X509Certificate::impl
	X509CertificateImpl_t3842064707 * ___impl_0;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::hideDates
	bool ___hideDates_1;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::issuer_name
	String_t* ___issuer_name_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::subject_name
	String_t* ___subject_name_3;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(X509Certificate_t283079845, ___impl_0)); }
	inline X509CertificateImpl_t3842064707 * get_impl_0() const { return ___impl_0; }
	inline X509CertificateImpl_t3842064707 ** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(X509CertificateImpl_t3842064707 * value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}

	inline static int32_t get_offset_of_hideDates_1() { return static_cast<int32_t>(offsetof(X509Certificate_t283079845, ___hideDates_1)); }
	inline bool get_hideDates_1() const { return ___hideDates_1; }
	inline bool* get_address_of_hideDates_1() { return &___hideDates_1; }
	inline void set_hideDates_1(bool value)
	{
		___hideDates_1 = value;
	}

	inline static int32_t get_offset_of_issuer_name_2() { return static_cast<int32_t>(offsetof(X509Certificate_t283079845, ___issuer_name_2)); }
	inline String_t* get_issuer_name_2() const { return ___issuer_name_2; }
	inline String_t** get_address_of_issuer_name_2() { return &___issuer_name_2; }
	inline void set_issuer_name_2(String_t* value)
	{
		___issuer_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_2), value);
	}

	inline static int32_t get_offset_of_subject_name_3() { return static_cast<int32_t>(offsetof(X509Certificate_t283079845, ___subject_name_3)); }
	inline String_t* get_subject_name_3() const { return ___subject_name_3; }
	inline String_t** get_address_of_subject_name_3() { return &___subject_name_3; }
	inline void set_subject_name_3(String_t* value)
	{
		___subject_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T283079845_H
#ifndef X509CERTIFICATEIMPL_T3842064707_H
#define X509CERTIFICATEIMPL_T3842064707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateImpl
struct  X509CertificateImpl_t3842064707  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509CertificateImpl::cachedCertificateHash
	ByteU5BU5D_t3397334013* ___cachedCertificateHash_0;

public:
	inline static int32_t get_offset_of_cachedCertificateHash_0() { return static_cast<int32_t>(offsetof(X509CertificateImpl_t3842064707, ___cachedCertificateHash_0)); }
	inline ByteU5BU5D_t3397334013* get_cachedCertificateHash_0() const { return ___cachedCertificateHash_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_cachedCertificateHash_0() { return &___cachedCertificateHash_0; }
	inline void set_cachedCertificateHash_0(ByteU5BU5D_t3397334013* value)
	{
		___cachedCertificateHash_0 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCertificateHash_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEIMPL_T3842064707_H
#ifndef SINGLERANGECOMPARER_T3640568023_H
#define SINGLERANGECOMPARER_T3640568023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/SingleRangeComparer
struct  SingleRangeComparer_t3640568023  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERANGECOMPARER_T3640568023_H
#ifndef UNCNAMEHELPER_T1663961013_H
#define UNCNAMEHELPER_T1663961013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UncNameHelper
struct  UncNameHelper_t1663961013  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNCNAMEHELPER_T1663961013_H
#ifndef LOCALAPPCONTEXTSWITCHES_T4208498603_H
#define LOCALAPPCONTEXTSWITCHES_T4208498603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalAppContextSwitches
struct  LocalAppContextSwitches_t4208498603  : public RuntimeObject
{
public:

public:
};

struct LocalAppContextSwitches_t4208498603_StaticFields
{
public:
	// System.Boolean System.LocalAppContextSwitches::MemberDescriptorEqualsReturnsFalseIfEquivalent
	bool ___MemberDescriptorEqualsReturnsFalseIfEquivalent_0;

public:
	inline static int32_t get_offset_of_MemberDescriptorEqualsReturnsFalseIfEquivalent_0() { return static_cast<int32_t>(offsetof(LocalAppContextSwitches_t4208498603_StaticFields, ___MemberDescriptorEqualsReturnsFalseIfEquivalent_0)); }
	inline bool get_MemberDescriptorEqualsReturnsFalseIfEquivalent_0() const { return ___MemberDescriptorEqualsReturnsFalseIfEquivalent_0; }
	inline bool* get_address_of_MemberDescriptorEqualsReturnsFalseIfEquivalent_0() { return &___MemberDescriptorEqualsReturnsFalseIfEquivalent_0; }
	inline void set_MemberDescriptorEqualsReturnsFalseIfEquivalent_0(bool value)
	{
		___MemberDescriptorEqualsReturnsFalseIfEquivalent_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALAPPCONTEXTSWITCHES_T4208498603_H
#ifndef URIBUILDER_T2016461725_H
#define URIBUILDER_T2016461725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriBuilder
struct  UriBuilder_t2016461725  : public RuntimeObject
{
public:
	// System.Boolean System.UriBuilder::m_changed
	bool ___m_changed_0;
	// System.String System.UriBuilder::m_fragment
	String_t* ___m_fragment_1;
	// System.String System.UriBuilder::m_host
	String_t* ___m_host_2;
	// System.String System.UriBuilder::m_password
	String_t* ___m_password_3;
	// System.String System.UriBuilder::m_path
	String_t* ___m_path_4;
	// System.Int32 System.UriBuilder::m_port
	int32_t ___m_port_5;
	// System.String System.UriBuilder::m_query
	String_t* ___m_query_6;
	// System.String System.UriBuilder::m_scheme
	String_t* ___m_scheme_7;
	// System.String System.UriBuilder::m_schemeDelimiter
	String_t* ___m_schemeDelimiter_8;
	// System.Uri System.UriBuilder::m_uri
	Uri_t19570940 * ___m_uri_9;
	// System.String System.UriBuilder::m_username
	String_t* ___m_username_10;

public:
	inline static int32_t get_offset_of_m_changed_0() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_changed_0)); }
	inline bool get_m_changed_0() const { return ___m_changed_0; }
	inline bool* get_address_of_m_changed_0() { return &___m_changed_0; }
	inline void set_m_changed_0(bool value)
	{
		___m_changed_0 = value;
	}

	inline static int32_t get_offset_of_m_fragment_1() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_fragment_1)); }
	inline String_t* get_m_fragment_1() const { return ___m_fragment_1; }
	inline String_t** get_address_of_m_fragment_1() { return &___m_fragment_1; }
	inline void set_m_fragment_1(String_t* value)
	{
		___m_fragment_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fragment_1), value);
	}

	inline static int32_t get_offset_of_m_host_2() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_host_2)); }
	inline String_t* get_m_host_2() const { return ___m_host_2; }
	inline String_t** get_address_of_m_host_2() { return &___m_host_2; }
	inline void set_m_host_2(String_t* value)
	{
		___m_host_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_host_2), value);
	}

	inline static int32_t get_offset_of_m_password_3() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_password_3)); }
	inline String_t* get_m_password_3() const { return ___m_password_3; }
	inline String_t** get_address_of_m_password_3() { return &___m_password_3; }
	inline void set_m_password_3(String_t* value)
	{
		___m_password_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_password_3), value);
	}

	inline static int32_t get_offset_of_m_path_4() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_path_4)); }
	inline String_t* get_m_path_4() const { return ___m_path_4; }
	inline String_t** get_address_of_m_path_4() { return &___m_path_4; }
	inline void set_m_path_4(String_t* value)
	{
		___m_path_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_path_4), value);
	}

	inline static int32_t get_offset_of_m_port_5() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_port_5)); }
	inline int32_t get_m_port_5() const { return ___m_port_5; }
	inline int32_t* get_address_of_m_port_5() { return &___m_port_5; }
	inline void set_m_port_5(int32_t value)
	{
		___m_port_5 = value;
	}

	inline static int32_t get_offset_of_m_query_6() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_query_6)); }
	inline String_t* get_m_query_6() const { return ___m_query_6; }
	inline String_t** get_address_of_m_query_6() { return &___m_query_6; }
	inline void set_m_query_6(String_t* value)
	{
		___m_query_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_query_6), value);
	}

	inline static int32_t get_offset_of_m_scheme_7() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_scheme_7)); }
	inline String_t* get_m_scheme_7() const { return ___m_scheme_7; }
	inline String_t** get_address_of_m_scheme_7() { return &___m_scheme_7; }
	inline void set_m_scheme_7(String_t* value)
	{
		___m_scheme_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_scheme_7), value);
	}

	inline static int32_t get_offset_of_m_schemeDelimiter_8() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_schemeDelimiter_8)); }
	inline String_t* get_m_schemeDelimiter_8() const { return ___m_schemeDelimiter_8; }
	inline String_t** get_address_of_m_schemeDelimiter_8() { return &___m_schemeDelimiter_8; }
	inline void set_m_schemeDelimiter_8(String_t* value)
	{
		___m_schemeDelimiter_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_schemeDelimiter_8), value);
	}

	inline static int32_t get_offset_of_m_uri_9() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_uri_9)); }
	inline Uri_t19570940 * get_m_uri_9() const { return ___m_uri_9; }
	inline Uri_t19570940 ** get_address_of_m_uri_9() { return &___m_uri_9; }
	inline void set_m_uri_9(Uri_t19570940 * value)
	{
		___m_uri_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_uri_9), value);
	}

	inline static int32_t get_offset_of_m_username_10() { return static_cast<int32_t>(offsetof(UriBuilder_t2016461725, ___m_username_10)); }
	inline String_t* get_m_username_10() const { return ___m_username_10; }
	inline String_t** get_address_of_m_username_10() { return &___m_username_10; }
	inline void set_m_username_10(String_t* value)
	{
		___m_username_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_username_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIBUILDER_T2016461725_H
#ifndef IPV6ADDRESSHELPER_T76851317_H
#define IPV6ADDRESSHELPER_T76851317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IPv6AddressHelper
struct  IPv6AddressHelper_t76851317  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESSHELPER_T76851317_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef DOMAINNAMEHELPER_T2237853587_H
#define DOMAINNAMEHELPER_T2237853587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DomainNameHelper
struct  DomainNameHelper_t2237853587  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOMAINNAMEHELPER_T2237853587_H
#ifndef IPV4ADDRESSHELPER_T3364954615_H
#define IPV4ADDRESSHELPER_T3364954615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IPv4AddressHelper
struct  IPv4AddressHelper_t3364954615  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV4ADDRESSHELPER_T3364954615_H
#ifndef X509CERTIFICATE2IMPL_T2703153821_H
#define X509CERTIFICATE2IMPL_T2703153821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Impl
struct  X509Certificate2Impl_t2703153821  : public X509CertificateImpl_t3842064707
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2IMPL_T2703153821_H
#ifndef X509CERTIFICATE2_T4056456767_H
#define X509CERTIFICATE2_T4056456767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct  X509Certificate2_t4056456767  : public X509Certificate_t283079845
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::friendlyName
	String_t* ___friendlyName_4;

public:
	inline static int32_t get_offset_of_friendlyName_4() { return static_cast<int32_t>(offsetof(X509Certificate2_t4056456767, ___friendlyName_4)); }
	inline String_t* get_friendlyName_4() const { return ___friendlyName_4; }
	inline String_t** get_address_of_friendlyName_4() { return &___friendlyName_4; }
	inline void set_friendlyName_4(String_t* value)
	{
		___friendlyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___friendlyName_4), value);
	}
};

struct X509Certificate2_t4056456767_StaticFields
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::signedData
	ByteU5BU5D_t3397334013* ___signedData_5;

public:
	inline static int32_t get_offset_of_signedData_5() { return static_cast<int32_t>(offsetof(X509Certificate2_t4056456767_StaticFields, ___signedData_5)); }
	inline ByteU5BU5D_t3397334013* get_signedData_5() const { return ___signedData_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_signedData_5() { return &___signedData_5; }
	inline void set_signedData_5(ByteU5BU5D_t3397334013* value)
	{
		___signedData_5 = value;
		Il2CppCodeGenWriteBarrier((&___signedData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2_T4056456767_H
#ifndef X509CERTIFICATECOLLECTION_T1197680765_H
#define X509CERTIFICATECOLLECTION_T1197680765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct  X509CertificateCollection_t1197680765  : public CollectionBase_t1101587467
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T1197680765_H
#ifndef X509EXTENSION_T1320896183_H
#define X509EXTENSION_T1320896183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Extension
struct  X509Extension_t1320896183  : public AsnEncodedData_t463456204
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Extension::_critical
	bool ____critical_2;

public:
	inline static int32_t get_offset_of__critical_2() { return static_cast<int32_t>(offsetof(X509Extension_t1320896183, ____critical_2)); }
	inline bool get__critical_2() const { return ____critical_2; }
	inline bool* get_address_of__critical_2() { return &____critical_2; }
	inline void set__critical_2(bool value)
	{
		____critical_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_T1320896183_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OFFSET_T266882373_H
#define OFFSET_T266882373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Offset
#pragma pack(push, tp, 1)
struct  Offset_t266882373 
{
public:
	// System.UInt16 System.Uri/Offset::Scheme
	uint16_t ___Scheme_0;
	// System.UInt16 System.Uri/Offset::User
	uint16_t ___User_1;
	// System.UInt16 System.Uri/Offset::Host
	uint16_t ___Host_2;
	// System.UInt16 System.Uri/Offset::PortValue
	uint16_t ___PortValue_3;
	// System.UInt16 System.Uri/Offset::Path
	uint16_t ___Path_4;
	// System.UInt16 System.Uri/Offset::Query
	uint16_t ___Query_5;
	// System.UInt16 System.Uri/Offset::Fragment
	uint16_t ___Fragment_6;
	// System.UInt16 System.Uri/Offset::End
	uint16_t ___End_7;

public:
	inline static int32_t get_offset_of_Scheme_0() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___Scheme_0)); }
	inline uint16_t get_Scheme_0() const { return ___Scheme_0; }
	inline uint16_t* get_address_of_Scheme_0() { return &___Scheme_0; }
	inline void set_Scheme_0(uint16_t value)
	{
		___Scheme_0 = value;
	}

	inline static int32_t get_offset_of_User_1() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___User_1)); }
	inline uint16_t get_User_1() const { return ___User_1; }
	inline uint16_t* get_address_of_User_1() { return &___User_1; }
	inline void set_User_1(uint16_t value)
	{
		___User_1 = value;
	}

	inline static int32_t get_offset_of_Host_2() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___Host_2)); }
	inline uint16_t get_Host_2() const { return ___Host_2; }
	inline uint16_t* get_address_of_Host_2() { return &___Host_2; }
	inline void set_Host_2(uint16_t value)
	{
		___Host_2 = value;
	}

	inline static int32_t get_offset_of_PortValue_3() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___PortValue_3)); }
	inline uint16_t get_PortValue_3() const { return ___PortValue_3; }
	inline uint16_t* get_address_of_PortValue_3() { return &___PortValue_3; }
	inline void set_PortValue_3(uint16_t value)
	{
		___PortValue_3 = value;
	}

	inline static int32_t get_offset_of_Path_4() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___Path_4)); }
	inline uint16_t get_Path_4() const { return ___Path_4; }
	inline uint16_t* get_address_of_Path_4() { return &___Path_4; }
	inline void set_Path_4(uint16_t value)
	{
		___Path_4 = value;
	}

	inline static int32_t get_offset_of_Query_5() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___Query_5)); }
	inline uint16_t get_Query_5() const { return ___Query_5; }
	inline uint16_t* get_address_of_Query_5() { return &___Query_5; }
	inline void set_Query_5(uint16_t value)
	{
		___Query_5 = value;
	}

	inline static int32_t get_offset_of_Fragment_6() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___Fragment_6)); }
	inline uint16_t get_Fragment_6() const { return ___Fragment_6; }
	inline uint16_t* get_address_of_Fragment_6() { return &___Fragment_6; }
	inline void set_Fragment_6(uint16_t value)
	{
		___Fragment_6 = value;
	}

	inline static int32_t get_offset_of_End_7() { return static_cast<int32_t>(offsetof(Offset_t266882373, ___End_7)); }
	inline uint16_t get_End_7() const { return ___End_7; }
	inline uint16_t* get_address_of_End_7() { return &___End_7; }
	inline void set_End_7(uint16_t value)
	{
		___End_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFSET_T266882373_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1841601450__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef LOWERCASEMAPPING_T2153935826_H
#define LOWERCASEMAPPING_T2153935826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct  LowerCaseMapping_t2153935826 
{
public:
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMin
	Il2CppChar ____chMin_0;
	// System.Char System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_chMax
	Il2CppChar ____chMax_1;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_lcOp
	int32_t ____lcOp_2;
	// System.Int32 System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping::_data
	int32_t ____data_3;

public:
	inline static int32_t get_offset_of__chMin_0() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____chMin_0)); }
	inline Il2CppChar get__chMin_0() const { return ____chMin_0; }
	inline Il2CppChar* get_address_of__chMin_0() { return &____chMin_0; }
	inline void set__chMin_0(Il2CppChar value)
	{
		____chMin_0 = value;
	}

	inline static int32_t get_offset_of__chMax_1() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____chMax_1)); }
	inline Il2CppChar get__chMax_1() const { return ____chMax_1; }
	inline Il2CppChar* get_address_of__chMax_1() { return &____chMax_1; }
	inline void set__chMax_1(Il2CppChar value)
	{
		____chMax_1 = value;
	}

	inline static int32_t get_offset_of__lcOp_2() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____lcOp_2)); }
	inline int32_t get__lcOp_2() const { return ____lcOp_2; }
	inline int32_t* get_address_of__lcOp_2() { return &____lcOp_2; }
	inline void set__lcOp_2(int32_t value)
	{
		____lcOp_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(LowerCaseMapping_t2153935826, ____data_3)); }
	inline int32_t get__data_3() const { return ____data_3; }
	inline int32_t* get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(int32_t value)
	{
		____data_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2153935826_marshaled_pinvoke
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.RegexCharClass/LowerCaseMapping
struct LowerCaseMapping_t2153935826_marshaled_com
{
	uint8_t ____chMin_0;
	uint8_t ____chMax_1;
	int32_t ____lcOp_2;
	int32_t ____data_3;
};
#endif // LOWERCASEMAPPING_T2153935826_H
#ifndef DESCRIPTIONATTRIBUTE_T3207779672_H
#define DESCRIPTIONATTRIBUTE_T3207779672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DescriptionAttribute
struct  DescriptionAttribute_t3207779672  : public Attribute_t542643598
{
public:
	// System.String System.ComponentModel.DescriptionAttribute::description
	String_t* ___description_1;

public:
	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t3207779672, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}
};

struct DescriptionAttribute_t3207779672_StaticFields
{
public:
	// System.ComponentModel.DescriptionAttribute System.ComponentModel.DescriptionAttribute::Default
	DescriptionAttribute_t3207779672 * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t3207779672_StaticFields, ___Default_0)); }
	inline DescriptionAttribute_t3207779672 * get_Default_0() const { return ___Default_0; }
	inline DescriptionAttribute_t3207779672 ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(DescriptionAttribute_t3207779672 * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCRIPTIONATTRIBUTE_T3207779672_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_4;

public:
	inline static int32_t get_offset_of_dateData_4() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___dateData_4)); }
	inline uint64_t get_dateData_4() const { return ___dateData_4; }
	inline uint64_t* get_address_of_dateData_4() { return &___dateData_4; }
	inline void set_dateData_4(uint64_t value)
	{
		___dateData_4 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t3030399641* ___DaysToMonth365_0;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t3030399641* ___DaysToMonth366_1;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_2;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_3;

public:
	inline static int32_t get_offset_of_DaysToMonth365_0() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DaysToMonth365_0)); }
	inline Int32U5BU5D_t3030399641* get_DaysToMonth365_0() const { return ___DaysToMonth365_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_DaysToMonth365_0() { return &___DaysToMonth365_0; }
	inline void set_DaysToMonth365_0(Int32U5BU5D_t3030399641* value)
	{
		___DaysToMonth365_0 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_0), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_1() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DaysToMonth366_1)); }
	inline Int32U5BU5D_t3030399641* get_DaysToMonth366_1() const { return ___DaysToMonth366_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_DaysToMonth366_1() { return &___DaysToMonth366_1; }
	inline void set_DaysToMonth366_1(Int32U5BU5D_t3030399641* value)
	{
		___DaysToMonth366_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_1), value);
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_2)); }
	inline DateTime_t693205669  get_MinValue_2() const { return ___MinValue_2; }
	inline DateTime_t693205669 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(DateTime_t693205669  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of_MaxValue_3() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_3)); }
	inline DateTime_t693205669  get_MaxValue_3() const { return ___MaxValue_3; }
	inline DateTime_t693205669 * get_address_of_MaxValue_3() { return &___MaxValue_3; }
	inline void set_MaxValue_3(DateTime_t693205669  value)
	{
		___MaxValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef X500DISTINGUISHEDNAME_T452415348_H
#define X500DISTINGUISHEDNAME_T452415348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct  X500DistinguishedName_t452415348  : public AsnEncodedData_t463456204
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::name
	String_t* ___name_2;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X500DistinguishedName::canonEncoding
	ByteU5BU5D_t3397334013* ___canonEncoding_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t452415348, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_canonEncoding_3() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t452415348, ___canonEncoding_3)); }
	inline ByteU5BU5D_t3397334013* get_canonEncoding_3() const { return ___canonEncoding_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_canonEncoding_3() { return &___canonEncoding_3; }
	inline void set_canonEncoding_3(ByteU5BU5D_t3397334013* value)
	{
		___canonEncoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___canonEncoding_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAME_T452415348_H
#ifndef X509NAMETYPE_T2669466891_H
#define X509NAMETYPE_T2669466891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509NameType
struct  X509NameType_t2669466891 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509NameType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509NameType_t2669466891, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMETYPE_T2669466891_H
#ifndef X509FINDTYPE_T3221716179_H
#define X509FINDTYPE_T3221716179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509FindType
struct  X509FindType_t3221716179 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509FindType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509FindType_t3221716179, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509FINDTYPE_T3221716179_H
#ifndef X509KEYUSAGEFLAGS_T2461349531_H
#define X509KEYUSAGEFLAGS_T2461349531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t2461349531 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t2461349531, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T2461349531_H
#ifndef X509VERIFICATIONFLAGS_T2169036324_H
#define X509VERIFICATIONFLAGS_T2169036324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509VerificationFlags
struct  X509VerificationFlags_t2169036324 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509VerificationFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509VerificationFlags_t2169036324, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509VERIFICATIONFLAGS_T2169036324_H
#ifndef EXCEPTIONARGUMENT_T2966871835_H
#define EXCEPTIONARGUMENT_T2966871835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ExceptionArgument
struct  ExceptionArgument_t2966871835 
{
public:
	// System.Int32 System.ExceptionArgument::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionArgument_t2966871835, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONARGUMENT_T2966871835_H
#ifndef FORMATEXCEPTION_T2948921286_H
#define FORMATEXCEPTION_T2948921286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t2948921286  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T2948921286_H
#ifndef X509REVOCATIONMODE_T2065307963_H
#define X509REVOCATIONMODE_T2065307963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t2065307963 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509RevocationMode_t2065307963, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T2065307963_H
#ifndef REGEXOPTIONS_T2418259727_H
#define REGEXOPTIONS_T2418259727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t2418259727 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RegexOptions_t2418259727, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T2418259727_H
#ifndef SSLPROTOCOLS_T894678499_H
#define SSLPROTOCOLS_T894678499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t894678499 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslProtocols_t894678499, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T894678499_H
#ifndef X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T110301003_H
#define X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T110301003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
struct  X509SubjectKeyIdentifierHashAlgorithm_t110301003 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierHashAlgorithm_t110301003, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIERHASHALGORITHM_T110301003_H
#ifndef X509REVOCATIONFLAG_T2166064554_H
#define X509REVOCATIONFLAG_T2166064554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationFlag
struct  X509RevocationFlag_t2166064554 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationFlag::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509RevocationFlag_t2166064554, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONFLAG_T2166064554_H
#ifndef TYPECONVERTER_T745995970_H
#define TYPECONVERTER_T745995970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t745995970  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t745995970_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t745995970_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T745995970_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_0)); }
	inline TimeSpan_t3430258949  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_t3430258949  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_t3430258949  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_t3430258949  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_2)); }
	inline TimeSpan_t3430258949  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_t3430258949  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef ASNDECODESTATUS_T1962003286_H
#define ASNDECODESTATUS_T1962003286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t1962003286 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t1962003286, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T1962003286_H
#ifndef URIIDNSCOPE_T761062207_H
#define URIIDNSCOPE_T761062207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriIdnScope
struct  UriIdnScope_t761062207 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_t761062207, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIIDNSCOPE_T761062207_H
#ifndef OIDGROUP_T4038341371_H
#define OIDGROUP_T4038341371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidGroup
struct  OidGroup_t4038341371 
{
public:
	// System.Int32 System.Security.Cryptography.OidGroup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OidGroup_t4038341371, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDGROUP_T4038341371_H
#ifndef SECTRUSTRESULT_T1335602280_H
#define SECTRUSTRESULT_T1335602280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OSX509Certificates/SecTrustResult
struct  SecTrustResult_t1335602280 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OSX509Certificates/SecTrustResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SecTrustResult_t1335602280, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTRUSTRESULT_T1335602280_H
#ifndef STORENAME_T2183514610_H
#define STORENAME_T2183514610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreName
struct  StoreName_t2183514610 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreName::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StoreName_t2183514610, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORENAME_T2183514610_H
#ifndef STORELOCATION_T1570828128_H
#define STORELOCATION_T1570828128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreLocation
struct  StoreLocation_t1570828128 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StoreLocation_t1570828128, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELOCATION_T1570828128_H
#ifndef OPENFLAGS_T2370524385_H
#define OPENFLAGS_T2370524385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OpenFlags
struct  OpenFlags_t2370524385 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OpenFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OpenFlags_t2370524385, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENFLAGS_T2370524385_H
#ifndef IOOPERATION_T4250055067_H
#define IOOPERATION_T4250055067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOOperation
struct  IOOperation_t4250055067 
{
public:
	// System.Int32 System.IOOperation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IOOperation_t4250055067, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOOPERATION_T4250055067_H
#ifndef URISYNTAXFLAGS_T1242716474_H
#define URISYNTAXFLAGS_T1242716474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriSyntaxFlags
struct  UriSyntaxFlags_t1242716474 
{
public:
	// System.Int32 System.UriSyntaxFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriSyntaxFlags_t1242716474, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URISYNTAXFLAGS_T1242716474_H
#ifndef URIQUIRKSVERSION_T4233729352_H
#define URIQUIRKSVERSION_T4233729352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser/UriQuirksVersion
struct  UriQuirksVersion_t4233729352 
{
public:
	// System.Int32 System.UriParser/UriQuirksVersion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriQuirksVersion_t4233729352, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIQUIRKSVERSION_T4233729352_H
#ifndef URIHOSTNAMETYPE_T2148127109_H
#define URIHOSTNAMETYPE_T2148127109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHostNameType
struct  UriHostNameType_t2148127109 
{
public:
	// System.Int32 System.UriHostNameType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriHostNameType_t2148127109, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHOSTNAMETYPE_T2148127109_H
#ifndef PARSINGERROR_T953959262_H
#define PARSINGERROR_T953959262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ParsingError
struct  ParsingError_t953959262 
{
public:
	// System.Int32 System.ParsingError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingError_t953959262, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGERROR_T953959262_H
#ifndef SRDESCRIPTIONATTRIBUTE_T431985547_H
#define SRDESCRIPTIONATTRIBUTE_T431985547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SRDescriptionAttribute
struct  SRDescriptionAttribute_t431985547  : public DescriptionAttribute_t3207779672
{
public:
	// System.Boolean System.SRDescriptionAttribute::isReplaced
	bool ___isReplaced_2;

public:
	inline static int32_t get_offset_of_isReplaced_2() { return static_cast<int32_t>(offsetof(SRDescriptionAttribute_t431985547, ___isReplaced_2)); }
	inline bool get_isReplaced_2() const { return ___isReplaced_2; }
	inline bool* get_address_of_isReplaced_2() { return &___isReplaced_2; }
	inline void set_isReplaced_2(bool value)
	{
		___isReplaced_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRDESCRIPTIONATTRIBUTE_T431985547_H
#ifndef UNESCAPEMODE_T584481035_H
#define UNESCAPEMODE_T584481035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnescapeMode
struct  UnescapeMode_t584481035 
{
public:
	// System.Int32 System.UnescapeMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnescapeMode_t584481035, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNESCAPEMODE_T584481035_H
#ifndef URIINFO_T4047916940_H
#define URIINFO_T4047916940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/UriInfo
struct  UriInfo_t4047916940  : public RuntimeObject
{
public:
	// System.String System.Uri/UriInfo::Host
	String_t* ___Host_0;
	// System.String System.Uri/UriInfo::ScopeId
	String_t* ___ScopeId_1;
	// System.String System.Uri/UriInfo::String
	String_t* ___String_2;
	// System.Uri/Offset System.Uri/UriInfo::Offset
	Offset_t266882373  ___Offset_3;
	// System.String System.Uri/UriInfo::DnsSafeHost
	String_t* ___DnsSafeHost_4;
	// System.Uri/MoreInfo System.Uri/UriInfo::MoreInfo
	MoreInfo_t2595315311 * ___MoreInfo_5;

public:
	inline static int32_t get_offset_of_Host_0() { return static_cast<int32_t>(offsetof(UriInfo_t4047916940, ___Host_0)); }
	inline String_t* get_Host_0() const { return ___Host_0; }
	inline String_t** get_address_of_Host_0() { return &___Host_0; }
	inline void set_Host_0(String_t* value)
	{
		___Host_0 = value;
		Il2CppCodeGenWriteBarrier((&___Host_0), value);
	}

	inline static int32_t get_offset_of_ScopeId_1() { return static_cast<int32_t>(offsetof(UriInfo_t4047916940, ___ScopeId_1)); }
	inline String_t* get_ScopeId_1() const { return ___ScopeId_1; }
	inline String_t** get_address_of_ScopeId_1() { return &___ScopeId_1; }
	inline void set_ScopeId_1(String_t* value)
	{
		___ScopeId_1 = value;
		Il2CppCodeGenWriteBarrier((&___ScopeId_1), value);
	}

	inline static int32_t get_offset_of_String_2() { return static_cast<int32_t>(offsetof(UriInfo_t4047916940, ___String_2)); }
	inline String_t* get_String_2() const { return ___String_2; }
	inline String_t** get_address_of_String_2() { return &___String_2; }
	inline void set_String_2(String_t* value)
	{
		___String_2 = value;
		Il2CppCodeGenWriteBarrier((&___String_2), value);
	}

	inline static int32_t get_offset_of_Offset_3() { return static_cast<int32_t>(offsetof(UriInfo_t4047916940, ___Offset_3)); }
	inline Offset_t266882373  get_Offset_3() const { return ___Offset_3; }
	inline Offset_t266882373 * get_address_of_Offset_3() { return &___Offset_3; }
	inline void set_Offset_3(Offset_t266882373  value)
	{
		___Offset_3 = value;
	}

	inline static int32_t get_offset_of_DnsSafeHost_4() { return static_cast<int32_t>(offsetof(UriInfo_t4047916940, ___DnsSafeHost_4)); }
	inline String_t* get_DnsSafeHost_4() const { return ___DnsSafeHost_4; }
	inline String_t** get_address_of_DnsSafeHost_4() { return &___DnsSafeHost_4; }
	inline void set_DnsSafeHost_4(String_t* value)
	{
		___DnsSafeHost_4 = value;
		Il2CppCodeGenWriteBarrier((&___DnsSafeHost_4), value);
	}

	inline static int32_t get_offset_of_MoreInfo_5() { return static_cast<int32_t>(offsetof(UriInfo_t4047916940, ___MoreInfo_5)); }
	inline MoreInfo_t2595315311 * get_MoreInfo_5() const { return ___MoreInfo_5; }
	inline MoreInfo_t2595315311 ** get_address_of_MoreInfo_5() { return &___MoreInfo_5; }
	inline void set_MoreInfo_5(MoreInfo_t2595315311 * value)
	{
		___MoreInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___MoreInfo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIINFO_T4047916940_H
#ifndef CHECK_T363272550_H
#define CHECK_T363272550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Check
struct  Check_t363272550 
{
public:
	// System.Int32 System.Uri/Check::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Check_t363272550, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T363272550_H
#ifndef X509CERTIFICATE2IMPLMONO_T2009068154_H
#define X509CERTIFICATE2IMPLMONO_T2009068154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono
struct  X509Certificate2ImplMono_t2009068154  : public X509Certificate2Impl_t2703153821
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_archived
	bool ____archived_1;
	// System.Security.Cryptography.X509Certificates.X509ExtensionCollection System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_extensions
	X509ExtensionCollection_t650873211 * ____extensions_2;
	// System.Security.Cryptography.X509Certificates.PublicKey System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_publicKey
	PublicKey_t870392 * ____publicKey_3;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::issuer_name
	X500DistinguishedName_t452415348 * ___issuer_name_4;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::subject_name
	X500DistinguishedName_t452415348 * ___subject_name_5;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::signature_algorithm
	Oid_t3221867120 * ___signature_algorithm_6;
	// System.Security.Cryptography.X509Certificates.X509CertificateImplCollection System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::intermediateCerts
	X509CertificateImplCollection_t255811311 * ___intermediateCerts_7;
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::_cert
	X509Certificate_t324051958 * ____cert_8;

public:
	inline static int32_t get_offset_of__archived_1() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____archived_1)); }
	inline bool get__archived_1() const { return ____archived_1; }
	inline bool* get_address_of__archived_1() { return &____archived_1; }
	inline void set__archived_1(bool value)
	{
		____archived_1 = value;
	}

	inline static int32_t get_offset_of__extensions_2() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____extensions_2)); }
	inline X509ExtensionCollection_t650873211 * get__extensions_2() const { return ____extensions_2; }
	inline X509ExtensionCollection_t650873211 ** get_address_of__extensions_2() { return &____extensions_2; }
	inline void set__extensions_2(X509ExtensionCollection_t650873211 * value)
	{
		____extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_2), value);
	}

	inline static int32_t get_offset_of__publicKey_3() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____publicKey_3)); }
	inline PublicKey_t870392 * get__publicKey_3() const { return ____publicKey_3; }
	inline PublicKey_t870392 ** get_address_of__publicKey_3() { return &____publicKey_3; }
	inline void set__publicKey_3(PublicKey_t870392 * value)
	{
		____publicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____publicKey_3), value);
	}

	inline static int32_t get_offset_of_issuer_name_4() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___issuer_name_4)); }
	inline X500DistinguishedName_t452415348 * get_issuer_name_4() const { return ___issuer_name_4; }
	inline X500DistinguishedName_t452415348 ** get_address_of_issuer_name_4() { return &___issuer_name_4; }
	inline void set_issuer_name_4(X500DistinguishedName_t452415348 * value)
	{
		___issuer_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_4), value);
	}

	inline static int32_t get_offset_of_subject_name_5() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___subject_name_5)); }
	inline X500DistinguishedName_t452415348 * get_subject_name_5() const { return ___subject_name_5; }
	inline X500DistinguishedName_t452415348 ** get_address_of_subject_name_5() { return &___subject_name_5; }
	inline void set_subject_name_5(X500DistinguishedName_t452415348 * value)
	{
		___subject_name_5 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_5), value);
	}

	inline static int32_t get_offset_of_signature_algorithm_6() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___signature_algorithm_6)); }
	inline Oid_t3221867120 * get_signature_algorithm_6() const { return ___signature_algorithm_6; }
	inline Oid_t3221867120 ** get_address_of_signature_algorithm_6() { return &___signature_algorithm_6; }
	inline void set_signature_algorithm_6(Oid_t3221867120 * value)
	{
		___signature_algorithm_6 = value;
		Il2CppCodeGenWriteBarrier((&___signature_algorithm_6), value);
	}

	inline static int32_t get_offset_of_intermediateCerts_7() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ___intermediateCerts_7)); }
	inline X509CertificateImplCollection_t255811311 * get_intermediateCerts_7() const { return ___intermediateCerts_7; }
	inline X509CertificateImplCollection_t255811311 ** get_address_of_intermediateCerts_7() { return &___intermediateCerts_7; }
	inline void set_intermediateCerts_7(X509CertificateImplCollection_t255811311 * value)
	{
		___intermediateCerts_7 = value;
		Il2CppCodeGenWriteBarrier((&___intermediateCerts_7), value);
	}

	inline static int32_t get_offset_of__cert_8() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154, ____cert_8)); }
	inline X509Certificate_t324051958 * get__cert_8() const { return ____cert_8; }
	inline X509Certificate_t324051958 ** get_address_of__cert_8() { return &____cert_8; }
	inline void set__cert_8(X509Certificate_t324051958 * value)
	{
		____cert_8 = value;
		Il2CppCodeGenWriteBarrier((&____cert_8), value);
	}
};

struct X509Certificate2ImplMono_t2009068154_StaticFields
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::empty_error
	String_t* ___empty_error_9;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::commonName
	ByteU5BU5D_t3397334013* ___commonName_10;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::email
	ByteU5BU5D_t3397334013* ___email_11;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2ImplMono::signedData
	ByteU5BU5D_t3397334013* ___signedData_12;

public:
	inline static int32_t get_offset_of_empty_error_9() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___empty_error_9)); }
	inline String_t* get_empty_error_9() const { return ___empty_error_9; }
	inline String_t** get_address_of_empty_error_9() { return &___empty_error_9; }
	inline void set_empty_error_9(String_t* value)
	{
		___empty_error_9 = value;
		Il2CppCodeGenWriteBarrier((&___empty_error_9), value);
	}

	inline static int32_t get_offset_of_commonName_10() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___commonName_10)); }
	inline ByteU5BU5D_t3397334013* get_commonName_10() const { return ___commonName_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_commonName_10() { return &___commonName_10; }
	inline void set_commonName_10(ByteU5BU5D_t3397334013* value)
	{
		___commonName_10 = value;
		Il2CppCodeGenWriteBarrier((&___commonName_10), value);
	}

	inline static int32_t get_offset_of_email_11() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___email_11)); }
	inline ByteU5BU5D_t3397334013* get_email_11() const { return ___email_11; }
	inline ByteU5BU5D_t3397334013** get_address_of_email_11() { return &___email_11; }
	inline void set_email_11(ByteU5BU5D_t3397334013* value)
	{
		___email_11 = value;
		Il2CppCodeGenWriteBarrier((&___email_11), value);
	}

	inline static int32_t get_offset_of_signedData_12() { return static_cast<int32_t>(offsetof(X509Certificate2ImplMono_t2009068154_StaticFields, ___signedData_12)); }
	inline ByteU5BU5D_t3397334013* get_signedData_12() const { return ___signedData_12; }
	inline ByteU5BU5D_t3397334013** get_address_of_signedData_12() { return &___signedData_12; }
	inline void set_signedData_12(ByteU5BU5D_t3397334013* value)
	{
		___signedData_12 = value;
		Il2CppCodeGenWriteBarrier((&___signedData_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2IMPLMONO_T2009068154_H
#ifndef EXCEPTIONRESOURCE_T2812258640_H
#define EXCEPTIONRESOURCE_T2812258640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ExceptionResource
struct  ExceptionResource_t2812258640 
{
public:
	// System.Int32 System.ExceptionResource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionResource_t2812258640, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONRESOURCE_T2812258640_H
#ifndef X509CHAINSTATUSFLAGS_T480677120_H
#define X509CHAINSTATUSFLAGS_T480677120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
struct  X509ChainStatusFlags_t480677120 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t480677120, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T480677120_H
#ifndef FLAGS_T455382755_H
#define FLAGS_T455382755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Flags
struct  Flags_t455382755 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t455382755, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T455382755_H
#ifndef URICOMPONENTS_T3302767704_H
#define URICOMPONENTS_T3302767704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriComponents
struct  UriComponents_t3302767704 
{
public:
	// System.Int32 System.UriComponents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriComponents_t3302767704, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URICOMPONENTS_T3302767704_H
#ifndef X500DISTINGUISHEDNAMEFLAGS_T2005802885_H
#define X500DISTINGUISHEDNAMEFLAGS_T2005802885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
struct  X500DistinguishedNameFlags_t2005802885 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X500DistinguishedNameFlags_t2005802885, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAMEFLAGS_T2005802885_H
#ifndef X509CERTIFICATE2COLLECTION_T1108969367_H
#define X509CERTIFICATE2COLLECTION_T1108969367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct  X509Certificate2Collection_t1108969367  : public X509CertificateCollection_t1197680765
{
public:

public:
};

struct X509Certificate2Collection_t1108969367_StaticFields
{
public:
	// System.String[] System.Security.Cryptography.X509Certificates.X509Certificate2Collection::newline_split
	StringU5BU5D_t1642385972* ___newline_split_1;

public:
	inline static int32_t get_offset_of_newline_split_1() { return static_cast<int32_t>(offsetof(X509Certificate2Collection_t1108969367_StaticFields, ___newline_split_1)); }
	inline StringU5BU5D_t1642385972* get_newline_split_1() const { return ___newline_split_1; }
	inline StringU5BU5D_t1642385972** get_address_of_newline_split_1() { return &___newline_split_1; }
	inline void set_newline_split_1(StringU5BU5D_t1642385972* value)
	{
		___newline_split_1 = value;
		Il2CppCodeGenWriteBarrier((&___newline_split_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2COLLECTION_T1108969367_H
#ifndef URIFORMAT_T2764505239_H
#define URIFORMAT_T2764505239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormat
struct  UriFormat_t2764505239 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t2764505239, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMAT_T2764505239_H
#ifndef URIKIND_T1128731744_H
#define URIKIND_T1128731744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriKind
struct  UriKind_t1128731744 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_t1128731744, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIKIND_T1128731744_H
#ifndef URIFORMATEXCEPTION_T3682083048_H
#define URIFORMATEXCEPTION_T3682083048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormatException
struct  UriFormatException_t3682083048  : public FormatException_t2948921286
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMATEXCEPTION_T3682083048_H
#ifndef URIPARSER_T1012511323_H
#define URIPARSER_T1012511323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser
struct  UriParser_t1012511323  : public RuntimeObject
{
public:
	// System.UriSyntaxFlags System.UriParser::m_Flags
	int32_t ___m_Flags_2;
	// System.UriSyntaxFlags modreq(System.Runtime.CompilerServices.IsVolatile) System.UriParser::m_UpdatableFlags
	int32_t ___m_UpdatableFlags_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.UriParser::m_UpdatableFlagsUsed
	bool ___m_UpdatableFlagsUsed_4;
	// System.Int32 System.UriParser::m_Port
	int32_t ___m_Port_5;
	// System.String System.UriParser::m_Scheme
	String_t* ___m_Scheme_6;

public:
	inline static int32_t get_offset_of_m_Flags_2() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_Flags_2)); }
	inline int32_t get_m_Flags_2() const { return ___m_Flags_2; }
	inline int32_t* get_address_of_m_Flags_2() { return &___m_Flags_2; }
	inline void set_m_Flags_2(int32_t value)
	{
		___m_Flags_2 = value;
	}

	inline static int32_t get_offset_of_m_UpdatableFlags_3() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_UpdatableFlags_3)); }
	inline int32_t get_m_UpdatableFlags_3() const { return ___m_UpdatableFlags_3; }
	inline int32_t* get_address_of_m_UpdatableFlags_3() { return &___m_UpdatableFlags_3; }
	inline void set_m_UpdatableFlags_3(int32_t value)
	{
		___m_UpdatableFlags_3 = value;
	}

	inline static int32_t get_offset_of_m_UpdatableFlagsUsed_4() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_UpdatableFlagsUsed_4)); }
	inline bool get_m_UpdatableFlagsUsed_4() const { return ___m_UpdatableFlagsUsed_4; }
	inline bool* get_address_of_m_UpdatableFlagsUsed_4() { return &___m_UpdatableFlagsUsed_4; }
	inline void set_m_UpdatableFlagsUsed_4(bool value)
	{
		___m_UpdatableFlagsUsed_4 = value;
	}

	inline static int32_t get_offset_of_m_Port_5() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_Port_5)); }
	inline int32_t get_m_Port_5() const { return ___m_Port_5; }
	inline int32_t* get_address_of_m_Port_5() { return &___m_Port_5; }
	inline void set_m_Port_5(int32_t value)
	{
		___m_Port_5 = value;
	}

	inline static int32_t get_offset_of_m_Scheme_6() { return static_cast<int32_t>(offsetof(UriParser_t1012511323, ___m_Scheme_6)); }
	inline String_t* get_m_Scheme_6() const { return ___m_Scheme_6; }
	inline String_t** get_address_of_m_Scheme_6() { return &___m_Scheme_6; }
	inline void set_m_Scheme_6(String_t* value)
	{
		___m_Scheme_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scheme_6), value);
	}
};

struct UriParser_t1012511323_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.UriParser> System.UriParser::m_Table
	Dictionary_2_t2927290585 * ___m_Table_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.UriParser> System.UriParser::m_TempTable
	Dictionary_2_t2927290585 * ___m_TempTable_1;
	// System.UriParser System.UriParser::HttpUri
	UriParser_t1012511323 * ___HttpUri_7;
	// System.UriParser System.UriParser::HttpsUri
	UriParser_t1012511323 * ___HttpsUri_8;
	// System.UriParser System.UriParser::WsUri
	UriParser_t1012511323 * ___WsUri_9;
	// System.UriParser System.UriParser::WssUri
	UriParser_t1012511323 * ___WssUri_10;
	// System.UriParser System.UriParser::FtpUri
	UriParser_t1012511323 * ___FtpUri_11;
	// System.UriParser System.UriParser::FileUri
	UriParser_t1012511323 * ___FileUri_12;
	// System.UriParser System.UriParser::GopherUri
	UriParser_t1012511323 * ___GopherUri_13;
	// System.UriParser System.UriParser::NntpUri
	UriParser_t1012511323 * ___NntpUri_14;
	// System.UriParser System.UriParser::NewsUri
	UriParser_t1012511323 * ___NewsUri_15;
	// System.UriParser System.UriParser::MailToUri
	UriParser_t1012511323 * ___MailToUri_16;
	// System.UriParser System.UriParser::UuidUri
	UriParser_t1012511323 * ___UuidUri_17;
	// System.UriParser System.UriParser::TelnetUri
	UriParser_t1012511323 * ___TelnetUri_18;
	// System.UriParser System.UriParser::LdapUri
	UriParser_t1012511323 * ___LdapUri_19;
	// System.UriParser System.UriParser::NetTcpUri
	UriParser_t1012511323 * ___NetTcpUri_20;
	// System.UriParser System.UriParser::NetPipeUri
	UriParser_t1012511323 * ___NetPipeUri_21;
	// System.UriParser System.UriParser::VsMacrosUri
	UriParser_t1012511323 * ___VsMacrosUri_22;
	// System.UriParser/UriQuirksVersion System.UriParser::s_QuirksVersion
	int32_t ___s_QuirksVersion_23;
	// System.UriSyntaxFlags System.UriParser::HttpSyntaxFlags
	int32_t ___HttpSyntaxFlags_24;
	// System.UriSyntaxFlags System.UriParser::FileSyntaxFlags
	int32_t ___FileSyntaxFlags_25;

public:
	inline static int32_t get_offset_of_m_Table_0() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___m_Table_0)); }
	inline Dictionary_2_t2927290585 * get_m_Table_0() const { return ___m_Table_0; }
	inline Dictionary_2_t2927290585 ** get_address_of_m_Table_0() { return &___m_Table_0; }
	inline void set_m_Table_0(Dictionary_2_t2927290585 * value)
	{
		___m_Table_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Table_0), value);
	}

	inline static int32_t get_offset_of_m_TempTable_1() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___m_TempTable_1)); }
	inline Dictionary_2_t2927290585 * get_m_TempTable_1() const { return ___m_TempTable_1; }
	inline Dictionary_2_t2927290585 ** get_address_of_m_TempTable_1() { return &___m_TempTable_1; }
	inline void set_m_TempTable_1(Dictionary_2_t2927290585 * value)
	{
		___m_TempTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempTable_1), value);
	}

	inline static int32_t get_offset_of_HttpUri_7() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___HttpUri_7)); }
	inline UriParser_t1012511323 * get_HttpUri_7() const { return ___HttpUri_7; }
	inline UriParser_t1012511323 ** get_address_of_HttpUri_7() { return &___HttpUri_7; }
	inline void set_HttpUri_7(UriParser_t1012511323 * value)
	{
		___HttpUri_7 = value;
		Il2CppCodeGenWriteBarrier((&___HttpUri_7), value);
	}

	inline static int32_t get_offset_of_HttpsUri_8() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___HttpsUri_8)); }
	inline UriParser_t1012511323 * get_HttpsUri_8() const { return ___HttpsUri_8; }
	inline UriParser_t1012511323 ** get_address_of_HttpsUri_8() { return &___HttpsUri_8; }
	inline void set_HttpsUri_8(UriParser_t1012511323 * value)
	{
		___HttpsUri_8 = value;
		Il2CppCodeGenWriteBarrier((&___HttpsUri_8), value);
	}

	inline static int32_t get_offset_of_WsUri_9() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___WsUri_9)); }
	inline UriParser_t1012511323 * get_WsUri_9() const { return ___WsUri_9; }
	inline UriParser_t1012511323 ** get_address_of_WsUri_9() { return &___WsUri_9; }
	inline void set_WsUri_9(UriParser_t1012511323 * value)
	{
		___WsUri_9 = value;
		Il2CppCodeGenWriteBarrier((&___WsUri_9), value);
	}

	inline static int32_t get_offset_of_WssUri_10() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___WssUri_10)); }
	inline UriParser_t1012511323 * get_WssUri_10() const { return ___WssUri_10; }
	inline UriParser_t1012511323 ** get_address_of_WssUri_10() { return &___WssUri_10; }
	inline void set_WssUri_10(UriParser_t1012511323 * value)
	{
		___WssUri_10 = value;
		Il2CppCodeGenWriteBarrier((&___WssUri_10), value);
	}

	inline static int32_t get_offset_of_FtpUri_11() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___FtpUri_11)); }
	inline UriParser_t1012511323 * get_FtpUri_11() const { return ___FtpUri_11; }
	inline UriParser_t1012511323 ** get_address_of_FtpUri_11() { return &___FtpUri_11; }
	inline void set_FtpUri_11(UriParser_t1012511323 * value)
	{
		___FtpUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___FtpUri_11), value);
	}

	inline static int32_t get_offset_of_FileUri_12() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___FileUri_12)); }
	inline UriParser_t1012511323 * get_FileUri_12() const { return ___FileUri_12; }
	inline UriParser_t1012511323 ** get_address_of_FileUri_12() { return &___FileUri_12; }
	inline void set_FileUri_12(UriParser_t1012511323 * value)
	{
		___FileUri_12 = value;
		Il2CppCodeGenWriteBarrier((&___FileUri_12), value);
	}

	inline static int32_t get_offset_of_GopherUri_13() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___GopherUri_13)); }
	inline UriParser_t1012511323 * get_GopherUri_13() const { return ___GopherUri_13; }
	inline UriParser_t1012511323 ** get_address_of_GopherUri_13() { return &___GopherUri_13; }
	inline void set_GopherUri_13(UriParser_t1012511323 * value)
	{
		___GopherUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___GopherUri_13), value);
	}

	inline static int32_t get_offset_of_NntpUri_14() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NntpUri_14)); }
	inline UriParser_t1012511323 * get_NntpUri_14() const { return ___NntpUri_14; }
	inline UriParser_t1012511323 ** get_address_of_NntpUri_14() { return &___NntpUri_14; }
	inline void set_NntpUri_14(UriParser_t1012511323 * value)
	{
		___NntpUri_14 = value;
		Il2CppCodeGenWriteBarrier((&___NntpUri_14), value);
	}

	inline static int32_t get_offset_of_NewsUri_15() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NewsUri_15)); }
	inline UriParser_t1012511323 * get_NewsUri_15() const { return ___NewsUri_15; }
	inline UriParser_t1012511323 ** get_address_of_NewsUri_15() { return &___NewsUri_15; }
	inline void set_NewsUri_15(UriParser_t1012511323 * value)
	{
		___NewsUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___NewsUri_15), value);
	}

	inline static int32_t get_offset_of_MailToUri_16() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___MailToUri_16)); }
	inline UriParser_t1012511323 * get_MailToUri_16() const { return ___MailToUri_16; }
	inline UriParser_t1012511323 ** get_address_of_MailToUri_16() { return &___MailToUri_16; }
	inline void set_MailToUri_16(UriParser_t1012511323 * value)
	{
		___MailToUri_16 = value;
		Il2CppCodeGenWriteBarrier((&___MailToUri_16), value);
	}

	inline static int32_t get_offset_of_UuidUri_17() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___UuidUri_17)); }
	inline UriParser_t1012511323 * get_UuidUri_17() const { return ___UuidUri_17; }
	inline UriParser_t1012511323 ** get_address_of_UuidUri_17() { return &___UuidUri_17; }
	inline void set_UuidUri_17(UriParser_t1012511323 * value)
	{
		___UuidUri_17 = value;
		Il2CppCodeGenWriteBarrier((&___UuidUri_17), value);
	}

	inline static int32_t get_offset_of_TelnetUri_18() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___TelnetUri_18)); }
	inline UriParser_t1012511323 * get_TelnetUri_18() const { return ___TelnetUri_18; }
	inline UriParser_t1012511323 ** get_address_of_TelnetUri_18() { return &___TelnetUri_18; }
	inline void set_TelnetUri_18(UriParser_t1012511323 * value)
	{
		___TelnetUri_18 = value;
		Il2CppCodeGenWriteBarrier((&___TelnetUri_18), value);
	}

	inline static int32_t get_offset_of_LdapUri_19() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___LdapUri_19)); }
	inline UriParser_t1012511323 * get_LdapUri_19() const { return ___LdapUri_19; }
	inline UriParser_t1012511323 ** get_address_of_LdapUri_19() { return &___LdapUri_19; }
	inline void set_LdapUri_19(UriParser_t1012511323 * value)
	{
		___LdapUri_19 = value;
		Il2CppCodeGenWriteBarrier((&___LdapUri_19), value);
	}

	inline static int32_t get_offset_of_NetTcpUri_20() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NetTcpUri_20)); }
	inline UriParser_t1012511323 * get_NetTcpUri_20() const { return ___NetTcpUri_20; }
	inline UriParser_t1012511323 ** get_address_of_NetTcpUri_20() { return &___NetTcpUri_20; }
	inline void set_NetTcpUri_20(UriParser_t1012511323 * value)
	{
		___NetTcpUri_20 = value;
		Il2CppCodeGenWriteBarrier((&___NetTcpUri_20), value);
	}

	inline static int32_t get_offset_of_NetPipeUri_21() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___NetPipeUri_21)); }
	inline UriParser_t1012511323 * get_NetPipeUri_21() const { return ___NetPipeUri_21; }
	inline UriParser_t1012511323 ** get_address_of_NetPipeUri_21() { return &___NetPipeUri_21; }
	inline void set_NetPipeUri_21(UriParser_t1012511323 * value)
	{
		___NetPipeUri_21 = value;
		Il2CppCodeGenWriteBarrier((&___NetPipeUri_21), value);
	}

	inline static int32_t get_offset_of_VsMacrosUri_22() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___VsMacrosUri_22)); }
	inline UriParser_t1012511323 * get_VsMacrosUri_22() const { return ___VsMacrosUri_22; }
	inline UriParser_t1012511323 ** get_address_of_VsMacrosUri_22() { return &___VsMacrosUri_22; }
	inline void set_VsMacrosUri_22(UriParser_t1012511323 * value)
	{
		___VsMacrosUri_22 = value;
		Il2CppCodeGenWriteBarrier((&___VsMacrosUri_22), value);
	}

	inline static int32_t get_offset_of_s_QuirksVersion_23() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___s_QuirksVersion_23)); }
	inline int32_t get_s_QuirksVersion_23() const { return ___s_QuirksVersion_23; }
	inline int32_t* get_address_of_s_QuirksVersion_23() { return &___s_QuirksVersion_23; }
	inline void set_s_QuirksVersion_23(int32_t value)
	{
		___s_QuirksVersion_23 = value;
	}

	inline static int32_t get_offset_of_HttpSyntaxFlags_24() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___HttpSyntaxFlags_24)); }
	inline int32_t get_HttpSyntaxFlags_24() const { return ___HttpSyntaxFlags_24; }
	inline int32_t* get_address_of_HttpSyntaxFlags_24() { return &___HttpSyntaxFlags_24; }
	inline void set_HttpSyntaxFlags_24(int32_t value)
	{
		___HttpSyntaxFlags_24 = value;
	}

	inline static int32_t get_offset_of_FileSyntaxFlags_25() { return static_cast<int32_t>(offsetof(UriParser_t1012511323_StaticFields, ___FileSyntaxFlags_25)); }
	inline int32_t get_FileSyntaxFlags_25() const { return ___FileSyntaxFlags_25; }
	inline int32_t* get_address_of_FileSyntaxFlags_25() { return &___FileSyntaxFlags_25; }
	inline void set_FileSyntaxFlags_25(int32_t value)
	{
		___FileSyntaxFlags_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIPARSER_T1012511323_H
#ifndef URI_T19570940_H
#define URI_T19570940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t19570940  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_13;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_14;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t1012511323 * ___m_Syntax_15;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_16;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_17;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_t4047916940 * ___m_Info_18;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_19;

public:
	inline static int32_t get_offset_of_m_String_13() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_String_13)); }
	inline String_t* get_m_String_13() const { return ___m_String_13; }
	inline String_t** get_address_of_m_String_13() { return &___m_String_13; }
	inline void set_m_String_13(String_t* value)
	{
		___m_String_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_String_13), value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_14() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_originalUnicodeString_14)); }
	inline String_t* get_m_originalUnicodeString_14() const { return ___m_originalUnicodeString_14; }
	inline String_t** get_address_of_m_originalUnicodeString_14() { return &___m_originalUnicodeString_14; }
	inline void set_m_originalUnicodeString_14(String_t* value)
	{
		___m_originalUnicodeString_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalUnicodeString_14), value);
	}

	inline static int32_t get_offset_of_m_Syntax_15() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_Syntax_15)); }
	inline UriParser_t1012511323 * get_m_Syntax_15() const { return ___m_Syntax_15; }
	inline UriParser_t1012511323 ** get_address_of_m_Syntax_15() { return &___m_Syntax_15; }
	inline void set_m_Syntax_15(UriParser_t1012511323 * value)
	{
		___m_Syntax_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Syntax_15), value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_16() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_DnsSafeHost_16)); }
	inline String_t* get_m_DnsSafeHost_16() const { return ___m_DnsSafeHost_16; }
	inline String_t** get_address_of_m_DnsSafeHost_16() { return &___m_DnsSafeHost_16; }
	inline void set_m_DnsSafeHost_16(String_t* value)
	{
		___m_DnsSafeHost_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_DnsSafeHost_16), value);
	}

	inline static int32_t get_offset_of_m_Flags_17() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_Flags_17)); }
	inline uint64_t get_m_Flags_17() const { return ___m_Flags_17; }
	inline uint64_t* get_address_of_m_Flags_17() { return &___m_Flags_17; }
	inline void set_m_Flags_17(uint64_t value)
	{
		___m_Flags_17 = value;
	}

	inline static int32_t get_offset_of_m_Info_18() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_Info_18)); }
	inline UriInfo_t4047916940 * get_m_Info_18() const { return ___m_Info_18; }
	inline UriInfo_t4047916940 ** get_address_of_m_Info_18() { return &___m_Info_18; }
	inline void set_m_Info_18(UriInfo_t4047916940 * value)
	{
		___m_Info_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Info_18), value);
	}

	inline static int32_t get_offset_of_m_iriParsing_19() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___m_iriParsing_19)); }
	inline bool get_m_iriParsing_19() const { return ___m_iriParsing_19; }
	inline bool* get_address_of_m_iriParsing_19() { return &___m_iriParsing_19; }
	inline void set_m_iriParsing_19(bool value)
	{
		___m_iriParsing_19 = value;
	}
};

struct Uri_t19570940_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_20;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_21;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_22;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_23;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_24;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_25;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_26;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t1328083999* ___HexLowerChars_27;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t1328083999* ____WSchars_28;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_0), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_1), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_2), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_3), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_4), value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWs_5), value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWss_6), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_7), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_8), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_9), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_10), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_11), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_12), value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_20() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___s_ConfigInitialized_20)); }
	inline bool get_s_ConfigInitialized_20() const { return ___s_ConfigInitialized_20; }
	inline bool* get_address_of_s_ConfigInitialized_20() { return &___s_ConfigInitialized_20; }
	inline void set_s_ConfigInitialized_20(bool value)
	{
		___s_ConfigInitialized_20 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_21() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___s_ConfigInitializing_21)); }
	inline bool get_s_ConfigInitializing_21() const { return ___s_ConfigInitializing_21; }
	inline bool* get_address_of_s_ConfigInitializing_21() { return &___s_ConfigInitializing_21; }
	inline void set_s_ConfigInitializing_21(bool value)
	{
		___s_ConfigInitializing_21 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_22() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___s_IdnScope_22)); }
	inline int32_t get_s_IdnScope_22() const { return ___s_IdnScope_22; }
	inline int32_t* get_address_of_s_IdnScope_22() { return &___s_IdnScope_22; }
	inline void set_s_IdnScope_22(int32_t value)
	{
		___s_IdnScope_22 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_23() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___s_IriParsing_23)); }
	inline bool get_s_IriParsing_23() const { return ___s_IriParsing_23; }
	inline bool* get_address_of_s_IriParsing_23() { return &___s_IriParsing_23; }
	inline void set_s_IriParsing_23(bool value)
	{
		___s_IriParsing_23 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_24() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___useDotNetRelativeOrAbsolute_24)); }
	inline bool get_useDotNetRelativeOrAbsolute_24() const { return ___useDotNetRelativeOrAbsolute_24; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_24() { return &___useDotNetRelativeOrAbsolute_24; }
	inline void set_useDotNetRelativeOrAbsolute_24(bool value)
	{
		___useDotNetRelativeOrAbsolute_24 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_25() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___IsWindowsFileSystem_25)); }
	inline bool get_IsWindowsFileSystem_25() const { return ___IsWindowsFileSystem_25; }
	inline bool* get_address_of_IsWindowsFileSystem_25() { return &___IsWindowsFileSystem_25; }
	inline void set_IsWindowsFileSystem_25(bool value)
	{
		___IsWindowsFileSystem_25 = value;
	}

	inline static int32_t get_offset_of_s_initLock_26() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___s_initLock_26)); }
	inline RuntimeObject * get_s_initLock_26() const { return ___s_initLock_26; }
	inline RuntimeObject ** get_address_of_s_initLock_26() { return &___s_initLock_26; }
	inline void set_s_initLock_26(RuntimeObject * value)
	{
		___s_initLock_26 = value;
		Il2CppCodeGenWriteBarrier((&___s_initLock_26), value);
	}

	inline static int32_t get_offset_of_HexLowerChars_27() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___HexLowerChars_27)); }
	inline CharU5BU5D_t1328083999* get_HexLowerChars_27() const { return ___HexLowerChars_27; }
	inline CharU5BU5D_t1328083999** get_address_of_HexLowerChars_27() { return &___HexLowerChars_27; }
	inline void set_HexLowerChars_27(CharU5BU5D_t1328083999* value)
	{
		___HexLowerChars_27 = value;
		Il2CppCodeGenWriteBarrier((&___HexLowerChars_27), value);
	}

	inline static int32_t get_offset_of__WSchars_28() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ____WSchars_28)); }
	inline CharU5BU5D_t1328083999* get__WSchars_28() const { return ____WSchars_28; }
	inline CharU5BU5D_t1328083999** get_address_of__WSchars_28() { return &____WSchars_28; }
	inline void set__WSchars_28(CharU5BU5D_t1328083999* value)
	{
		____WSchars_28 = value;
		Il2CppCodeGenWriteBarrier((&____WSchars_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T19570940_H
#ifndef X509ENHANCEDKEYUSAGEEXTENSION_T2099881051_H
#define X509ENHANCEDKEYUSAGEEXTENSION_T2099881051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
struct  X509EnhancedKeyUsageExtension_t2099881051  : public X509Extension_t1320896183
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_enhKeyUsage
	OidCollection_t3790243618 * ____enhKeyUsage_3;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_status
	int32_t ____status_4;

public:
	inline static int32_t get_offset_of__enhKeyUsage_3() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t2099881051, ____enhKeyUsage_3)); }
	inline OidCollection_t3790243618 * get__enhKeyUsage_3() const { return ____enhKeyUsage_3; }
	inline OidCollection_t3790243618 ** get_address_of__enhKeyUsage_3() { return &____enhKeyUsage_3; }
	inline void set__enhKeyUsage_3(OidCollection_t3790243618 * value)
	{
		____enhKeyUsage_3 = value;
		Il2CppCodeGenWriteBarrier((&____enhKeyUsage_3), value);
	}

	inline static int32_t get_offset_of__status_4() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t2099881051, ____status_4)); }
	inline int32_t get__status_4() const { return ____status_4; }
	inline int32_t* get_address_of__status_4() { return &____status_4; }
	inline void set__status_4(int32_t value)
	{
		____status_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ENHANCEDKEYUSAGEEXTENSION_T2099881051_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef X509CHAINELEMENT_T528874471_H
#define X509CHAINELEMENT_T528874471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElement
struct  X509ChainElement_t528874471  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainElement::certificate
	X509Certificate2_t4056456767 * ___certificate_0;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainElement::status
	X509ChainStatusU5BU5D_t830390908* ___status_1;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainElement::info
	String_t* ___info_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainElement::compressed_status_flags
	int32_t ___compressed_status_flags_3;

public:
	inline static int32_t get_offset_of_certificate_0() { return static_cast<int32_t>(offsetof(X509ChainElement_t528874471, ___certificate_0)); }
	inline X509Certificate2_t4056456767 * get_certificate_0() const { return ___certificate_0; }
	inline X509Certificate2_t4056456767 ** get_address_of_certificate_0() { return &___certificate_0; }
	inline void set_certificate_0(X509Certificate2_t4056456767 * value)
	{
		___certificate_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(X509ChainElement_t528874471, ___status_1)); }
	inline X509ChainStatusU5BU5D_t830390908* get_status_1() const { return ___status_1; }
	inline X509ChainStatusU5BU5D_t830390908** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(X509ChainStatusU5BU5D_t830390908* value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier((&___status_1), value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(X509ChainElement_t528874471, ___info_2)); }
	inline String_t* get_info_2() const { return ___info_2; }
	inline String_t** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(String_t* value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}

	inline static int32_t get_offset_of_compressed_status_flags_3() { return static_cast<int32_t>(offsetof(X509ChainElement_t528874471, ___compressed_status_flags_3)); }
	inline int32_t get_compressed_status_flags_3() const { return ___compressed_status_flags_3; }
	inline int32_t* get_address_of_compressed_status_flags_3() { return &___compressed_status_flags_3; }
	inline void set_compressed_status_flags_3(int32_t value)
	{
		___compressed_status_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENT_T528874471_H
#ifndef X509STORE_T1617430119_H
#define X509STORE_T1617430119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Store
struct  X509Store_t1617430119  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Store::_name
	String_t* ____name_0;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Store::_location
	int32_t ____location_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Store::list
	X509Certificate2Collection_t1108969367 * ___list_2;
	// System.Security.Cryptography.X509Certificates.OpenFlags System.Security.Cryptography.X509Certificates.X509Store::_flags
	int32_t ____flags_3;
	// Mono.Security.X509.X509Store System.Security.Cryptography.X509Certificates.X509Store::store
	X509Store_t4028973564 * ___store_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__location_1() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ____location_1)); }
	inline int32_t get__location_1() const { return ____location_1; }
	inline int32_t* get_address_of__location_1() { return &____location_1; }
	inline void set__location_1(int32_t value)
	{
		____location_1 = value;
	}

	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ___list_2)); }
	inline X509Certificate2Collection_t1108969367 * get_list_2() const { return ___list_2; }
	inline X509Certificate2Collection_t1108969367 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(X509Certificate2Collection_t1108969367 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(X509Store_t1617430119, ___store_4)); }
	inline X509Store_t4028973564 * get_store_4() const { return ___store_4; }
	inline X509Store_t4028973564 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(X509Store_t4028973564 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_T1617430119_H
#ifndef X509SUBJECTKEYIDENTIFIEREXTENSION_T2508879999_H
#define X509SUBJECTKEYIDENTIFIEREXTENSION_T2508879999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
struct  X509SubjectKeyIdentifierExtension_t2508879999  : public X509Extension_t1320896183
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_subjectKeyIdentifier
	ByteU5BU5D_t3397334013* ____subjectKeyIdentifier_5;
	// System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_ski
	String_t* ____ski_6;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_status
	int32_t ____status_7;

public:
	inline static int32_t get_offset_of__subjectKeyIdentifier_5() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2508879999, ____subjectKeyIdentifier_5)); }
	inline ByteU5BU5D_t3397334013* get__subjectKeyIdentifier_5() const { return ____subjectKeyIdentifier_5; }
	inline ByteU5BU5D_t3397334013** get_address_of__subjectKeyIdentifier_5() { return &____subjectKeyIdentifier_5; }
	inline void set__subjectKeyIdentifier_5(ByteU5BU5D_t3397334013* value)
	{
		____subjectKeyIdentifier_5 = value;
		Il2CppCodeGenWriteBarrier((&____subjectKeyIdentifier_5), value);
	}

	inline static int32_t get_offset_of__ski_6() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2508879999, ____ski_6)); }
	inline String_t* get__ski_6() const { return ____ski_6; }
	inline String_t** get_address_of__ski_6() { return &____ski_6; }
	inline void set__ski_6(String_t* value)
	{
		____ski_6 = value;
		Il2CppCodeGenWriteBarrier((&____ski_6), value);
	}

	inline static int32_t get_offset_of__status_7() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_t2508879999, ____status_7)); }
	inline int32_t get__status_7() const { return ____status_7; }
	inline int32_t* get_address_of__status_7() { return &____status_7; }
	inline void set__status_7(int32_t value)
	{
		____status_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIEREXTENSION_T2508879999_H
#ifndef X509KEYUSAGEEXTENSION_T1038124237_H
#define X509KEYUSAGEEXTENSION_T1038124237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
struct  X509KeyUsageExtension_t1038124237  : public X509Extension_t1320896183
{
public:
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_keyUsages
	int32_t ____keyUsages_6;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_status
	int32_t ____status_7;

public:
	inline static int32_t get_offset_of__keyUsages_6() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t1038124237, ____keyUsages_6)); }
	inline int32_t get__keyUsages_6() const { return ____keyUsages_6; }
	inline int32_t* get_address_of__keyUsages_6() { return &____keyUsages_6; }
	inline void set__keyUsages_6(int32_t value)
	{
		____keyUsages_6 = value;
	}

	inline static int32_t get_offset_of__status_7() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t1038124237, ____status_7)); }
	inline int32_t get__status_7() const { return ____status_7; }
	inline int32_t* get_address_of__status_7() { return &____status_7; }
	inline void set__status_7(int32_t value)
	{
		____status_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEEXTENSION_T1038124237_H
#ifndef X509CHAINPOLICY_T3452126517_H
#define X509CHAINPOLICY_T3452126517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct  X509ChainPolicy_t3452126517  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::apps
	OidCollection_t3790243618 * ___apps_0;
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::cert
	OidCollection_t3790243618 * ___cert_1;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::store
	X509CertificateCollection_t1197680765 * ___store_2;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainPolicy::store2
	X509Certificate2Collection_t1108969367 * ___store2_3;
	// System.Security.Cryptography.X509Certificates.X509RevocationFlag System.Security.Cryptography.X509Certificates.X509ChainPolicy::rflag
	int32_t ___rflag_4;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.Security.Cryptography.X509Certificates.X509ChainPolicy::mode
	int32_t ___mode_5;
	// System.TimeSpan System.Security.Cryptography.X509Certificates.X509ChainPolicy::timeout
	TimeSpan_t3430258949  ___timeout_6;
	// System.Security.Cryptography.X509Certificates.X509VerificationFlags System.Security.Cryptography.X509Certificates.X509ChainPolicy::vflags
	int32_t ___vflags_7;
	// System.DateTime System.Security.Cryptography.X509Certificates.X509ChainPolicy::vtime
	DateTime_t693205669  ___vtime_8;

public:
	inline static int32_t get_offset_of_apps_0() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___apps_0)); }
	inline OidCollection_t3790243618 * get_apps_0() const { return ___apps_0; }
	inline OidCollection_t3790243618 ** get_address_of_apps_0() { return &___apps_0; }
	inline void set_apps_0(OidCollection_t3790243618 * value)
	{
		___apps_0 = value;
		Il2CppCodeGenWriteBarrier((&___apps_0), value);
	}

	inline static int32_t get_offset_of_cert_1() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___cert_1)); }
	inline OidCollection_t3790243618 * get_cert_1() const { return ___cert_1; }
	inline OidCollection_t3790243618 ** get_address_of_cert_1() { return &___cert_1; }
	inline void set_cert_1(OidCollection_t3790243618 * value)
	{
		___cert_1 = value;
		Il2CppCodeGenWriteBarrier((&___cert_1), value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___store_2)); }
	inline X509CertificateCollection_t1197680765 * get_store_2() const { return ___store_2; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(X509CertificateCollection_t1197680765 * value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier((&___store_2), value);
	}

	inline static int32_t get_offset_of_store2_3() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___store2_3)); }
	inline X509Certificate2Collection_t1108969367 * get_store2_3() const { return ___store2_3; }
	inline X509Certificate2Collection_t1108969367 ** get_address_of_store2_3() { return &___store2_3; }
	inline void set_store2_3(X509Certificate2Collection_t1108969367 * value)
	{
		___store2_3 = value;
		Il2CppCodeGenWriteBarrier((&___store2_3), value);
	}

	inline static int32_t get_offset_of_rflag_4() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___rflag_4)); }
	inline int32_t get_rflag_4() const { return ___rflag_4; }
	inline int32_t* get_address_of_rflag_4() { return &___rflag_4; }
	inline void set_rflag_4(int32_t value)
	{
		___rflag_4 = value;
	}

	inline static int32_t get_offset_of_mode_5() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___mode_5)); }
	inline int32_t get_mode_5() const { return ___mode_5; }
	inline int32_t* get_address_of_mode_5() { return &___mode_5; }
	inline void set_mode_5(int32_t value)
	{
		___mode_5 = value;
	}

	inline static int32_t get_offset_of_timeout_6() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___timeout_6)); }
	inline TimeSpan_t3430258949  get_timeout_6() const { return ___timeout_6; }
	inline TimeSpan_t3430258949 * get_address_of_timeout_6() { return &___timeout_6; }
	inline void set_timeout_6(TimeSpan_t3430258949  value)
	{
		___timeout_6 = value;
	}

	inline static int32_t get_offset_of_vflags_7() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___vflags_7)); }
	inline int32_t get_vflags_7() const { return ___vflags_7; }
	inline int32_t* get_address_of_vflags_7() { return &___vflags_7; }
	inline void set_vflags_7(int32_t value)
	{
		___vflags_7 = value;
	}

	inline static int32_t get_offset_of_vtime_8() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t3452126517, ___vtime_8)); }
	inline DateTime_t693205669  get_vtime_8() const { return ___vtime_8; }
	inline DateTime_t693205669 * get_address_of_vtime_8() { return &___vtime_8; }
	inline void set_vtime_8(DateTime_t693205669  value)
	{
		___vtime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINPOLICY_T3452126517_H
#ifndef X509CHAINIMPLMONO_T2085032772_H
#define X509CHAINIMPLMONO_T2085032772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainImplMono
struct  X509ChainImplMono_t2085032772  : public X509ChainImpl_t2968295413
{
public:
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509ChainImplMono::location
	int32_t ___location_0;
	// System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509ChainImplMono::elements
	X509ChainElementCollection_t2081831987 * ___elements_1;
	// System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509ChainImplMono::policy
	X509ChainPolicy_t3452126517 * ___policy_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainImplMono::status
	X509ChainStatusU5BU5D_t830390908* ___status_3;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainImplMono::max_path_length
	int32_t ___max_path_length_5;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509ChainImplMono::working_issuer_name
	X500DistinguishedName_t452415348 * ___working_issuer_name_6;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509ChainImplMono::working_public_key
	AsymmetricAlgorithm_t784058677 * ___working_public_key_7;
	// System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509ChainImplMono::bce_restriction
	X509ChainElement_t528874471 * ___bce_restriction_8;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainImplMono::roots
	X509Certificate2Collection_t1108969367 * ___roots_9;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainImplMono::cas
	X509Certificate2Collection_t1108969367 * ___cas_10;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::root_store
	X509Store_t1617430119 * ___root_store_11;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::ca_store
	X509Store_t1617430119 * ___ca_store_12;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::user_root_store
	X509Store_t1617430119 * ___user_root_store_13;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509ChainImplMono::user_ca_store
	X509Store_t1617430119 * ___user_ca_store_14;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainImplMono::collection
	X509Certificate2Collection_t1108969367 * ___collection_15;

public:
	inline static int32_t get_offset_of_location_0() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___location_0)); }
	inline int32_t get_location_0() const { return ___location_0; }
	inline int32_t* get_address_of_location_0() { return &___location_0; }
	inline void set_location_0(int32_t value)
	{
		___location_0 = value;
	}

	inline static int32_t get_offset_of_elements_1() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___elements_1)); }
	inline X509ChainElementCollection_t2081831987 * get_elements_1() const { return ___elements_1; }
	inline X509ChainElementCollection_t2081831987 ** get_address_of_elements_1() { return &___elements_1; }
	inline void set_elements_1(X509ChainElementCollection_t2081831987 * value)
	{
		___elements_1 = value;
		Il2CppCodeGenWriteBarrier((&___elements_1), value);
	}

	inline static int32_t get_offset_of_policy_2() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___policy_2)); }
	inline X509ChainPolicy_t3452126517 * get_policy_2() const { return ___policy_2; }
	inline X509ChainPolicy_t3452126517 ** get_address_of_policy_2() { return &___policy_2; }
	inline void set_policy_2(X509ChainPolicy_t3452126517 * value)
	{
		___policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___policy_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___status_3)); }
	inline X509ChainStatusU5BU5D_t830390908* get_status_3() const { return ___status_3; }
	inline X509ChainStatusU5BU5D_t830390908** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(X509ChainStatusU5BU5D_t830390908* value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}

	inline static int32_t get_offset_of_max_path_length_5() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___max_path_length_5)); }
	inline int32_t get_max_path_length_5() const { return ___max_path_length_5; }
	inline int32_t* get_address_of_max_path_length_5() { return &___max_path_length_5; }
	inline void set_max_path_length_5(int32_t value)
	{
		___max_path_length_5 = value;
	}

	inline static int32_t get_offset_of_working_issuer_name_6() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___working_issuer_name_6)); }
	inline X500DistinguishedName_t452415348 * get_working_issuer_name_6() const { return ___working_issuer_name_6; }
	inline X500DistinguishedName_t452415348 ** get_address_of_working_issuer_name_6() { return &___working_issuer_name_6; }
	inline void set_working_issuer_name_6(X500DistinguishedName_t452415348 * value)
	{
		___working_issuer_name_6 = value;
		Il2CppCodeGenWriteBarrier((&___working_issuer_name_6), value);
	}

	inline static int32_t get_offset_of_working_public_key_7() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___working_public_key_7)); }
	inline AsymmetricAlgorithm_t784058677 * get_working_public_key_7() const { return ___working_public_key_7; }
	inline AsymmetricAlgorithm_t784058677 ** get_address_of_working_public_key_7() { return &___working_public_key_7; }
	inline void set_working_public_key_7(AsymmetricAlgorithm_t784058677 * value)
	{
		___working_public_key_7 = value;
		Il2CppCodeGenWriteBarrier((&___working_public_key_7), value);
	}

	inline static int32_t get_offset_of_bce_restriction_8() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___bce_restriction_8)); }
	inline X509ChainElement_t528874471 * get_bce_restriction_8() const { return ___bce_restriction_8; }
	inline X509ChainElement_t528874471 ** get_address_of_bce_restriction_8() { return &___bce_restriction_8; }
	inline void set_bce_restriction_8(X509ChainElement_t528874471 * value)
	{
		___bce_restriction_8 = value;
		Il2CppCodeGenWriteBarrier((&___bce_restriction_8), value);
	}

	inline static int32_t get_offset_of_roots_9() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___roots_9)); }
	inline X509Certificate2Collection_t1108969367 * get_roots_9() const { return ___roots_9; }
	inline X509Certificate2Collection_t1108969367 ** get_address_of_roots_9() { return &___roots_9; }
	inline void set_roots_9(X509Certificate2Collection_t1108969367 * value)
	{
		___roots_9 = value;
		Il2CppCodeGenWriteBarrier((&___roots_9), value);
	}

	inline static int32_t get_offset_of_cas_10() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___cas_10)); }
	inline X509Certificate2Collection_t1108969367 * get_cas_10() const { return ___cas_10; }
	inline X509Certificate2Collection_t1108969367 ** get_address_of_cas_10() { return &___cas_10; }
	inline void set_cas_10(X509Certificate2Collection_t1108969367 * value)
	{
		___cas_10 = value;
		Il2CppCodeGenWriteBarrier((&___cas_10), value);
	}

	inline static int32_t get_offset_of_root_store_11() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___root_store_11)); }
	inline X509Store_t1617430119 * get_root_store_11() const { return ___root_store_11; }
	inline X509Store_t1617430119 ** get_address_of_root_store_11() { return &___root_store_11; }
	inline void set_root_store_11(X509Store_t1617430119 * value)
	{
		___root_store_11 = value;
		Il2CppCodeGenWriteBarrier((&___root_store_11), value);
	}

	inline static int32_t get_offset_of_ca_store_12() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___ca_store_12)); }
	inline X509Store_t1617430119 * get_ca_store_12() const { return ___ca_store_12; }
	inline X509Store_t1617430119 ** get_address_of_ca_store_12() { return &___ca_store_12; }
	inline void set_ca_store_12(X509Store_t1617430119 * value)
	{
		___ca_store_12 = value;
		Il2CppCodeGenWriteBarrier((&___ca_store_12), value);
	}

	inline static int32_t get_offset_of_user_root_store_13() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___user_root_store_13)); }
	inline X509Store_t1617430119 * get_user_root_store_13() const { return ___user_root_store_13; }
	inline X509Store_t1617430119 ** get_address_of_user_root_store_13() { return &___user_root_store_13; }
	inline void set_user_root_store_13(X509Store_t1617430119 * value)
	{
		___user_root_store_13 = value;
		Il2CppCodeGenWriteBarrier((&___user_root_store_13), value);
	}

	inline static int32_t get_offset_of_user_ca_store_14() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___user_ca_store_14)); }
	inline X509Store_t1617430119 * get_user_ca_store_14() const { return ___user_ca_store_14; }
	inline X509Store_t1617430119 ** get_address_of_user_ca_store_14() { return &___user_ca_store_14; }
	inline void set_user_ca_store_14(X509Store_t1617430119 * value)
	{
		___user_ca_store_14 = value;
		Il2CppCodeGenWriteBarrier((&___user_ca_store_14), value);
	}

	inline static int32_t get_offset_of_collection_15() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772, ___collection_15)); }
	inline X509Certificate2Collection_t1108969367 * get_collection_15() const { return ___collection_15; }
	inline X509Certificate2Collection_t1108969367 ** get_address_of_collection_15() { return &___collection_15; }
	inline void set_collection_15(X509Certificate2Collection_t1108969367 * value)
	{
		___collection_15 = value;
		Il2CppCodeGenWriteBarrier((&___collection_15), value);
	}
};

struct X509ChainImplMono_t2085032772_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainImplMono::Empty
	X509ChainStatusU5BU5D_t830390908* ___Empty_4;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(X509ChainImplMono_t2085032772_StaticFields, ___Empty_4)); }
	inline X509ChainStatusU5BU5D_t830390908* get_Empty_4() const { return ___Empty_4; }
	inline X509ChainStatusU5BU5D_t830390908** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(X509ChainStatusU5BU5D_t830390908* value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINIMPLMONO_T2085032772_H
#ifndef X509CHAINSTATUS_T4278378721_H
#define X509CHAINSTATUS_T4278378721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatus
struct  X509ChainStatus_t4278378721 
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::status
	int32_t ___status_0;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::info
	String_t* ___info_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(X509ChainStatus_t4278378721, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(X509ChainStatus_t4278378721, ___info_1)); }
	inline String_t* get_info_1() const { return ___info_1; }
	inline String_t** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(String_t* value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t4278378721_marshaled_pinvoke
{
	int32_t ___status_0;
	char* ___info_1;
};
// Native definition for COM marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t4278378721_marshaled_com
{
	int32_t ___status_0;
	Il2CppChar* ___info_1;
};
#endif // X509CHAINSTATUS_T4278378721_H
#ifndef IOSELECTORJOB_T2021937086_H
#define IOSELECTORJOB_T2021937086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOSelectorJob
struct  IOSelectorJob_t2021937086  : public RuntimeObject
{
public:
	// System.IOOperation System.IOSelectorJob::operation
	int32_t ___operation_0;
	// System.IOAsyncCallback System.IOSelectorJob::callback
	IOAsyncCallback_t2427139621 * ___callback_1;
	// System.IOAsyncResult System.IOSelectorJob::state
	IOAsyncResult_t1276329107 * ___state_2;

public:
	inline static int32_t get_offset_of_operation_0() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2021937086, ___operation_0)); }
	inline int32_t get_operation_0() const { return ___operation_0; }
	inline int32_t* get_address_of_operation_0() { return &___operation_0; }
	inline void set_operation_0(int32_t value)
	{
		___operation_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2021937086, ___callback_1)); }
	inline IOAsyncCallback_t2427139621 * get_callback_1() const { return ___callback_1; }
	inline IOAsyncCallback_t2427139621 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(IOAsyncCallback_t2427139621 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(IOSelectorJob_t2021937086, ___state_2)); }
	inline IOAsyncResult_t1276329107 * get_state_2() const { return ___state_2; }
	inline IOAsyncResult_t1276329107 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(IOAsyncResult_t1276329107 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IOSelectorJob
struct IOSelectorJob_t2021937086_marshaled_pinvoke
{
	int32_t ___operation_0;
	Il2CppMethodPointer ___callback_1;
	IOAsyncResult_t1276329107_marshaled_pinvoke* ___state_2;
};
// Native definition for COM marshalling of System.IOSelectorJob
struct IOSelectorJob_t2021937086_marshaled_com
{
	int32_t ___operation_0;
	Il2CppMethodPointer ___callback_1;
	IOAsyncResult_t1276329107_marshaled_com* ___state_2;
};
#endif // IOSELECTORJOB_T2021937086_H
#ifndef X509BASICCONSTRAINTSEXTENSION_T1562873317_H
#define X509BASICCONSTRAINTSEXTENSION_T1562873317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
struct  X509BasicConstraintsExtension_t1562873317  : public X509Extension_t1320896183
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_certificateAuthority
	bool ____certificateAuthority_5;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_hasPathLengthConstraint
	bool ____hasPathLengthConstraint_6;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_pathLengthConstraint
	int32_t ____pathLengthConstraint_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__certificateAuthority_5() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1562873317, ____certificateAuthority_5)); }
	inline bool get__certificateAuthority_5() const { return ____certificateAuthority_5; }
	inline bool* get_address_of__certificateAuthority_5() { return &____certificateAuthority_5; }
	inline void set__certificateAuthority_5(bool value)
	{
		____certificateAuthority_5 = value;
	}

	inline static int32_t get_offset_of__hasPathLengthConstraint_6() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1562873317, ____hasPathLengthConstraint_6)); }
	inline bool get__hasPathLengthConstraint_6() const { return ____hasPathLengthConstraint_6; }
	inline bool* get_address_of__hasPathLengthConstraint_6() { return &____hasPathLengthConstraint_6; }
	inline void set__hasPathLengthConstraint_6(bool value)
	{
		____hasPathLengthConstraint_6 = value;
	}

	inline static int32_t get_offset_of__pathLengthConstraint_7() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1562873317, ____pathLengthConstraint_7)); }
	inline int32_t get__pathLengthConstraint_7() const { return ____pathLengthConstraint_7; }
	inline int32_t* get_address_of__pathLengthConstraint_7() { return &____pathLengthConstraint_7; }
	inline void set__pathLengthConstraint_7(int32_t value)
	{
		____pathLengthConstraint_7 = value;
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t1562873317, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509BASICCONSTRAINTSEXTENSION_T1562873317_H
#ifndef URITYPECONVERTER_T3912970448_H
#define URITYPECONVERTER_T3912970448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriTypeConverter
struct  UriTypeConverter_t3912970448  : public TypeConverter_t745995970
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URITYPECONVERTER_T3912970448_H
#ifndef REGEX_T1803876613_H
#define REGEX_T1803876613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t1803876613  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_0;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.Regex::factory
	RegexRunnerFactory_t3902733837 * ___factory_1;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_2;
	// System.TimeSpan System.Text.RegularExpressions.Regex::internalMatchTimeout
	TimeSpan_t3430258949  ___internalMatchTimeout_5;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::caps
	Hashtable_t909839986 * ___caps_8;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::capnames
	Hashtable_t909839986 * ___capnames_9;
	// System.String[] System.Text.RegularExpressions.Regex::capslist
	StringU5BU5D_t1642385972* ___capslist_10;
	// System.Int32 System.Text.RegularExpressions.Regex::capsize
	int32_t ___capsize_11;
	// System.Text.RegularExpressions.ExclusiveReference System.Text.RegularExpressions.Regex::runnerref
	ExclusiveReference_t708182869 * ___runnerref_12;
	// System.Text.RegularExpressions.SharedReference System.Text.RegularExpressions.Regex::replref
	SharedReference_t2137668360 * ___replref_13;
	// System.Text.RegularExpressions.RegexCode System.Text.RegularExpressions.Regex::code
	RegexCode_t2469392150 * ___code_14;
	// System.Boolean System.Text.RegularExpressions.Regex::refsInitialized
	bool ___refsInitialized_15;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_factory_1() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___factory_1)); }
	inline RegexRunnerFactory_t3902733837 * get_factory_1() const { return ___factory_1; }
	inline RegexRunnerFactory_t3902733837 ** get_address_of_factory_1() { return &___factory_1; }
	inline void set_factory_1(RegexRunnerFactory_t3902733837 * value)
	{
		___factory_1 = value;
		Il2CppCodeGenWriteBarrier((&___factory_1), value);
	}

	inline static int32_t get_offset_of_roptions_2() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___roptions_2)); }
	inline int32_t get_roptions_2() const { return ___roptions_2; }
	inline int32_t* get_address_of_roptions_2() { return &___roptions_2; }
	inline void set_roptions_2(int32_t value)
	{
		___roptions_2 = value;
	}

	inline static int32_t get_offset_of_internalMatchTimeout_5() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___internalMatchTimeout_5)); }
	inline TimeSpan_t3430258949  get_internalMatchTimeout_5() const { return ___internalMatchTimeout_5; }
	inline TimeSpan_t3430258949 * get_address_of_internalMatchTimeout_5() { return &___internalMatchTimeout_5; }
	inline void set_internalMatchTimeout_5(TimeSpan_t3430258949  value)
	{
		___internalMatchTimeout_5 = value;
	}

	inline static int32_t get_offset_of_caps_8() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___caps_8)); }
	inline Hashtable_t909839986 * get_caps_8() const { return ___caps_8; }
	inline Hashtable_t909839986 ** get_address_of_caps_8() { return &___caps_8; }
	inline void set_caps_8(Hashtable_t909839986 * value)
	{
		___caps_8 = value;
		Il2CppCodeGenWriteBarrier((&___caps_8), value);
	}

	inline static int32_t get_offset_of_capnames_9() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___capnames_9)); }
	inline Hashtable_t909839986 * get_capnames_9() const { return ___capnames_9; }
	inline Hashtable_t909839986 ** get_address_of_capnames_9() { return &___capnames_9; }
	inline void set_capnames_9(Hashtable_t909839986 * value)
	{
		___capnames_9 = value;
		Il2CppCodeGenWriteBarrier((&___capnames_9), value);
	}

	inline static int32_t get_offset_of_capslist_10() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___capslist_10)); }
	inline StringU5BU5D_t1642385972* get_capslist_10() const { return ___capslist_10; }
	inline StringU5BU5D_t1642385972** get_address_of_capslist_10() { return &___capslist_10; }
	inline void set_capslist_10(StringU5BU5D_t1642385972* value)
	{
		___capslist_10 = value;
		Il2CppCodeGenWriteBarrier((&___capslist_10), value);
	}

	inline static int32_t get_offset_of_capsize_11() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___capsize_11)); }
	inline int32_t get_capsize_11() const { return ___capsize_11; }
	inline int32_t* get_address_of_capsize_11() { return &___capsize_11; }
	inline void set_capsize_11(int32_t value)
	{
		___capsize_11 = value;
	}

	inline static int32_t get_offset_of_runnerref_12() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___runnerref_12)); }
	inline ExclusiveReference_t708182869 * get_runnerref_12() const { return ___runnerref_12; }
	inline ExclusiveReference_t708182869 ** get_address_of_runnerref_12() { return &___runnerref_12; }
	inline void set_runnerref_12(ExclusiveReference_t708182869 * value)
	{
		___runnerref_12 = value;
		Il2CppCodeGenWriteBarrier((&___runnerref_12), value);
	}

	inline static int32_t get_offset_of_replref_13() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___replref_13)); }
	inline SharedReference_t2137668360 * get_replref_13() const { return ___replref_13; }
	inline SharedReference_t2137668360 ** get_address_of_replref_13() { return &___replref_13; }
	inline void set_replref_13(SharedReference_t2137668360 * value)
	{
		___replref_13 = value;
		Il2CppCodeGenWriteBarrier((&___replref_13), value);
	}

	inline static int32_t get_offset_of_code_14() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___code_14)); }
	inline RegexCode_t2469392150 * get_code_14() const { return ___code_14; }
	inline RegexCode_t2469392150 ** get_address_of_code_14() { return &___code_14; }
	inline void set_code_14(RegexCode_t2469392150 * value)
	{
		___code_14 = value;
		Il2CppCodeGenWriteBarrier((&___code_14), value);
	}

	inline static int32_t get_offset_of_refsInitialized_15() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___refsInitialized_15)); }
	inline bool get_refsInitialized_15() const { return ___refsInitialized_15; }
	inline bool* get_address_of_refsInitialized_15() { return &___refsInitialized_15; }
	inline void set_refsInitialized_15(bool value)
	{
		___refsInitialized_15 = value;
	}
};

struct Regex_t1803876613_StaticFields
{
public:
	// System.TimeSpan System.Text.RegularExpressions.Regex::MaximumMatchTimeout
	TimeSpan_t3430258949  ___MaximumMatchTimeout_3;
	// System.TimeSpan System.Text.RegularExpressions.Regex::InfiniteMatchTimeout
	TimeSpan_t3430258949  ___InfiniteMatchTimeout_4;
	// System.TimeSpan System.Text.RegularExpressions.Regex::FallbackDefaultMatchTimeout
	TimeSpan_t3430258949  ___FallbackDefaultMatchTimeout_6;
	// System.TimeSpan System.Text.RegularExpressions.Regex::DefaultMatchTimeout
	TimeSpan_t3430258949  ___DefaultMatchTimeout_7;
	// System.Collections.Generic.LinkedList`1<System.Text.RegularExpressions.CachedCodeEntry> System.Text.RegularExpressions.Regex::livecode
	LinkedList_1_t3858529280 * ___livecode_16;
	// System.Int32 System.Text.RegularExpressions.Regex::cacheSize
	int32_t ___cacheSize_17;

public:
	inline static int32_t get_offset_of_MaximumMatchTimeout_3() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___MaximumMatchTimeout_3)); }
	inline TimeSpan_t3430258949  get_MaximumMatchTimeout_3() const { return ___MaximumMatchTimeout_3; }
	inline TimeSpan_t3430258949 * get_address_of_MaximumMatchTimeout_3() { return &___MaximumMatchTimeout_3; }
	inline void set_MaximumMatchTimeout_3(TimeSpan_t3430258949  value)
	{
		___MaximumMatchTimeout_3 = value;
	}

	inline static int32_t get_offset_of_InfiniteMatchTimeout_4() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___InfiniteMatchTimeout_4)); }
	inline TimeSpan_t3430258949  get_InfiniteMatchTimeout_4() const { return ___InfiniteMatchTimeout_4; }
	inline TimeSpan_t3430258949 * get_address_of_InfiniteMatchTimeout_4() { return &___InfiniteMatchTimeout_4; }
	inline void set_InfiniteMatchTimeout_4(TimeSpan_t3430258949  value)
	{
		___InfiniteMatchTimeout_4 = value;
	}

	inline static int32_t get_offset_of_FallbackDefaultMatchTimeout_6() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___FallbackDefaultMatchTimeout_6)); }
	inline TimeSpan_t3430258949  get_FallbackDefaultMatchTimeout_6() const { return ___FallbackDefaultMatchTimeout_6; }
	inline TimeSpan_t3430258949 * get_address_of_FallbackDefaultMatchTimeout_6() { return &___FallbackDefaultMatchTimeout_6; }
	inline void set_FallbackDefaultMatchTimeout_6(TimeSpan_t3430258949  value)
	{
		___FallbackDefaultMatchTimeout_6 = value;
	}

	inline static int32_t get_offset_of_DefaultMatchTimeout_7() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___DefaultMatchTimeout_7)); }
	inline TimeSpan_t3430258949  get_DefaultMatchTimeout_7() const { return ___DefaultMatchTimeout_7; }
	inline TimeSpan_t3430258949 * get_address_of_DefaultMatchTimeout_7() { return &___DefaultMatchTimeout_7; }
	inline void set_DefaultMatchTimeout_7(TimeSpan_t3430258949  value)
	{
		___DefaultMatchTimeout_7 = value;
	}

	inline static int32_t get_offset_of_livecode_16() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___livecode_16)); }
	inline LinkedList_1_t3858529280 * get_livecode_16() const { return ___livecode_16; }
	inline LinkedList_1_t3858529280 ** get_address_of_livecode_16() { return &___livecode_16; }
	inline void set_livecode_16(LinkedList_1_t3858529280 * value)
	{
		___livecode_16 = value;
		Il2CppCodeGenWriteBarrier((&___livecode_16), value);
	}

	inline static int32_t get_offset_of_cacheSize_17() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___cacheSize_17)); }
	inline int32_t get_cacheSize_17() const { return ___cacheSize_17; }
	inline int32_t* get_address_of_cacheSize_17() { return &___cacheSize_17; }
	inline void set_cacheSize_17(int32_t value)
	{
		___cacheSize_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T1803876613_H
#ifndef OID_T3221867120_H
#define OID_T3221867120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Oid
struct  Oid_t3221867120  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.Oid::m_value
	String_t* ___m_value_0;
	// System.String System.Security.Cryptography.Oid::m_friendlyName
	String_t* ___m_friendlyName_1;
	// System.Security.Cryptography.OidGroup System.Security.Cryptography.Oid::m_group
	int32_t ___m_group_2;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Oid_t3221867120, ___m_value_0)); }
	inline String_t* get_m_value_0() const { return ___m_value_0; }
	inline String_t** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(String_t* value)
	{
		___m_value_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_0), value);
	}

	inline static int32_t get_offset_of_m_friendlyName_1() { return static_cast<int32_t>(offsetof(Oid_t3221867120, ___m_friendlyName_1)); }
	inline String_t* get_m_friendlyName_1() const { return ___m_friendlyName_1; }
	inline String_t** get_address_of_m_friendlyName_1() { return &___m_friendlyName_1; }
	inline void set_m_friendlyName_1(String_t* value)
	{
		___m_friendlyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_friendlyName_1), value);
	}

	inline static int32_t get_offset_of_m_group_2() { return static_cast<int32_t>(offsetof(Oid_t3221867120, ___m_group_2)); }
	inline int32_t get_m_group_2() const { return ___m_group_2; }
	inline int32_t* get_address_of_m_group_2() { return &___m_group_2; }
	inline void set_m_group_2(int32_t value)
	{
		___m_group_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_T3221867120_H
#ifndef BUILTINURIPARSER_T2634503859_H
#define BUILTINURIPARSER_T2634503859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser/BuiltInUriParser
struct  BuiltInUriParser_t2634503859  : public UriParser_t1012511323
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINURIPARSER_T2634503859_H
#ifndef IOASYNCCALLBACK_T2427139621_H
#define IOASYNCCALLBACK_T2427139621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IOAsyncCallback
struct  IOAsyncCallback_t2427139621  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOASYNCCALLBACK_T2427139621_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (ExceptionArgument_t2966871835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[18] = 
{
	ExceptionArgument_t2966871835::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (ExceptionResource_t2812258640)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2401[27] = 
{
	ExceptionResource_t2812258640::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (SecurityUtils_t1714011141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (InvariantComparer_t3322871449), -1, sizeof(InvariantComparer_t3322871449_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2403[2] = 
{
	InvariantComparer_t3322871449::get_offset_of_m_compareInfo_0(),
	InvariantComparer_t3322871449_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (IriHelper_t306005226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (Uri_t19570940), -1, sizeof(Uri_t19570940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2405[29] = 
{
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFile_0(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFtp_1(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeGopher_2(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttp_3(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttps_4(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeWs_5(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeWss_6(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeMailto_7(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNews_8(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNntp_9(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetTcp_10(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetPipe_11(),
	Uri_t19570940_StaticFields::get_offset_of_SchemeDelimiter_12(),
	Uri_t19570940::get_offset_of_m_String_13(),
	Uri_t19570940::get_offset_of_m_originalUnicodeString_14(),
	Uri_t19570940::get_offset_of_m_Syntax_15(),
	Uri_t19570940::get_offset_of_m_DnsSafeHost_16(),
	Uri_t19570940::get_offset_of_m_Flags_17(),
	Uri_t19570940::get_offset_of_m_Info_18(),
	Uri_t19570940::get_offset_of_m_iriParsing_19(),
	Uri_t19570940_StaticFields::get_offset_of_s_ConfigInitialized_20(),
	Uri_t19570940_StaticFields::get_offset_of_s_ConfigInitializing_21(),
	Uri_t19570940_StaticFields::get_offset_of_s_IdnScope_22(),
	Uri_t19570940_StaticFields::get_offset_of_s_IriParsing_23(),
	Uri_t19570940_StaticFields::get_offset_of_useDotNetRelativeOrAbsolute_24(),
	Uri_t19570940_StaticFields::get_offset_of_IsWindowsFileSystem_25(),
	Uri_t19570940_StaticFields::get_offset_of_s_initLock_26(),
	Uri_t19570940_StaticFields::get_offset_of_HexLowerChars_27(),
	Uri_t19570940_StaticFields::get_offset_of__WSchars_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (Flags_t455382755)+ sizeof (RuntimeObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2406[56] = 
{
	Flags_t455382755::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (UriInfo_t4047916940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[6] = 
{
	UriInfo_t4047916940::get_offset_of_Host_0(),
	UriInfo_t4047916940::get_offset_of_ScopeId_1(),
	UriInfo_t4047916940::get_offset_of_String_2(),
	UriInfo_t4047916940::get_offset_of_Offset_3(),
	UriInfo_t4047916940::get_offset_of_DnsSafeHost_4(),
	UriInfo_t4047916940::get_offset_of_MoreInfo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (Offset_t266882373)+ sizeof (RuntimeObject), sizeof(Offset_t266882373 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[8] = 
{
	Offset_t266882373::get_offset_of_Scheme_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_User_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_Host_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_PortValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_Path_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_Query_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_Fragment_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Offset_t266882373::get_offset_of_End_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (MoreInfo_t2595315311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[6] = 
{
	MoreInfo_t2595315311::get_offset_of_Path_0(),
	MoreInfo_t2595315311::get_offset_of_Query_1(),
	MoreInfo_t2595315311::get_offset_of_Fragment_2(),
	MoreInfo_t2595315311::get_offset_of_AbsoluteUri_3(),
	MoreInfo_t2595315311::get_offset_of_Hash_4(),
	MoreInfo_t2595315311::get_offset_of_RemoteUrl_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (Check_t363272550)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2410[10] = 
{
	Check_t363272550::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (UriFormatException_t3682083048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (UriKind_t1128731744)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2412[4] = 
{
	UriKind_t1128731744::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (UriComponents_t3302767704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[18] = 
{
	UriComponents_t3302767704::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (UriFormat_t2764505239)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[4] = 
{
	UriFormat_t2764505239::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (UriIdnScope_t761062207)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2415[4] = 
{
	UriIdnScope_t761062207::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (ParsingError_t953959262)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2416[15] = 
{
	ParsingError_t953959262::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (UnescapeMode_t584481035)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2417[8] = 
{
	UnescapeMode_t584481035::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (UriHelper_t2566857206), -1, sizeof(UriHelper_t2566857206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2418[1] = 
{
	UriHelper_t2566857206_StaticFields::get_offset_of_HexUpperChars_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (UriHostNameType_t2148127109)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2419[6] = 
{
	UriHostNameType_t2148127109::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (UriParser_t1012511323), -1, sizeof(UriParser_t1012511323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2420[26] = 
{
	UriParser_t1012511323_StaticFields::get_offset_of_m_Table_0(),
	UriParser_t1012511323_StaticFields::get_offset_of_m_TempTable_1(),
	UriParser_t1012511323::get_offset_of_m_Flags_2(),
	UriParser_t1012511323::get_offset_of_m_UpdatableFlags_3(),
	UriParser_t1012511323::get_offset_of_m_UpdatableFlagsUsed_4(),
	UriParser_t1012511323::get_offset_of_m_Port_5(),
	UriParser_t1012511323::get_offset_of_m_Scheme_6(),
	UriParser_t1012511323_StaticFields::get_offset_of_HttpUri_7(),
	UriParser_t1012511323_StaticFields::get_offset_of_HttpsUri_8(),
	UriParser_t1012511323_StaticFields::get_offset_of_WsUri_9(),
	UriParser_t1012511323_StaticFields::get_offset_of_WssUri_10(),
	UriParser_t1012511323_StaticFields::get_offset_of_FtpUri_11(),
	UriParser_t1012511323_StaticFields::get_offset_of_FileUri_12(),
	UriParser_t1012511323_StaticFields::get_offset_of_GopherUri_13(),
	UriParser_t1012511323_StaticFields::get_offset_of_NntpUri_14(),
	UriParser_t1012511323_StaticFields::get_offset_of_NewsUri_15(),
	UriParser_t1012511323_StaticFields::get_offset_of_MailToUri_16(),
	UriParser_t1012511323_StaticFields::get_offset_of_UuidUri_17(),
	UriParser_t1012511323_StaticFields::get_offset_of_TelnetUri_18(),
	UriParser_t1012511323_StaticFields::get_offset_of_LdapUri_19(),
	UriParser_t1012511323_StaticFields::get_offset_of_NetTcpUri_20(),
	UriParser_t1012511323_StaticFields::get_offset_of_NetPipeUri_21(),
	UriParser_t1012511323_StaticFields::get_offset_of_VsMacrosUri_22(),
	UriParser_t1012511323_StaticFields::get_offset_of_s_QuirksVersion_23(),
	UriParser_t1012511323_StaticFields::get_offset_of_HttpSyntaxFlags_24(),
	UriParser_t1012511323_StaticFields::get_offset_of_FileSyntaxFlags_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (UriQuirksVersion_t4233729352)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	UriQuirksVersion_t4233729352::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (BuiltInUriParser_t2634503859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (DomainNameHelper_t2237853587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (IPv4AddressHelper_t3364954615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (IPv6AddressHelper_t76851317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (UncNameHelper_t1663961013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (UriSyntaxFlags_t1242716474)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2427[30] = 
{
	UriSyntaxFlags_t1242716474::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (UriBuilder_t2016461725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[11] = 
{
	UriBuilder_t2016461725::get_offset_of_m_changed_0(),
	UriBuilder_t2016461725::get_offset_of_m_fragment_1(),
	UriBuilder_t2016461725::get_offset_of_m_host_2(),
	UriBuilder_t2016461725::get_offset_of_m_password_3(),
	UriBuilder_t2016461725::get_offset_of_m_path_4(),
	UriBuilder_t2016461725::get_offset_of_m_port_5(),
	UriBuilder_t2016461725::get_offset_of_m_query_6(),
	UriBuilder_t2016461725::get_offset_of_m_scheme_7(),
	UriBuilder_t2016461725::get_offset_of_m_schemeDelimiter_8(),
	UriBuilder_t2016461725::get_offset_of_m_uri_9(),
	UriBuilder_t2016461725::get_offset_of_m_username_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (LocalAppContextSwitches_t4208498603), -1, sizeof(LocalAppContextSwitches_t4208498603_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[1] = 
{
	LocalAppContextSwitches_t4208498603_StaticFields::get_offset_of_MemberDescriptorEqualsReturnsFalseIfEquivalent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (IOOperation_t4250055067)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2430[3] = 
{
	IOOperation_t4250055067::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (IOAsyncCallback_t2427139621), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (IOAsyncResult_t1276329107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[5] = 
{
	IOAsyncResult_t1276329107::get_offset_of_async_callback_0(),
	IOAsyncResult_t1276329107::get_offset_of_async_state_1(),
	IOAsyncResult_t1276329107::get_offset_of_wait_handle_2(),
	IOAsyncResult_t1276329107::get_offset_of_completed_synchronously_3(),
	IOAsyncResult_t1276329107::get_offset_of_completed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (IOSelectorJob_t2021937086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[3] = 
{
	IOSelectorJob_t2021937086::get_offset_of_operation_0(),
	IOSelectorJob_t2021937086::get_offset_of_callback_1(),
	IOSelectorJob_t2021937086::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (IOSelector_t2669134661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (Platform_t3160142327), -1, sizeof(Platform_t3160142327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[3] = 
{
	Platform_t3160142327_StaticFields::get_offset_of_checkedOS_0(),
	Platform_t3160142327_StaticFields::get_offset_of_isMacOS_1(),
	Platform_t3160142327_StaticFields::get_offset_of_isFreeBSD_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (SRDescriptionAttribute_t431985547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[1] = 
{
	SRDescriptionAttribute_t431985547::get_offset_of_isReplaced_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (UriTypeConverter_t3912970448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (OidGroup_t4038341371)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2438[12] = 
{
	OidGroup_t4038341371::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (Oid_t3221867120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[3] = 
{
	Oid_t3221867120::get_offset_of_m_value_0(),
	Oid_t3221867120::get_offset_of_m_friendlyName_1(),
	Oid_t3221867120::get_offset_of_m_group_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (OidCollection_t3790243618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[1] = 
{
	OidCollection_t3790243618::get_offset_of_m_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (OidEnumerator_t3674631724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[2] = 
{
	OidEnumerator_t3674631724::get_offset_of_m_oids_0(),
	OidEnumerator_t3674631724::get_offset_of_m_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (CAPI_t3094615495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (AsnDecodeStatus_t1962003286)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[7] = 
{
	AsnDecodeStatus_t1962003286::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (AsnEncodedData_t463456204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[2] = 
{
	AsnEncodedData_t463456204::get_offset_of__oid_0(),
	AsnEncodedData_t463456204::get_offset_of__raw_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (X509Utils_t810863315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (OSX509Certificates_t384932784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (SecTrustResult_t1335602280)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[9] = 
{
	SecTrustResult_t1335602280::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (OpenFlags_t2370524385)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2448[6] = 
{
	OpenFlags_t2370524385::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (PublicKey_t870392), -1, sizeof(PublicKey_t870392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2449[5] = 
{
	PublicKey_t870392::get_offset_of__key_0(),
	PublicKey_t870392::get_offset_of__keyValue_1(),
	PublicKey_t870392::get_offset_of__params_2(),
	PublicKey_t870392::get_offset_of__oid_3(),
	PublicKey_t870392_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (StoreLocation_t1570828128)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2450[3] = 
{
	StoreLocation_t1570828128::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (StoreName_t2183514610)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2451[9] = 
{
	StoreName_t2183514610::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (X500DistinguishedName_t452415348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[2] = 
{
	X500DistinguishedName_t452415348::get_offset_of_name_2(),
	X500DistinguishedName_t452415348::get_offset_of_canonEncoding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (X500DistinguishedNameFlags_t2005802885)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2453[11] = 
{
	X500DistinguishedNameFlags_t2005802885::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (X509BasicConstraintsExtension_t1562873317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t1562873317::get_offset_of__certificateAuthority_5(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__hasPathLengthConstraint_6(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__pathLengthConstraint_7(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (X509Certificate2_t4056456767), -1, sizeof(X509Certificate2_t4056456767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2455[2] = 
{
	X509Certificate2_t4056456767::get_offset_of_friendlyName_4(),
	X509Certificate2_t4056456767_StaticFields::get_offset_of_signedData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (X509Certificate2Collection_t1108969367), -1, sizeof(X509Certificate2Collection_t1108969367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[1] = 
{
	X509Certificate2Collection_t1108969367_StaticFields::get_offset_of_newline_split_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (X509Certificate2Enumerator_t2356134957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[1] = 
{
	X509Certificate2Enumerator_t2356134957::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (X509Certificate2Impl_t2703153821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (X509Certificate2ImplMono_t2009068154), -1, sizeof(X509Certificate2ImplMono_t2009068154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2459[12] = 
{
	X509Certificate2ImplMono_t2009068154::get_offset_of__archived_1(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__extensions_2(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__publicKey_3(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_issuer_name_4(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_subject_name_5(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_signature_algorithm_6(),
	X509Certificate2ImplMono_t2009068154::get_offset_of_intermediateCerts_7(),
	X509Certificate2ImplMono_t2009068154::get_offset_of__cert_8(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_empty_error_9(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_commonName_10(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_email_11(),
	X509Certificate2ImplMono_t2009068154_StaticFields::get_offset_of_signedData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (X509CertificateCollection_t1197680765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (X509CertificateEnumerator_t1208230922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[1] = 
{
	X509CertificateEnumerator_t1208230922::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (X509CertificateImplCollection_t255811311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[1] = 
{
	X509CertificateImplCollection_t255811311::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (X509Chain_t777637347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[1] = 
{
	X509Chain_t777637347::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (X509ChainElement_t528874471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[4] = 
{
	X509ChainElement_t528874471::get_offset_of_certificate_0(),
	X509ChainElement_t528874471::get_offset_of_status_1(),
	X509ChainElement_t528874471::get_offset_of_info_2(),
	X509ChainElement_t528874471::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (X509ChainElementCollection_t2081831987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[1] = 
{
	X509ChainElementCollection_t2081831987::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (X509ChainElementEnumerator_t3304975821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[1] = 
{
	X509ChainElementEnumerator_t3304975821::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (X509ChainImpl_t2968295413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (X509ChainImplMono_t2085032772), -1, sizeof(X509ChainImplMono_t2085032772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2468[16] = 
{
	X509ChainImplMono_t2085032772::get_offset_of_location_0(),
	X509ChainImplMono_t2085032772::get_offset_of_elements_1(),
	X509ChainImplMono_t2085032772::get_offset_of_policy_2(),
	X509ChainImplMono_t2085032772::get_offset_of_status_3(),
	X509ChainImplMono_t2085032772_StaticFields::get_offset_of_Empty_4(),
	X509ChainImplMono_t2085032772::get_offset_of_max_path_length_5(),
	X509ChainImplMono_t2085032772::get_offset_of_working_issuer_name_6(),
	X509ChainImplMono_t2085032772::get_offset_of_working_public_key_7(),
	X509ChainImplMono_t2085032772::get_offset_of_bce_restriction_8(),
	X509ChainImplMono_t2085032772::get_offset_of_roots_9(),
	X509ChainImplMono_t2085032772::get_offset_of_cas_10(),
	X509ChainImplMono_t2085032772::get_offset_of_root_store_11(),
	X509ChainImplMono_t2085032772::get_offset_of_ca_store_12(),
	X509ChainImplMono_t2085032772::get_offset_of_user_root_store_13(),
	X509ChainImplMono_t2085032772::get_offset_of_user_ca_store_14(),
	X509ChainImplMono_t2085032772::get_offset_of_collection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (X509ChainPolicy_t3452126517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[9] = 
{
	X509ChainPolicy_t3452126517::get_offset_of_apps_0(),
	X509ChainPolicy_t3452126517::get_offset_of_cert_1(),
	X509ChainPolicy_t3452126517::get_offset_of_store_2(),
	X509ChainPolicy_t3452126517::get_offset_of_store2_3(),
	X509ChainPolicy_t3452126517::get_offset_of_rflag_4(),
	X509ChainPolicy_t3452126517::get_offset_of_mode_5(),
	X509ChainPolicy_t3452126517::get_offset_of_timeout_6(),
	X509ChainPolicy_t3452126517::get_offset_of_vflags_7(),
	X509ChainPolicy_t3452126517::get_offset_of_vtime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (X509ChainStatus_t4278378721)+ sizeof (RuntimeObject), sizeof(X509ChainStatus_t4278378721_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2470[2] = 
{
	X509ChainStatus_t4278378721::get_offset_of_status_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	X509ChainStatus_t4278378721::get_offset_of_info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (X509ChainStatusFlags_t480677120)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[27] = 
{
	X509ChainStatusFlags_t480677120::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (X509EnhancedKeyUsageExtension_t2099881051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__enhKeyUsage_3(),
	X509EnhancedKeyUsageExtension_t2099881051::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (X509Extension_t1320896183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[1] = 
{
	X509Extension_t1320896183::get_offset_of__critical_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (X509ExtensionCollection_t650873211), -1, sizeof(X509ExtensionCollection_t650873211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2474[2] = 
{
	X509ExtensionCollection_t650873211_StaticFields::get_offset_of_Empty_0(),
	X509ExtensionCollection_t650873211::get_offset_of__list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (X509ExtensionEnumerator_t3763443773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[1] = 
{
	X509ExtensionEnumerator_t3763443773::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (X509FindType_t3221716179)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2476[16] = 
{
	X509FindType_t3221716179::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (X509Helper2_t1000999714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (MyNativeHelper_t2161630009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (X509KeyUsageExtension_t1038124237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t1038124237::get_offset_of__keyUsages_6(),
	X509KeyUsageExtension_t1038124237::get_offset_of__status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (X509KeyUsageFlags_t2461349531)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2480[11] = 
{
	X509KeyUsageFlags_t2461349531::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (X509NameType_t2669466891)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2481[7] = 
{
	X509NameType_t2669466891::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (X509RevocationFlag_t2166064554)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2482[4] = 
{
	X509RevocationFlag_t2166064554::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (X509RevocationMode_t2065307963)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2483[4] = 
{
	X509RevocationMode_t2065307963::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (X509Store_t1617430119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[5] = 
{
	X509Store_t1617430119::get_offset_of__name_0(),
	X509Store_t1617430119::get_offset_of__location_1(),
	X509Store_t1617430119::get_offset_of_list_2(),
	X509Store_t1617430119::get_offset_of__flags_3(),
	X509Store_t1617430119::get_offset_of_store_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (X509SubjectKeyIdentifierExtension_t2508879999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__subjectKeyIdentifier_5(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__ski_6(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t110301003)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2486[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t110301003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (X509VerificationFlags_t2169036324)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2487[15] = 
{
	X509VerificationFlags_t2169036324::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (SslProtocols_t894678499)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2488[8] = 
{
	SslProtocols_t894678499::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (Regex_t1803876613), -1, sizeof(Regex_t1803876613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2489[18] = 
{
	Regex_t1803876613::get_offset_of_pattern_0(),
	Regex_t1803876613::get_offset_of_factory_1(),
	Regex_t1803876613::get_offset_of_roptions_2(),
	Regex_t1803876613_StaticFields::get_offset_of_MaximumMatchTimeout_3(),
	Regex_t1803876613_StaticFields::get_offset_of_InfiniteMatchTimeout_4(),
	Regex_t1803876613::get_offset_of_internalMatchTimeout_5(),
	Regex_t1803876613_StaticFields::get_offset_of_FallbackDefaultMatchTimeout_6(),
	Regex_t1803876613_StaticFields::get_offset_of_DefaultMatchTimeout_7(),
	Regex_t1803876613::get_offset_of_caps_8(),
	Regex_t1803876613::get_offset_of_capnames_9(),
	Regex_t1803876613::get_offset_of_capslist_10(),
	Regex_t1803876613::get_offset_of_capsize_11(),
	Regex_t1803876613::get_offset_of_runnerref_12(),
	Regex_t1803876613::get_offset_of_replref_13(),
	Regex_t1803876613::get_offset_of_code_14(),
	Regex_t1803876613::get_offset_of_refsInitialized_15(),
	Regex_t1803876613_StaticFields::get_offset_of_livecode_16(),
	Regex_t1803876613_StaticFields::get_offset_of_cacheSize_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (CachedCodeEntry_t3553821051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[9] = 
{
	CachedCodeEntry_t3553821051::get_offset_of__key_0(),
	CachedCodeEntry_t3553821051::get_offset_of__code_1(),
	CachedCodeEntry_t3553821051::get_offset_of__caps_2(),
	CachedCodeEntry_t3553821051::get_offset_of__capnames_3(),
	CachedCodeEntry_t3553821051::get_offset_of__capslist_4(),
	CachedCodeEntry_t3553821051::get_offset_of__capsize_5(),
	CachedCodeEntry_t3553821051::get_offset_of__factory_6(),
	CachedCodeEntry_t3553821051::get_offset_of__runnerref_7(),
	CachedCodeEntry_t3553821051::get_offset_of__replref_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (ExclusiveReference_t708182869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[3] = 
{
	ExclusiveReference_t708182869::get_offset_of__ref_0(),
	ExclusiveReference_t708182869::get_offset_of__obj_1(),
	ExclusiveReference_t708182869::get_offset_of__locked_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (SharedReference_t2137668360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[1] = 
{
	SharedReference_t2137668360::get_offset_of__ref_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (RegexBoyerMoore_t2204811018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[9] = 
{
	RegexBoyerMoore_t2204811018::get_offset_of__positive_0(),
	RegexBoyerMoore_t2204811018::get_offset_of__negativeASCII_1(),
	RegexBoyerMoore_t2204811018::get_offset_of__negativeUnicode_2(),
	RegexBoyerMoore_t2204811018::get_offset_of__pattern_3(),
	RegexBoyerMoore_t2204811018::get_offset_of__lowASCII_4(),
	RegexBoyerMoore_t2204811018::get_offset_of__highASCII_5(),
	RegexBoyerMoore_t2204811018::get_offset_of__rightToLeft_6(),
	RegexBoyerMoore_t2204811018::get_offset_of__caseInsensitive_7(),
	RegexBoyerMoore_t2204811018::get_offset_of__culture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[3] = 
{
	Capture_t4157900610::get_offset_of__text_0(),
	Capture_t4157900610::get_offset_of__index_1(),
	Capture_t4157900610::get_offset_of__length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (RegexCharClass_t2441867401), -1, sizeof(RegexCharClass_t2441867401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2495[19] = 
{
	RegexCharClass_t2441867401::get_offset_of__rangelist_0(),
	RegexCharClass_t2441867401::get_offset_of__categories_1(),
	RegexCharClass_t2441867401::get_offset_of__canonical_2(),
	RegexCharClass_t2441867401::get_offset_of__negate_3(),
	RegexCharClass_t2441867401::get_offset_of__subtractor_4(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_InternalRegexIgnoreCase_5(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_Space_6(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotSpace_7(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_Word_8(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotWord_9(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_SpaceClass_10(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotSpaceClass_11(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_WordClass_12(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotWordClass_13(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_DigitClass_14(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of_NotDigitClass_15(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__definedCategories_16(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__propTable_17(),
	RegexCharClass_t2441867401_StaticFields::get_offset_of__lcTable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (LowerCaseMapping_t2153935826)+ sizeof (RuntimeObject), sizeof(LowerCaseMapping_t2153935826_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2496[4] = 
{
	LowerCaseMapping_t2153935826::get_offset_of__chMin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2153935826::get_offset_of__chMax_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2153935826::get_offset_of__lcOp_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LowerCaseMapping_t2153935826::get_offset_of__data_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (SingleRangeComparer_t3640568023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (SingleRange_t3794243288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[2] = 
{
	SingleRange_t3794243288::get_offset_of__first_0(),
	SingleRange_t3794243288::get_offset_of__last_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (RegexCode_t2469392150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[9] = 
{
	RegexCode_t2469392150::get_offset_of__codes_0(),
	RegexCode_t2469392150::get_offset_of__strings_1(),
	RegexCode_t2469392150::get_offset_of__trackcount_2(),
	RegexCode_t2469392150::get_offset_of__caps_3(),
	RegexCode_t2469392150::get_offset_of__capsize_4(),
	RegexCode_t2469392150::get_offset_of__fcPrefix_5(),
	RegexCode_t2469392150::get_offset_of__bmPrefix_6(),
	RegexCode_t2469392150::get_offset_of__anchors_7(),
	RegexCode_t2469392150::get_offset_of__rightToLeft_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
