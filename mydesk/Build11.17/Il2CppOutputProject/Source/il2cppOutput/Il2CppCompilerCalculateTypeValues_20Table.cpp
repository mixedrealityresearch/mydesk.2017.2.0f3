﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t1619962557;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t2971213394;
// System.String
struct String_t;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t2064808786;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t3518500204;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_t482152337;
// System.Xml.PositionInfo
struct PositionInfo_t3273236083;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Uri
struct Uri_t19570940;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_t87206461;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t1507412803;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Schema.ValidationState
struct ValidationState_t3143048826;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_t1152094268;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Xml.Schema.DtdValidator/NamespaceManager
struct NamespaceManager_t1407344;
// System.Xml.HWStack
struct HWStack_t738999989;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t224554150;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Xml.Schema.RestrictionFacets
struct RestrictionFacets_t4012658256;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t2024909611;
// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map[]
struct MapU5BU5D_t3908492414;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_t68179724;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t192177157;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t1235574227;
// System.Xml.Schema.DatatypeImplementation[]
struct DatatypeImplementationU5BU5D_t3131202389;
// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[]
struct SchemaDatatypeMapU5BU5D_t863562528;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.Schema.SchemaBuilder
struct SchemaBuilder_t908297946;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Xml.Schema.BitSet
struct BitSet_t1062448123;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4015859774;
// System.Type
struct Type_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef SCHEMABUILDER_T908297946_H
#define SCHEMABUILDER_T908297946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaBuilder
struct  SchemaBuilder_t908297946  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMABUILDER_T908297946_H
#ifndef BASEPROCESSOR_T2373158431_H
#define BASEPROCESSOR_T2373158431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseProcessor
struct  BaseProcessor_t2373158431  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.BaseProcessor::nameTable
	XmlNameTable_t1345805268 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseProcessor::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.BaseProcessor::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.BaseProcessor::compilationSettings
	XmlSchemaCompilationSettings_t2971213394 * ___compilationSettings_3;
	// System.Int32 System.Xml.Schema.BaseProcessor::errorCount
	int32_t ___errorCount_4;
	// System.String System.Xml.Schema.BaseProcessor::NsXml
	String_t* ___NsXml_5;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___nameTable_0)); }
	inline XmlNameTable_t1345805268 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t1345805268 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___schemaNames_1)); }
	inline SchemaNames_t1619962557 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t1619962557 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_1), value);
	}

	inline static int32_t get_offset_of_eventHandler_2() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___eventHandler_2)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_2() const { return ___eventHandler_2; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_2() { return &___eventHandler_2; }
	inline void set_eventHandler_2(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_2), value);
	}

	inline static int32_t get_offset_of_compilationSettings_3() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___compilationSettings_3)); }
	inline XmlSchemaCompilationSettings_t2971213394 * get_compilationSettings_3() const { return ___compilationSettings_3; }
	inline XmlSchemaCompilationSettings_t2971213394 ** get_address_of_compilationSettings_3() { return &___compilationSettings_3; }
	inline void set_compilationSettings_3(XmlSchemaCompilationSettings_t2971213394 * value)
	{
		___compilationSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___compilationSettings_3), value);
	}

	inline static int32_t get_offset_of_errorCount_4() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___errorCount_4)); }
	inline int32_t get_errorCount_4() const { return ___errorCount_4; }
	inline int32_t* get_address_of_errorCount_4() { return &___errorCount_4; }
	inline void set_errorCount_4(int32_t value)
	{
		___errorCount_4 = value;
	}

	inline static int32_t get_offset_of_NsXml_5() { return static_cast<int32_t>(offsetof(BaseProcessor_t2373158431, ___NsXml_5)); }
	inline String_t* get_NsXml_5() const { return ___NsXml_5; }
	inline String_t** get_address_of_NsXml_5() { return &___NsXml_5; }
	inline void set_NsXml_5(String_t* value)
	{
		___NsXml_5 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPROCESSOR_T2373158431_H
#ifndef XMLNAMESPACEMANAGER_T486731501_H
#define XMLNAMESPACEMANAGER_T486731501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t486731501  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t2064808786* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_t3986656710 * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t2064808786* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t2064808786** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t2064808786* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_0), value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___hashTable_4)); }
	inline Dictionary_2_t3986656710 * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_t3986656710 ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_t3986656710 * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashTable_4), value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((&___xml_6), value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlNs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T486731501_H
#ifndef XMLSCHEMADATATYPE_T1195946242_H
#define XMLSCHEMADATATYPE_T1195946242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t1195946242  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T1195946242_H
#ifndef FACETSCHECKER_T1235574227_H
#define FACETSCHECKER_T1235574227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker
struct  FacetsChecker_t1235574227  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACETSCHECKER_T1235574227_H
#ifndef BASEVALIDATOR_T3557140249_H
#define BASEVALIDATOR_T3557140249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_t3557140249  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_t3518500204 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t1345805268 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t19570940 * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_t87206461 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t1507412803 * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_t1944712516 * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t3143048826 * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t1221177846 * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaCollection_0)); }
	inline XmlSchemaCollection_t3518500204 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_t3518500204 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_t3518500204 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___nameTable_2)); }
	inline XmlNameTable_t1345805268 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t1345805268 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaNames_3)); }
	inline SchemaNames_t1619962557 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t1619962557 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___positionInfo_4)); }
	inline PositionInfo_t3273236083 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_t3273236083 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___xmlResolver_5)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___baseUri_6)); }
	inline Uri_t19570940 * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t19570940 ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t19570940 * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___schemaInfo_7)); }
	inline SchemaInfo_t87206461 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_t87206461 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_t87206461 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___reader_8)); }
	inline XmlValidatingReaderImpl_t1507412803 * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t1507412803 ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t1507412803 * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___elementName_9)); }
	inline XmlQualifiedName_t1944712516 * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_t1944712516 * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___context_10)); }
	inline ValidationState_t3143048826 * get_context_10() const { return ___context_10; }
	inline ValidationState_t3143048826 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t3143048826 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textValue_11)); }
	inline StringBuilder_t1221177846 * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t1221177846 ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t1221177846 * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_t3557140249, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_T3557140249_H
#ifndef SCHEMADATATYPEMAP_T2661667341_H
#define SCHEMADATATYPEMAP_T2661667341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap
struct  SchemaDatatypeMap_t2661667341  : public RuntimeObject
{
public:
	// System.String System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::name
	String_t* ___name_0;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::type
	DatatypeImplementation_t1152094268 * ___type_1;
	// System.Int32 System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap::parentIndex
	int32_t ___parentIndex_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t2661667341, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t2661667341, ___type_1)); }
	inline DatatypeImplementation_t1152094268 * get_type_1() const { return ___type_1; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(DatatypeImplementation_t1152094268 * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_parentIndex_2() { return static_cast<int32_t>(offsetof(SchemaDatatypeMap_t2661667341, ___parentIndex_2)); }
	inline int32_t get_parentIndex_2() const { return ___parentIndex_2; }
	inline int32_t* get_address_of_parentIndex_2() { return &___parentIndex_2; }
	inline void set_parentIndex_2(int32_t value)
	{
		___parentIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMADATATYPEMAP_T2661667341_H
#ifndef XSDSIMPLEVALUE_T478440528_H
#define XSDSIMPLEVALUE_T478440528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdSimpleValue
struct  XsdSimpleValue_t478440528  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XsdSimpleValue::xmlType
	XmlSchemaSimpleType_t248156492 * ___xmlType_0;
	// System.Object System.Xml.Schema.XsdSimpleValue::typedValue
	RuntimeObject * ___typedValue_1;

public:
	inline static int32_t get_offset_of_xmlType_0() { return static_cast<int32_t>(offsetof(XsdSimpleValue_t478440528, ___xmlType_0)); }
	inline XmlSchemaSimpleType_t248156492 * get_xmlType_0() const { return ___xmlType_0; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_xmlType_0() { return &___xmlType_0; }
	inline void set_xmlType_0(XmlSchemaSimpleType_t248156492 * value)
	{
		___xmlType_0 = value;
		Il2CppCodeGenWriteBarrier((&___xmlType_0), value);
	}

	inline static int32_t get_offset_of_typedValue_1() { return static_cast<int32_t>(offsetof(XsdSimpleValue_t478440528, ___typedValue_1)); }
	inline RuntimeObject * get_typedValue_1() const { return ___typedValue_1; }
	inline RuntimeObject ** get_address_of_typedValue_1() { return &___typedValue_1; }
	inline void set_typedValue_1(RuntimeObject * value)
	{
		___typedValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___typedValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSIMPLEVALUE_T478440528_H
#ifndef LISTFACETSCHECKER_T961741019_H
#define LISTFACETSCHECKER_T961741019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ListFacetsChecker
struct  ListFacetsChecker_t961741019  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTFACETSCHECKER_T961741019_H
#ifndef UNIONFACETSCHECKER_T3025714558_H
#define UNIONFACETSCHECKER_T3025714558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UnionFacetsChecker
struct  UnionFacetsChecker_t3025714558  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONFACETSCHECKER_T3025714558_H
#ifndef BINARYFACETSCHECKER_T4115715422_H
#define BINARYFACETSCHECKER_T4115715422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BinaryFacetsChecker
struct  BinaryFacetsChecker_t4115715422  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYFACETSCHECKER_T4115715422_H
#ifndef MISCFACETSCHECKER_T3607863675_H
#define MISCFACETSCHECKER_T3607863675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.MiscFacetsChecker
struct  MiscFacetsChecker_t3607863675  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCFACETSCHECKER_T3607863675_H
#ifndef QNAMEFACETSCHECKER_T3974186457_H
#define QNAMEFACETSCHECKER_T3974186457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QNameFacetsChecker
struct  QNameFacetsChecker_t3974186457  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QNAMEFACETSCHECKER_T3974186457_H
#ifndef PREPROCESSOR_T4137165425_H
#define PREPROCESSOR_T4137165425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Preprocessor
struct  Preprocessor_t4137165425  : public BaseProcessor_t2373158431
{
public:

public:
};

struct Preprocessor_t4137165425_StaticFields
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Preprocessor::builtInSchemaForXmlNS
	XmlSchema_t880472818 * ___builtInSchemaForXmlNS_6;

public:
	inline static int32_t get_offset_of_builtInSchemaForXmlNS_6() { return static_cast<int32_t>(offsetof(Preprocessor_t4137165425_StaticFields, ___builtInSchemaForXmlNS_6)); }
	inline XmlSchema_t880472818 * get_builtInSchemaForXmlNS_6() const { return ___builtInSchemaForXmlNS_6; }
	inline XmlSchema_t880472818 ** get_address_of_builtInSchemaForXmlNS_6() { return &___builtInSchemaForXmlNS_6; }
	inline void set_builtInSchemaForXmlNS_6(XmlSchema_t880472818 * value)
	{
		___builtInSchemaForXmlNS_6 = value;
		Il2CppCodeGenWriteBarrier((&___builtInSchemaForXmlNS_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREPROCESSOR_T4137165425_H
#ifndef NUMERIC2FACETSCHECKER_T2224492892_H
#define NUMERIC2FACETSCHECKER_T2224492892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Numeric2FacetsChecker
struct  Numeric2FacetsChecker_t2224492892  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERIC2FACETSCHECKER_T2224492892_H
#ifndef DURATIONFACETSCHECKER_T4254067383_H
#define DURATIONFACETSCHECKER_T4254067383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DurationFacetsChecker
struct  DurationFacetsChecker_t4254067383  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONFACETSCHECKER_T4254067383_H
#ifndef MAP_T2552390023_H
#define MAP_T2552390023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct  Map_t2552390023 
{
public:
	// System.Char System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::match
	Il2CppChar ___match_0;
	// System.String System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::replacement
	String_t* ___replacement_1;

public:
	inline static int32_t get_offset_of_match_0() { return static_cast<int32_t>(offsetof(Map_t2552390023, ___match_0)); }
	inline Il2CppChar get_match_0() const { return ___match_0; }
	inline Il2CppChar* get_address_of_match_0() { return &___match_0; }
	inline void set_match_0(Il2CppChar value)
	{
		___match_0 = value;
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(Map_t2552390023, ___replacement_1)); }
	inline String_t* get_replacement_1() const { return ___replacement_1; }
	inline String_t** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(String_t* value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t2552390023_marshaled_pinvoke
{
	uint8_t ___match_0;
	char* ___replacement_1;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t2552390023_marshaled_com
{
	uint8_t ___match_0;
	Il2CppChar* ___replacement_1;
};
#endif // MAP_T2552390023_H
#ifndef SCHEMACOLLECTIONCOMPILER_T1443051254_H
#define SCHEMACOLLECTIONCOMPILER_T1443051254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionCompiler
struct  SchemaCollectionCompiler_t1443051254  : public BaseProcessor_t2373158431
{
public:
	// System.Boolean System.Xml.Schema.SchemaCollectionCompiler::compileContentModel
	bool ___compileContentModel_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.SchemaCollectionCompiler::examplars
	XmlSchemaObjectTable_t3364835593 * ___examplars_7;
	// System.Collections.Stack System.Xml.Schema.SchemaCollectionCompiler::complexTypeStack
	Stack_t1043988394 * ___complexTypeStack_8;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.SchemaCollectionCompiler::schema
	XmlSchema_t880472818 * ___schema_9;

public:
	inline static int32_t get_offset_of_compileContentModel_6() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___compileContentModel_6)); }
	inline bool get_compileContentModel_6() const { return ___compileContentModel_6; }
	inline bool* get_address_of_compileContentModel_6() { return &___compileContentModel_6; }
	inline void set_compileContentModel_6(bool value)
	{
		___compileContentModel_6 = value;
	}

	inline static int32_t get_offset_of_examplars_7() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___examplars_7)); }
	inline XmlSchemaObjectTable_t3364835593 * get_examplars_7() const { return ___examplars_7; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_examplars_7() { return &___examplars_7; }
	inline void set_examplars_7(XmlSchemaObjectTable_t3364835593 * value)
	{
		___examplars_7 = value;
		Il2CppCodeGenWriteBarrier((&___examplars_7), value);
	}

	inline static int32_t get_offset_of_complexTypeStack_8() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___complexTypeStack_8)); }
	inline Stack_t1043988394 * get_complexTypeStack_8() const { return ___complexTypeStack_8; }
	inline Stack_t1043988394 ** get_address_of_complexTypeStack_8() { return &___complexTypeStack_8; }
	inline void set_complexTypeStack_8(Stack_t1043988394 * value)
	{
		___complexTypeStack_8 = value;
		Il2CppCodeGenWriteBarrier((&___complexTypeStack_8), value);
	}

	inline static int32_t get_offset_of_schema_9() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_t1443051254, ___schema_9)); }
	inline XmlSchema_t880472818 * get_schema_9() const { return ___schema_9; }
	inline XmlSchema_t880472818 ** get_address_of_schema_9() { return &___schema_9; }
	inline void set_schema_9(XmlSchema_t880472818 * value)
	{
		___schema_9 = value;
		Il2CppCodeGenWriteBarrier((&___schema_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMACOLLECTIONCOMPILER_T1443051254_H
#ifndef DATETIMEFACETSCHECKER_T2220827556_H
#define DATETIMEFACETSCHECKER_T2220827556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DateTimeFacetsChecker
struct  DateTimeFacetsChecker_t2220827556  : public FacetsChecker_t1235574227
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEFACETSCHECKER_T2220827556_H
#ifndef STRINGFACETSCHECKER_T2109036348_H
#define STRINGFACETSCHECKER_T2109036348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringFacetsChecker
struct  StringFacetsChecker_t2109036348  : public FacetsChecker_t1235574227
{
public:

public:
};

struct StringFacetsChecker_t2109036348_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex System.Xml.Schema.StringFacetsChecker::languagePattern
	Regex_t1803876613 * ___languagePattern_0;

public:
	inline static int32_t get_offset_of_languagePattern_0() { return static_cast<int32_t>(offsetof(StringFacetsChecker_t2109036348_StaticFields, ___languagePattern_0)); }
	inline Regex_t1803876613 * get_languagePattern_0() const { return ___languagePattern_0; }
	inline Regex_t1803876613 ** get_address_of_languagePattern_0() { return &___languagePattern_0; }
	inline void set_languagePattern_0(Regex_t1803876613 * value)
	{
		___languagePattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___languagePattern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGFACETSCHECKER_T2109036348_H
#ifndef XMLCHARTYPE_T1050521405_H
#define XMLCHARTYPE_T1050521405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t1050521405 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_t3397334013* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405, ___charProperties_2)); }
	inline ByteU5BU5D_t3397334013* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_t3397334013* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t1050521405_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_t3397334013* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t1050521405_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_t3397334013* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_t3397334013* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t1050521405_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t1050521405_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T1050521405_H
#ifndef NAMESPACEMANAGER_T1407344_H
#define NAMESPACEMANAGER_T1407344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DtdValidator/NamespaceManager
struct  NamespaceManager_t1407344  : public XmlNamespaceManager_t486731501
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEMANAGER_T1407344_H
#ifndef DTDVALIDATOR_T1639720164_H
#define DTDVALIDATOR_T1639720164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DtdValidator
struct  DtdValidator_t1639720164  : public BaseValidator_t3557140249
{
public:
	// System.Xml.HWStack System.Xml.Schema.DtdValidator::validationStack
	HWStack_t738999989 * ___validationStack_16;
	// System.Collections.Hashtable System.Xml.Schema.DtdValidator::attPresence
	Hashtable_t909839986 * ___attPresence_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DtdValidator::name
	XmlQualifiedName_t1944712516 * ___name_18;
	// System.Collections.Hashtable System.Xml.Schema.DtdValidator::IDs
	Hashtable_t909839986 * ___IDs_19;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.DtdValidator::idRefListHead
	IdRefNode_t224554150 * ___idRefListHead_20;
	// System.Boolean System.Xml.Schema.DtdValidator::processIdentityConstraints
	bool ___processIdentityConstraints_21;

public:
	inline static int32_t get_offset_of_validationStack_16() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___validationStack_16)); }
	inline HWStack_t738999989 * get_validationStack_16() const { return ___validationStack_16; }
	inline HWStack_t738999989 ** get_address_of_validationStack_16() { return &___validationStack_16; }
	inline void set_validationStack_16(HWStack_t738999989 * value)
	{
		___validationStack_16 = value;
		Il2CppCodeGenWriteBarrier((&___validationStack_16), value);
	}

	inline static int32_t get_offset_of_attPresence_17() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___attPresence_17)); }
	inline Hashtable_t909839986 * get_attPresence_17() const { return ___attPresence_17; }
	inline Hashtable_t909839986 ** get_address_of_attPresence_17() { return &___attPresence_17; }
	inline void set_attPresence_17(Hashtable_t909839986 * value)
	{
		___attPresence_17 = value;
		Il2CppCodeGenWriteBarrier((&___attPresence_17), value);
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___name_18)); }
	inline XmlQualifiedName_t1944712516 * get_name_18() const { return ___name_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(XmlQualifiedName_t1944712516 * value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_IDs_19() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___IDs_19)); }
	inline Hashtable_t909839986 * get_IDs_19() const { return ___IDs_19; }
	inline Hashtable_t909839986 ** get_address_of_IDs_19() { return &___IDs_19; }
	inline void set_IDs_19(Hashtable_t909839986 * value)
	{
		___IDs_19 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_19), value);
	}

	inline static int32_t get_offset_of_idRefListHead_20() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___idRefListHead_20)); }
	inline IdRefNode_t224554150 * get_idRefListHead_20() const { return ___idRefListHead_20; }
	inline IdRefNode_t224554150 ** get_address_of_idRefListHead_20() { return &___idRefListHead_20; }
	inline void set_idRefListHead_20(IdRefNode_t224554150 * value)
	{
		___idRefListHead_20 = value;
		Il2CppCodeGenWriteBarrier((&___idRefListHead_20), value);
	}

	inline static int32_t get_offset_of_processIdentityConstraints_21() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164, ___processIdentityConstraints_21)); }
	inline bool get_processIdentityConstraints_21() const { return ___processIdentityConstraints_21; }
	inline bool* get_address_of_processIdentityConstraints_21() { return &___processIdentityConstraints_21; }
	inline void set_processIdentityConstraints_21(bool value)
	{
		___processIdentityConstraints_21 = value;
	}
};

struct DtdValidator_t1639720164_StaticFields
{
public:
	// System.Xml.Schema.DtdValidator/NamespaceManager System.Xml.Schema.DtdValidator::namespaceManager
	NamespaceManager_t1407344 * ___namespaceManager_15;

public:
	inline static int32_t get_offset_of_namespaceManager_15() { return static_cast<int32_t>(offsetof(DtdValidator_t1639720164_StaticFields, ___namespaceManager_15)); }
	inline NamespaceManager_t1407344 * get_namespaceManager_15() const { return ___namespaceManager_15; }
	inline NamespaceManager_t1407344 ** get_address_of_namespaceManager_15() { return &___namespaceManager_15; }
	inline void set_namespaceManager_15(NamespaceManager_t1407344 * value)
	{
		___namespaceManager_15 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDVALIDATOR_T1639720164_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef DECIMAL_T724701077_H
#define DECIMAL_T724701077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t724701077 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t724701077_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t59386216* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t724701077  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t724701077  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t724701077  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t724701077  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t724701077  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t724701077  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t724701077  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t59386216* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t59386216** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t59386216* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___Zero_7)); }
	inline Decimal_t724701077  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t724701077 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t724701077  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___One_8)); }
	inline Decimal_t724701077  get_One_8() const { return ___One_8; }
	inline Decimal_t724701077 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t724701077  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinusOne_9)); }
	inline Decimal_t724701077  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t724701077 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t724701077  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MaxValue_10)); }
	inline Decimal_t724701077  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t724701077 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t724701077  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinValue_11)); }
	inline Decimal_t724701077  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t724701077 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t724701077  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t724701077  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t724701077 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t724701077  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t724701077  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t724701077 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t724701077  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T724701077_H
#ifndef XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#define XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_t3165007540 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_t3165007540, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#ifndef LISTTYPE_T2879180877_H
#define LISTTYPE_T2879180877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceList/ListType
struct  ListType_t2879180877 
{
public:
	// System.Int32 System.Xml.Schema.NamespaceList/ListType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ListType_t2879180877, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTTYPE_T2879180877_H
#ifndef RESTRICTIONFLAGS_T2588355947_H
#define RESTRICTIONFLAGS_T2588355947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RestrictionFlags
struct  RestrictionFlags_t2588355947 
{
public:
	// System.Int32 System.Xml.Schema.RestrictionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RestrictionFlags_t2588355947, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTIONFLAGS_T2588355947_H
#ifndef XMLSCHEMADATATYPEVARIETY_T2237606318_H
#define XMLSCHEMADATATYPEVARIETY_T2237606318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatypeVariety
struct  XmlSchemaDatatypeVariety_t2237606318 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDatatypeVariety::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatypeVariety_t2237606318, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPEVARIETY_T2237606318_H
#ifndef XMLSCHEMACONTENTTYPE_T2874429441_H
#define XMLSCHEMACONTENTTYPE_T2874429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentType
struct  XmlSchemaContentType_t2874429441 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentType_t2874429441, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTTYPE_T2874429441_H
#ifndef XMLSCHEMAWHITESPACE_T3746245107_H
#define XMLSCHEMAWHITESPACE_T3746245107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaWhiteSpace
struct  XmlSchemaWhiteSpace_t3746245107 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaWhiteSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaWhiteSpace_t3746245107, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAWHITESPACE_T3746245107_H
#ifndef XMLSCHEMAFORM_T1143227640_H
#define XMLSCHEMAFORM_T1143227640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t1143227640 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t1143227640, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T1143227640_H
#ifndef COMPOSITOR_T3622502717_H
#define COMPOSITOR_T3622502717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionPreprocessor/Compositor
struct  Compositor_t3622502717 
{
public:
	// System.Int32 System.Xml.Schema.SchemaCollectionPreprocessor/Compositor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compositor_t3622502717, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITOR_T3622502717_H
#ifndef XMLTYPECODE_T58293802_H
#define XMLTYPECODE_T58293802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlTypeCode
struct  XmlTypeCode_t58293802 
{
public:
	// System.Int32 System.Xml.Schema.XmlTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlTypeCode_t58293802, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECODE_T58293802_H
#ifndef XSDDATETIMEFLAGS_T2172197783_H
#define XSDDATETIMEFLAGS_T2172197783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTimeFlags
struct  XsdDateTimeFlags_t2172197783 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTimeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XsdDateTimeFlags_t2172197783, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIMEFLAGS_T2172197783_H
#ifndef USE_T2612116671_H
#define USE_T2612116671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaDeclBase/Use
struct  Use_t2612116671 
{
public:
	// System.Int32 System.Xml.Schema.SchemaDeclBase/Use::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Use_t2612116671, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USE_T2612116671_H
#ifndef COMPOSITOR_T2211431003_H
#define COMPOSITOR_T2211431003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Compositor
struct  Compositor_t2211431003 
{
public:
	// System.Int32 System.Xml.Schema.Compositor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compositor_t2211431003, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITOR_T2211431003_H
#ifndef NUMERIC10FACETSCHECKER_T3431899547_H
#define NUMERIC10FACETSCHECKER_T3431899547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Numeric10FacetsChecker
struct  Numeric10FacetsChecker_t3431899547  : public FacetsChecker_t1235574227
{
public:
	// System.Decimal System.Xml.Schema.Numeric10FacetsChecker::maxValue
	Decimal_t724701077  ___maxValue_1;
	// System.Decimal System.Xml.Schema.Numeric10FacetsChecker::minValue
	Decimal_t724701077  ___minValue_2;

public:
	inline static int32_t get_offset_of_maxValue_1() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_t3431899547, ___maxValue_1)); }
	inline Decimal_t724701077  get_maxValue_1() const { return ___maxValue_1; }
	inline Decimal_t724701077 * get_address_of_maxValue_1() { return &___maxValue_1; }
	inline void set_maxValue_1(Decimal_t724701077  value)
	{
		___maxValue_1 = value;
	}

	inline static int32_t get_offset_of_minValue_2() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_t3431899547, ___minValue_2)); }
	inline Decimal_t724701077  get_minValue_2() const { return ___minValue_2; }
	inline Decimal_t724701077 * get_address_of_minValue_2() { return &___minValue_2; }
	inline void set_minValue_2(Decimal_t724701077  value)
	{
		___minValue_2 = value;
	}
};

struct Numeric10FacetsChecker_t3431899547_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.Numeric10FacetsChecker::signs
	CharU5BU5D_t1328083999* ___signs_0;

public:
	inline static int32_t get_offset_of_signs_0() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_t3431899547_StaticFields, ___signs_0)); }
	inline CharU5BU5D_t1328083999* get_signs_0() const { return ___signs_0; }
	inline CharU5BU5D_t1328083999** get_address_of_signs_0() { return &___signs_0; }
	inline void set_signs_0(CharU5BU5D_t1328083999* value)
	{
		___signs_0 = value;
		Il2CppCodeGenWriteBarrier((&___signs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERIC10FACETSCHECKER_T3431899547_H
#ifndef SCHEMATYPE_T3522160305_H
#define SCHEMATYPE_T3522160305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaType
struct  SchemaType_t3522160305 
{
public:
	// System.Int32 System.Xml.Schema.SchemaType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SchemaType_t3522160305, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMATYPE_T3522160305_H
#ifndef RESERVE_T2330987269_H
#define RESERVE_T2330987269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaAttDef/Reserve
struct  Reserve_t2330987269 
{
public:
	// System.Int32 System.Xml.Schema.SchemaAttDef/Reserve::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Reserve_t2330987269, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESERVE_T2330987269_H
#ifndef FACETSCOMPILER_T3565032206_H
#define FACETSCOMPILER_T3565032206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler
struct  FacetsCompiler_t3565032206 
{
public:
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.FacetsChecker/FacetsCompiler::datatype
	DatatypeImplementation_t1152094268 * ___datatype_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.FacetsChecker/FacetsCompiler::derivedRestriction
	RestrictionFacets_t4012658256 * ___derivedRestriction_1;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::baseFlags
	int32_t ___baseFlags_2;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::baseFixedFlags
	int32_t ___baseFixedFlags_3;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::validRestrictionFlags
	int32_t ___validRestrictionFlags_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.FacetsChecker/FacetsCompiler::nonNegativeInt
	XmlSchemaDatatype_t1195946242 * ___nonNegativeInt_5;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.FacetsChecker/FacetsCompiler::builtInType
	XmlSchemaDatatype_t1195946242 * ___builtInType_6;
	// System.Xml.Schema.XmlTypeCode System.Xml.Schema.FacetsChecker/FacetsCompiler::builtInEnum
	int32_t ___builtInEnum_7;
	// System.Boolean System.Xml.Schema.FacetsChecker/FacetsCompiler::firstPattern
	bool ___firstPattern_8;
	// System.Text.StringBuilder System.Xml.Schema.FacetsChecker/FacetsCompiler::regStr
	StringBuilder_t1221177846 * ___regStr_9;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Schema.FacetsChecker/FacetsCompiler::pattern_facet
	XmlSchemaPatternFacet_t2024909611 * ___pattern_facet_10;

public:
	inline static int32_t get_offset_of_datatype_0() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___datatype_0)); }
	inline DatatypeImplementation_t1152094268 * get_datatype_0() const { return ___datatype_0; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_datatype_0() { return &___datatype_0; }
	inline void set_datatype_0(DatatypeImplementation_t1152094268 * value)
	{
		___datatype_0 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_0), value);
	}

	inline static int32_t get_offset_of_derivedRestriction_1() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___derivedRestriction_1)); }
	inline RestrictionFacets_t4012658256 * get_derivedRestriction_1() const { return ___derivedRestriction_1; }
	inline RestrictionFacets_t4012658256 ** get_address_of_derivedRestriction_1() { return &___derivedRestriction_1; }
	inline void set_derivedRestriction_1(RestrictionFacets_t4012658256 * value)
	{
		___derivedRestriction_1 = value;
		Il2CppCodeGenWriteBarrier((&___derivedRestriction_1), value);
	}

	inline static int32_t get_offset_of_baseFlags_2() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___baseFlags_2)); }
	inline int32_t get_baseFlags_2() const { return ___baseFlags_2; }
	inline int32_t* get_address_of_baseFlags_2() { return &___baseFlags_2; }
	inline void set_baseFlags_2(int32_t value)
	{
		___baseFlags_2 = value;
	}

	inline static int32_t get_offset_of_baseFixedFlags_3() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___baseFixedFlags_3)); }
	inline int32_t get_baseFixedFlags_3() const { return ___baseFixedFlags_3; }
	inline int32_t* get_address_of_baseFixedFlags_3() { return &___baseFixedFlags_3; }
	inline void set_baseFixedFlags_3(int32_t value)
	{
		___baseFixedFlags_3 = value;
	}

	inline static int32_t get_offset_of_validRestrictionFlags_4() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___validRestrictionFlags_4)); }
	inline int32_t get_validRestrictionFlags_4() const { return ___validRestrictionFlags_4; }
	inline int32_t* get_address_of_validRestrictionFlags_4() { return &___validRestrictionFlags_4; }
	inline void set_validRestrictionFlags_4(int32_t value)
	{
		___validRestrictionFlags_4 = value;
	}

	inline static int32_t get_offset_of_nonNegativeInt_5() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___nonNegativeInt_5)); }
	inline XmlSchemaDatatype_t1195946242 * get_nonNegativeInt_5() const { return ___nonNegativeInt_5; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_nonNegativeInt_5() { return &___nonNegativeInt_5; }
	inline void set_nonNegativeInt_5(XmlSchemaDatatype_t1195946242 * value)
	{
		___nonNegativeInt_5 = value;
		Il2CppCodeGenWriteBarrier((&___nonNegativeInt_5), value);
	}

	inline static int32_t get_offset_of_builtInType_6() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___builtInType_6)); }
	inline XmlSchemaDatatype_t1195946242 * get_builtInType_6() const { return ___builtInType_6; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_builtInType_6() { return &___builtInType_6; }
	inline void set_builtInType_6(XmlSchemaDatatype_t1195946242 * value)
	{
		___builtInType_6 = value;
		Il2CppCodeGenWriteBarrier((&___builtInType_6), value);
	}

	inline static int32_t get_offset_of_builtInEnum_7() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___builtInEnum_7)); }
	inline int32_t get_builtInEnum_7() const { return ___builtInEnum_7; }
	inline int32_t* get_address_of_builtInEnum_7() { return &___builtInEnum_7; }
	inline void set_builtInEnum_7(int32_t value)
	{
		___builtInEnum_7 = value;
	}

	inline static int32_t get_offset_of_firstPattern_8() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___firstPattern_8)); }
	inline bool get_firstPattern_8() const { return ___firstPattern_8; }
	inline bool* get_address_of_firstPattern_8() { return &___firstPattern_8; }
	inline void set_firstPattern_8(bool value)
	{
		___firstPattern_8 = value;
	}

	inline static int32_t get_offset_of_regStr_9() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___regStr_9)); }
	inline StringBuilder_t1221177846 * get_regStr_9() const { return ___regStr_9; }
	inline StringBuilder_t1221177846 ** get_address_of_regStr_9() { return &___regStr_9; }
	inline void set_regStr_9(StringBuilder_t1221177846 * value)
	{
		___regStr_9 = value;
		Il2CppCodeGenWriteBarrier((&___regStr_9), value);
	}

	inline static int32_t get_offset_of_pattern_facet_10() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206, ___pattern_facet_10)); }
	inline XmlSchemaPatternFacet_t2024909611 * get_pattern_facet_10() const { return ___pattern_facet_10; }
	inline XmlSchemaPatternFacet_t2024909611 ** get_address_of_pattern_facet_10() { return &___pattern_facet_10; }
	inline void set_pattern_facet_10(XmlSchemaPatternFacet_t2024909611 * value)
	{
		___pattern_facet_10 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_facet_10), value);
	}
};

struct FacetsCompiler_t3565032206_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map[] System.Xml.Schema.FacetsChecker/FacetsCompiler::c_map
	MapU5BU5D_t3908492414* ___c_map_11;

public:
	inline static int32_t get_offset_of_c_map_11() { return static_cast<int32_t>(offsetof(FacetsCompiler_t3565032206_StaticFields, ___c_map_11)); }
	inline MapU5BU5D_t3908492414* get_c_map_11() const { return ___c_map_11; }
	inline MapU5BU5D_t3908492414** get_address_of_c_map_11() { return &___c_map_11; }
	inline void set_c_map_11(MapU5BU5D_t3908492414* value)
	{
		___c_map_11 = value;
		Il2CppCodeGenWriteBarrier((&___c_map_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler
struct FacetsCompiler_t3565032206_marshaled_pinvoke
{
	DatatypeImplementation_t1152094268 * ___datatype_0;
	RestrictionFacets_t4012658256 * ___derivedRestriction_1;
	int32_t ___baseFlags_2;
	int32_t ___baseFixedFlags_3;
	int32_t ___validRestrictionFlags_4;
	XmlSchemaDatatype_t1195946242 * ___nonNegativeInt_5;
	XmlSchemaDatatype_t1195946242 * ___builtInType_6;
	int32_t ___builtInEnum_7;
	int32_t ___firstPattern_8;
	char* ___regStr_9;
	XmlSchemaPatternFacet_t2024909611 * ___pattern_facet_10;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler
struct FacetsCompiler_t3565032206_marshaled_com
{
	DatatypeImplementation_t1152094268 * ___datatype_0;
	RestrictionFacets_t4012658256 * ___derivedRestriction_1;
	int32_t ___baseFlags_2;
	int32_t ___baseFixedFlags_3;
	int32_t ___validRestrictionFlags_4;
	XmlSchemaDatatype_t1195946242 * ___nonNegativeInt_5;
	XmlSchemaDatatype_t1195946242 * ___builtInType_6;
	int32_t ___builtInEnum_7;
	int32_t ___firstPattern_8;
	Il2CppChar* ___regStr_9;
	XmlSchemaPatternFacet_t2024909611 * ___pattern_facet_10;
};
#endif // FACETSCOMPILER_T3565032206_H
#ifndef CONTENTVALIDATOR_T2510151843_H
#define CONTENTVALIDATOR_T2510151843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ContentValidator
struct  ContentValidator_t2510151843  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.ContentValidator::contentType
	int32_t ___contentType_0;
	// System.Boolean System.Xml.Schema.ContentValidator::isOpen
	bool ___isOpen_1;
	// System.Boolean System.Xml.Schema.ContentValidator::isEmptiable
	bool ___isEmptiable_2;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___contentType_0)); }
	inline int32_t get_contentType_0() const { return ___contentType_0; }
	inline int32_t* get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(int32_t value)
	{
		___contentType_0 = value;
	}

	inline static int32_t get_offset_of_isOpen_1() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___isOpen_1)); }
	inline bool get_isOpen_1() const { return ___isOpen_1; }
	inline bool* get_address_of_isOpen_1() { return &___isOpen_1; }
	inline void set_isOpen_1(bool value)
	{
		___isOpen_1 = value;
	}

	inline static int32_t get_offset_of_isEmptiable_2() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843, ___isEmptiable_2)); }
	inline bool get_isEmptiable_2() const { return ___isEmptiable_2; }
	inline bool* get_address_of_isEmptiable_2() { return &___isEmptiable_2; }
	inline void set_isEmptiable_2(bool value)
	{
		___isEmptiable_2 = value;
	}
};

struct ContentValidator_t2510151843_StaticFields
{
public:
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Empty
	ContentValidator_t2510151843 * ___Empty_3;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::TextOnly
	ContentValidator_t2510151843 * ___TextOnly_4;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Mixed
	ContentValidator_t2510151843 * ___Mixed_5;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.ContentValidator::Any
	ContentValidator_t2510151843 * ___Any_6;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Empty_3)); }
	inline ContentValidator_t2510151843 * get_Empty_3() const { return ___Empty_3; }
	inline ContentValidator_t2510151843 ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(ContentValidator_t2510151843 * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}

	inline static int32_t get_offset_of_TextOnly_4() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___TextOnly_4)); }
	inline ContentValidator_t2510151843 * get_TextOnly_4() const { return ___TextOnly_4; }
	inline ContentValidator_t2510151843 ** get_address_of_TextOnly_4() { return &___TextOnly_4; }
	inline void set_TextOnly_4(ContentValidator_t2510151843 * value)
	{
		___TextOnly_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextOnly_4), value);
	}

	inline static int32_t get_offset_of_Mixed_5() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Mixed_5)); }
	inline ContentValidator_t2510151843 * get_Mixed_5() const { return ___Mixed_5; }
	inline ContentValidator_t2510151843 ** get_address_of_Mixed_5() { return &___Mixed_5; }
	inline void set_Mixed_5(ContentValidator_t2510151843 * value)
	{
		___Mixed_5 = value;
		Il2CppCodeGenWriteBarrier((&___Mixed_5), value);
	}

	inline static int32_t get_offset_of_Any_6() { return static_cast<int32_t>(offsetof(ContentValidator_t2510151843_StaticFields, ___Any_6)); }
	inline ContentValidator_t2510151843 * get_Any_6() const { return ___Any_6; }
	inline ContentValidator_t2510151843 ** get_address_of_Any_6() { return &___Any_6; }
	inline void set_Any_6(ContentValidator_t2510151843 * value)
	{
		___Any_6 = value;
		Il2CppCodeGenWriteBarrier((&___Any_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTVALIDATOR_T2510151843_H
#ifndef SCHEMADECLBASE_T797759480_H
#define SCHEMADECLBASE_T797759480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaDeclBase
struct  SchemaDeclBase_t797759480  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaDeclBase::name
	XmlQualifiedName_t1944712516 * ___name_0;
	// System.String System.Xml.Schema.SchemaDeclBase::prefix
	String_t* ___prefix_1;
	// System.Boolean System.Xml.Schema.SchemaDeclBase::isDeclaredInExternal
	bool ___isDeclaredInExternal_2;
	// System.Xml.Schema.SchemaDeclBase/Use System.Xml.Schema.SchemaDeclBase::presence
	int32_t ___presence_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.SchemaDeclBase::schemaType
	XmlSchemaType_t1795078578 * ___schemaType_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.SchemaDeclBase::datatype
	XmlSchemaDatatype_t1195946242 * ___datatype_5;
	// System.String System.Xml.Schema.SchemaDeclBase::defaultValueRaw
	String_t* ___defaultValueRaw_6;
	// System.Object System.Xml.Schema.SchemaDeclBase::defaultValueTyped
	RuntimeObject * ___defaultValueTyped_7;
	// System.Int64 System.Xml.Schema.SchemaDeclBase::maxLength
	int64_t ___maxLength_8;
	// System.Int64 System.Xml.Schema.SchemaDeclBase::minLength
	int64_t ___minLength_9;
	// System.Collections.Generic.List`1<System.String> System.Xml.Schema.SchemaDeclBase::values
	List_1_t1398341365 * ___values_10;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___name_0)); }
	inline XmlQualifiedName_t1944712516 * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_t1944712516 * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_isDeclaredInExternal_2() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___isDeclaredInExternal_2)); }
	inline bool get_isDeclaredInExternal_2() const { return ___isDeclaredInExternal_2; }
	inline bool* get_address_of_isDeclaredInExternal_2() { return &___isDeclaredInExternal_2; }
	inline void set_isDeclaredInExternal_2(bool value)
	{
		___isDeclaredInExternal_2 = value;
	}

	inline static int32_t get_offset_of_presence_3() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___presence_3)); }
	inline int32_t get_presence_3() const { return ___presence_3; }
	inline int32_t* get_address_of_presence_3() { return &___presence_3; }
	inline void set_presence_3(int32_t value)
	{
		___presence_3 = value;
	}

	inline static int32_t get_offset_of_schemaType_4() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___schemaType_4)); }
	inline XmlSchemaType_t1795078578 * get_schemaType_4() const { return ___schemaType_4; }
	inline XmlSchemaType_t1795078578 ** get_address_of_schemaType_4() { return &___schemaType_4; }
	inline void set_schemaType_4(XmlSchemaType_t1795078578 * value)
	{
		___schemaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_4), value);
	}

	inline static int32_t get_offset_of_datatype_5() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___datatype_5)); }
	inline XmlSchemaDatatype_t1195946242 * get_datatype_5() const { return ___datatype_5; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_datatype_5() { return &___datatype_5; }
	inline void set_datatype_5(XmlSchemaDatatype_t1195946242 * value)
	{
		___datatype_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_5), value);
	}

	inline static int32_t get_offset_of_defaultValueRaw_6() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___defaultValueRaw_6)); }
	inline String_t* get_defaultValueRaw_6() const { return ___defaultValueRaw_6; }
	inline String_t** get_address_of_defaultValueRaw_6() { return &___defaultValueRaw_6; }
	inline void set_defaultValueRaw_6(String_t* value)
	{
		___defaultValueRaw_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueRaw_6), value);
	}

	inline static int32_t get_offset_of_defaultValueTyped_7() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___defaultValueTyped_7)); }
	inline RuntimeObject * get_defaultValueTyped_7() const { return ___defaultValueTyped_7; }
	inline RuntimeObject ** get_address_of_defaultValueTyped_7() { return &___defaultValueTyped_7; }
	inline void set_defaultValueTyped_7(RuntimeObject * value)
	{
		___defaultValueTyped_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueTyped_7), value);
	}

	inline static int32_t get_offset_of_maxLength_8() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___maxLength_8)); }
	inline int64_t get_maxLength_8() const { return ___maxLength_8; }
	inline int64_t* get_address_of_maxLength_8() { return &___maxLength_8; }
	inline void set_maxLength_8(int64_t value)
	{
		___maxLength_8 = value;
	}

	inline static int32_t get_offset_of_minLength_9() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___minLength_9)); }
	inline int64_t get_minLength_9() const { return ___minLength_9; }
	inline int64_t* get_address_of_minLength_9() { return &___minLength_9; }
	inline void set_minLength_9(int64_t value)
	{
		___minLength_9 = value;
	}

	inline static int32_t get_offset_of_values_10() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t797759480, ___values_10)); }
	inline List_1_t1398341365 * get_values_10() const { return ___values_10; }
	inline List_1_t1398341365 ** get_address_of_values_10() { return &___values_10; }
	inline void set_values_10(List_1_t1398341365 * value)
	{
		___values_10 = value;
		Il2CppCodeGenWriteBarrier((&___values_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMADECLBASE_T797759480_H
#ifndef RESTRICTIONFACETS_T4012658256_H
#define RESTRICTIONFACETS_T4012658256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RestrictionFacets
struct  RestrictionFacets_t4012658256  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.RestrictionFacets::Length
	int32_t ___Length_0;
	// System.Int32 System.Xml.Schema.RestrictionFacets::MinLength
	int32_t ___MinLength_1;
	// System.Int32 System.Xml.Schema.RestrictionFacets::MaxLength
	int32_t ___MaxLength_2;
	// System.Collections.ArrayList System.Xml.Schema.RestrictionFacets::Patterns
	ArrayList_t4252133567 * ___Patterns_3;
	// System.Collections.ArrayList System.Xml.Schema.RestrictionFacets::Enumeration
	ArrayList_t4252133567 * ___Enumeration_4;
	// System.Xml.Schema.XmlSchemaWhiteSpace System.Xml.Schema.RestrictionFacets::WhiteSpace
	int32_t ___WhiteSpace_5;
	// System.Object System.Xml.Schema.RestrictionFacets::MaxInclusive
	RuntimeObject * ___MaxInclusive_6;
	// System.Object System.Xml.Schema.RestrictionFacets::MaxExclusive
	RuntimeObject * ___MaxExclusive_7;
	// System.Object System.Xml.Schema.RestrictionFacets::MinInclusive
	RuntimeObject * ___MinInclusive_8;
	// System.Object System.Xml.Schema.RestrictionFacets::MinExclusive
	RuntimeObject * ___MinExclusive_9;
	// System.Int32 System.Xml.Schema.RestrictionFacets::TotalDigits
	int32_t ___TotalDigits_10;
	// System.Int32 System.Xml.Schema.RestrictionFacets::FractionDigits
	int32_t ___FractionDigits_11;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.RestrictionFacets::Flags
	int32_t ___Flags_12;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.RestrictionFacets::FixedFlags
	int32_t ___FixedFlags_13;

public:
	inline static int32_t get_offset_of_Length_0() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Length_0)); }
	inline int32_t get_Length_0() const { return ___Length_0; }
	inline int32_t* get_address_of_Length_0() { return &___Length_0; }
	inline void set_Length_0(int32_t value)
	{
		___Length_0 = value;
	}

	inline static int32_t get_offset_of_MinLength_1() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MinLength_1)); }
	inline int32_t get_MinLength_1() const { return ___MinLength_1; }
	inline int32_t* get_address_of_MinLength_1() { return &___MinLength_1; }
	inline void set_MinLength_1(int32_t value)
	{
		___MinLength_1 = value;
	}

	inline static int32_t get_offset_of_MaxLength_2() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MaxLength_2)); }
	inline int32_t get_MaxLength_2() const { return ___MaxLength_2; }
	inline int32_t* get_address_of_MaxLength_2() { return &___MaxLength_2; }
	inline void set_MaxLength_2(int32_t value)
	{
		___MaxLength_2 = value;
	}

	inline static int32_t get_offset_of_Patterns_3() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Patterns_3)); }
	inline ArrayList_t4252133567 * get_Patterns_3() const { return ___Patterns_3; }
	inline ArrayList_t4252133567 ** get_address_of_Patterns_3() { return &___Patterns_3; }
	inline void set_Patterns_3(ArrayList_t4252133567 * value)
	{
		___Patterns_3 = value;
		Il2CppCodeGenWriteBarrier((&___Patterns_3), value);
	}

	inline static int32_t get_offset_of_Enumeration_4() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Enumeration_4)); }
	inline ArrayList_t4252133567 * get_Enumeration_4() const { return ___Enumeration_4; }
	inline ArrayList_t4252133567 ** get_address_of_Enumeration_4() { return &___Enumeration_4; }
	inline void set_Enumeration_4(ArrayList_t4252133567 * value)
	{
		___Enumeration_4 = value;
		Il2CppCodeGenWriteBarrier((&___Enumeration_4), value);
	}

	inline static int32_t get_offset_of_WhiteSpace_5() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___WhiteSpace_5)); }
	inline int32_t get_WhiteSpace_5() const { return ___WhiteSpace_5; }
	inline int32_t* get_address_of_WhiteSpace_5() { return &___WhiteSpace_5; }
	inline void set_WhiteSpace_5(int32_t value)
	{
		___WhiteSpace_5 = value;
	}

	inline static int32_t get_offset_of_MaxInclusive_6() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MaxInclusive_6)); }
	inline RuntimeObject * get_MaxInclusive_6() const { return ___MaxInclusive_6; }
	inline RuntimeObject ** get_address_of_MaxInclusive_6() { return &___MaxInclusive_6; }
	inline void set_MaxInclusive_6(RuntimeObject * value)
	{
		___MaxInclusive_6 = value;
		Il2CppCodeGenWriteBarrier((&___MaxInclusive_6), value);
	}

	inline static int32_t get_offset_of_MaxExclusive_7() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MaxExclusive_7)); }
	inline RuntimeObject * get_MaxExclusive_7() const { return ___MaxExclusive_7; }
	inline RuntimeObject ** get_address_of_MaxExclusive_7() { return &___MaxExclusive_7; }
	inline void set_MaxExclusive_7(RuntimeObject * value)
	{
		___MaxExclusive_7 = value;
		Il2CppCodeGenWriteBarrier((&___MaxExclusive_7), value);
	}

	inline static int32_t get_offset_of_MinInclusive_8() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MinInclusive_8)); }
	inline RuntimeObject * get_MinInclusive_8() const { return ___MinInclusive_8; }
	inline RuntimeObject ** get_address_of_MinInclusive_8() { return &___MinInclusive_8; }
	inline void set_MinInclusive_8(RuntimeObject * value)
	{
		___MinInclusive_8 = value;
		Il2CppCodeGenWriteBarrier((&___MinInclusive_8), value);
	}

	inline static int32_t get_offset_of_MinExclusive_9() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___MinExclusive_9)); }
	inline RuntimeObject * get_MinExclusive_9() const { return ___MinExclusive_9; }
	inline RuntimeObject ** get_address_of_MinExclusive_9() { return &___MinExclusive_9; }
	inline void set_MinExclusive_9(RuntimeObject * value)
	{
		___MinExclusive_9 = value;
		Il2CppCodeGenWriteBarrier((&___MinExclusive_9), value);
	}

	inline static int32_t get_offset_of_TotalDigits_10() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___TotalDigits_10)); }
	inline int32_t get_TotalDigits_10() const { return ___TotalDigits_10; }
	inline int32_t* get_address_of_TotalDigits_10() { return &___TotalDigits_10; }
	inline void set_TotalDigits_10(int32_t value)
	{
		___TotalDigits_10 = value;
	}

	inline static int32_t get_offset_of_FractionDigits_11() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___FractionDigits_11)); }
	inline int32_t get_FractionDigits_11() const { return ___FractionDigits_11; }
	inline int32_t* get_address_of_FractionDigits_11() { return &___FractionDigits_11; }
	inline void set_FractionDigits_11(int32_t value)
	{
		___FractionDigits_11 = value;
	}

	inline static int32_t get_offset_of_Flags_12() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___Flags_12)); }
	inline int32_t get_Flags_12() const { return ___Flags_12; }
	inline int32_t* get_address_of_Flags_12() { return &___Flags_12; }
	inline void set_Flags_12(int32_t value)
	{
		___Flags_12 = value;
	}

	inline static int32_t get_offset_of_FixedFlags_13() { return static_cast<int32_t>(offsetof(RestrictionFacets_t4012658256, ___FixedFlags_13)); }
	inline int32_t get_FixedFlags_13() const { return ___FixedFlags_13; }
	inline int32_t* get_address_of_FixedFlags_13() { return &___FixedFlags_13; }
	inline void set_FixedFlags_13(int32_t value)
	{
		___FixedFlags_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTIONFACETS_T4012658256_H
#ifndef DATATYPEIMPLEMENTATION_T1152094268_H
#define DATATYPEIMPLEMENTATION_T1152094268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation
struct  DatatypeImplementation_t1152094268  : public XmlSchemaDatatype_t1195946242
{
public:
	// System.Xml.Schema.XmlSchemaDatatypeVariety System.Xml.Schema.DatatypeImplementation::variety
	int32_t ___variety_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.DatatypeImplementation::restriction
	RestrictionFacets_t4012658256 * ___restriction_1;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::baseType
	DatatypeImplementation_t1152094268 * ___baseType_2;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.DatatypeImplementation::valueConverter
	XmlValueConverter_t68179724 * ___valueConverter_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.DatatypeImplementation::parentSchemaType
	XmlSchemaType_t1795078578 * ___parentSchemaType_4;

public:
	inline static int32_t get_offset_of_variety_0() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___variety_0)); }
	inline int32_t get_variety_0() const { return ___variety_0; }
	inline int32_t* get_address_of_variety_0() { return &___variety_0; }
	inline void set_variety_0(int32_t value)
	{
		___variety_0 = value;
	}

	inline static int32_t get_offset_of_restriction_1() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___restriction_1)); }
	inline RestrictionFacets_t4012658256 * get_restriction_1() const { return ___restriction_1; }
	inline RestrictionFacets_t4012658256 ** get_address_of_restriction_1() { return &___restriction_1; }
	inline void set_restriction_1(RestrictionFacets_t4012658256 * value)
	{
		___restriction_1 = value;
		Il2CppCodeGenWriteBarrier((&___restriction_1), value);
	}

	inline static int32_t get_offset_of_baseType_2() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___baseType_2)); }
	inline DatatypeImplementation_t1152094268 * get_baseType_2() const { return ___baseType_2; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_baseType_2() { return &___baseType_2; }
	inline void set_baseType_2(DatatypeImplementation_t1152094268 * value)
	{
		___baseType_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_2), value);
	}

	inline static int32_t get_offset_of_valueConverter_3() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___valueConverter_3)); }
	inline XmlValueConverter_t68179724 * get_valueConverter_3() const { return ___valueConverter_3; }
	inline XmlValueConverter_t68179724 ** get_address_of_valueConverter_3() { return &___valueConverter_3; }
	inline void set_valueConverter_3(XmlValueConverter_t68179724 * value)
	{
		___valueConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___valueConverter_3), value);
	}

	inline static int32_t get_offset_of_parentSchemaType_4() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268, ___parentSchemaType_4)); }
	inline XmlSchemaType_t1795078578 * get_parentSchemaType_4() const { return ___parentSchemaType_4; }
	inline XmlSchemaType_t1795078578 ** get_address_of_parentSchemaType_4() { return &___parentSchemaType_4; }
	inline void set_parentSchemaType_4(XmlSchemaType_t1795078578 * value)
	{
		___parentSchemaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentSchemaType_4), value);
	}
};

struct DatatypeImplementation_t1152094268_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Schema.DatatypeImplementation::builtinTypes
	Hashtable_t909839986 * ___builtinTypes_5;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.DatatypeImplementation::enumToTypeCode
	XmlSchemaSimpleTypeU5BU5D_t192177157* ___enumToTypeCode_6;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anySimpleType
	XmlSchemaSimpleType_t248156492 * ___anySimpleType_7;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anyAtomicType
	XmlSchemaSimpleType_t248156492 * ___anyAtomicType_8;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::untypedAtomicType
	XmlSchemaSimpleType_t248156492 * ___untypedAtomicType_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::yearMonthDurationType
	XmlSchemaSimpleType_t248156492 * ___yearMonthDurationType_10;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::dayTimeDurationType
	XmlSchemaSimpleType_t248156492 * ___dayTimeDurationType_11;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::normalizedStringTypeV1Compat
	XmlSchemaSimpleType_t248156492 * ___normalizedStringTypeV1Compat_12;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::tokenTypeV1Compat
	XmlSchemaSimpleType_t248156492 * ___tokenTypeV1Compat_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnySimpleType
	XmlQualifiedName_t1944712516 * ___QnAnySimpleType_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnyType
	XmlQualifiedName_t1944712516 * ___QnAnyType_15;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::stringFacetsChecker
	FacetsChecker_t1235574227 * ___stringFacetsChecker_16;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::miscFacetsChecker
	FacetsChecker_t1235574227 * ___miscFacetsChecker_17;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::numeric2FacetsChecker
	FacetsChecker_t1235574227 * ___numeric2FacetsChecker_18;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::binaryFacetsChecker
	FacetsChecker_t1235574227 * ___binaryFacetsChecker_19;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::dateTimeFacetsChecker
	FacetsChecker_t1235574227 * ___dateTimeFacetsChecker_20;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::durationFacetsChecker
	FacetsChecker_t1235574227 * ___durationFacetsChecker_21;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::listFacetsChecker
	FacetsChecker_t1235574227 * ___listFacetsChecker_22;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::qnameFacetsChecker
	FacetsChecker_t1235574227 * ___qnameFacetsChecker_23;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::unionFacetsChecker
	FacetsChecker_t1235574227 * ___unionFacetsChecker_24;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anySimpleType
	DatatypeImplementation_t1152094268 * ___c_anySimpleType_25;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyURI
	DatatypeImplementation_t1152094268 * ___c_anyURI_26;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_base64Binary
	DatatypeImplementation_t1152094268 * ___c_base64Binary_27;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_boolean
	DatatypeImplementation_t1152094268 * ___c_boolean_28;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_byte
	DatatypeImplementation_t1152094268 * ___c_byte_29;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_char
	DatatypeImplementation_t1152094268 * ___c_char_30;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_date
	DatatypeImplementation_t1152094268 * ___c_date_31;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTime
	DatatypeImplementation_t1152094268 * ___c_dateTime_32;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeNoTz
	DatatypeImplementation_t1152094268 * ___c_dateTimeNoTz_33;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeTz
	DatatypeImplementation_t1152094268 * ___c_dateTimeTz_34;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_day
	DatatypeImplementation_t1152094268 * ___c_day_35;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_decimal
	DatatypeImplementation_t1152094268 * ___c_decimal_36;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_double
	DatatypeImplementation_t1152094268 * ___c_double_37;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_doubleXdr
	DatatypeImplementation_t1152094268 * ___c_doubleXdr_38;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_duration
	DatatypeImplementation_t1152094268 * ___c_duration_39;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITY
	DatatypeImplementation_t1152094268 * ___c_ENTITY_40;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITIES
	DatatypeImplementation_t1152094268 * ___c_ENTITIES_41;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENUMERATION
	DatatypeImplementation_t1152094268 * ___c_ENUMERATION_42;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_fixed
	DatatypeImplementation_t1152094268 * ___c_fixed_43;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_float
	DatatypeImplementation_t1152094268 * ___c_float_44;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_floatXdr
	DatatypeImplementation_t1152094268 * ___c_floatXdr_45;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_hexBinary
	DatatypeImplementation_t1152094268 * ___c_hexBinary_46;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ID
	DatatypeImplementation_t1152094268 * ___c_ID_47;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREF
	DatatypeImplementation_t1152094268 * ___c_IDREF_48;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREFS
	DatatypeImplementation_t1152094268 * ___c_IDREFS_49;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_int
	DatatypeImplementation_t1152094268 * ___c_int_50;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_integer
	DatatypeImplementation_t1152094268 * ___c_integer_51;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_language
	DatatypeImplementation_t1152094268 * ___c_language_52;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_long
	DatatypeImplementation_t1152094268 * ___c_long_53;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_month
	DatatypeImplementation_t1152094268 * ___c_month_54;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_monthDay
	DatatypeImplementation_t1152094268 * ___c_monthDay_55;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_Name
	DatatypeImplementation_t1152094268 * ___c_Name_56;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NCName
	DatatypeImplementation_t1152094268 * ___c_NCName_57;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_negativeInteger
	DatatypeImplementation_t1152094268 * ___c_negativeInteger_58;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKEN
	DatatypeImplementation_t1152094268 * ___c_NMTOKEN_59;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKENS
	DatatypeImplementation_t1152094268 * ___c_NMTOKENS_60;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonNegativeInteger
	DatatypeImplementation_t1152094268 * ___c_nonNegativeInteger_61;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonPositiveInteger
	DatatypeImplementation_t1152094268 * ___c_nonPositiveInteger_62;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedString
	DatatypeImplementation_t1152094268 * ___c_normalizedString_63;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NOTATION
	DatatypeImplementation_t1152094268 * ___c_NOTATION_64;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_positiveInteger
	DatatypeImplementation_t1152094268 * ___c_positiveInteger_65;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QName
	DatatypeImplementation_t1152094268 * ___c_QName_66;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QNameXdr
	DatatypeImplementation_t1152094268 * ___c_QNameXdr_67;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_short
	DatatypeImplementation_t1152094268 * ___c_short_68;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_string
	DatatypeImplementation_t1152094268 * ___c_string_69;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_time
	DatatypeImplementation_t1152094268 * ___c_time_70;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeNoTz
	DatatypeImplementation_t1152094268 * ___c_timeNoTz_71;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeTz
	DatatypeImplementation_t1152094268 * ___c_timeTz_72;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_token
	DatatypeImplementation_t1152094268 * ___c_token_73;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedByte
	DatatypeImplementation_t1152094268 * ___c_unsignedByte_74;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedInt
	DatatypeImplementation_t1152094268 * ___c_unsignedInt_75;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedLong
	DatatypeImplementation_t1152094268 * ___c_unsignedLong_76;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedShort
	DatatypeImplementation_t1152094268 * ___c_unsignedShort_77;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_uuid
	DatatypeImplementation_t1152094268 * ___c_uuid_78;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_year
	DatatypeImplementation_t1152094268 * ___c_year_79;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonth
	DatatypeImplementation_t1152094268 * ___c_yearMonth_80;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedStringV1Compat
	DatatypeImplementation_t1152094268 * ___c_normalizedStringV1Compat_81;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_tokenV1Compat
	DatatypeImplementation_t1152094268 * ___c_tokenV1Compat_82;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyAtomicType
	DatatypeImplementation_t1152094268 * ___c_anyAtomicType_83;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dayTimeDuration
	DatatypeImplementation_t1152094268 * ___c_dayTimeDuration_84;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_untypedAtomicType
	DatatypeImplementation_t1152094268 * ___c_untypedAtomicType_85;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonthDuration
	DatatypeImplementation_t1152094268 * ___c_yearMonthDuration_86;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypes
	DatatypeImplementationU5BU5D_t3131202389* ___c_tokenizedTypes_87;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypesXsd
	DatatypeImplementationU5BU5D_t3131202389* ___c_tokenizedTypesXsd_88;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XdrTypes
	SchemaDatatypeMapU5BU5D_t863562528* ___c_XdrTypes_89;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XsdTypes
	SchemaDatatypeMapU5BU5D_t863562528* ___c_XsdTypes_90;

public:
	inline static int32_t get_offset_of_builtinTypes_5() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___builtinTypes_5)); }
	inline Hashtable_t909839986 * get_builtinTypes_5() const { return ___builtinTypes_5; }
	inline Hashtable_t909839986 ** get_address_of_builtinTypes_5() { return &___builtinTypes_5; }
	inline void set_builtinTypes_5(Hashtable_t909839986 * value)
	{
		___builtinTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___builtinTypes_5), value);
	}

	inline static int32_t get_offset_of_enumToTypeCode_6() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___enumToTypeCode_6)); }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157* get_enumToTypeCode_6() const { return ___enumToTypeCode_6; }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157** get_address_of_enumToTypeCode_6() { return &___enumToTypeCode_6; }
	inline void set_enumToTypeCode_6(XmlSchemaSimpleTypeU5BU5D_t192177157* value)
	{
		___enumToTypeCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___enumToTypeCode_6), value);
	}

	inline static int32_t get_offset_of_anySimpleType_7() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___anySimpleType_7)); }
	inline XmlSchemaSimpleType_t248156492 * get_anySimpleType_7() const { return ___anySimpleType_7; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_anySimpleType_7() { return &___anySimpleType_7; }
	inline void set_anySimpleType_7(XmlSchemaSimpleType_t248156492 * value)
	{
		___anySimpleType_7 = value;
		Il2CppCodeGenWriteBarrier((&___anySimpleType_7), value);
	}

	inline static int32_t get_offset_of_anyAtomicType_8() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___anyAtomicType_8)); }
	inline XmlSchemaSimpleType_t248156492 * get_anyAtomicType_8() const { return ___anyAtomicType_8; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_anyAtomicType_8() { return &___anyAtomicType_8; }
	inline void set_anyAtomicType_8(XmlSchemaSimpleType_t248156492 * value)
	{
		___anyAtomicType_8 = value;
		Il2CppCodeGenWriteBarrier((&___anyAtomicType_8), value);
	}

	inline static int32_t get_offset_of_untypedAtomicType_9() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___untypedAtomicType_9)); }
	inline XmlSchemaSimpleType_t248156492 * get_untypedAtomicType_9() const { return ___untypedAtomicType_9; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_untypedAtomicType_9() { return &___untypedAtomicType_9; }
	inline void set_untypedAtomicType_9(XmlSchemaSimpleType_t248156492 * value)
	{
		___untypedAtomicType_9 = value;
		Il2CppCodeGenWriteBarrier((&___untypedAtomicType_9), value);
	}

	inline static int32_t get_offset_of_yearMonthDurationType_10() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___yearMonthDurationType_10)); }
	inline XmlSchemaSimpleType_t248156492 * get_yearMonthDurationType_10() const { return ___yearMonthDurationType_10; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_yearMonthDurationType_10() { return &___yearMonthDurationType_10; }
	inline void set_yearMonthDurationType_10(XmlSchemaSimpleType_t248156492 * value)
	{
		___yearMonthDurationType_10 = value;
		Il2CppCodeGenWriteBarrier((&___yearMonthDurationType_10), value);
	}

	inline static int32_t get_offset_of_dayTimeDurationType_11() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___dayTimeDurationType_11)); }
	inline XmlSchemaSimpleType_t248156492 * get_dayTimeDurationType_11() const { return ___dayTimeDurationType_11; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_dayTimeDurationType_11() { return &___dayTimeDurationType_11; }
	inline void set_dayTimeDurationType_11(XmlSchemaSimpleType_t248156492 * value)
	{
		___dayTimeDurationType_11 = value;
		Il2CppCodeGenWriteBarrier((&___dayTimeDurationType_11), value);
	}

	inline static int32_t get_offset_of_normalizedStringTypeV1Compat_12() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___normalizedStringTypeV1Compat_12)); }
	inline XmlSchemaSimpleType_t248156492 * get_normalizedStringTypeV1Compat_12() const { return ___normalizedStringTypeV1Compat_12; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_normalizedStringTypeV1Compat_12() { return &___normalizedStringTypeV1Compat_12; }
	inline void set_normalizedStringTypeV1Compat_12(XmlSchemaSimpleType_t248156492 * value)
	{
		___normalizedStringTypeV1Compat_12 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedStringTypeV1Compat_12), value);
	}

	inline static int32_t get_offset_of_tokenTypeV1Compat_13() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___tokenTypeV1Compat_13)); }
	inline XmlSchemaSimpleType_t248156492 * get_tokenTypeV1Compat_13() const { return ___tokenTypeV1Compat_13; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_tokenTypeV1Compat_13() { return &___tokenTypeV1Compat_13; }
	inline void set_tokenTypeV1Compat_13(XmlSchemaSimpleType_t248156492 * value)
	{
		___tokenTypeV1Compat_13 = value;
		Il2CppCodeGenWriteBarrier((&___tokenTypeV1Compat_13), value);
	}

	inline static int32_t get_offset_of_QnAnySimpleType_14() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___QnAnySimpleType_14)); }
	inline XmlQualifiedName_t1944712516 * get_QnAnySimpleType_14() const { return ___QnAnySimpleType_14; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnAnySimpleType_14() { return &___QnAnySimpleType_14; }
	inline void set_QnAnySimpleType_14(XmlQualifiedName_t1944712516 * value)
	{
		___QnAnySimpleType_14 = value;
		Il2CppCodeGenWriteBarrier((&___QnAnySimpleType_14), value);
	}

	inline static int32_t get_offset_of_QnAnyType_15() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___QnAnyType_15)); }
	inline XmlQualifiedName_t1944712516 * get_QnAnyType_15() const { return ___QnAnyType_15; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QnAnyType_15() { return &___QnAnyType_15; }
	inline void set_QnAnyType_15(XmlQualifiedName_t1944712516 * value)
	{
		___QnAnyType_15 = value;
		Il2CppCodeGenWriteBarrier((&___QnAnyType_15), value);
	}

	inline static int32_t get_offset_of_stringFacetsChecker_16() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___stringFacetsChecker_16)); }
	inline FacetsChecker_t1235574227 * get_stringFacetsChecker_16() const { return ___stringFacetsChecker_16; }
	inline FacetsChecker_t1235574227 ** get_address_of_stringFacetsChecker_16() { return &___stringFacetsChecker_16; }
	inline void set_stringFacetsChecker_16(FacetsChecker_t1235574227 * value)
	{
		___stringFacetsChecker_16 = value;
		Il2CppCodeGenWriteBarrier((&___stringFacetsChecker_16), value);
	}

	inline static int32_t get_offset_of_miscFacetsChecker_17() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___miscFacetsChecker_17)); }
	inline FacetsChecker_t1235574227 * get_miscFacetsChecker_17() const { return ___miscFacetsChecker_17; }
	inline FacetsChecker_t1235574227 ** get_address_of_miscFacetsChecker_17() { return &___miscFacetsChecker_17; }
	inline void set_miscFacetsChecker_17(FacetsChecker_t1235574227 * value)
	{
		___miscFacetsChecker_17 = value;
		Il2CppCodeGenWriteBarrier((&___miscFacetsChecker_17), value);
	}

	inline static int32_t get_offset_of_numeric2FacetsChecker_18() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___numeric2FacetsChecker_18)); }
	inline FacetsChecker_t1235574227 * get_numeric2FacetsChecker_18() const { return ___numeric2FacetsChecker_18; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric2FacetsChecker_18() { return &___numeric2FacetsChecker_18; }
	inline void set_numeric2FacetsChecker_18(FacetsChecker_t1235574227 * value)
	{
		___numeric2FacetsChecker_18 = value;
		Il2CppCodeGenWriteBarrier((&___numeric2FacetsChecker_18), value);
	}

	inline static int32_t get_offset_of_binaryFacetsChecker_19() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___binaryFacetsChecker_19)); }
	inline FacetsChecker_t1235574227 * get_binaryFacetsChecker_19() const { return ___binaryFacetsChecker_19; }
	inline FacetsChecker_t1235574227 ** get_address_of_binaryFacetsChecker_19() { return &___binaryFacetsChecker_19; }
	inline void set_binaryFacetsChecker_19(FacetsChecker_t1235574227 * value)
	{
		___binaryFacetsChecker_19 = value;
		Il2CppCodeGenWriteBarrier((&___binaryFacetsChecker_19), value);
	}

	inline static int32_t get_offset_of_dateTimeFacetsChecker_20() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___dateTimeFacetsChecker_20)); }
	inline FacetsChecker_t1235574227 * get_dateTimeFacetsChecker_20() const { return ___dateTimeFacetsChecker_20; }
	inline FacetsChecker_t1235574227 ** get_address_of_dateTimeFacetsChecker_20() { return &___dateTimeFacetsChecker_20; }
	inline void set_dateTimeFacetsChecker_20(FacetsChecker_t1235574227 * value)
	{
		___dateTimeFacetsChecker_20 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeFacetsChecker_20), value);
	}

	inline static int32_t get_offset_of_durationFacetsChecker_21() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___durationFacetsChecker_21)); }
	inline FacetsChecker_t1235574227 * get_durationFacetsChecker_21() const { return ___durationFacetsChecker_21; }
	inline FacetsChecker_t1235574227 ** get_address_of_durationFacetsChecker_21() { return &___durationFacetsChecker_21; }
	inline void set_durationFacetsChecker_21(FacetsChecker_t1235574227 * value)
	{
		___durationFacetsChecker_21 = value;
		Il2CppCodeGenWriteBarrier((&___durationFacetsChecker_21), value);
	}

	inline static int32_t get_offset_of_listFacetsChecker_22() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___listFacetsChecker_22)); }
	inline FacetsChecker_t1235574227 * get_listFacetsChecker_22() const { return ___listFacetsChecker_22; }
	inline FacetsChecker_t1235574227 ** get_address_of_listFacetsChecker_22() { return &___listFacetsChecker_22; }
	inline void set_listFacetsChecker_22(FacetsChecker_t1235574227 * value)
	{
		___listFacetsChecker_22 = value;
		Il2CppCodeGenWriteBarrier((&___listFacetsChecker_22), value);
	}

	inline static int32_t get_offset_of_qnameFacetsChecker_23() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___qnameFacetsChecker_23)); }
	inline FacetsChecker_t1235574227 * get_qnameFacetsChecker_23() const { return ___qnameFacetsChecker_23; }
	inline FacetsChecker_t1235574227 ** get_address_of_qnameFacetsChecker_23() { return &___qnameFacetsChecker_23; }
	inline void set_qnameFacetsChecker_23(FacetsChecker_t1235574227 * value)
	{
		___qnameFacetsChecker_23 = value;
		Il2CppCodeGenWriteBarrier((&___qnameFacetsChecker_23), value);
	}

	inline static int32_t get_offset_of_unionFacetsChecker_24() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___unionFacetsChecker_24)); }
	inline FacetsChecker_t1235574227 * get_unionFacetsChecker_24() const { return ___unionFacetsChecker_24; }
	inline FacetsChecker_t1235574227 ** get_address_of_unionFacetsChecker_24() { return &___unionFacetsChecker_24; }
	inline void set_unionFacetsChecker_24(FacetsChecker_t1235574227 * value)
	{
		___unionFacetsChecker_24 = value;
		Il2CppCodeGenWriteBarrier((&___unionFacetsChecker_24), value);
	}

	inline static int32_t get_offset_of_c_anySimpleType_25() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_anySimpleType_25)); }
	inline DatatypeImplementation_t1152094268 * get_c_anySimpleType_25() const { return ___c_anySimpleType_25; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_anySimpleType_25() { return &___c_anySimpleType_25; }
	inline void set_c_anySimpleType_25(DatatypeImplementation_t1152094268 * value)
	{
		___c_anySimpleType_25 = value;
		Il2CppCodeGenWriteBarrier((&___c_anySimpleType_25), value);
	}

	inline static int32_t get_offset_of_c_anyURI_26() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_anyURI_26)); }
	inline DatatypeImplementation_t1152094268 * get_c_anyURI_26() const { return ___c_anyURI_26; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_anyURI_26() { return &___c_anyURI_26; }
	inline void set_c_anyURI_26(DatatypeImplementation_t1152094268 * value)
	{
		___c_anyURI_26 = value;
		Il2CppCodeGenWriteBarrier((&___c_anyURI_26), value);
	}

	inline static int32_t get_offset_of_c_base64Binary_27() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_base64Binary_27)); }
	inline DatatypeImplementation_t1152094268 * get_c_base64Binary_27() const { return ___c_base64Binary_27; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_base64Binary_27() { return &___c_base64Binary_27; }
	inline void set_c_base64Binary_27(DatatypeImplementation_t1152094268 * value)
	{
		___c_base64Binary_27 = value;
		Il2CppCodeGenWriteBarrier((&___c_base64Binary_27), value);
	}

	inline static int32_t get_offset_of_c_boolean_28() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_boolean_28)); }
	inline DatatypeImplementation_t1152094268 * get_c_boolean_28() const { return ___c_boolean_28; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_boolean_28() { return &___c_boolean_28; }
	inline void set_c_boolean_28(DatatypeImplementation_t1152094268 * value)
	{
		___c_boolean_28 = value;
		Il2CppCodeGenWriteBarrier((&___c_boolean_28), value);
	}

	inline static int32_t get_offset_of_c_byte_29() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_byte_29)); }
	inline DatatypeImplementation_t1152094268 * get_c_byte_29() const { return ___c_byte_29; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_byte_29() { return &___c_byte_29; }
	inline void set_c_byte_29(DatatypeImplementation_t1152094268 * value)
	{
		___c_byte_29 = value;
		Il2CppCodeGenWriteBarrier((&___c_byte_29), value);
	}

	inline static int32_t get_offset_of_c_char_30() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_char_30)); }
	inline DatatypeImplementation_t1152094268 * get_c_char_30() const { return ___c_char_30; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_char_30() { return &___c_char_30; }
	inline void set_c_char_30(DatatypeImplementation_t1152094268 * value)
	{
		___c_char_30 = value;
		Il2CppCodeGenWriteBarrier((&___c_char_30), value);
	}

	inline static int32_t get_offset_of_c_date_31() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_date_31)); }
	inline DatatypeImplementation_t1152094268 * get_c_date_31() const { return ___c_date_31; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_date_31() { return &___c_date_31; }
	inline void set_c_date_31(DatatypeImplementation_t1152094268 * value)
	{
		___c_date_31 = value;
		Il2CppCodeGenWriteBarrier((&___c_date_31), value);
	}

	inline static int32_t get_offset_of_c_dateTime_32() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dateTime_32)); }
	inline DatatypeImplementation_t1152094268 * get_c_dateTime_32() const { return ___c_dateTime_32; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dateTime_32() { return &___c_dateTime_32; }
	inline void set_c_dateTime_32(DatatypeImplementation_t1152094268 * value)
	{
		___c_dateTime_32 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTime_32), value);
	}

	inline static int32_t get_offset_of_c_dateTimeNoTz_33() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dateTimeNoTz_33)); }
	inline DatatypeImplementation_t1152094268 * get_c_dateTimeNoTz_33() const { return ___c_dateTimeNoTz_33; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dateTimeNoTz_33() { return &___c_dateTimeNoTz_33; }
	inline void set_c_dateTimeNoTz_33(DatatypeImplementation_t1152094268 * value)
	{
		___c_dateTimeNoTz_33 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTimeNoTz_33), value);
	}

	inline static int32_t get_offset_of_c_dateTimeTz_34() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dateTimeTz_34)); }
	inline DatatypeImplementation_t1152094268 * get_c_dateTimeTz_34() const { return ___c_dateTimeTz_34; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dateTimeTz_34() { return &___c_dateTimeTz_34; }
	inline void set_c_dateTimeTz_34(DatatypeImplementation_t1152094268 * value)
	{
		___c_dateTimeTz_34 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTimeTz_34), value);
	}

	inline static int32_t get_offset_of_c_day_35() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_day_35)); }
	inline DatatypeImplementation_t1152094268 * get_c_day_35() const { return ___c_day_35; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_day_35() { return &___c_day_35; }
	inline void set_c_day_35(DatatypeImplementation_t1152094268 * value)
	{
		___c_day_35 = value;
		Il2CppCodeGenWriteBarrier((&___c_day_35), value);
	}

	inline static int32_t get_offset_of_c_decimal_36() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_decimal_36)); }
	inline DatatypeImplementation_t1152094268 * get_c_decimal_36() const { return ___c_decimal_36; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_decimal_36() { return &___c_decimal_36; }
	inline void set_c_decimal_36(DatatypeImplementation_t1152094268 * value)
	{
		___c_decimal_36 = value;
		Il2CppCodeGenWriteBarrier((&___c_decimal_36), value);
	}

	inline static int32_t get_offset_of_c_double_37() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_double_37)); }
	inline DatatypeImplementation_t1152094268 * get_c_double_37() const { return ___c_double_37; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_double_37() { return &___c_double_37; }
	inline void set_c_double_37(DatatypeImplementation_t1152094268 * value)
	{
		___c_double_37 = value;
		Il2CppCodeGenWriteBarrier((&___c_double_37), value);
	}

	inline static int32_t get_offset_of_c_doubleXdr_38() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_doubleXdr_38)); }
	inline DatatypeImplementation_t1152094268 * get_c_doubleXdr_38() const { return ___c_doubleXdr_38; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_doubleXdr_38() { return &___c_doubleXdr_38; }
	inline void set_c_doubleXdr_38(DatatypeImplementation_t1152094268 * value)
	{
		___c_doubleXdr_38 = value;
		Il2CppCodeGenWriteBarrier((&___c_doubleXdr_38), value);
	}

	inline static int32_t get_offset_of_c_duration_39() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_duration_39)); }
	inline DatatypeImplementation_t1152094268 * get_c_duration_39() const { return ___c_duration_39; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_duration_39() { return &___c_duration_39; }
	inline void set_c_duration_39(DatatypeImplementation_t1152094268 * value)
	{
		___c_duration_39 = value;
		Il2CppCodeGenWriteBarrier((&___c_duration_39), value);
	}

	inline static int32_t get_offset_of_c_ENTITY_40() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ENTITY_40)); }
	inline DatatypeImplementation_t1152094268 * get_c_ENTITY_40() const { return ___c_ENTITY_40; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ENTITY_40() { return &___c_ENTITY_40; }
	inline void set_c_ENTITY_40(DatatypeImplementation_t1152094268 * value)
	{
		___c_ENTITY_40 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENTITY_40), value);
	}

	inline static int32_t get_offset_of_c_ENTITIES_41() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ENTITIES_41)); }
	inline DatatypeImplementation_t1152094268 * get_c_ENTITIES_41() const { return ___c_ENTITIES_41; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ENTITIES_41() { return &___c_ENTITIES_41; }
	inline void set_c_ENTITIES_41(DatatypeImplementation_t1152094268 * value)
	{
		___c_ENTITIES_41 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENTITIES_41), value);
	}

	inline static int32_t get_offset_of_c_ENUMERATION_42() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ENUMERATION_42)); }
	inline DatatypeImplementation_t1152094268 * get_c_ENUMERATION_42() const { return ___c_ENUMERATION_42; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ENUMERATION_42() { return &___c_ENUMERATION_42; }
	inline void set_c_ENUMERATION_42(DatatypeImplementation_t1152094268 * value)
	{
		___c_ENUMERATION_42 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENUMERATION_42), value);
	}

	inline static int32_t get_offset_of_c_fixed_43() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_fixed_43)); }
	inline DatatypeImplementation_t1152094268 * get_c_fixed_43() const { return ___c_fixed_43; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_fixed_43() { return &___c_fixed_43; }
	inline void set_c_fixed_43(DatatypeImplementation_t1152094268 * value)
	{
		___c_fixed_43 = value;
		Il2CppCodeGenWriteBarrier((&___c_fixed_43), value);
	}

	inline static int32_t get_offset_of_c_float_44() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_float_44)); }
	inline DatatypeImplementation_t1152094268 * get_c_float_44() const { return ___c_float_44; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_float_44() { return &___c_float_44; }
	inline void set_c_float_44(DatatypeImplementation_t1152094268 * value)
	{
		___c_float_44 = value;
		Il2CppCodeGenWriteBarrier((&___c_float_44), value);
	}

	inline static int32_t get_offset_of_c_floatXdr_45() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_floatXdr_45)); }
	inline DatatypeImplementation_t1152094268 * get_c_floatXdr_45() const { return ___c_floatXdr_45; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_floatXdr_45() { return &___c_floatXdr_45; }
	inline void set_c_floatXdr_45(DatatypeImplementation_t1152094268 * value)
	{
		___c_floatXdr_45 = value;
		Il2CppCodeGenWriteBarrier((&___c_floatXdr_45), value);
	}

	inline static int32_t get_offset_of_c_hexBinary_46() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_hexBinary_46)); }
	inline DatatypeImplementation_t1152094268 * get_c_hexBinary_46() const { return ___c_hexBinary_46; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_hexBinary_46() { return &___c_hexBinary_46; }
	inline void set_c_hexBinary_46(DatatypeImplementation_t1152094268 * value)
	{
		___c_hexBinary_46 = value;
		Il2CppCodeGenWriteBarrier((&___c_hexBinary_46), value);
	}

	inline static int32_t get_offset_of_c_ID_47() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_ID_47)); }
	inline DatatypeImplementation_t1152094268 * get_c_ID_47() const { return ___c_ID_47; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_ID_47() { return &___c_ID_47; }
	inline void set_c_ID_47(DatatypeImplementation_t1152094268 * value)
	{
		___c_ID_47 = value;
		Il2CppCodeGenWriteBarrier((&___c_ID_47), value);
	}

	inline static int32_t get_offset_of_c_IDREF_48() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_IDREF_48)); }
	inline DatatypeImplementation_t1152094268 * get_c_IDREF_48() const { return ___c_IDREF_48; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_IDREF_48() { return &___c_IDREF_48; }
	inline void set_c_IDREF_48(DatatypeImplementation_t1152094268 * value)
	{
		___c_IDREF_48 = value;
		Il2CppCodeGenWriteBarrier((&___c_IDREF_48), value);
	}

	inline static int32_t get_offset_of_c_IDREFS_49() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_IDREFS_49)); }
	inline DatatypeImplementation_t1152094268 * get_c_IDREFS_49() const { return ___c_IDREFS_49; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_IDREFS_49() { return &___c_IDREFS_49; }
	inline void set_c_IDREFS_49(DatatypeImplementation_t1152094268 * value)
	{
		___c_IDREFS_49 = value;
		Il2CppCodeGenWriteBarrier((&___c_IDREFS_49), value);
	}

	inline static int32_t get_offset_of_c_int_50() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_int_50)); }
	inline DatatypeImplementation_t1152094268 * get_c_int_50() const { return ___c_int_50; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_int_50() { return &___c_int_50; }
	inline void set_c_int_50(DatatypeImplementation_t1152094268 * value)
	{
		___c_int_50 = value;
		Il2CppCodeGenWriteBarrier((&___c_int_50), value);
	}

	inline static int32_t get_offset_of_c_integer_51() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_integer_51)); }
	inline DatatypeImplementation_t1152094268 * get_c_integer_51() const { return ___c_integer_51; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_integer_51() { return &___c_integer_51; }
	inline void set_c_integer_51(DatatypeImplementation_t1152094268 * value)
	{
		___c_integer_51 = value;
		Il2CppCodeGenWriteBarrier((&___c_integer_51), value);
	}

	inline static int32_t get_offset_of_c_language_52() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_language_52)); }
	inline DatatypeImplementation_t1152094268 * get_c_language_52() const { return ___c_language_52; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_language_52() { return &___c_language_52; }
	inline void set_c_language_52(DatatypeImplementation_t1152094268 * value)
	{
		___c_language_52 = value;
		Il2CppCodeGenWriteBarrier((&___c_language_52), value);
	}

	inline static int32_t get_offset_of_c_long_53() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_long_53)); }
	inline DatatypeImplementation_t1152094268 * get_c_long_53() const { return ___c_long_53; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_long_53() { return &___c_long_53; }
	inline void set_c_long_53(DatatypeImplementation_t1152094268 * value)
	{
		___c_long_53 = value;
		Il2CppCodeGenWriteBarrier((&___c_long_53), value);
	}

	inline static int32_t get_offset_of_c_month_54() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_month_54)); }
	inline DatatypeImplementation_t1152094268 * get_c_month_54() const { return ___c_month_54; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_month_54() { return &___c_month_54; }
	inline void set_c_month_54(DatatypeImplementation_t1152094268 * value)
	{
		___c_month_54 = value;
		Il2CppCodeGenWriteBarrier((&___c_month_54), value);
	}

	inline static int32_t get_offset_of_c_monthDay_55() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_monthDay_55)); }
	inline DatatypeImplementation_t1152094268 * get_c_monthDay_55() const { return ___c_monthDay_55; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_monthDay_55() { return &___c_monthDay_55; }
	inline void set_c_monthDay_55(DatatypeImplementation_t1152094268 * value)
	{
		___c_monthDay_55 = value;
		Il2CppCodeGenWriteBarrier((&___c_monthDay_55), value);
	}

	inline static int32_t get_offset_of_c_Name_56() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_Name_56)); }
	inline DatatypeImplementation_t1152094268 * get_c_Name_56() const { return ___c_Name_56; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_Name_56() { return &___c_Name_56; }
	inline void set_c_Name_56(DatatypeImplementation_t1152094268 * value)
	{
		___c_Name_56 = value;
		Il2CppCodeGenWriteBarrier((&___c_Name_56), value);
	}

	inline static int32_t get_offset_of_c_NCName_57() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NCName_57)); }
	inline DatatypeImplementation_t1152094268 * get_c_NCName_57() const { return ___c_NCName_57; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NCName_57() { return &___c_NCName_57; }
	inline void set_c_NCName_57(DatatypeImplementation_t1152094268 * value)
	{
		___c_NCName_57 = value;
		Il2CppCodeGenWriteBarrier((&___c_NCName_57), value);
	}

	inline static int32_t get_offset_of_c_negativeInteger_58() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_negativeInteger_58)); }
	inline DatatypeImplementation_t1152094268 * get_c_negativeInteger_58() const { return ___c_negativeInteger_58; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_negativeInteger_58() { return &___c_negativeInteger_58; }
	inline void set_c_negativeInteger_58(DatatypeImplementation_t1152094268 * value)
	{
		___c_negativeInteger_58 = value;
		Il2CppCodeGenWriteBarrier((&___c_negativeInteger_58), value);
	}

	inline static int32_t get_offset_of_c_NMTOKEN_59() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NMTOKEN_59)); }
	inline DatatypeImplementation_t1152094268 * get_c_NMTOKEN_59() const { return ___c_NMTOKEN_59; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NMTOKEN_59() { return &___c_NMTOKEN_59; }
	inline void set_c_NMTOKEN_59(DatatypeImplementation_t1152094268 * value)
	{
		___c_NMTOKEN_59 = value;
		Il2CppCodeGenWriteBarrier((&___c_NMTOKEN_59), value);
	}

	inline static int32_t get_offset_of_c_NMTOKENS_60() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NMTOKENS_60)); }
	inline DatatypeImplementation_t1152094268 * get_c_NMTOKENS_60() const { return ___c_NMTOKENS_60; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NMTOKENS_60() { return &___c_NMTOKENS_60; }
	inline void set_c_NMTOKENS_60(DatatypeImplementation_t1152094268 * value)
	{
		___c_NMTOKENS_60 = value;
		Il2CppCodeGenWriteBarrier((&___c_NMTOKENS_60), value);
	}

	inline static int32_t get_offset_of_c_nonNegativeInteger_61() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_nonNegativeInteger_61)); }
	inline DatatypeImplementation_t1152094268 * get_c_nonNegativeInteger_61() const { return ___c_nonNegativeInteger_61; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_nonNegativeInteger_61() { return &___c_nonNegativeInteger_61; }
	inline void set_c_nonNegativeInteger_61(DatatypeImplementation_t1152094268 * value)
	{
		___c_nonNegativeInteger_61 = value;
		Il2CppCodeGenWriteBarrier((&___c_nonNegativeInteger_61), value);
	}

	inline static int32_t get_offset_of_c_nonPositiveInteger_62() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_nonPositiveInteger_62)); }
	inline DatatypeImplementation_t1152094268 * get_c_nonPositiveInteger_62() const { return ___c_nonPositiveInteger_62; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_nonPositiveInteger_62() { return &___c_nonPositiveInteger_62; }
	inline void set_c_nonPositiveInteger_62(DatatypeImplementation_t1152094268 * value)
	{
		___c_nonPositiveInteger_62 = value;
		Il2CppCodeGenWriteBarrier((&___c_nonPositiveInteger_62), value);
	}

	inline static int32_t get_offset_of_c_normalizedString_63() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_normalizedString_63)); }
	inline DatatypeImplementation_t1152094268 * get_c_normalizedString_63() const { return ___c_normalizedString_63; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_normalizedString_63() { return &___c_normalizedString_63; }
	inline void set_c_normalizedString_63(DatatypeImplementation_t1152094268 * value)
	{
		___c_normalizedString_63 = value;
		Il2CppCodeGenWriteBarrier((&___c_normalizedString_63), value);
	}

	inline static int32_t get_offset_of_c_NOTATION_64() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_NOTATION_64)); }
	inline DatatypeImplementation_t1152094268 * get_c_NOTATION_64() const { return ___c_NOTATION_64; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_NOTATION_64() { return &___c_NOTATION_64; }
	inline void set_c_NOTATION_64(DatatypeImplementation_t1152094268 * value)
	{
		___c_NOTATION_64 = value;
		Il2CppCodeGenWriteBarrier((&___c_NOTATION_64), value);
	}

	inline static int32_t get_offset_of_c_positiveInteger_65() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_positiveInteger_65)); }
	inline DatatypeImplementation_t1152094268 * get_c_positiveInteger_65() const { return ___c_positiveInteger_65; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_positiveInteger_65() { return &___c_positiveInteger_65; }
	inline void set_c_positiveInteger_65(DatatypeImplementation_t1152094268 * value)
	{
		___c_positiveInteger_65 = value;
		Il2CppCodeGenWriteBarrier((&___c_positiveInteger_65), value);
	}

	inline static int32_t get_offset_of_c_QName_66() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_QName_66)); }
	inline DatatypeImplementation_t1152094268 * get_c_QName_66() const { return ___c_QName_66; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_QName_66() { return &___c_QName_66; }
	inline void set_c_QName_66(DatatypeImplementation_t1152094268 * value)
	{
		___c_QName_66 = value;
		Il2CppCodeGenWriteBarrier((&___c_QName_66), value);
	}

	inline static int32_t get_offset_of_c_QNameXdr_67() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_QNameXdr_67)); }
	inline DatatypeImplementation_t1152094268 * get_c_QNameXdr_67() const { return ___c_QNameXdr_67; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_QNameXdr_67() { return &___c_QNameXdr_67; }
	inline void set_c_QNameXdr_67(DatatypeImplementation_t1152094268 * value)
	{
		___c_QNameXdr_67 = value;
		Il2CppCodeGenWriteBarrier((&___c_QNameXdr_67), value);
	}

	inline static int32_t get_offset_of_c_short_68() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_short_68)); }
	inline DatatypeImplementation_t1152094268 * get_c_short_68() const { return ___c_short_68; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_short_68() { return &___c_short_68; }
	inline void set_c_short_68(DatatypeImplementation_t1152094268 * value)
	{
		___c_short_68 = value;
		Il2CppCodeGenWriteBarrier((&___c_short_68), value);
	}

	inline static int32_t get_offset_of_c_string_69() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_string_69)); }
	inline DatatypeImplementation_t1152094268 * get_c_string_69() const { return ___c_string_69; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_string_69() { return &___c_string_69; }
	inline void set_c_string_69(DatatypeImplementation_t1152094268 * value)
	{
		___c_string_69 = value;
		Il2CppCodeGenWriteBarrier((&___c_string_69), value);
	}

	inline static int32_t get_offset_of_c_time_70() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_time_70)); }
	inline DatatypeImplementation_t1152094268 * get_c_time_70() const { return ___c_time_70; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_time_70() { return &___c_time_70; }
	inline void set_c_time_70(DatatypeImplementation_t1152094268 * value)
	{
		___c_time_70 = value;
		Il2CppCodeGenWriteBarrier((&___c_time_70), value);
	}

	inline static int32_t get_offset_of_c_timeNoTz_71() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_timeNoTz_71)); }
	inline DatatypeImplementation_t1152094268 * get_c_timeNoTz_71() const { return ___c_timeNoTz_71; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_timeNoTz_71() { return &___c_timeNoTz_71; }
	inline void set_c_timeNoTz_71(DatatypeImplementation_t1152094268 * value)
	{
		___c_timeNoTz_71 = value;
		Il2CppCodeGenWriteBarrier((&___c_timeNoTz_71), value);
	}

	inline static int32_t get_offset_of_c_timeTz_72() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_timeTz_72)); }
	inline DatatypeImplementation_t1152094268 * get_c_timeTz_72() const { return ___c_timeTz_72; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_timeTz_72() { return &___c_timeTz_72; }
	inline void set_c_timeTz_72(DatatypeImplementation_t1152094268 * value)
	{
		___c_timeTz_72 = value;
		Il2CppCodeGenWriteBarrier((&___c_timeTz_72), value);
	}

	inline static int32_t get_offset_of_c_token_73() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_token_73)); }
	inline DatatypeImplementation_t1152094268 * get_c_token_73() const { return ___c_token_73; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_token_73() { return &___c_token_73; }
	inline void set_c_token_73(DatatypeImplementation_t1152094268 * value)
	{
		___c_token_73 = value;
		Il2CppCodeGenWriteBarrier((&___c_token_73), value);
	}

	inline static int32_t get_offset_of_c_unsignedByte_74() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedByte_74)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedByte_74() const { return ___c_unsignedByte_74; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedByte_74() { return &___c_unsignedByte_74; }
	inline void set_c_unsignedByte_74(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedByte_74 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedByte_74), value);
	}

	inline static int32_t get_offset_of_c_unsignedInt_75() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedInt_75)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedInt_75() const { return ___c_unsignedInt_75; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedInt_75() { return &___c_unsignedInt_75; }
	inline void set_c_unsignedInt_75(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedInt_75 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedInt_75), value);
	}

	inline static int32_t get_offset_of_c_unsignedLong_76() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedLong_76)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedLong_76() const { return ___c_unsignedLong_76; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedLong_76() { return &___c_unsignedLong_76; }
	inline void set_c_unsignedLong_76(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedLong_76 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedLong_76), value);
	}

	inline static int32_t get_offset_of_c_unsignedShort_77() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_unsignedShort_77)); }
	inline DatatypeImplementation_t1152094268 * get_c_unsignedShort_77() const { return ___c_unsignedShort_77; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_unsignedShort_77() { return &___c_unsignedShort_77; }
	inline void set_c_unsignedShort_77(DatatypeImplementation_t1152094268 * value)
	{
		___c_unsignedShort_77 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedShort_77), value);
	}

	inline static int32_t get_offset_of_c_uuid_78() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_uuid_78)); }
	inline DatatypeImplementation_t1152094268 * get_c_uuid_78() const { return ___c_uuid_78; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_uuid_78() { return &___c_uuid_78; }
	inline void set_c_uuid_78(DatatypeImplementation_t1152094268 * value)
	{
		___c_uuid_78 = value;
		Il2CppCodeGenWriteBarrier((&___c_uuid_78), value);
	}

	inline static int32_t get_offset_of_c_year_79() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_year_79)); }
	inline DatatypeImplementation_t1152094268 * get_c_year_79() const { return ___c_year_79; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_year_79() { return &___c_year_79; }
	inline void set_c_year_79(DatatypeImplementation_t1152094268 * value)
	{
		___c_year_79 = value;
		Il2CppCodeGenWriteBarrier((&___c_year_79), value);
	}

	inline static int32_t get_offset_of_c_yearMonth_80() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_yearMonth_80)); }
	inline DatatypeImplementation_t1152094268 * get_c_yearMonth_80() const { return ___c_yearMonth_80; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_yearMonth_80() { return &___c_yearMonth_80; }
	inline void set_c_yearMonth_80(DatatypeImplementation_t1152094268 * value)
	{
		___c_yearMonth_80 = value;
		Il2CppCodeGenWriteBarrier((&___c_yearMonth_80), value);
	}

	inline static int32_t get_offset_of_c_normalizedStringV1Compat_81() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_normalizedStringV1Compat_81)); }
	inline DatatypeImplementation_t1152094268 * get_c_normalizedStringV1Compat_81() const { return ___c_normalizedStringV1Compat_81; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_normalizedStringV1Compat_81() { return &___c_normalizedStringV1Compat_81; }
	inline void set_c_normalizedStringV1Compat_81(DatatypeImplementation_t1152094268 * value)
	{
		___c_normalizedStringV1Compat_81 = value;
		Il2CppCodeGenWriteBarrier((&___c_normalizedStringV1Compat_81), value);
	}

	inline static int32_t get_offset_of_c_tokenV1Compat_82() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_tokenV1Compat_82)); }
	inline DatatypeImplementation_t1152094268 * get_c_tokenV1Compat_82() const { return ___c_tokenV1Compat_82; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_tokenV1Compat_82() { return &___c_tokenV1Compat_82; }
	inline void set_c_tokenV1Compat_82(DatatypeImplementation_t1152094268 * value)
	{
		___c_tokenV1Compat_82 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenV1Compat_82), value);
	}

	inline static int32_t get_offset_of_c_anyAtomicType_83() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_anyAtomicType_83)); }
	inline DatatypeImplementation_t1152094268 * get_c_anyAtomicType_83() const { return ___c_anyAtomicType_83; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_anyAtomicType_83() { return &___c_anyAtomicType_83; }
	inline void set_c_anyAtomicType_83(DatatypeImplementation_t1152094268 * value)
	{
		___c_anyAtomicType_83 = value;
		Il2CppCodeGenWriteBarrier((&___c_anyAtomicType_83), value);
	}

	inline static int32_t get_offset_of_c_dayTimeDuration_84() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_dayTimeDuration_84)); }
	inline DatatypeImplementation_t1152094268 * get_c_dayTimeDuration_84() const { return ___c_dayTimeDuration_84; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_dayTimeDuration_84() { return &___c_dayTimeDuration_84; }
	inline void set_c_dayTimeDuration_84(DatatypeImplementation_t1152094268 * value)
	{
		___c_dayTimeDuration_84 = value;
		Il2CppCodeGenWriteBarrier((&___c_dayTimeDuration_84), value);
	}

	inline static int32_t get_offset_of_c_untypedAtomicType_85() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_untypedAtomicType_85)); }
	inline DatatypeImplementation_t1152094268 * get_c_untypedAtomicType_85() const { return ___c_untypedAtomicType_85; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_untypedAtomicType_85() { return &___c_untypedAtomicType_85; }
	inline void set_c_untypedAtomicType_85(DatatypeImplementation_t1152094268 * value)
	{
		___c_untypedAtomicType_85 = value;
		Il2CppCodeGenWriteBarrier((&___c_untypedAtomicType_85), value);
	}

	inline static int32_t get_offset_of_c_yearMonthDuration_86() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_yearMonthDuration_86)); }
	inline DatatypeImplementation_t1152094268 * get_c_yearMonthDuration_86() const { return ___c_yearMonthDuration_86; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_c_yearMonthDuration_86() { return &___c_yearMonthDuration_86; }
	inline void set_c_yearMonthDuration_86(DatatypeImplementation_t1152094268 * value)
	{
		___c_yearMonthDuration_86 = value;
		Il2CppCodeGenWriteBarrier((&___c_yearMonthDuration_86), value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypes_87() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_tokenizedTypes_87)); }
	inline DatatypeImplementationU5BU5D_t3131202389* get_c_tokenizedTypes_87() const { return ___c_tokenizedTypes_87; }
	inline DatatypeImplementationU5BU5D_t3131202389** get_address_of_c_tokenizedTypes_87() { return &___c_tokenizedTypes_87; }
	inline void set_c_tokenizedTypes_87(DatatypeImplementationU5BU5D_t3131202389* value)
	{
		___c_tokenizedTypes_87 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenizedTypes_87), value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypesXsd_88() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_tokenizedTypesXsd_88)); }
	inline DatatypeImplementationU5BU5D_t3131202389* get_c_tokenizedTypesXsd_88() const { return ___c_tokenizedTypesXsd_88; }
	inline DatatypeImplementationU5BU5D_t3131202389** get_address_of_c_tokenizedTypesXsd_88() { return &___c_tokenizedTypesXsd_88; }
	inline void set_c_tokenizedTypesXsd_88(DatatypeImplementationU5BU5D_t3131202389* value)
	{
		___c_tokenizedTypesXsd_88 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenizedTypesXsd_88), value);
	}

	inline static int32_t get_offset_of_c_XdrTypes_89() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_XdrTypes_89)); }
	inline SchemaDatatypeMapU5BU5D_t863562528* get_c_XdrTypes_89() const { return ___c_XdrTypes_89; }
	inline SchemaDatatypeMapU5BU5D_t863562528** get_address_of_c_XdrTypes_89() { return &___c_XdrTypes_89; }
	inline void set_c_XdrTypes_89(SchemaDatatypeMapU5BU5D_t863562528* value)
	{
		___c_XdrTypes_89 = value;
		Il2CppCodeGenWriteBarrier((&___c_XdrTypes_89), value);
	}

	inline static int32_t get_offset_of_c_XsdTypes_90() { return static_cast<int32_t>(offsetof(DatatypeImplementation_t1152094268_StaticFields, ___c_XsdTypes_90)); }
	inline SchemaDatatypeMapU5BU5D_t863562528* get_c_XsdTypes_90() const { return ___c_XsdTypes_90; }
	inline SchemaDatatypeMapU5BU5D_t863562528** get_address_of_c_XsdTypes_90() { return &___c_XsdTypes_90; }
	inline void set_c_XsdTypes_90(SchemaDatatypeMapU5BU5D_t863562528* value)
	{
		___c_XsdTypes_90 = value;
		Il2CppCodeGenWriteBarrier((&___c_XsdTypes_90), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPEIMPLEMENTATION_T1152094268_H
#ifndef SCHEMACOLLECTIONPREPROCESSOR_T2028437652_H
#define SCHEMACOLLECTIONPREPROCESSOR_T2028437652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionPreprocessor
struct  SchemaCollectionPreprocessor_t2028437652  : public BaseProcessor_t2373158431
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.SchemaCollectionPreprocessor::schema
	XmlSchema_t880472818 * ___schema_6;
	// System.String System.Xml.Schema.SchemaCollectionPreprocessor::targetNamespace
	String_t* ___targetNamespace_7;
	// System.Boolean System.Xml.Schema.SchemaCollectionPreprocessor::buildinIncluded
	bool ___buildinIncluded_8;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.SchemaCollectionPreprocessor::elementFormDefault
	int32_t ___elementFormDefault_9;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.SchemaCollectionPreprocessor::attributeFormDefault
	int32_t ___attributeFormDefault_10;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaCollectionPreprocessor::blockDefault
	int32_t ___blockDefault_11;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaCollectionPreprocessor::finalDefault
	int32_t ___finalDefault_12;
	// System.Collections.Hashtable System.Xml.Schema.SchemaCollectionPreprocessor::schemaLocations
	Hashtable_t909839986 * ___schemaLocations_13;
	// System.Collections.Hashtable System.Xml.Schema.SchemaCollectionPreprocessor::referenceNamespaces
	Hashtable_t909839986 * ___referenceNamespaces_14;
	// System.String System.Xml.Schema.SchemaCollectionPreprocessor::Xmlns
	String_t* ___Xmlns_15;
	// System.Xml.XmlResolver System.Xml.Schema.SchemaCollectionPreprocessor::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_16;

public:
	inline static int32_t get_offset_of_schema_6() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___schema_6)); }
	inline XmlSchema_t880472818 * get_schema_6() const { return ___schema_6; }
	inline XmlSchema_t880472818 ** get_address_of_schema_6() { return &___schema_6; }
	inline void set_schema_6(XmlSchema_t880472818 * value)
	{
		___schema_6 = value;
		Il2CppCodeGenWriteBarrier((&___schema_6), value);
	}

	inline static int32_t get_offset_of_targetNamespace_7() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___targetNamespace_7)); }
	inline String_t* get_targetNamespace_7() const { return ___targetNamespace_7; }
	inline String_t** get_address_of_targetNamespace_7() { return &___targetNamespace_7; }
	inline void set_targetNamespace_7(String_t* value)
	{
		___targetNamespace_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_7), value);
	}

	inline static int32_t get_offset_of_buildinIncluded_8() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___buildinIncluded_8)); }
	inline bool get_buildinIncluded_8() const { return ___buildinIncluded_8; }
	inline bool* get_address_of_buildinIncluded_8() { return &___buildinIncluded_8; }
	inline void set_buildinIncluded_8(bool value)
	{
		___buildinIncluded_8 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_9() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___elementFormDefault_9)); }
	inline int32_t get_elementFormDefault_9() const { return ___elementFormDefault_9; }
	inline int32_t* get_address_of_elementFormDefault_9() { return &___elementFormDefault_9; }
	inline void set_elementFormDefault_9(int32_t value)
	{
		___elementFormDefault_9 = value;
	}

	inline static int32_t get_offset_of_attributeFormDefault_10() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___attributeFormDefault_10)); }
	inline int32_t get_attributeFormDefault_10() const { return ___attributeFormDefault_10; }
	inline int32_t* get_address_of_attributeFormDefault_10() { return &___attributeFormDefault_10; }
	inline void set_attributeFormDefault_10(int32_t value)
	{
		___attributeFormDefault_10 = value;
	}

	inline static int32_t get_offset_of_blockDefault_11() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___blockDefault_11)); }
	inline int32_t get_blockDefault_11() const { return ___blockDefault_11; }
	inline int32_t* get_address_of_blockDefault_11() { return &___blockDefault_11; }
	inline void set_blockDefault_11(int32_t value)
	{
		___blockDefault_11 = value;
	}

	inline static int32_t get_offset_of_finalDefault_12() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___finalDefault_12)); }
	inline int32_t get_finalDefault_12() const { return ___finalDefault_12; }
	inline int32_t* get_address_of_finalDefault_12() { return &___finalDefault_12; }
	inline void set_finalDefault_12(int32_t value)
	{
		___finalDefault_12 = value;
	}

	inline static int32_t get_offset_of_schemaLocations_13() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___schemaLocations_13)); }
	inline Hashtable_t909839986 * get_schemaLocations_13() const { return ___schemaLocations_13; }
	inline Hashtable_t909839986 ** get_address_of_schemaLocations_13() { return &___schemaLocations_13; }
	inline void set_schemaLocations_13(Hashtable_t909839986 * value)
	{
		___schemaLocations_13 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocations_13), value);
	}

	inline static int32_t get_offset_of_referenceNamespaces_14() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___referenceNamespaces_14)); }
	inline Hashtable_t909839986 * get_referenceNamespaces_14() const { return ___referenceNamespaces_14; }
	inline Hashtable_t909839986 ** get_address_of_referenceNamespaces_14() { return &___referenceNamespaces_14; }
	inline void set_referenceNamespaces_14(Hashtable_t909839986 * value)
	{
		___referenceNamespaces_14 = value;
		Il2CppCodeGenWriteBarrier((&___referenceNamespaces_14), value);
	}

	inline static int32_t get_offset_of_Xmlns_15() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___Xmlns_15)); }
	inline String_t* get_Xmlns_15() const { return ___Xmlns_15; }
	inline String_t** get_address_of_Xmlns_15() { return &___Xmlns_15; }
	inline void set_Xmlns_15(String_t* value)
	{
		___Xmlns_15 = value;
		Il2CppCodeGenWriteBarrier((&___Xmlns_15), value);
	}

	inline static int32_t get_offset_of_xmlResolver_16() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t2028437652, ___xmlResolver_16)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_16() const { return ___xmlResolver_16; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_16() { return &___xmlResolver_16; }
	inline void set_xmlResolver_16(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMACOLLECTIONPREPROCESSOR_T2028437652_H
#ifndef NAMESPACELIST_T848177191_H
#define NAMESPACELIST_T848177191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceList
struct  NamespaceList_t848177191  : public RuntimeObject
{
public:
	// System.Xml.Schema.NamespaceList/ListType System.Xml.Schema.NamespaceList::type
	int32_t ___type_0;
	// System.Collections.Hashtable System.Xml.Schema.NamespaceList::set
	Hashtable_t909839986 * ___set_1;
	// System.String System.Xml.Schema.NamespaceList::targetNamespace
	String_t* ___targetNamespace_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(NamespaceList_t848177191, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_set_1() { return static_cast<int32_t>(offsetof(NamespaceList_t848177191, ___set_1)); }
	inline Hashtable_t909839986 * get_set_1() const { return ___set_1; }
	inline Hashtable_t909839986 ** get_address_of_set_1() { return &___set_1; }
	inline void set_set_1(Hashtable_t909839986 * value)
	{
		___set_1 = value;
		Il2CppCodeGenWriteBarrier((&___set_1), value);
	}

	inline static int32_t get_offset_of_targetNamespace_2() { return static_cast<int32_t>(offsetof(NamespaceList_t848177191, ___targetNamespace_2)); }
	inline String_t* get_targetNamespace_2() const { return ___targetNamespace_2; }
	inline String_t** get_address_of_targetNamespace_2() { return &___targetNamespace_2; }
	inline void set_targetNamespace_2(String_t* value)
	{
		___targetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACELIST_T848177191_H
#ifndef PARSER_T1940171737_H
#define PARSER_T1940171737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Parser
struct  Parser_t1940171737  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaType System.Xml.Schema.Parser::schemaType
	int32_t ___schemaType_0;
	// System.Xml.XmlNameTable System.Xml.Schema.Parser::nameTable
	XmlNameTable_t1345805268 * ___nameTable_1;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.Parser::schemaNames
	SchemaNames_t1619962557 * ___schemaNames_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.Parser::eventHandler
	ValidationEventHandler_t1580700381 * ___eventHandler_3;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Parser::namespaceManager
	XmlNamespaceManager_t486731501 * ___namespaceManager_4;
	// System.Xml.XmlReader System.Xml.Schema.Parser::reader
	XmlReader_t3675626668 * ___reader_5;
	// System.Xml.PositionInfo System.Xml.Schema.Parser::positionInfo
	PositionInfo_t3273236083 * ___positionInfo_6;
	// System.Boolean System.Xml.Schema.Parser::isProcessNamespaces
	bool ___isProcessNamespaces_7;
	// System.Int32 System.Xml.Schema.Parser::schemaXmlDepth
	int32_t ___schemaXmlDepth_8;
	// System.Int32 System.Xml.Schema.Parser::markupDepth
	int32_t ___markupDepth_9;
	// System.Xml.Schema.SchemaBuilder System.Xml.Schema.Parser::builder
	SchemaBuilder_t908297946 * ___builder_10;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Parser::schema
	XmlSchema_t880472818 * ___schema_11;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.Parser::xdrSchema
	SchemaInfo_t87206461 * ___xdrSchema_12;
	// System.Xml.XmlResolver System.Xml.Schema.Parser::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_13;
	// System.Xml.XmlDocument System.Xml.Schema.Parser::dummyDocument
	XmlDocument_t3649534162 * ___dummyDocument_14;
	// System.Boolean System.Xml.Schema.Parser::processMarkup
	bool ___processMarkup_15;
	// System.Xml.XmlNode System.Xml.Schema.Parser::parentNode
	XmlNode_t616554813 * ___parentNode_16;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Parser::annotationNSManager
	XmlNamespaceManager_t486731501 * ___annotationNSManager_17;
	// System.String System.Xml.Schema.Parser::xmlns
	String_t* ___xmlns_18;
	// System.Xml.XmlCharType System.Xml.Schema.Parser::xmlCharType
	XmlCharType_t1050521405  ___xmlCharType_19;

public:
	inline static int32_t get_offset_of_schemaType_0() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schemaType_0)); }
	inline int32_t get_schemaType_0() const { return ___schemaType_0; }
	inline int32_t* get_address_of_schemaType_0() { return &___schemaType_0; }
	inline void set_schemaType_0(int32_t value)
	{
		___schemaType_0 = value;
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___nameTable_1)); }
	inline XmlNameTable_t1345805268 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t1345805268 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_schemaNames_2() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schemaNames_2)); }
	inline SchemaNames_t1619962557 * get_schemaNames_2() const { return ___schemaNames_2; }
	inline SchemaNames_t1619962557 ** get_address_of_schemaNames_2() { return &___schemaNames_2; }
	inline void set_schemaNames_2(SchemaNames_t1619962557 * value)
	{
		___schemaNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_2), value);
	}

	inline static int32_t get_offset_of_eventHandler_3() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___eventHandler_3)); }
	inline ValidationEventHandler_t1580700381 * get_eventHandler_3() const { return ___eventHandler_3; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_eventHandler_3() { return &___eventHandler_3; }
	inline void set_eventHandler_3(ValidationEventHandler_t1580700381 * value)
	{
		___eventHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_3), value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t486731501 * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t486731501 * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_4), value);
	}

	inline static int32_t get_offset_of_reader_5() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___reader_5)); }
	inline XmlReader_t3675626668 * get_reader_5() const { return ___reader_5; }
	inline XmlReader_t3675626668 ** get_address_of_reader_5() { return &___reader_5; }
	inline void set_reader_5(XmlReader_t3675626668 * value)
	{
		___reader_5 = value;
		Il2CppCodeGenWriteBarrier((&___reader_5), value);
	}

	inline static int32_t get_offset_of_positionInfo_6() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___positionInfo_6)); }
	inline PositionInfo_t3273236083 * get_positionInfo_6() const { return ___positionInfo_6; }
	inline PositionInfo_t3273236083 ** get_address_of_positionInfo_6() { return &___positionInfo_6; }
	inline void set_positionInfo_6(PositionInfo_t3273236083 * value)
	{
		___positionInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_6), value);
	}

	inline static int32_t get_offset_of_isProcessNamespaces_7() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___isProcessNamespaces_7)); }
	inline bool get_isProcessNamespaces_7() const { return ___isProcessNamespaces_7; }
	inline bool* get_address_of_isProcessNamespaces_7() { return &___isProcessNamespaces_7; }
	inline void set_isProcessNamespaces_7(bool value)
	{
		___isProcessNamespaces_7 = value;
	}

	inline static int32_t get_offset_of_schemaXmlDepth_8() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schemaXmlDepth_8)); }
	inline int32_t get_schemaXmlDepth_8() const { return ___schemaXmlDepth_8; }
	inline int32_t* get_address_of_schemaXmlDepth_8() { return &___schemaXmlDepth_8; }
	inline void set_schemaXmlDepth_8(int32_t value)
	{
		___schemaXmlDepth_8 = value;
	}

	inline static int32_t get_offset_of_markupDepth_9() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___markupDepth_9)); }
	inline int32_t get_markupDepth_9() const { return ___markupDepth_9; }
	inline int32_t* get_address_of_markupDepth_9() { return &___markupDepth_9; }
	inline void set_markupDepth_9(int32_t value)
	{
		___markupDepth_9 = value;
	}

	inline static int32_t get_offset_of_builder_10() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___builder_10)); }
	inline SchemaBuilder_t908297946 * get_builder_10() const { return ___builder_10; }
	inline SchemaBuilder_t908297946 ** get_address_of_builder_10() { return &___builder_10; }
	inline void set_builder_10(SchemaBuilder_t908297946 * value)
	{
		___builder_10 = value;
		Il2CppCodeGenWriteBarrier((&___builder_10), value);
	}

	inline static int32_t get_offset_of_schema_11() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___schema_11)); }
	inline XmlSchema_t880472818 * get_schema_11() const { return ___schema_11; }
	inline XmlSchema_t880472818 ** get_address_of_schema_11() { return &___schema_11; }
	inline void set_schema_11(XmlSchema_t880472818 * value)
	{
		___schema_11 = value;
		Il2CppCodeGenWriteBarrier((&___schema_11), value);
	}

	inline static int32_t get_offset_of_xdrSchema_12() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xdrSchema_12)); }
	inline SchemaInfo_t87206461 * get_xdrSchema_12() const { return ___xdrSchema_12; }
	inline SchemaInfo_t87206461 ** get_address_of_xdrSchema_12() { return &___xdrSchema_12; }
	inline void set_xdrSchema_12(SchemaInfo_t87206461 * value)
	{
		___xdrSchema_12 = value;
		Il2CppCodeGenWriteBarrier((&___xdrSchema_12), value);
	}

	inline static int32_t get_offset_of_xmlResolver_13() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xmlResolver_13)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_13() const { return ___xmlResolver_13; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_13() { return &___xmlResolver_13; }
	inline void set_xmlResolver_13(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_13 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_13), value);
	}

	inline static int32_t get_offset_of_dummyDocument_14() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___dummyDocument_14)); }
	inline XmlDocument_t3649534162 * get_dummyDocument_14() const { return ___dummyDocument_14; }
	inline XmlDocument_t3649534162 ** get_address_of_dummyDocument_14() { return &___dummyDocument_14; }
	inline void set_dummyDocument_14(XmlDocument_t3649534162 * value)
	{
		___dummyDocument_14 = value;
		Il2CppCodeGenWriteBarrier((&___dummyDocument_14), value);
	}

	inline static int32_t get_offset_of_processMarkup_15() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___processMarkup_15)); }
	inline bool get_processMarkup_15() const { return ___processMarkup_15; }
	inline bool* get_address_of_processMarkup_15() { return &___processMarkup_15; }
	inline void set_processMarkup_15(bool value)
	{
		___processMarkup_15 = value;
	}

	inline static int32_t get_offset_of_parentNode_16() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___parentNode_16)); }
	inline XmlNode_t616554813 * get_parentNode_16() const { return ___parentNode_16; }
	inline XmlNode_t616554813 ** get_address_of_parentNode_16() { return &___parentNode_16; }
	inline void set_parentNode_16(XmlNode_t616554813 * value)
	{
		___parentNode_16 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_16), value);
	}

	inline static int32_t get_offset_of_annotationNSManager_17() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___annotationNSManager_17)); }
	inline XmlNamespaceManager_t486731501 * get_annotationNSManager_17() const { return ___annotationNSManager_17; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_annotationNSManager_17() { return &___annotationNSManager_17; }
	inline void set_annotationNSManager_17(XmlNamespaceManager_t486731501 * value)
	{
		___annotationNSManager_17 = value;
		Il2CppCodeGenWriteBarrier((&___annotationNSManager_17), value);
	}

	inline static int32_t get_offset_of_xmlns_18() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xmlns_18)); }
	inline String_t* get_xmlns_18() const { return ___xmlns_18; }
	inline String_t** get_address_of_xmlns_18() { return &___xmlns_18; }
	inline void set_xmlns_18(String_t* value)
	{
		___xmlns_18 = value;
		Il2CppCodeGenWriteBarrier((&___xmlns_18), value);
	}

	inline static int32_t get_offset_of_xmlCharType_19() { return static_cast<int32_t>(offsetof(Parser_t1940171737, ___xmlCharType_19)); }
	inline XmlCharType_t1050521405  get_xmlCharType_19() const { return ___xmlCharType_19; }
	inline XmlCharType_t1050521405 * get_address_of_xmlCharType_19() { return &___xmlCharType_19; }
	inline void set_xmlCharType_19(XmlCharType_t1050521405  value)
	{
		___xmlCharType_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T1940171737_H
#ifndef ALLELEMENTSCONTENTVALIDATOR_T2095068475_H
#define ALLELEMENTSCONTENTVALIDATOR_T2095068475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AllElementsContentValidator
struct  AllElementsContentValidator_t2095068475  : public ContentValidator_t2510151843
{
public:
	// System.Collections.Hashtable System.Xml.Schema.AllElementsContentValidator::elements
	Hashtable_t909839986 * ___elements_7;
	// System.Object[] System.Xml.Schema.AllElementsContentValidator::particles
	ObjectU5BU5D_t3614634134* ___particles_8;
	// System.Xml.Schema.BitSet System.Xml.Schema.AllElementsContentValidator::isRequired
	BitSet_t1062448123 * ___isRequired_9;
	// System.Int32 System.Xml.Schema.AllElementsContentValidator::countRequired
	int32_t ___countRequired_10;

public:
	inline static int32_t get_offset_of_elements_7() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___elements_7)); }
	inline Hashtable_t909839986 * get_elements_7() const { return ___elements_7; }
	inline Hashtable_t909839986 ** get_address_of_elements_7() { return &___elements_7; }
	inline void set_elements_7(Hashtable_t909839986 * value)
	{
		___elements_7 = value;
		Il2CppCodeGenWriteBarrier((&___elements_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___particles_8)); }
	inline ObjectU5BU5D_t3614634134* get_particles_8() const { return ___particles_8; }
	inline ObjectU5BU5D_t3614634134** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ObjectU5BU5D_t3614634134* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}

	inline static int32_t get_offset_of_isRequired_9() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___isRequired_9)); }
	inline BitSet_t1062448123 * get_isRequired_9() const { return ___isRequired_9; }
	inline BitSet_t1062448123 ** get_address_of_isRequired_9() { return &___isRequired_9; }
	inline void set_isRequired_9(BitSet_t1062448123 * value)
	{
		___isRequired_9 = value;
		Il2CppCodeGenWriteBarrier((&___isRequired_9), value);
	}

	inline static int32_t get_offset_of_countRequired_10() { return static_cast<int32_t>(offsetof(AllElementsContentValidator_t2095068475, ___countRequired_10)); }
	inline int32_t get_countRequired_10() const { return ___countRequired_10; }
	inline int32_t* get_address_of_countRequired_10() { return &___countRequired_10; }
	inline void set_countRequired_10(int32_t value)
	{
		___countRequired_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLELEMENTSCONTENTVALIDATOR_T2095068475_H
#ifndef SCHEMAATTDEF_T1510907267_H
#define SCHEMAATTDEF_T1510907267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaAttDef
struct  SchemaAttDef_t1510907267  : public SchemaDeclBase_t797759480
{
public:
	// System.String System.Xml.Schema.SchemaAttDef::defExpanded
	String_t* ___defExpanded_11;
	// System.Int32 System.Xml.Schema.SchemaAttDef::lineNum
	int32_t ___lineNum_12;
	// System.Int32 System.Xml.Schema.SchemaAttDef::linePos
	int32_t ___linePos_13;
	// System.Int32 System.Xml.Schema.SchemaAttDef::valueLineNum
	int32_t ___valueLineNum_14;
	// System.Int32 System.Xml.Schema.SchemaAttDef::valueLinePos
	int32_t ___valueLinePos_15;
	// System.Xml.Schema.SchemaAttDef/Reserve System.Xml.Schema.SchemaAttDef::reserved
	int32_t ___reserved_16;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.SchemaAttDef::schemaAttribute
	XmlSchemaAttribute_t4015859774 * ___schemaAttribute_17;

public:
	inline static int32_t get_offset_of_defExpanded_11() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___defExpanded_11)); }
	inline String_t* get_defExpanded_11() const { return ___defExpanded_11; }
	inline String_t** get_address_of_defExpanded_11() { return &___defExpanded_11; }
	inline void set_defExpanded_11(String_t* value)
	{
		___defExpanded_11 = value;
		Il2CppCodeGenWriteBarrier((&___defExpanded_11), value);
	}

	inline static int32_t get_offset_of_lineNum_12() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___lineNum_12)); }
	inline int32_t get_lineNum_12() const { return ___lineNum_12; }
	inline int32_t* get_address_of_lineNum_12() { return &___lineNum_12; }
	inline void set_lineNum_12(int32_t value)
	{
		___lineNum_12 = value;
	}

	inline static int32_t get_offset_of_linePos_13() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___linePos_13)); }
	inline int32_t get_linePos_13() const { return ___linePos_13; }
	inline int32_t* get_address_of_linePos_13() { return &___linePos_13; }
	inline void set_linePos_13(int32_t value)
	{
		___linePos_13 = value;
	}

	inline static int32_t get_offset_of_valueLineNum_14() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___valueLineNum_14)); }
	inline int32_t get_valueLineNum_14() const { return ___valueLineNum_14; }
	inline int32_t* get_address_of_valueLineNum_14() { return &___valueLineNum_14; }
	inline void set_valueLineNum_14(int32_t value)
	{
		___valueLineNum_14 = value;
	}

	inline static int32_t get_offset_of_valueLinePos_15() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___valueLinePos_15)); }
	inline int32_t get_valueLinePos_15() const { return ___valueLinePos_15; }
	inline int32_t* get_address_of_valueLinePos_15() { return &___valueLinePos_15; }
	inline void set_valueLinePos_15(int32_t value)
	{
		___valueLinePos_15 = value;
	}

	inline static int32_t get_offset_of_reserved_16() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___reserved_16)); }
	inline int32_t get_reserved_16() const { return ___reserved_16; }
	inline int32_t* get_address_of_reserved_16() { return &___reserved_16; }
	inline void set_reserved_16(int32_t value)
	{
		___reserved_16 = value;
	}

	inline static int32_t get_offset_of_schemaAttribute_17() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267, ___schemaAttribute_17)); }
	inline XmlSchemaAttribute_t4015859774 * get_schemaAttribute_17() const { return ___schemaAttribute_17; }
	inline XmlSchemaAttribute_t4015859774 ** get_address_of_schemaAttribute_17() { return &___schemaAttribute_17; }
	inline void set_schemaAttribute_17(XmlSchemaAttribute_t4015859774 * value)
	{
		___schemaAttribute_17 = value;
		Il2CppCodeGenWriteBarrier((&___schemaAttribute_17), value);
	}
};

struct SchemaAttDef_t1510907267_StaticFields
{
public:
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.SchemaAttDef::Empty
	SchemaAttDef_t1510907267 * ___Empty_18;

public:
	inline static int32_t get_offset_of_Empty_18() { return static_cast<int32_t>(offsetof(SchemaAttDef_t1510907267_StaticFields, ___Empty_18)); }
	inline SchemaAttDef_t1510907267 * get_Empty_18() const { return ___Empty_18; }
	inline SchemaAttDef_t1510907267 ** get_address_of_Empty_18() { return &___Empty_18; }
	inline void set_Empty_18(SchemaAttDef_t1510907267 * value)
	{
		___Empty_18 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAATTDEF_T1510907267_H
#ifndef NAMESPACELISTV1COMPAT_T2783980816_H
#define NAMESPACELISTV1COMPAT_T2783980816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceListV1Compat
struct  NamespaceListV1Compat_t2783980816  : public NamespaceList_t848177191
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACELISTV1COMPAT_T2783980816_H
#ifndef DATATYPE_ANYSIMPLETYPE_T4012795865_H
#define DATATYPE_ANYSIMPLETYPE_T4012795865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anySimpleType
struct  Datatype_anySimpleType_t4012795865  : public DatatypeImplementation_t1152094268
{
public:

public:
};

struct Datatype_anySimpleType_t4012795865_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_anySimpleType::atomicValueType
	Type_t * ___atomicValueType_91;
	// System.Type System.Xml.Schema.Datatype_anySimpleType::listValueType
	Type_t * ___listValueType_92;

public:
	inline static int32_t get_offset_of_atomicValueType_91() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_t4012795865_StaticFields, ___atomicValueType_91)); }
	inline Type_t * get_atomicValueType_91() const { return ___atomicValueType_91; }
	inline Type_t ** get_address_of_atomicValueType_91() { return &___atomicValueType_91; }
	inline void set_atomicValueType_91(Type_t * value)
	{
		___atomicValueType_91 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_91), value);
	}

	inline static int32_t get_offset_of_listValueType_92() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_t4012795865_StaticFields, ___listValueType_92)); }
	inline Type_t * get_listValueType_92() const { return ___listValueType_92; }
	inline Type_t ** get_address_of_listValueType_92() { return &___listValueType_92; }
	inline void set_listValueType_92(Type_t * value)
	{
		___listValueType_92 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_92), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYSIMPLETYPE_T4012795865_H
#ifndef DATATYPE_UUID_T2771989020_H
#define DATATYPE_UUID_T2771989020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_uuid
struct  Datatype_uuid_t2771989020  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_uuid_t2771989020_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_uuid::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_uuid::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_uuid_t2771989020_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_uuid_t2771989020_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UUID_T2771989020_H
#ifndef DATATYPE_DECIMAL_T2973594954_H
#define DATATYPE_DECIMAL_T2973594954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_decimal
struct  Datatype_decimal_t2973594954  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_decimal_t2973594954_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_decimal::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_decimal::listValueType
	Type_t * ___listValueType_94;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_decimal::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_95;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_decimal_t2973594954_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_decimal_t2973594954_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_95() { return static_cast<int32_t>(offsetof(Datatype_decimal_t2973594954_StaticFields, ___numeric10FacetsChecker_95)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_95() const { return ___numeric10FacetsChecker_95; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_95() { return &___numeric10FacetsChecker_95; }
	inline void set_numeric10FacetsChecker_95(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_95 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_95), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DECIMAL_T2973594954_H
#ifndef DATATYPE_QNAMEXDR_T2452527483_H
#define DATATYPE_QNAMEXDR_T2452527483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_QNameXdr
struct  Datatype_QNameXdr_t2452527483  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_QNameXdr_t2452527483_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_QNameXdr::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_QNameXdr::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_QNameXdr_t2452527483_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_QNameXdr_t2452527483_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_QNAMEXDR_T2452527483_H
#ifndef DATATYPE_DOUBLE_T1050796240_H
#define DATATYPE_DOUBLE_T1050796240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_double
struct  Datatype_double_t1050796240  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_double_t1050796240_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_double::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_double::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_double_t1050796240_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_double_t1050796240_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DOUBLE_T1050796240_H
#ifndef DATATYPE_CHAR_T534108237_H
#define DATATYPE_CHAR_T534108237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_char
struct  Datatype_char_t534108237  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_char_t534108237_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_char::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_char::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_char_t534108237_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_char_t534108237_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_CHAR_T534108237_H
#ifndef DATATYPE_NOTATION_T3412197483_H
#define DATATYPE_NOTATION_T3412197483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_NOTATION
struct  Datatype_NOTATION_t3412197483  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_NOTATION_t3412197483_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_NOTATION::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_NOTATION::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_NOTATION_t3412197483_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_NOTATION_t3412197483_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NOTATION_T3412197483_H
#ifndef DATATYPE_DATETIMEBASE_T2449194189_H
#define DATATYPE_DATETIMEBASE_T2449194189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTimeBase
struct  Datatype_dateTimeBase_t2449194189  : public Datatype_anySimpleType_t4012795865
{
public:
	// System.Xml.Schema.XsdDateTimeFlags System.Xml.Schema.Datatype_dateTimeBase::dateTimeFlags
	int32_t ___dateTimeFlags_95;

public:
	inline static int32_t get_offset_of_dateTimeFlags_95() { return static_cast<int32_t>(offsetof(Datatype_dateTimeBase_t2449194189, ___dateTimeFlags_95)); }
	inline int32_t get_dateTimeFlags_95() const { return ___dateTimeFlags_95; }
	inline int32_t* get_address_of_dateTimeFlags_95() { return &___dateTimeFlags_95; }
	inline void set_dateTimeFlags_95(int32_t value)
	{
		___dateTimeFlags_95 = value;
	}
};

struct Datatype_dateTimeBase_t2449194189_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_dateTimeBase::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_dateTimeBase::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_dateTimeBase_t2449194189_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_dateTimeBase_t2449194189_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIMEBASE_T2449194189_H
#ifndef DATATYPE_QNAME_T2180543649_H
#define DATATYPE_QNAME_T2180543649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_QName
struct  Datatype_QName_t2180543649  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_QName_t2180543649_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_QName::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_QName::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_QName_t2180543649_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_QName_t2180543649_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_QNAME_T2180543649_H
#ifndef DATATYPE_ANYURI_T2417434093_H
#define DATATYPE_ANYURI_T2417434093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anyURI
struct  Datatype_anyURI_t2417434093  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_anyURI_t2417434093_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_anyURI::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_anyURI::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_anyURI_t2417434093_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_anyURI_t2417434093_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYURI_T2417434093_H
#ifndef DATATYPE_HEXBINARY_T2599154205_H
#define DATATYPE_HEXBINARY_T2599154205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_hexBinary
struct  Datatype_hexBinary_t2599154205  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_hexBinary_t2599154205_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_hexBinary::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_hexBinary::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_hexBinary_t2599154205_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_hexBinary_t2599154205_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_HEXBINARY_T2599154205_H
#ifndef DATATYPE_BASE64BINARY_T3902009307_H
#define DATATYPE_BASE64BINARY_T3902009307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_base64Binary
struct  Datatype_base64Binary_t3902009307  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_base64Binary_t3902009307_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_base64Binary::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_base64Binary::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_base64Binary_t3902009307_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_base64Binary_t3902009307_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_BASE64BINARY_T3902009307_H
#ifndef DATATYPE_DURATION_T1871787273_H
#define DATATYPE_DURATION_T1871787273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_duration
struct  Datatype_duration_t1871787273  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_duration_t1871787273_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_duration::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_duration::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_duration_t1871787273_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_duration_t1871787273_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DURATION_T1871787273_H
#ifndef DATATYPE_LIST_T1892289229_H
#define DATATYPE_LIST_T1892289229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_List
struct  Datatype_List_t1892289229  : public Datatype_anySimpleType_t4012795865
{
public:
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.Datatype_List::itemType
	DatatypeImplementation_t1152094268 * ___itemType_93;
	// System.Int32 System.Xml.Schema.Datatype_List::minListSize
	int32_t ___minListSize_94;

public:
	inline static int32_t get_offset_of_itemType_93() { return static_cast<int32_t>(offsetof(Datatype_List_t1892289229, ___itemType_93)); }
	inline DatatypeImplementation_t1152094268 * get_itemType_93() const { return ___itemType_93; }
	inline DatatypeImplementation_t1152094268 ** get_address_of_itemType_93() { return &___itemType_93; }
	inline void set_itemType_93(DatatypeImplementation_t1152094268 * value)
	{
		___itemType_93 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_93), value);
	}

	inline static int32_t get_offset_of_minListSize_94() { return static_cast<int32_t>(offsetof(Datatype_List_t1892289229, ___minListSize_94)); }
	inline int32_t get_minListSize_94() const { return ___minListSize_94; }
	inline int32_t* get_address_of_minListSize_94() { return &___minListSize_94; }
	inline void set_minListSize_94(int32_t value)
	{
		___minListSize_94 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_LIST_T1892289229_H
#ifndef DATATYPE_UNION_T2515141346_H
#define DATATYPE_UNION_T2515141346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_union
struct  Datatype_union_t2515141346  : public Datatype_anySimpleType_t4012795865
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.Datatype_union::types
	XmlSchemaSimpleTypeU5BU5D_t192177157* ___types_95;

public:
	inline static int32_t get_offset_of_types_95() { return static_cast<int32_t>(offsetof(Datatype_union_t2515141346, ___types_95)); }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157* get_types_95() const { return ___types_95; }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157** get_address_of_types_95() { return &___types_95; }
	inline void set_types_95(XmlSchemaSimpleTypeU5BU5D_t192177157* value)
	{
		___types_95 = value;
		Il2CppCodeGenWriteBarrier((&___types_95), value);
	}
};

struct Datatype_union_t2515141346_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_union::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_union::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_union_t2515141346_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_union_t2515141346_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNION_T2515141346_H
#ifndef DATATYPE_ANYATOMICTYPE_T4212901794_H
#define DATATYPE_ANYATOMICTYPE_T4212901794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anyAtomicType
struct  Datatype_anyAtomicType_t4212901794  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYATOMICTYPE_T4212901794_H
#ifndef DATATYPE_BOOLEAN_T293982753_H
#define DATATYPE_BOOLEAN_T293982753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_boolean
struct  Datatype_boolean_t293982753  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_boolean_t293982753_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_boolean::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_boolean::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_boolean_t293982753_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_boolean_t293982753_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_BOOLEAN_T293982753_H
#ifndef DATATYPE_STRING_T995037180_H
#define DATATYPE_STRING_T995037180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_string
struct  Datatype_string_t995037180  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_STRING_T995037180_H
#ifndef DATATYPE_FLOAT_T3149441939_H
#define DATATYPE_FLOAT_T3149441939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_float
struct  Datatype_float_t3149441939  : public Datatype_anySimpleType_t4012795865
{
public:

public:
};

struct Datatype_float_t3149441939_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_float::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_float::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_float_t3149441939_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_float_t3149441939_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FLOAT_T3149441939_H
#ifndef DATATYPE_TIMETIMEZONE_T3702809551_H
#define DATATYPE_TIMETIMEZONE_T3702809551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_timeTimeZone
struct  Datatype_timeTimeZone_t3702809551  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TIMETIMEZONE_T3702809551_H
#ifndef DATATYPE_NORMALIZEDSTRINGV1COMPAT_T2394903178_H
#define DATATYPE_NORMALIZEDSTRINGV1COMPAT_T2394903178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_normalizedStringV1Compat
struct  Datatype_normalizedStringV1Compat_t2394903178  : public Datatype_string_t995037180
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NORMALIZEDSTRINGV1COMPAT_T2394903178_H
#ifndef DATATYPE_TIME_T305227304_H
#define DATATYPE_TIME_T305227304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_time
struct  Datatype_time_t305227304  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TIME_T305227304_H
#ifndef DATATYPE_DATE_T903634781_H
#define DATATYPE_DATE_T903634781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_date
struct  Datatype_date_t903634781  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATE_T903634781_H
#ifndef DATATYPE_YEAR_T527201590_H
#define DATATYPE_YEAR_T527201590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_year
struct  Datatype_year_t527201590  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_YEAR_T527201590_H
#ifndef DATATYPE_MONTHDAY_T1515801313_H
#define DATATYPE_MONTHDAY_T1515801313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_monthDay
struct  Datatype_monthDay_t1515801313  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_MONTHDAY_T1515801313_H
#ifndef DATATYPE_DAY_T3625353511_H
#define DATATYPE_DAY_T3625353511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_day
struct  Datatype_day_t3625353511  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DAY_T3625353511_H
#ifndef DATATYPE_YEARMONTH_T1775492394_H
#define DATATYPE_YEARMONTH_T1775492394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_yearMonth
struct  Datatype_yearMonth_t1775492394  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_YEARMONTH_T1775492394_H
#ifndef DATATYPE_MONTH_T2424718095_H
#define DATATYPE_MONTH_T2424718095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_month
struct  Datatype_month_t2424718095  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_MONTH_T2424718095_H
#ifndef DATATYPE_FIXED_T1126112663_H
#define DATATYPE_FIXED_T1126112663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_fixed
struct  Datatype_fixed_t1126112663  : public Datatype_decimal_t2973594954
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FIXED_T1126112663_H
#ifndef DATATYPE_INTEGER_T404053727_H
#define DATATYPE_INTEGER_T404053727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_integer
struct  Datatype_integer_t404053727  : public Datatype_decimal_t2973594954
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_INTEGER_T404053727_H
#ifndef DATATYPE_NORMALIZEDSTRING_T2314411649_H
#define DATATYPE_NORMALIZEDSTRING_T2314411649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_normalizedString
struct  Datatype_normalizedString_t2314411649  : public Datatype_string_t995037180
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NORMALIZEDSTRING_T2314411649_H
#ifndef DATATYPE_UNTYPEDATOMICTYPE_T3332440493_H
#define DATATYPE_UNTYPEDATOMICTYPE_T3332440493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_untypedAtomicType
struct  Datatype_untypedAtomicType_t3332440493  : public Datatype_anyAtomicType_t4212901794
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNTYPEDATOMICTYPE_T3332440493_H
#ifndef DATATYPE_DOUBLEXDR_T2833069974_H
#define DATATYPE_DOUBLEXDR_T2833069974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_doubleXdr
struct  Datatype_doubleXdr_t2833069974  : public Datatype_double_t1050796240
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DOUBLEXDR_T2833069974_H
#ifndef DATATYPE_FLOATXDR_T1510312673_H
#define DATATYPE_FLOATXDR_T1510312673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_floatXdr
struct  Datatype_floatXdr_t1510312673  : public Datatype_float_t3149441939
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FLOATXDR_T1510312673_H
#ifndef DATATYPE_DATETIMETIMEZONE_T3703324323_H
#define DATATYPE_DATETIMETIMEZONE_T3703324323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTimeTimeZone
struct  Datatype_dateTimeTimeZone_t3703324323  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIMETIMEZONE_T3703324323_H
#ifndef DATATYPE_DATETIME_T1101103220_H
#define DATATYPE_DATETIME_T1101103220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTime
struct  Datatype_dateTime_t1101103220  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIME_T1101103220_H
#ifndef DATATYPE_TIMENOTIMEZONE_T1747796622_H
#define DATATYPE_TIMENOTIMEZONE_T1747796622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_timeNoTimeZone
struct  Datatype_timeNoTimeZone_t1747796622  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TIMENOTIMEZONE_T1747796622_H
#ifndef DATATYPE_YEARMONTHDURATION_T1235863080_H
#define DATATYPE_YEARMONTHDURATION_T1235863080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_yearMonthDuration
struct  Datatype_yearMonthDuration_t1235863080  : public Datatype_duration_t1871787273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_YEARMONTHDURATION_T1235863080_H
#ifndef DATATYPE_DAYTIMEDURATION_T2638197894_H
#define DATATYPE_DAYTIMEDURATION_T2638197894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dayTimeDuration
struct  Datatype_dayTimeDuration_t2638197894  : public Datatype_duration_t1871787273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DAYTIMEDURATION_T2638197894_H
#ifndef DATATYPE_DATETIMENOTIMEZONE_T3887107098_H
#define DATATYPE_DATETIMENOTIMEZONE_T3887107098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTimeNoTimeZone
struct  Datatype_dateTimeNoTimeZone_t3887107098  : public Datatype_dateTimeBase_t2449194189
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIMENOTIMEZONE_T3887107098_H
#ifndef DATATYPE_NONNEGATIVEINTEGER_T1851861419_H
#define DATATYPE_NONNEGATIVEINTEGER_T1851861419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_nonNegativeInteger
struct  Datatype_nonNegativeInteger_t1851861419  : public Datatype_integer_t404053727
{
public:

public:
};

struct Datatype_nonNegativeInteger_t1851861419_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_nonNegativeInteger::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_96;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_96() { return static_cast<int32_t>(offsetof(Datatype_nonNegativeInteger_t1851861419_StaticFields, ___numeric10FacetsChecker_96)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_96() const { return ___numeric10FacetsChecker_96; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_96() { return &___numeric10FacetsChecker_96; }
	inline void set_numeric10FacetsChecker_96(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_96 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_96), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NONNEGATIVEINTEGER_T1851861419_H
#ifndef DATATYPE_TOKENV1COMPAT_T64797735_H
#define DATATYPE_TOKENV1COMPAT_T64797735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_tokenV1Compat
struct  Datatype_tokenV1Compat_t64797735  : public Datatype_normalizedStringV1Compat_t2394903178
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TOKENV1COMPAT_T64797735_H
#ifndef DATATYPE_NONPOSITIVEINTEGER_T863439515_H
#define DATATYPE_NONPOSITIVEINTEGER_T863439515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_nonPositiveInteger
struct  Datatype_nonPositiveInteger_t863439515  : public Datatype_integer_t404053727
{
public:

public:
};

struct Datatype_nonPositiveInteger_t863439515_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_nonPositiveInteger::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_96;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_96() { return static_cast<int32_t>(offsetof(Datatype_nonPositiveInteger_t863439515_StaticFields, ___numeric10FacetsChecker_96)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_96() const { return ___numeric10FacetsChecker_96; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_96() { return &___numeric10FacetsChecker_96; }
	inline void set_numeric10FacetsChecker_96(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_96 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_96), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NONPOSITIVEINTEGER_T863439515_H
#ifndef DATATYPE_LONG_T513905339_H
#define DATATYPE_LONG_T513905339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_long
struct  Datatype_long_t513905339  : public Datatype_integer_t404053727
{
public:

public:
};

struct Datatype_long_t513905339_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_long::atomicValueType
	Type_t * ___atomicValueType_96;
	// System.Type System.Xml.Schema.Datatype_long::listValueType
	Type_t * ___listValueType_97;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_long::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_98;

public:
	inline static int32_t get_offset_of_atomicValueType_96() { return static_cast<int32_t>(offsetof(Datatype_long_t513905339_StaticFields, ___atomicValueType_96)); }
	inline Type_t * get_atomicValueType_96() const { return ___atomicValueType_96; }
	inline Type_t ** get_address_of_atomicValueType_96() { return &___atomicValueType_96; }
	inline void set_atomicValueType_96(Type_t * value)
	{
		___atomicValueType_96 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_96), value);
	}

	inline static int32_t get_offset_of_listValueType_97() { return static_cast<int32_t>(offsetof(Datatype_long_t513905339_StaticFields, ___listValueType_97)); }
	inline Type_t * get_listValueType_97() const { return ___listValueType_97; }
	inline Type_t ** get_address_of_listValueType_97() { return &___listValueType_97; }
	inline void set_listValueType_97(Type_t * value)
	{
		___listValueType_97 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_97), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_98() { return static_cast<int32_t>(offsetof(Datatype_long_t513905339_StaticFields, ___numeric10FacetsChecker_98)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_98() const { return ___numeric10FacetsChecker_98; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_98() { return &___numeric10FacetsChecker_98; }
	inline void set_numeric10FacetsChecker_98(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_98 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_98), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_LONG_T513905339_H
#ifndef DATATYPE_TOKEN_T464702686_H
#define DATATYPE_TOKEN_T464702686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_token
struct  Datatype_token_t464702686  : public Datatype_normalizedString_t2314411649
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TOKEN_T464702686_H
#ifndef DATATYPE_INT_T2281243990_H
#define DATATYPE_INT_T2281243990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_int
struct  Datatype_int_t2281243990  : public Datatype_long_t513905339
{
public:

public:
};

struct Datatype_int_t2281243990_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_int::atomicValueType
	Type_t * ___atomicValueType_99;
	// System.Type System.Xml.Schema.Datatype_int::listValueType
	Type_t * ___listValueType_100;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_int::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_101;

public:
	inline static int32_t get_offset_of_atomicValueType_99() { return static_cast<int32_t>(offsetof(Datatype_int_t2281243990_StaticFields, ___atomicValueType_99)); }
	inline Type_t * get_atomicValueType_99() const { return ___atomicValueType_99; }
	inline Type_t ** get_address_of_atomicValueType_99() { return &___atomicValueType_99; }
	inline void set_atomicValueType_99(Type_t * value)
	{
		___atomicValueType_99 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_99), value);
	}

	inline static int32_t get_offset_of_listValueType_100() { return static_cast<int32_t>(offsetof(Datatype_int_t2281243990_StaticFields, ___listValueType_100)); }
	inline Type_t * get_listValueType_100() const { return ___listValueType_100; }
	inline Type_t ** get_address_of_listValueType_100() { return &___listValueType_100; }
	inline void set_listValueType_100(Type_t * value)
	{
		___listValueType_100 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_100), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_101() { return static_cast<int32_t>(offsetof(Datatype_int_t2281243990_StaticFields, ___numeric10FacetsChecker_101)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_101() const { return ___numeric10FacetsChecker_101; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_101() { return &___numeric10FacetsChecker_101; }
	inline void set_numeric10FacetsChecker_101(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_101 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_101), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_INT_T2281243990_H
#ifndef DATATYPE_UNSIGNEDLONG_T3462046402_H
#define DATATYPE_UNSIGNEDLONG_T3462046402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedLong
struct  Datatype_unsignedLong_t3462046402  : public Datatype_nonNegativeInteger_t1851861419
{
public:

public:
};

struct Datatype_unsignedLong_t3462046402_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedLong::atomicValueType
	Type_t * ___atomicValueType_97;
	// System.Type System.Xml.Schema.Datatype_unsignedLong::listValueType
	Type_t * ___listValueType_98;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedLong::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_99;

public:
	inline static int32_t get_offset_of_atomicValueType_97() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t3462046402_StaticFields, ___atomicValueType_97)); }
	inline Type_t * get_atomicValueType_97() const { return ___atomicValueType_97; }
	inline Type_t ** get_address_of_atomicValueType_97() { return &___atomicValueType_97; }
	inline void set_atomicValueType_97(Type_t * value)
	{
		___atomicValueType_97 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_97), value);
	}

	inline static int32_t get_offset_of_listValueType_98() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t3462046402_StaticFields, ___listValueType_98)); }
	inline Type_t * get_listValueType_98() const { return ___listValueType_98; }
	inline Type_t ** get_address_of_listValueType_98() { return &___listValueType_98; }
	inline void set_listValueType_98(Type_t * value)
	{
		___listValueType_98 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_98), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_99() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t3462046402_StaticFields, ___numeric10FacetsChecker_99)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_99() const { return ___numeric10FacetsChecker_99; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_99() { return &___numeric10FacetsChecker_99; }
	inline void set_numeric10FacetsChecker_99(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_99 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_99), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDLONG_T3462046402_H
#ifndef DATATYPE_NEGATIVEINTEGER_T1070964020_H
#define DATATYPE_NEGATIVEINTEGER_T1070964020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_negativeInteger
struct  Datatype_negativeInteger_t1070964020  : public Datatype_nonPositiveInteger_t863439515
{
public:

public:
};

struct Datatype_negativeInteger_t1070964020_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_negativeInteger::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_97;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_97() { return static_cast<int32_t>(offsetof(Datatype_negativeInteger_t1070964020_StaticFields, ___numeric10FacetsChecker_97)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_97() const { return ___numeric10FacetsChecker_97; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_97() { return &___numeric10FacetsChecker_97; }
	inline void set_numeric10FacetsChecker_97(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_97 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_97), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NEGATIVEINTEGER_T1070964020_H
#ifndef DATATYPE_POSITIVEINTEGER_T3806316496_H
#define DATATYPE_POSITIVEINTEGER_T3806316496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_positiveInteger
struct  Datatype_positiveInteger_t3806316496  : public Datatype_nonNegativeInteger_t1851861419
{
public:

public:
};

struct Datatype_positiveInteger_t3806316496_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_positiveInteger::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_97;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_97() { return static_cast<int32_t>(offsetof(Datatype_positiveInteger_t3806316496_StaticFields, ___numeric10FacetsChecker_97)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_97() const { return ___numeric10FacetsChecker_97; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_97() { return &___numeric10FacetsChecker_97; }
	inline void set_numeric10FacetsChecker_97(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_97 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_97), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_POSITIVEINTEGER_T3806316496_H
#ifndef DATATYPE_NMTOKEN_T2677332311_H
#define DATATYPE_NMTOKEN_T2677332311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_NMTOKEN
struct  Datatype_NMTOKEN_t2677332311  : public Datatype_token_t464702686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NMTOKEN_T2677332311_H
#ifndef DATATYPE_LANGUAGE_T2173537947_H
#define DATATYPE_LANGUAGE_T2173537947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_language
struct  Datatype_language_t2173537947  : public Datatype_token_t464702686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_LANGUAGE_T2173537947_H
#ifndef DATATYPE_NAME_T2060302642_H
#define DATATYPE_NAME_T2060302642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_Name
struct  Datatype_Name_t2060302642  : public Datatype_token_t464702686
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NAME_T2060302642_H
#ifndef DATATYPE_ENUMERATION_T3978335496_H
#define DATATYPE_ENUMERATION_T3978335496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_ENUMERATION
struct  Datatype_ENUMERATION_t3978335496  : public Datatype_NMTOKEN_t2677332311
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ENUMERATION_T3978335496_H
#ifndef DATATYPE_UNSIGNEDINT_T4210266973_H
#define DATATYPE_UNSIGNEDINT_T4210266973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedInt
struct  Datatype_unsignedInt_t4210266973  : public Datatype_unsignedLong_t3462046402
{
public:

public:
};

struct Datatype_unsignedInt_t4210266973_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedInt::atomicValueType
	Type_t * ___atomicValueType_100;
	// System.Type System.Xml.Schema.Datatype_unsignedInt::listValueType
	Type_t * ___listValueType_101;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedInt::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_102;

public:
	inline static int32_t get_offset_of_atomicValueType_100() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t4210266973_StaticFields, ___atomicValueType_100)); }
	inline Type_t * get_atomicValueType_100() const { return ___atomicValueType_100; }
	inline Type_t ** get_address_of_atomicValueType_100() { return &___atomicValueType_100; }
	inline void set_atomicValueType_100(Type_t * value)
	{
		___atomicValueType_100 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_100), value);
	}

	inline static int32_t get_offset_of_listValueType_101() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t4210266973_StaticFields, ___listValueType_101)); }
	inline Type_t * get_listValueType_101() const { return ___listValueType_101; }
	inline Type_t ** get_address_of_listValueType_101() { return &___listValueType_101; }
	inline void set_listValueType_101(Type_t * value)
	{
		___listValueType_101 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_101), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_102() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t4210266973_StaticFields, ___numeric10FacetsChecker_102)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_102() const { return ___numeric10FacetsChecker_102; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_102() { return &___numeric10FacetsChecker_102; }
	inline void set_numeric10FacetsChecker_102(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_102 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_102), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDINT_T4210266973_H
#ifndef DATATYPE_SHORT_T3426858539_H
#define DATATYPE_SHORT_T3426858539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_short
struct  Datatype_short_t3426858539  : public Datatype_int_t2281243990
{
public:

public:
};

struct Datatype_short_t3426858539_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_short::atomicValueType
	Type_t * ___atomicValueType_102;
	// System.Type System.Xml.Schema.Datatype_short::listValueType
	Type_t * ___listValueType_103;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_short::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_104;

public:
	inline static int32_t get_offset_of_atomicValueType_102() { return static_cast<int32_t>(offsetof(Datatype_short_t3426858539_StaticFields, ___atomicValueType_102)); }
	inline Type_t * get_atomicValueType_102() const { return ___atomicValueType_102; }
	inline Type_t ** get_address_of_atomicValueType_102() { return &___atomicValueType_102; }
	inline void set_atomicValueType_102(Type_t * value)
	{
		___atomicValueType_102 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_102), value);
	}

	inline static int32_t get_offset_of_listValueType_103() { return static_cast<int32_t>(offsetof(Datatype_short_t3426858539_StaticFields, ___listValueType_103)); }
	inline Type_t * get_listValueType_103() const { return ___listValueType_103; }
	inline Type_t ** get_address_of_listValueType_103() { return &___listValueType_103; }
	inline void set_listValueType_103(Type_t * value)
	{
		___listValueType_103 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_103), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_104() { return static_cast<int32_t>(offsetof(Datatype_short_t3426858539_StaticFields, ___numeric10FacetsChecker_104)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_104() const { return ___numeric10FacetsChecker_104; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_104() { return &___numeric10FacetsChecker_104; }
	inline void set_numeric10FacetsChecker_104(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_104 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_104), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_SHORT_T3426858539_H
#ifndef DATATYPE_NCNAME_T1627675837_H
#define DATATYPE_NCNAME_T1627675837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_NCName
struct  Datatype_NCName_t1627675837  : public Datatype_Name_t2060302642
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NCNAME_T1627675837_H
#ifndef DATATYPE_BYTE_T702122475_H
#define DATATYPE_BYTE_T702122475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_byte
struct  Datatype_byte_t702122475  : public Datatype_short_t3426858539
{
public:

public:
};

struct Datatype_byte_t702122475_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_byte::atomicValueType
	Type_t * ___atomicValueType_105;
	// System.Type System.Xml.Schema.Datatype_byte::listValueType
	Type_t * ___listValueType_106;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_byte::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_107;

public:
	inline static int32_t get_offset_of_atomicValueType_105() { return static_cast<int32_t>(offsetof(Datatype_byte_t702122475_StaticFields, ___atomicValueType_105)); }
	inline Type_t * get_atomicValueType_105() const { return ___atomicValueType_105; }
	inline Type_t ** get_address_of_atomicValueType_105() { return &___atomicValueType_105; }
	inline void set_atomicValueType_105(Type_t * value)
	{
		___atomicValueType_105 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_105), value);
	}

	inline static int32_t get_offset_of_listValueType_106() { return static_cast<int32_t>(offsetof(Datatype_byte_t702122475_StaticFields, ___listValueType_106)); }
	inline Type_t * get_listValueType_106() const { return ___listValueType_106; }
	inline Type_t ** get_address_of_listValueType_106() { return &___listValueType_106; }
	inline void set_listValueType_106(Type_t * value)
	{
		___listValueType_106 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_106), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_107() { return static_cast<int32_t>(offsetof(Datatype_byte_t702122475_StaticFields, ___numeric10FacetsChecker_107)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_107() const { return ___numeric10FacetsChecker_107; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_107() { return &___numeric10FacetsChecker_107; }
	inline void set_numeric10FacetsChecker_107(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_107 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_107), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_BYTE_T702122475_H
#ifndef DATATYPE_IDREF_T3125569863_H
#define DATATYPE_IDREF_T3125569863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_IDREF
struct  Datatype_IDREF_t3125569863  : public Datatype_NCName_t1627675837
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_IDREF_T3125569863_H
#ifndef DATATYPE_ID_T3661482256_H
#define DATATYPE_ID_T3661482256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_ID
struct  Datatype_ID_t3661482256  : public Datatype_NCName_t1627675837
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ID_T3661482256_H
#ifndef DATATYPE_ENTITY_T863271950_H
#define DATATYPE_ENTITY_T863271950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_ENTITY
struct  Datatype_ENTITY_t863271950  : public Datatype_NCName_t1627675837
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ENTITY_T863271950_H
#ifndef DATATYPE_UNSIGNEDSHORT_T2975932210_H
#define DATATYPE_UNSIGNEDSHORT_T2975932210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedShort
struct  Datatype_unsignedShort_t2975932210  : public Datatype_unsignedInt_t4210266973
{
public:

public:
};

struct Datatype_unsignedShort_t2975932210_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedShort::atomicValueType
	Type_t * ___atomicValueType_103;
	// System.Type System.Xml.Schema.Datatype_unsignedShort::listValueType
	Type_t * ___listValueType_104;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedShort::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_105;

public:
	inline static int32_t get_offset_of_atomicValueType_103() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_t2975932210_StaticFields, ___atomicValueType_103)); }
	inline Type_t * get_atomicValueType_103() const { return ___atomicValueType_103; }
	inline Type_t ** get_address_of_atomicValueType_103() { return &___atomicValueType_103; }
	inline void set_atomicValueType_103(Type_t * value)
	{
		___atomicValueType_103 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_103), value);
	}

	inline static int32_t get_offset_of_listValueType_104() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_t2975932210_StaticFields, ___listValueType_104)); }
	inline Type_t * get_listValueType_104() const { return ___listValueType_104; }
	inline Type_t ** get_address_of_listValueType_104() { return &___listValueType_104; }
	inline void set_listValueType_104(Type_t * value)
	{
		___listValueType_104 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_104), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_105() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_t2975932210_StaticFields, ___numeric10FacetsChecker_105)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_105() const { return ___numeric10FacetsChecker_105; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_105() { return &___numeric10FacetsChecker_105; }
	inline void set_numeric10FacetsChecker_105(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_105 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_105), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDSHORT_T2975932210_H
#ifndef DATATYPE_UNSIGNEDBYTE_T322972434_H
#define DATATYPE_UNSIGNEDBYTE_T322972434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedByte
struct  Datatype_unsignedByte_t322972434  : public Datatype_unsignedShort_t2975932210
{
public:

public:
};

struct Datatype_unsignedByte_t322972434_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedByte::atomicValueType
	Type_t * ___atomicValueType_106;
	// System.Type System.Xml.Schema.Datatype_unsignedByte::listValueType
	Type_t * ___listValueType_107;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedByte::numeric10FacetsChecker
	FacetsChecker_t1235574227 * ___numeric10FacetsChecker_108;

public:
	inline static int32_t get_offset_of_atomicValueType_106() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t322972434_StaticFields, ___atomicValueType_106)); }
	inline Type_t * get_atomicValueType_106() const { return ___atomicValueType_106; }
	inline Type_t ** get_address_of_atomicValueType_106() { return &___atomicValueType_106; }
	inline void set_atomicValueType_106(Type_t * value)
	{
		___atomicValueType_106 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_106), value);
	}

	inline static int32_t get_offset_of_listValueType_107() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t322972434_StaticFields, ___listValueType_107)); }
	inline Type_t * get_listValueType_107() const { return ___listValueType_107; }
	inline Type_t ** get_address_of_listValueType_107() { return &___listValueType_107; }
	inline void set_listValueType_107(Type_t * value)
	{
		___listValueType_107 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_107), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_108() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t322972434_StaticFields, ___numeric10FacetsChecker_108)); }
	inline FacetsChecker_t1235574227 * get_numeric10FacetsChecker_108() const { return ___numeric10FacetsChecker_108; }
	inline FacetsChecker_t1235574227 ** get_address_of_numeric10FacetsChecker_108() { return &___numeric10FacetsChecker_108; }
	inline void set_numeric10FacetsChecker_108(FacetsChecker_t1235574227 * value)
	{
		___numeric10FacetsChecker_108 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_108), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDBYTE_T322972434_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (AllElementsContentValidator_t2095068475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[4] = 
{
	AllElementsContentValidator_t2095068475::get_offset_of_elements_7(),
	AllElementsContentValidator_t2095068475::get_offset_of_particles_8(),
	AllElementsContentValidator_t2095068475::get_offset_of_isRequired_9(),
	AllElementsContentValidator_t2095068475::get_offset_of_countRequired_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (XmlSchemaDatatypeVariety_t2237606318)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2001[4] = 
{
	XmlSchemaDatatypeVariety_t2237606318::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (XsdSimpleValue_t478440528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[2] = 
{
	XsdSimpleValue_t478440528::get_offset_of_xmlType_0(),
	XsdSimpleValue_t478440528::get_offset_of_typedValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (RestrictionFlags_t2588355947)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[13] = 
{
	RestrictionFlags_t2588355947::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (XmlSchemaWhiteSpace_t3746245107)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[4] = 
{
	XmlSchemaWhiteSpace_t3746245107::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (RestrictionFacets_t4012658256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[14] = 
{
	RestrictionFacets_t4012658256::get_offset_of_Length_0(),
	RestrictionFacets_t4012658256::get_offset_of_MinLength_1(),
	RestrictionFacets_t4012658256::get_offset_of_MaxLength_2(),
	RestrictionFacets_t4012658256::get_offset_of_Patterns_3(),
	RestrictionFacets_t4012658256::get_offset_of_Enumeration_4(),
	RestrictionFacets_t4012658256::get_offset_of_WhiteSpace_5(),
	RestrictionFacets_t4012658256::get_offset_of_MaxInclusive_6(),
	RestrictionFacets_t4012658256::get_offset_of_MaxExclusive_7(),
	RestrictionFacets_t4012658256::get_offset_of_MinInclusive_8(),
	RestrictionFacets_t4012658256::get_offset_of_MinExclusive_9(),
	RestrictionFacets_t4012658256::get_offset_of_TotalDigits_10(),
	RestrictionFacets_t4012658256::get_offset_of_FractionDigits_11(),
	RestrictionFacets_t4012658256::get_offset_of_Flags_12(),
	RestrictionFacets_t4012658256::get_offset_of_FixedFlags_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (DatatypeImplementation_t1152094268), -1, sizeof(DatatypeImplementation_t1152094268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2006[91] = 
{
	DatatypeImplementation_t1152094268::get_offset_of_variety_0(),
	DatatypeImplementation_t1152094268::get_offset_of_restriction_1(),
	DatatypeImplementation_t1152094268::get_offset_of_baseType_2(),
	DatatypeImplementation_t1152094268::get_offset_of_valueConverter_3(),
	DatatypeImplementation_t1152094268::get_offset_of_parentSchemaType_4(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_builtinTypes_5(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_enumToTypeCode_6(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_anySimpleType_7(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_anyAtomicType_8(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_untypedAtomicType_9(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_yearMonthDurationType_10(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_dayTimeDurationType_11(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_normalizedStringTypeV1Compat_12(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_tokenTypeV1Compat_13(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_QnAnySimpleType_14(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_QnAnyType_15(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_stringFacetsChecker_16(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_miscFacetsChecker_17(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_numeric2FacetsChecker_18(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_binaryFacetsChecker_19(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_dateTimeFacetsChecker_20(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_durationFacetsChecker_21(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_listFacetsChecker_22(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_qnameFacetsChecker_23(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_unionFacetsChecker_24(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_anySimpleType_25(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_anyURI_26(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_base64Binary_27(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_boolean_28(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_byte_29(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_char_30(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_date_31(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dateTime_32(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dateTimeNoTz_33(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dateTimeTz_34(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_day_35(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_decimal_36(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_double_37(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_doubleXdr_38(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_duration_39(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ENTITY_40(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ENTITIES_41(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ENUMERATION_42(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_fixed_43(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_float_44(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_floatXdr_45(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_hexBinary_46(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_ID_47(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_IDREF_48(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_IDREFS_49(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_int_50(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_integer_51(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_language_52(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_long_53(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_month_54(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_monthDay_55(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_Name_56(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NCName_57(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_negativeInteger_58(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NMTOKEN_59(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NMTOKENS_60(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_nonNegativeInteger_61(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_nonPositiveInteger_62(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_normalizedString_63(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_NOTATION_64(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_positiveInteger_65(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_QName_66(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_QNameXdr_67(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_short_68(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_string_69(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_time_70(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_timeNoTz_71(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_timeTz_72(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_token_73(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedByte_74(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedInt_75(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedLong_76(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_unsignedShort_77(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_uuid_78(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_year_79(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_yearMonth_80(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_normalizedStringV1Compat_81(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_tokenV1Compat_82(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_anyAtomicType_83(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_dayTimeDuration_84(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_untypedAtomicType_85(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_yearMonthDuration_86(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_tokenizedTypes_87(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_tokenizedTypesXsd_88(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_XdrTypes_89(),
	DatatypeImplementation_t1152094268_StaticFields::get_offset_of_c_XsdTypes_90(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (SchemaDatatypeMap_t2661667341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[3] = 
{
	SchemaDatatypeMap_t2661667341::get_offset_of_name_0(),
	SchemaDatatypeMap_t2661667341::get_offset_of_type_1(),
	SchemaDatatypeMap_t2661667341::get_offset_of_parentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (Datatype_List_t1892289229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	Datatype_List_t1892289229::get_offset_of_itemType_93(),
	Datatype_List_t1892289229::get_offset_of_minListSize_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (Datatype_union_t2515141346), -1, sizeof(Datatype_union_t2515141346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2009[3] = 
{
	Datatype_union_t2515141346_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_union_t2515141346_StaticFields::get_offset_of_listValueType_94(),
	Datatype_union_t2515141346::get_offset_of_types_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (Datatype_anySimpleType_t4012795865), -1, sizeof(Datatype_anySimpleType_t4012795865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2010[2] = 
{
	Datatype_anySimpleType_t4012795865_StaticFields::get_offset_of_atomicValueType_91(),
	Datatype_anySimpleType_t4012795865_StaticFields::get_offset_of_listValueType_92(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (Datatype_anyAtomicType_t4212901794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (Datatype_untypedAtomicType_t3332440493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (Datatype_string_t995037180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Datatype_boolean_t293982753), -1, sizeof(Datatype_boolean_t293982753_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2014[2] = 
{
	Datatype_boolean_t293982753_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_boolean_t293982753_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (Datatype_float_t3149441939), -1, sizeof(Datatype_float_t3149441939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	Datatype_float_t3149441939_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_float_t3149441939_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (Datatype_double_t1050796240), -1, sizeof(Datatype_double_t1050796240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2016[2] = 
{
	Datatype_double_t1050796240_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_double_t1050796240_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (Datatype_decimal_t2973594954), -1, sizeof(Datatype_decimal_t2973594954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[3] = 
{
	Datatype_decimal_t2973594954_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_decimal_t2973594954_StaticFields::get_offset_of_listValueType_94(),
	Datatype_decimal_t2973594954_StaticFields::get_offset_of_numeric10FacetsChecker_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Datatype_duration_t1871787273), -1, sizeof(Datatype_duration_t1871787273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[2] = 
{
	Datatype_duration_t1871787273_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_duration_t1871787273_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (Datatype_yearMonthDuration_t1235863080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Datatype_dayTimeDuration_t2638197894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (Datatype_dateTimeBase_t2449194189), -1, sizeof(Datatype_dateTimeBase_t2449194189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[3] = 
{
	Datatype_dateTimeBase_t2449194189_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_dateTimeBase_t2449194189_StaticFields::get_offset_of_listValueType_94(),
	Datatype_dateTimeBase_t2449194189::get_offset_of_dateTimeFlags_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (Datatype_dateTimeNoTimeZone_t3887107098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (Datatype_dateTimeTimeZone_t3703324323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (Datatype_dateTime_t1101103220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (Datatype_timeNoTimeZone_t1747796622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (Datatype_timeTimeZone_t3702809551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (Datatype_time_t305227304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (Datatype_date_t903634781), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (Datatype_yearMonth_t1775492394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (Datatype_year_t527201590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (Datatype_monthDay_t1515801313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (Datatype_day_t3625353511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (Datatype_month_t2424718095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (Datatype_hexBinary_t2599154205), -1, sizeof(Datatype_hexBinary_t2599154205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2034[2] = 
{
	Datatype_hexBinary_t2599154205_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_hexBinary_t2599154205_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (Datatype_base64Binary_t3902009307), -1, sizeof(Datatype_base64Binary_t3902009307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2035[2] = 
{
	Datatype_base64Binary_t3902009307_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_base64Binary_t3902009307_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (Datatype_anyURI_t2417434093), -1, sizeof(Datatype_anyURI_t2417434093_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[2] = 
{
	Datatype_anyURI_t2417434093_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_anyURI_t2417434093_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (Datatype_QName_t2180543649), -1, sizeof(Datatype_QName_t2180543649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2037[2] = 
{
	Datatype_QName_t2180543649_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_QName_t2180543649_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (Datatype_normalizedString_t2314411649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (Datatype_normalizedStringV1Compat_t2394903178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (Datatype_token_t464702686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (Datatype_tokenV1Compat_t64797735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (Datatype_language_t2173537947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (Datatype_NMTOKEN_t2677332311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (Datatype_Name_t2060302642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (Datatype_NCName_t1627675837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (Datatype_ID_t3661482256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (Datatype_IDREF_t3125569863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (Datatype_ENTITY_t863271950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (Datatype_NOTATION_t3412197483), -1, sizeof(Datatype_NOTATION_t3412197483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2049[2] = 
{
	Datatype_NOTATION_t3412197483_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_NOTATION_t3412197483_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (Datatype_integer_t404053727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (Datatype_nonPositiveInteger_t863439515), -1, sizeof(Datatype_nonPositiveInteger_t863439515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2051[1] = 
{
	Datatype_nonPositiveInteger_t863439515_StaticFields::get_offset_of_numeric10FacetsChecker_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (Datatype_negativeInteger_t1070964020), -1, sizeof(Datatype_negativeInteger_t1070964020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2052[1] = 
{
	Datatype_negativeInteger_t1070964020_StaticFields::get_offset_of_numeric10FacetsChecker_97(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (Datatype_long_t513905339), -1, sizeof(Datatype_long_t513905339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2053[3] = 
{
	Datatype_long_t513905339_StaticFields::get_offset_of_atomicValueType_96(),
	Datatype_long_t513905339_StaticFields::get_offset_of_listValueType_97(),
	Datatype_long_t513905339_StaticFields::get_offset_of_numeric10FacetsChecker_98(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Datatype_int_t2281243990), -1, sizeof(Datatype_int_t2281243990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2054[3] = 
{
	Datatype_int_t2281243990_StaticFields::get_offset_of_atomicValueType_99(),
	Datatype_int_t2281243990_StaticFields::get_offset_of_listValueType_100(),
	Datatype_int_t2281243990_StaticFields::get_offset_of_numeric10FacetsChecker_101(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (Datatype_short_t3426858539), -1, sizeof(Datatype_short_t3426858539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2055[3] = 
{
	Datatype_short_t3426858539_StaticFields::get_offset_of_atomicValueType_102(),
	Datatype_short_t3426858539_StaticFields::get_offset_of_listValueType_103(),
	Datatype_short_t3426858539_StaticFields::get_offset_of_numeric10FacetsChecker_104(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (Datatype_byte_t702122475), -1, sizeof(Datatype_byte_t702122475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2056[3] = 
{
	Datatype_byte_t702122475_StaticFields::get_offset_of_atomicValueType_105(),
	Datatype_byte_t702122475_StaticFields::get_offset_of_listValueType_106(),
	Datatype_byte_t702122475_StaticFields::get_offset_of_numeric10FacetsChecker_107(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (Datatype_nonNegativeInteger_t1851861419), -1, sizeof(Datatype_nonNegativeInteger_t1851861419_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[1] = 
{
	Datatype_nonNegativeInteger_t1851861419_StaticFields::get_offset_of_numeric10FacetsChecker_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (Datatype_unsignedLong_t3462046402), -1, sizeof(Datatype_unsignedLong_t3462046402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2058[3] = 
{
	Datatype_unsignedLong_t3462046402_StaticFields::get_offset_of_atomicValueType_97(),
	Datatype_unsignedLong_t3462046402_StaticFields::get_offset_of_listValueType_98(),
	Datatype_unsignedLong_t3462046402_StaticFields::get_offset_of_numeric10FacetsChecker_99(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (Datatype_unsignedInt_t4210266973), -1, sizeof(Datatype_unsignedInt_t4210266973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2059[3] = 
{
	Datatype_unsignedInt_t4210266973_StaticFields::get_offset_of_atomicValueType_100(),
	Datatype_unsignedInt_t4210266973_StaticFields::get_offset_of_listValueType_101(),
	Datatype_unsignedInt_t4210266973_StaticFields::get_offset_of_numeric10FacetsChecker_102(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (Datatype_unsignedShort_t2975932210), -1, sizeof(Datatype_unsignedShort_t2975932210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2060[3] = 
{
	Datatype_unsignedShort_t2975932210_StaticFields::get_offset_of_atomicValueType_103(),
	Datatype_unsignedShort_t2975932210_StaticFields::get_offset_of_listValueType_104(),
	Datatype_unsignedShort_t2975932210_StaticFields::get_offset_of_numeric10FacetsChecker_105(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (Datatype_unsignedByte_t322972434), -1, sizeof(Datatype_unsignedByte_t322972434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2061[3] = 
{
	Datatype_unsignedByte_t322972434_StaticFields::get_offset_of_atomicValueType_106(),
	Datatype_unsignedByte_t322972434_StaticFields::get_offset_of_listValueType_107(),
	Datatype_unsignedByte_t322972434_StaticFields::get_offset_of_numeric10FacetsChecker_108(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (Datatype_positiveInteger_t3806316496), -1, sizeof(Datatype_positiveInteger_t3806316496_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2062[1] = 
{
	Datatype_positiveInteger_t3806316496_StaticFields::get_offset_of_numeric10FacetsChecker_97(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (Datatype_doubleXdr_t2833069974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (Datatype_floatXdr_t1510312673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (Datatype_QNameXdr_t2452527483), -1, sizeof(Datatype_QNameXdr_t2452527483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2065[2] = 
{
	Datatype_QNameXdr_t2452527483_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_QNameXdr_t2452527483_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (Datatype_ENUMERATION_t3978335496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (Datatype_char_t534108237), -1, sizeof(Datatype_char_t534108237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2067[2] = 
{
	Datatype_char_t534108237_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_char_t534108237_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (Datatype_fixed_t1126112663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (Datatype_uuid_t2771989020), -1, sizeof(Datatype_uuid_t2771989020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2069[2] = 
{
	Datatype_uuid_t2771989020_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_uuid_t2771989020_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (DtdValidator_t1639720164), -1, sizeof(DtdValidator_t1639720164_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2070[7] = 
{
	DtdValidator_t1639720164_StaticFields::get_offset_of_namespaceManager_15(),
	DtdValidator_t1639720164::get_offset_of_validationStack_16(),
	DtdValidator_t1639720164::get_offset_of_attPresence_17(),
	DtdValidator_t1639720164::get_offset_of_name_18(),
	DtdValidator_t1639720164::get_offset_of_IDs_19(),
	DtdValidator_t1639720164::get_offset_of_idRefListHead_20(),
	DtdValidator_t1639720164::get_offset_of_processIdentityConstraints_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (NamespaceManager_t1407344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (FacetsChecker_t1235574227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (FacetsCompiler_t3565032206)+ sizeof (RuntimeObject), -1, sizeof(FacetsCompiler_t3565032206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2073[12] = 
{
	FacetsCompiler_t3565032206::get_offset_of_datatype_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_derivedRestriction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_baseFlags_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_baseFixedFlags_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_validRestrictionFlags_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_nonNegativeInt_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_builtInType_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_builtInEnum_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_firstPattern_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_regStr_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206::get_offset_of_pattern_facet_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t3565032206_StaticFields::get_offset_of_c_map_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (Map_t2552390023)+ sizeof (RuntimeObject), sizeof(Map_t2552390023_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2074[2] = 
{
	Map_t2552390023::get_offset_of_match_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Map_t2552390023::get_offset_of_replacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (Numeric10FacetsChecker_t3431899547), -1, sizeof(Numeric10FacetsChecker_t3431899547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2075[3] = 
{
	Numeric10FacetsChecker_t3431899547_StaticFields::get_offset_of_signs_0(),
	Numeric10FacetsChecker_t3431899547::get_offset_of_maxValue_1(),
	Numeric10FacetsChecker_t3431899547::get_offset_of_minValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (Numeric2FacetsChecker_t2224492892), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (DurationFacetsChecker_t4254067383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (DateTimeFacetsChecker_t2220827556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (StringFacetsChecker_t2109036348), -1, sizeof(StringFacetsChecker_t2109036348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	StringFacetsChecker_t2109036348_StaticFields::get_offset_of_languagePattern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (QNameFacetsChecker_t3974186457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (MiscFacetsChecker_t3607863675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (BinaryFacetsChecker_t4115715422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (ListFacetsChecker_t961741019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (UnionFacetsChecker_t3025714558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (NamespaceList_t848177191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[3] = 
{
	NamespaceList_t848177191::get_offset_of_type_0(),
	NamespaceList_t848177191::get_offset_of_set_1(),
	NamespaceList_t848177191::get_offset_of_targetNamespace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (ListType_t2879180877)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2087[4] = 
{
	ListType_t2879180877::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (NamespaceListV1Compat_t2783980816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (Parser_t1940171737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[20] = 
{
	Parser_t1940171737::get_offset_of_schemaType_0(),
	Parser_t1940171737::get_offset_of_nameTable_1(),
	Parser_t1940171737::get_offset_of_schemaNames_2(),
	Parser_t1940171737::get_offset_of_eventHandler_3(),
	Parser_t1940171737::get_offset_of_namespaceManager_4(),
	Parser_t1940171737::get_offset_of_reader_5(),
	Parser_t1940171737::get_offset_of_positionInfo_6(),
	Parser_t1940171737::get_offset_of_isProcessNamespaces_7(),
	Parser_t1940171737::get_offset_of_schemaXmlDepth_8(),
	Parser_t1940171737::get_offset_of_markupDepth_9(),
	Parser_t1940171737::get_offset_of_builder_10(),
	Parser_t1940171737::get_offset_of_schema_11(),
	Parser_t1940171737::get_offset_of_xdrSchema_12(),
	Parser_t1940171737::get_offset_of_xmlResolver_13(),
	Parser_t1940171737::get_offset_of_dummyDocument_14(),
	Parser_t1940171737::get_offset_of_processMarkup_15(),
	Parser_t1940171737::get_offset_of_parentNode_16(),
	Parser_t1940171737::get_offset_of_annotationNSManager_17(),
	Parser_t1940171737::get_offset_of_xmlns_18(),
	Parser_t1940171737::get_offset_of_xmlCharType_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (Compositor_t2211431003)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2090[5] = 
{
	Compositor_t2211431003::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (Preprocessor_t4137165425), -1, sizeof(Preprocessor_t4137165425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2091[1] = 
{
	Preprocessor_t4137165425_StaticFields::get_offset_of_builtInSchemaForXmlNS_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (SchemaAttDef_t1510907267), -1, sizeof(SchemaAttDef_t1510907267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2092[8] = 
{
	SchemaAttDef_t1510907267::get_offset_of_defExpanded_11(),
	SchemaAttDef_t1510907267::get_offset_of_lineNum_12(),
	SchemaAttDef_t1510907267::get_offset_of_linePos_13(),
	SchemaAttDef_t1510907267::get_offset_of_valueLineNum_14(),
	SchemaAttDef_t1510907267::get_offset_of_valueLinePos_15(),
	SchemaAttDef_t1510907267::get_offset_of_reserved_16(),
	SchemaAttDef_t1510907267::get_offset_of_schemaAttribute_17(),
	SchemaAttDef_t1510907267_StaticFields::get_offset_of_Empty_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (Reserve_t2330987269)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2093[4] = 
{
	Reserve_t2330987269::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (SchemaBuilder_t908297946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (SchemaCollectionCompiler_t1443051254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[4] = 
{
	SchemaCollectionCompiler_t1443051254::get_offset_of_compileContentModel_6(),
	SchemaCollectionCompiler_t1443051254::get_offset_of_examplars_7(),
	SchemaCollectionCompiler_t1443051254::get_offset_of_complexTypeStack_8(),
	SchemaCollectionCompiler_t1443051254::get_offset_of_schema_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (SchemaCollectionPreprocessor_t2028437652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[11] = 
{
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_schema_6(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_targetNamespace_7(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_buildinIncluded_8(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_elementFormDefault_9(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_attributeFormDefault_10(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_blockDefault_11(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_finalDefault_12(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_schemaLocations_13(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_referenceNamespaces_14(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_Xmlns_15(),
	SchemaCollectionPreprocessor_t2028437652::get_offset_of_xmlResolver_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (Compositor_t3622502717)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[4] = 
{
	Compositor_t3622502717::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (SchemaDeclBase_t797759480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[11] = 
{
	SchemaDeclBase_t797759480::get_offset_of_name_0(),
	SchemaDeclBase_t797759480::get_offset_of_prefix_1(),
	SchemaDeclBase_t797759480::get_offset_of_isDeclaredInExternal_2(),
	SchemaDeclBase_t797759480::get_offset_of_presence_3(),
	SchemaDeclBase_t797759480::get_offset_of_schemaType_4(),
	SchemaDeclBase_t797759480::get_offset_of_datatype_5(),
	SchemaDeclBase_t797759480::get_offset_of_defaultValueRaw_6(),
	SchemaDeclBase_t797759480::get_offset_of_defaultValueTyped_7(),
	SchemaDeclBase_t797759480::get_offset_of_maxLength_8(),
	SchemaDeclBase_t797759480::get_offset_of_minLength_9(),
	SchemaDeclBase_t797759480::get_offset_of_values_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Use_t2612116671)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[6] = 
{
	Use_t2612116671::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
