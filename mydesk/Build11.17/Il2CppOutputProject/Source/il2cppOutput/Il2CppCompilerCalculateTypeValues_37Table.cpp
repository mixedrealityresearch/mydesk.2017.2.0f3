﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.List`1<HoloToolkit.Unity.PlaneFinding/MeshData>
struct List_1_t880011750;
// HoloToolkit.Unity.SurfaceMeshesToPlanes
struct SurfaceMeshesToPlanes_t2358468568;
// HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_t1668648882;
// System.Collections.Generic.List`1<UnityEngine.MeshFilter>
struct List_1_t2396058581;
// System.Threading.Tasks.Task`1<HoloToolkit.Unity.BoundedPlane[]>
struct Task_1_t4230515705;
// HoloToolkit.Unity.BoundedPlane[]
struct BoundedPlaneU5BU5D_t815519402;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.EventArgs
struct EventArgs_t3289624707;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMappingSource/SurfaceObject>
struct List_1_t557165259;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// HoloToolkit.Unity.SurfaceMeshesToPlanes/EventHandler
struct EventHandler_t1067160880;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_T1668648882_H
#define U3CU3EC__DISPLAYCLASS24_0_T1668648882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_t1668648882  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.PlaneFinding/MeshData> HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0::meshData
	List_1_t880011750 * ___meshData_0;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0::<>4__this
	SurfaceMeshesToPlanes_t2358468568 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_meshData_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t1668648882, ___meshData_0)); }
	inline List_1_t880011750 * get_meshData_0() const { return ___meshData_0; }
	inline List_1_t880011750 ** get_address_of_meshData_0() { return &___meshData_0; }
	inline void set_meshData_0(List_1_t880011750 * value)
	{
		___meshData_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshData_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t1668648882, ___U3CU3E4__this_1)); }
	inline SurfaceMeshesToPlanes_t2358468568 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SurfaceMeshesToPlanes_t2358468568 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SurfaceMeshesToPlanes_t2358468568 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_T1668648882_H
#ifndef U3CMAKEPLANESROUTINEU3ED__24_T2308419383_H
#define U3CMAKEPLANESROUTINEU3ED__24_T2308419383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24
struct  U3CMakePlanesRoutineU3Ed__24_t2308419383  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>4__this
	SurfaceMeshesToPlanes_t2358468568 * ___U3CU3E4__this_2;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes/<>c__DisplayClass24_0 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<>8__1
	U3CU3Ec__DisplayClass24_0_t1668648882 * ___U3CU3E8__1_3;
	// System.Collections.Generic.List`1<UnityEngine.MeshFilter> HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<filters>5__2
	List_1_t2396058581 * ___U3CfiltersU3E5__2_4;
	// System.Int32 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<index>5__3
	int32_t ___U3CindexU3E5__3_5;
	// System.Threading.Tasks.Task`1<HoloToolkit.Unity.BoundedPlane[]> HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<planeTask>5__4
	Task_1_t4230515705 * ___U3CplaneTaskU3E5__4_6;
	// HoloToolkit.Unity.BoundedPlane[] HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<planes>5__5
	BoundedPlaneU5BU5D_t815519402* ___U3CplanesU3E5__5_7;
	// System.Int32 HoloToolkit.Unity.SurfaceMeshesToPlanes/<MakePlanesRoutine>d__24::<index>5__6
	int32_t ___U3CindexU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E4__this_2)); }
	inline SurfaceMeshesToPlanes_t2358468568 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SurfaceMeshesToPlanes_t2358468568 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SurfaceMeshesToPlanes_t2358468568 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass24_0_t1668648882 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass24_0_t1668648882 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass24_0_t1668648882 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_U3CfiltersU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CfiltersU3E5__2_4)); }
	inline List_1_t2396058581 * get_U3CfiltersU3E5__2_4() const { return ___U3CfiltersU3E5__2_4; }
	inline List_1_t2396058581 ** get_address_of_U3CfiltersU3E5__2_4() { return &___U3CfiltersU3E5__2_4; }
	inline void set_U3CfiltersU3E5__2_4(List_1_t2396058581 * value)
	{
		___U3CfiltersU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfiltersU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CindexU3E5__3_5)); }
	inline int32_t get_U3CindexU3E5__3_5() const { return ___U3CindexU3E5__3_5; }
	inline int32_t* get_address_of_U3CindexU3E5__3_5() { return &___U3CindexU3E5__3_5; }
	inline void set_U3CindexU3E5__3_5(int32_t value)
	{
		___U3CindexU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CplaneTaskU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CplaneTaskU3E5__4_6)); }
	inline Task_1_t4230515705 * get_U3CplaneTaskU3E5__4_6() const { return ___U3CplaneTaskU3E5__4_6; }
	inline Task_1_t4230515705 ** get_address_of_U3CplaneTaskU3E5__4_6() { return &___U3CplaneTaskU3E5__4_6; }
	inline void set_U3CplaneTaskU3E5__4_6(Task_1_t4230515705 * value)
	{
		___U3CplaneTaskU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplaneTaskU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CplanesU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CplanesU3E5__5_7)); }
	inline BoundedPlaneU5BU5D_t815519402* get_U3CplanesU3E5__5_7() const { return ___U3CplanesU3E5__5_7; }
	inline BoundedPlaneU5BU5D_t815519402** get_address_of_U3CplanesU3E5__5_7() { return &___U3CplanesU3E5__5_7; }
	inline void set_U3CplanesU3E5__5_7(BoundedPlaneU5BU5D_t815519402* value)
	{
		___U3CplanesU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplanesU3E5__5_7), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CMakePlanesRoutineU3Ed__24_t2308419383, ___U3CindexU3E5__6_8)); }
	inline int32_t get_U3CindexU3E5__6_8() const { return ___U3CindexU3E5__6_8; }
	inline int32_t* get_address_of_U3CindexU3E5__6_8() { return &___U3CindexU3E5__6_8; }
	inline void set_U3CindexU3E5__6_8(int32_t value)
	{
		___U3CindexU3E5__6_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAKEPLANESROUTINEU3ED__24_T2308419383_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1841601450__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef SURFACEOBJECT_T1188044127_H
#define SURFACEOBJECT_T1188044127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMappingSource/SurfaceObject
struct  SurfaceObject_t1188044127 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::ID
	int32_t ___ID_0;
	// System.Int32 HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::UpdateID
	int32_t ___UpdateID_1;
	// UnityEngine.GameObject HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::Object
	GameObject_t1756533147 * ___Object_2;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::Renderer
	MeshRenderer_t1268241104 * ___Renderer_3;
	// UnityEngine.MeshFilter HoloToolkit.Unity.SpatialMappingSource/SurfaceObject::Filter
	MeshFilter_t3026937449 * ___Filter_4;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___ID_0)); }
	inline int32_t get_ID_0() const { return ___ID_0; }
	inline int32_t* get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(int32_t value)
	{
		___ID_0 = value;
	}

	inline static int32_t get_offset_of_UpdateID_1() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___UpdateID_1)); }
	inline int32_t get_UpdateID_1() const { return ___UpdateID_1; }
	inline int32_t* get_address_of_UpdateID_1() { return &___UpdateID_1; }
	inline void set_UpdateID_1(int32_t value)
	{
		___UpdateID_1 = value;
	}

	inline static int32_t get_offset_of_Object_2() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___Object_2)); }
	inline GameObject_t1756533147 * get_Object_2() const { return ___Object_2; }
	inline GameObject_t1756533147 ** get_address_of_Object_2() { return &___Object_2; }
	inline void set_Object_2(GameObject_t1756533147 * value)
	{
		___Object_2 = value;
		Il2CppCodeGenWriteBarrier((&___Object_2), value);
	}

	inline static int32_t get_offset_of_Renderer_3() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___Renderer_3)); }
	inline MeshRenderer_t1268241104 * get_Renderer_3() const { return ___Renderer_3; }
	inline MeshRenderer_t1268241104 ** get_address_of_Renderer_3() { return &___Renderer_3; }
	inline void set_Renderer_3(MeshRenderer_t1268241104 * value)
	{
		___Renderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___Renderer_3), value);
	}

	inline static int32_t get_offset_of_Filter_4() { return static_cast<int32_t>(offsetof(SurfaceObject_t1188044127, ___Filter_4)); }
	inline MeshFilter_t3026937449 * get_Filter_4() const { return ___Filter_4; }
	inline MeshFilter_t3026937449 ** get_address_of_Filter_4() { return &___Filter_4; }
	inline void set_Filter_4(MeshFilter_t3026937449 * value)
	{
		___Filter_4 = value;
		Il2CppCodeGenWriteBarrier((&___Filter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialMappingSource/SurfaceObject
struct SurfaceObject_t1188044127_marshaled_pinvoke
{
	int32_t ___ID_0;
	int32_t ___UpdateID_1;
	GameObject_t1756533147 * ___Object_2;
	MeshRenderer_t1268241104 * ___Renderer_3;
	MeshFilter_t3026937449 * ___Filter_4;
};
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialMappingSource/SurfaceObject
struct SurfaceObject_t1188044127_marshaled_com
{
	int32_t ___ID_0;
	int32_t ___UpdateID_1;
	GameObject_t1756533147 * ___Object_2;
	MeshRenderer_t1268241104 * ___Renderer_3;
	MeshFilter_t3026937449 * ___Filter_4;
};
#endif // SURFACEOBJECT_T1188044127_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ORIENTEDBOUNDINGBOX_T566734849_H
#define ORIENTEDBOUNDINGBOX_T566734849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.OrientedBoundingBox
struct  OrientedBoundingBox_t566734849 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.OrientedBoundingBox::Center
	Vector3_t2243707580  ___Center_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.OrientedBoundingBox::Extents
	Vector3_t2243707580  ___Extents_1;
	// UnityEngine.Quaternion HoloToolkit.Unity.OrientedBoundingBox::Rotation
	Quaternion_t4030073918  ___Rotation_2;

public:
	inline static int32_t get_offset_of_Center_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t566734849, ___Center_0)); }
	inline Vector3_t2243707580  get_Center_0() const { return ___Center_0; }
	inline Vector3_t2243707580 * get_address_of_Center_0() { return &___Center_0; }
	inline void set_Center_0(Vector3_t2243707580  value)
	{
		___Center_0 = value;
	}

	inline static int32_t get_offset_of_Extents_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t566734849, ___Extents_1)); }
	inline Vector3_t2243707580  get_Extents_1() const { return ___Extents_1; }
	inline Vector3_t2243707580 * get_address_of_Extents_1() { return &___Extents_1; }
	inline void set_Extents_1(Vector3_t2243707580  value)
	{
		___Extents_1 = value;
	}

	inline static int32_t get_offset_of_Rotation_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t566734849, ___Rotation_2)); }
	inline Quaternion_t4030073918  get_Rotation_2() const { return ___Rotation_2; }
	inline Quaternion_t4030073918 * get_address_of_Rotation_2() { return &___Rotation_2; }
	inline void set_Rotation_2(Quaternion_t4030073918  value)
	{
		___Rotation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX_T566734849_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef PLANE_T3727654732_H
#define PLANE_T3727654732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t3727654732 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t2243707580  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t3727654732, ___m_Normal_0)); }
	inline Vector3_t2243707580  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t2243707580  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t3727654732, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T3727654732_H
#ifndef PLANETYPES_T512441349_H
#define PLANETYPES_T512441349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PlaneTypes
struct  PlaneTypes_t512441349 
{
public:
	// System.Int32 HoloToolkit.Unity.PlaneTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneTypes_t512441349, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANETYPES_T512441349_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef BOUNDEDPLANE_T3629713867_H
#define BOUNDEDPLANE_T3629713867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.BoundedPlane
struct  BoundedPlane_t3629713867 
{
public:
	// UnityEngine.Plane HoloToolkit.Unity.BoundedPlane::Plane
	Plane_t3727654732  ___Plane_0;
	// HoloToolkit.Unity.OrientedBoundingBox HoloToolkit.Unity.BoundedPlane::Bounds
	OrientedBoundingBox_t566734849  ___Bounds_1;
	// System.Single HoloToolkit.Unity.BoundedPlane::Area
	float ___Area_2;

public:
	inline static int32_t get_offset_of_Plane_0() { return static_cast<int32_t>(offsetof(BoundedPlane_t3629713867, ___Plane_0)); }
	inline Plane_t3727654732  get_Plane_0() const { return ___Plane_0; }
	inline Plane_t3727654732 * get_address_of_Plane_0() { return &___Plane_0; }
	inline void set_Plane_0(Plane_t3727654732  value)
	{
		___Plane_0 = value;
	}

	inline static int32_t get_offset_of_Bounds_1() { return static_cast<int32_t>(offsetof(BoundedPlane_t3629713867, ___Bounds_1)); }
	inline OrientedBoundingBox_t566734849  get_Bounds_1() const { return ___Bounds_1; }
	inline OrientedBoundingBox_t566734849 * get_address_of_Bounds_1() { return &___Bounds_1; }
	inline void set_Bounds_1(OrientedBoundingBox_t566734849  value)
	{
		___Bounds_1 = value;
	}

	inline static int32_t get_offset_of_Area_2() { return static_cast<int32_t>(offsetof(BoundedPlane_t3629713867, ___Area_2)); }
	inline float get_Area_2() const { return ___Area_2; }
	inline float* get_address_of_Area_2() { return &___Area_2; }
	inline void set_Area_2(float value)
	{
		___Area_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDEDPLANE_T3629713867_H
#ifndef EVENTHANDLER_T1067160880_H
#define EVENTHANDLER_T1067160880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes/EventHandler
struct  EventHandler_t1067160880  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T1067160880_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SPATIALMAPPINGSOURCE_T2008115569_H
#define SPATIALMAPPINGSOURCE_T2008115569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMappingSource
struct  SpatialMappingSource_t2008115569  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMappingSource/SurfaceObject> HoloToolkit.Unity.SpatialMappingSource::<SurfaceObjects>k__BackingField
	List_1_t557165259 * ___U3CSurfaceObjectsU3Ek__BackingField_2;
	// System.Type[] HoloToolkit.Unity.SpatialMappingSource::componentsRequiredForSurfaceMesh
	TypeU5BU5D_t1664964607* ___componentsRequiredForSurfaceMesh_3;

public:
	inline static int32_t get_offset_of_U3CSurfaceObjectsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t2008115569, ___U3CSurfaceObjectsU3Ek__BackingField_2)); }
	inline List_1_t557165259 * get_U3CSurfaceObjectsU3Ek__BackingField_2() const { return ___U3CSurfaceObjectsU3Ek__BackingField_2; }
	inline List_1_t557165259 ** get_address_of_U3CSurfaceObjectsU3Ek__BackingField_2() { return &___U3CSurfaceObjectsU3Ek__BackingField_2; }
	inline void set_U3CSurfaceObjectsU3Ek__BackingField_2(List_1_t557165259 * value)
	{
		___U3CSurfaceObjectsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSurfaceObjectsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_componentsRequiredForSurfaceMesh_3() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t2008115569, ___componentsRequiredForSurfaceMesh_3)); }
	inline TypeU5BU5D_t1664964607* get_componentsRequiredForSurfaceMesh_3() const { return ___componentsRequiredForSurfaceMesh_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_componentsRequiredForSurfaceMesh_3() { return &___componentsRequiredForSurfaceMesh_3; }
	inline void set_componentsRequiredForSurfaceMesh_3(TypeU5BU5D_t1664964607* value)
	{
		___componentsRequiredForSurfaceMesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___componentsRequiredForSurfaceMesh_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALMAPPINGSOURCE_T2008115569_H
#ifndef SINGLETON_1_T2912066921_H
#define SINGLETON_1_T2912066921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SurfaceMeshesToPlanes>
struct  Singleton_1_t2912066921  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Singleton_1_t2912066921_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::_Instance
	SurfaceMeshesToPlanes_t2358468568 * ____Instance_2;

public:
	inline static int32_t get_offset_of__Instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2912066921_StaticFields, ____Instance_2)); }
	inline SurfaceMeshesToPlanes_t2358468568 * get__Instance_2() const { return ____Instance_2; }
	inline SurfaceMeshesToPlanes_t2358468568 ** get_address_of__Instance_2() { return &____Instance_2; }
	inline void set__Instance_2(SurfaceMeshesToPlanes_t2358468568 * value)
	{
		____Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T2912066921_H
#ifndef SURFACEPLANE_T191565817_H
#define SURFACEPLANE_T191565817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfacePlane
struct  SurfacePlane_t191565817  : public MonoBehaviour_t1158329972
{
public:
	// System.Single HoloToolkit.Unity.SurfacePlane::PlaneThickness
	float ___PlaneThickness_2;
	// System.Single HoloToolkit.Unity.SurfacePlane::UpNormalThreshold
	float ___UpNormalThreshold_3;
	// System.Single HoloToolkit.Unity.SurfacePlane::FloorBuffer
	float ___FloorBuffer_4;
	// System.Single HoloToolkit.Unity.SurfacePlane::CeilingBuffer
	float ___CeilingBuffer_5;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::WallMaterial
	Material_t193706927 * ___WallMaterial_6;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::FloorMaterial
	Material_t193706927 * ___FloorMaterial_7;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::CeilingMaterial
	Material_t193706927 * ___CeilingMaterial_8;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::TableMaterial
	Material_t193706927 * ___TableMaterial_9;
	// UnityEngine.Material HoloToolkit.Unity.SurfacePlane::UnknownMaterial
	Material_t193706927 * ___UnknownMaterial_10;
	// HoloToolkit.Unity.PlaneTypes HoloToolkit.Unity.SurfacePlane::PlaneType
	int32_t ___PlaneType_11;
	// HoloToolkit.Unity.BoundedPlane HoloToolkit.Unity.SurfacePlane::plane
	BoundedPlane_t3629713867  ___plane_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.SurfacePlane::<SurfaceNormal>k__BackingField
	Vector3_t2243707580  ___U3CSurfaceNormalU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_PlaneThickness_2() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___PlaneThickness_2)); }
	inline float get_PlaneThickness_2() const { return ___PlaneThickness_2; }
	inline float* get_address_of_PlaneThickness_2() { return &___PlaneThickness_2; }
	inline void set_PlaneThickness_2(float value)
	{
		___PlaneThickness_2 = value;
	}

	inline static int32_t get_offset_of_UpNormalThreshold_3() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___UpNormalThreshold_3)); }
	inline float get_UpNormalThreshold_3() const { return ___UpNormalThreshold_3; }
	inline float* get_address_of_UpNormalThreshold_3() { return &___UpNormalThreshold_3; }
	inline void set_UpNormalThreshold_3(float value)
	{
		___UpNormalThreshold_3 = value;
	}

	inline static int32_t get_offset_of_FloorBuffer_4() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___FloorBuffer_4)); }
	inline float get_FloorBuffer_4() const { return ___FloorBuffer_4; }
	inline float* get_address_of_FloorBuffer_4() { return &___FloorBuffer_4; }
	inline void set_FloorBuffer_4(float value)
	{
		___FloorBuffer_4 = value;
	}

	inline static int32_t get_offset_of_CeilingBuffer_5() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___CeilingBuffer_5)); }
	inline float get_CeilingBuffer_5() const { return ___CeilingBuffer_5; }
	inline float* get_address_of_CeilingBuffer_5() { return &___CeilingBuffer_5; }
	inline void set_CeilingBuffer_5(float value)
	{
		___CeilingBuffer_5 = value;
	}

	inline static int32_t get_offset_of_WallMaterial_6() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___WallMaterial_6)); }
	inline Material_t193706927 * get_WallMaterial_6() const { return ___WallMaterial_6; }
	inline Material_t193706927 ** get_address_of_WallMaterial_6() { return &___WallMaterial_6; }
	inline void set_WallMaterial_6(Material_t193706927 * value)
	{
		___WallMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___WallMaterial_6), value);
	}

	inline static int32_t get_offset_of_FloorMaterial_7() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___FloorMaterial_7)); }
	inline Material_t193706927 * get_FloorMaterial_7() const { return ___FloorMaterial_7; }
	inline Material_t193706927 ** get_address_of_FloorMaterial_7() { return &___FloorMaterial_7; }
	inline void set_FloorMaterial_7(Material_t193706927 * value)
	{
		___FloorMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___FloorMaterial_7), value);
	}

	inline static int32_t get_offset_of_CeilingMaterial_8() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___CeilingMaterial_8)); }
	inline Material_t193706927 * get_CeilingMaterial_8() const { return ___CeilingMaterial_8; }
	inline Material_t193706927 ** get_address_of_CeilingMaterial_8() { return &___CeilingMaterial_8; }
	inline void set_CeilingMaterial_8(Material_t193706927 * value)
	{
		___CeilingMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___CeilingMaterial_8), value);
	}

	inline static int32_t get_offset_of_TableMaterial_9() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___TableMaterial_9)); }
	inline Material_t193706927 * get_TableMaterial_9() const { return ___TableMaterial_9; }
	inline Material_t193706927 ** get_address_of_TableMaterial_9() { return &___TableMaterial_9; }
	inline void set_TableMaterial_9(Material_t193706927 * value)
	{
		___TableMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___TableMaterial_9), value);
	}

	inline static int32_t get_offset_of_UnknownMaterial_10() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___UnknownMaterial_10)); }
	inline Material_t193706927 * get_UnknownMaterial_10() const { return ___UnknownMaterial_10; }
	inline Material_t193706927 ** get_address_of_UnknownMaterial_10() { return &___UnknownMaterial_10; }
	inline void set_UnknownMaterial_10(Material_t193706927 * value)
	{
		___UnknownMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___UnknownMaterial_10), value);
	}

	inline static int32_t get_offset_of_PlaneType_11() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___PlaneType_11)); }
	inline int32_t get_PlaneType_11() const { return ___PlaneType_11; }
	inline int32_t* get_address_of_PlaneType_11() { return &___PlaneType_11; }
	inline void set_PlaneType_11(int32_t value)
	{
		___PlaneType_11 = value;
	}

	inline static int32_t get_offset_of_plane_12() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___plane_12)); }
	inline BoundedPlane_t3629713867  get_plane_12() const { return ___plane_12; }
	inline BoundedPlane_t3629713867 * get_address_of_plane_12() { return &___plane_12; }
	inline void set_plane_12(BoundedPlane_t3629713867  value)
	{
		___plane_12 = value;
	}

	inline static int32_t get_offset_of_U3CSurfaceNormalU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SurfacePlane_t191565817, ___U3CSurfaceNormalU3Ek__BackingField_13)); }
	inline Vector3_t2243707580  get_U3CSurfaceNormalU3Ek__BackingField_13() const { return ___U3CSurfaceNormalU3Ek__BackingField_13; }
	inline Vector3_t2243707580 * get_address_of_U3CSurfaceNormalU3Ek__BackingField_13() { return &___U3CSurfaceNormalU3Ek__BackingField_13; }
	inline void set_U3CSurfaceNormalU3Ek__BackingField_13(Vector3_t2243707580  value)
	{
		___U3CSurfaceNormalU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEPLANE_T191565817_H
#ifndef SURFACEMESHESTOPLANES_T2358468568_H
#define SURFACEMESHESTOPLANES_T2358468568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SurfaceMeshesToPlanes
struct  SurfaceMeshesToPlanes_t2358468568  : public Singleton_1_t2912066921
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.SurfaceMeshesToPlanes::ActivePlanes
	List_1_t1125654279 * ___ActivePlanes_3;
	// UnityEngine.GameObject HoloToolkit.Unity.SurfaceMeshesToPlanes::SurfacePlanePrefab
	GameObject_t1756533147 * ___SurfacePlanePrefab_4;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::MinArea
	float ___MinArea_5;
	// HoloToolkit.Unity.PlaneTypes HoloToolkit.Unity.SurfaceMeshesToPlanes::drawPlanesMask
	int32_t ___drawPlanesMask_6;
	// HoloToolkit.Unity.PlaneTypes HoloToolkit.Unity.SurfaceMeshesToPlanes::destroyPlanesMask
	int32_t ___destroyPlanesMask_7;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::<FloorYPosition>k__BackingField
	float ___U3CFloorYPositionU3Ek__BackingField_8;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::<CeilingYPosition>k__BackingField
	float ___U3CCeilingYPositionU3Ek__BackingField_9;
	// HoloToolkit.Unity.SurfaceMeshesToPlanes/EventHandler HoloToolkit.Unity.SurfaceMeshesToPlanes::MakePlanesComplete
	EventHandler_t1067160880 * ___MakePlanesComplete_10;
	// UnityEngine.GameObject HoloToolkit.Unity.SurfaceMeshesToPlanes::planesParent
	GameObject_t1756533147 * ___planesParent_11;
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::snapToGravityThreshold
	float ___snapToGravityThreshold_12;
	// System.Boolean HoloToolkit.Unity.SurfaceMeshesToPlanes::makingPlanes
	bool ___makingPlanes_13;

public:
	inline static int32_t get_offset_of_ActivePlanes_3() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___ActivePlanes_3)); }
	inline List_1_t1125654279 * get_ActivePlanes_3() const { return ___ActivePlanes_3; }
	inline List_1_t1125654279 ** get_address_of_ActivePlanes_3() { return &___ActivePlanes_3; }
	inline void set_ActivePlanes_3(List_1_t1125654279 * value)
	{
		___ActivePlanes_3 = value;
		Il2CppCodeGenWriteBarrier((&___ActivePlanes_3), value);
	}

	inline static int32_t get_offset_of_SurfacePlanePrefab_4() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___SurfacePlanePrefab_4)); }
	inline GameObject_t1756533147 * get_SurfacePlanePrefab_4() const { return ___SurfacePlanePrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_SurfacePlanePrefab_4() { return &___SurfacePlanePrefab_4; }
	inline void set_SurfacePlanePrefab_4(GameObject_t1756533147 * value)
	{
		___SurfacePlanePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___SurfacePlanePrefab_4), value);
	}

	inline static int32_t get_offset_of_MinArea_5() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___MinArea_5)); }
	inline float get_MinArea_5() const { return ___MinArea_5; }
	inline float* get_address_of_MinArea_5() { return &___MinArea_5; }
	inline void set_MinArea_5(float value)
	{
		___MinArea_5 = value;
	}

	inline static int32_t get_offset_of_drawPlanesMask_6() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___drawPlanesMask_6)); }
	inline int32_t get_drawPlanesMask_6() const { return ___drawPlanesMask_6; }
	inline int32_t* get_address_of_drawPlanesMask_6() { return &___drawPlanesMask_6; }
	inline void set_drawPlanesMask_6(int32_t value)
	{
		___drawPlanesMask_6 = value;
	}

	inline static int32_t get_offset_of_destroyPlanesMask_7() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___destroyPlanesMask_7)); }
	inline int32_t get_destroyPlanesMask_7() const { return ___destroyPlanesMask_7; }
	inline int32_t* get_address_of_destroyPlanesMask_7() { return &___destroyPlanesMask_7; }
	inline void set_destroyPlanesMask_7(int32_t value)
	{
		___destroyPlanesMask_7 = value;
	}

	inline static int32_t get_offset_of_U3CFloorYPositionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___U3CFloorYPositionU3Ek__BackingField_8)); }
	inline float get_U3CFloorYPositionU3Ek__BackingField_8() const { return ___U3CFloorYPositionU3Ek__BackingField_8; }
	inline float* get_address_of_U3CFloorYPositionU3Ek__BackingField_8() { return &___U3CFloorYPositionU3Ek__BackingField_8; }
	inline void set_U3CFloorYPositionU3Ek__BackingField_8(float value)
	{
		___U3CFloorYPositionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCeilingYPositionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___U3CCeilingYPositionU3Ek__BackingField_9)); }
	inline float get_U3CCeilingYPositionU3Ek__BackingField_9() const { return ___U3CCeilingYPositionU3Ek__BackingField_9; }
	inline float* get_address_of_U3CCeilingYPositionU3Ek__BackingField_9() { return &___U3CCeilingYPositionU3Ek__BackingField_9; }
	inline void set_U3CCeilingYPositionU3Ek__BackingField_9(float value)
	{
		___U3CCeilingYPositionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_MakePlanesComplete_10() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___MakePlanesComplete_10)); }
	inline EventHandler_t1067160880 * get_MakePlanesComplete_10() const { return ___MakePlanesComplete_10; }
	inline EventHandler_t1067160880 ** get_address_of_MakePlanesComplete_10() { return &___MakePlanesComplete_10; }
	inline void set_MakePlanesComplete_10(EventHandler_t1067160880 * value)
	{
		___MakePlanesComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___MakePlanesComplete_10), value);
	}

	inline static int32_t get_offset_of_planesParent_11() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___planesParent_11)); }
	inline GameObject_t1756533147 * get_planesParent_11() const { return ___planesParent_11; }
	inline GameObject_t1756533147 ** get_address_of_planesParent_11() { return &___planesParent_11; }
	inline void set_planesParent_11(GameObject_t1756533147 * value)
	{
		___planesParent_11 = value;
		Il2CppCodeGenWriteBarrier((&___planesParent_11), value);
	}

	inline static int32_t get_offset_of_snapToGravityThreshold_12() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___snapToGravityThreshold_12)); }
	inline float get_snapToGravityThreshold_12() const { return ___snapToGravityThreshold_12; }
	inline float* get_address_of_snapToGravityThreshold_12() { return &___snapToGravityThreshold_12; }
	inline void set_snapToGravityThreshold_12(float value)
	{
		___snapToGravityThreshold_12 = value;
	}

	inline static int32_t get_offset_of_makingPlanes_13() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568, ___makingPlanes_13)); }
	inline bool get_makingPlanes_13() const { return ___makingPlanes_13; }
	inline bool* get_address_of_makingPlanes_13() { return &___makingPlanes_13; }
	inline void set_makingPlanes_13(bool value)
	{
		___makingPlanes_13 = value;
	}
};

struct SurfaceMeshesToPlanes_t2358468568_StaticFields
{
public:
	// System.Single HoloToolkit.Unity.SurfaceMeshesToPlanes::FrameTime
	float ___FrameTime_14;

public:
	inline static int32_t get_offset_of_FrameTime_14() { return static_cast<int32_t>(offsetof(SurfaceMeshesToPlanes_t2358468568_StaticFields, ___FrameTime_14)); }
	inline float get_FrameTime_14() const { return ___FrameTime_14; }
	inline float* get_address_of_FrameTime_14() { return &___FrameTime_14; }
	inline void set_FrameTime_14(float value)
	{
		___FrameTime_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEMESHESTOPLANES_T2358468568_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (SpatialMappingSource_t2008115569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[2] = 
{
	SpatialMappingSource_t2008115569::get_offset_of_U3CSurfaceObjectsU3Ek__BackingField_2(),
	SpatialMappingSource_t2008115569::get_offset_of_componentsRequiredForSurfaceMesh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (SurfaceObject_t1188044127)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[5] = 
{
	SurfaceObject_t1188044127::get_offset_of_ID_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_UpdateID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_Object_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_Renderer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceObject_t1188044127::get_offset_of_Filter_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (SurfaceMeshesToPlanes_t2358468568), -1, sizeof(SurfaceMeshesToPlanes_t2358468568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3702[12] = 
{
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_ActivePlanes_3(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_SurfacePlanePrefab_4(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_MinArea_5(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_drawPlanesMask_6(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_destroyPlanesMask_7(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_U3CFloorYPositionU3Ek__BackingField_8(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_U3CCeilingYPositionU3Ek__BackingField_9(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_MakePlanesComplete_10(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_planesParent_11(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_snapToGravityThreshold_12(),
	SurfaceMeshesToPlanes_t2358468568::get_offset_of_makingPlanes_13(),
	SurfaceMeshesToPlanes_t2358468568_StaticFields::get_offset_of_FrameTime_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (EventHandler_t1067160880), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (U3CU3Ec__DisplayClass24_0_t1668648882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[2] = 
{
	U3CU3Ec__DisplayClass24_0_t1668648882::get_offset_of_meshData_0(),
	U3CU3Ec__DisplayClass24_0_t1668648882::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (U3CMakePlanesRoutineU3Ed__24_t2308419383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[9] = 
{
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E1__state_0(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E2__current_1(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E4__this_2(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CU3E8__1_3(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CfiltersU3E5__2_4(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CindexU3E5__3_5(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CplaneTaskU3E5__4_6(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CplanesU3E5__5_7(),
	U3CMakePlanesRoutineU3Ed__24_t2308419383::get_offset_of_U3CindexU3E5__6_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (PlaneTypes_t512441349)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3706[6] = 
{
	PlaneTypes_t512441349::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (SurfacePlane_t191565817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[12] = 
{
	SurfacePlane_t191565817::get_offset_of_PlaneThickness_2(),
	SurfacePlane_t191565817::get_offset_of_UpNormalThreshold_3(),
	SurfacePlane_t191565817::get_offset_of_FloorBuffer_4(),
	SurfacePlane_t191565817::get_offset_of_CeilingBuffer_5(),
	SurfacePlane_t191565817::get_offset_of_WallMaterial_6(),
	SurfacePlane_t191565817::get_offset_of_FloorMaterial_7(),
	SurfacePlane_t191565817::get_offset_of_CeilingMaterial_8(),
	SurfacePlane_t191565817::get_offset_of_TableMaterial_9(),
	SurfacePlane_t191565817::get_offset_of_UnknownMaterial_10(),
	SurfacePlane_t191565817::get_offset_of_PlaneType_11(),
	SurfacePlane_t191565817::get_offset_of_plane_12(),
	SurfacePlane_t191565817::get_offset_of_U3CSurfaceNormalU3Ek__BackingField_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
