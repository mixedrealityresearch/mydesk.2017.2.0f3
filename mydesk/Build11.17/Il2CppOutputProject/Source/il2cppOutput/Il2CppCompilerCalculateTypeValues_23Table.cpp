﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1656058977;
// Mono.Security.Interface.MonoTlsSettings
struct MonoTlsSettings_t302829305;
// Mono.Security.Interface.MonoTlsProvider
struct MonoTlsProvider_t823784021;
// System.Net.ServerCertValidationCallback
struct ServerCertValidationCallback_t2774612835;
// System.Net.Security.LocalCertSelectionCallback
struct LocalCertSelectionCallback_t2279727292;
// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct ServerCertValidationCallbackWrapper_t93117366;
// Mono.Net.Security.MonoTlsStream
struct MonoTlsStream_t1249652258;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// Mono.Security.Interface.MonoLocalCertificateSelectionCallback
struct MonoLocalCertificateSelectionCallback_t4080334132;
// Mono.Security.Interface.MonoRemoteCertificateValidationCallback
struct MonoRemoteCertificateValidationCallback_t1929047274;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.String
struct String_t;
// Mono.Net.Security.Private.LegacySslStream
struct LegacySslStream_t83997747;
// Mono.Net.CFProxy[]
struct CFProxyU5BU5D_t2453615604;
// Mono.Net.CFRunLoop
struct CFRunLoop_t1302886598;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Collections.Generic.Queue`1<Mono.Net.CFNetwork/GetProxyData>
struct Queue_1_t1206146221;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// Mono.Net.Security.IMonoSslStream
struct IMonoSslStream_t424679660;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry
struct NameObjectEntry_t4229094479;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t633582367;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.StringComparer
struct StringComparer_t1574862926;
// System.Type
struct Type_t;
// System.Configuration.Internal.IInternalConfigHost
struct IInternalConfigHost_t3115158152;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Mono.Net.Dns.DnsHeader
struct DnsHeader_t2481892084;
// Mono.Security.Interface.IMonoSslStream
struct IMonoSslStream_t3678778144;
// System.Configuration.ElementMap
struct ElementMap_t997038224;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t1911180302;
// System.Configuration.ElementInformation
struct ElementInformation_t3165583784;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Configuration.ConfigurationLockCollection
struct ConfigurationLockCollection_t1011762925;
// System.Configuration.ConfigurationElement/SaveContext
struct SaveContext_t3996373180;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Net.IPEndPoint[]
struct IPEndPointU5BU5D_t294524195;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Collections.Generic.Dictionary`2<System.Int32,Mono.Net.Dns.SimpleResolverEventArgs>
struct Dictionary_2_t3302389772;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Threading.TimerCallback
struct TimerCallback_t1684927372;
// System.Configuration.PropertyInformation
struct PropertyInformation_t2089433965;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.Configuration.PropertyInformationCollection
struct PropertyInformationCollection_t954922393;
// System.Configuration.ConfigurationSectionCollection
struct ConfigurationSectionCollection_t4261113299;
// System.Configuration.ConfigurationSectionGroupCollection
struct ConfigurationSectionGroupCollection_t575145286;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t2346323570;
// System.Configuration.Internal.IInternalConfigRoot
struct IInternalConfigRoot_t3316671250;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Mono.Net.Security.IMonoTlsProvider
struct IMonoTlsProvider_t2506971578;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Configuration.SectionInformation
struct SectionInformation_t2754609709;
// System.Configuration.IConfigurationSectionHandler
struct IConfigurationSectionHandler_t4214479838;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t2745753060;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t461808439;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord>
struct ReadOnlyCollection_1_t3129240104;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion>
struct ReadOnlyCollection_1_t3276628651;
// System.Configuration.ConfigNameValueCollection
struct ConfigNameValueCollection_t2395569530;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t2048066811;
// System.Configuration.ConfigInfoCollection
struct ConfigInfoCollection_t3264723080;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.IO.Stream
struct Stream_t3255436806;
// System.Configuration.ProtectedConfigurationProviderCollection
struct ProtectedConfigurationProviderCollection_t388338823;
// Mono.Net.CFDictionary
struct CFDictionary_t3548969133;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Delegate[]
struct DelegateU5BU5D_t1606206610;
// System.Configuration.ConfigurationSection
struct ConfigurationSection_t2600766927;
// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t3971982415;
// System.Configuration.ExeConfigurationFileMap
struct ExeConfigurationFileMap_t1419586304;
// Mono.Security.Protocol.Tls.SslStreamBase
struct SslStreamBase_t934199321;
// Mono.Security.Interface.ICertificateValidator
struct ICertificateValidator_t3067886638;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t581172200;
// System.EventHandler`1<Mono.Net.Dns.SimpleResolverEventArgs>
struct EventHandler_1_t2885871309;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.Threading.Timer
struct Timer_t791717973;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534218_H
#define U3CMODULEU3E_T3783534218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534218 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534218_H
#ifndef MARSHALBYREFOBJECT_T1285298191_H
#define MARSHALBYREFOBJECT_T1285298191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1285298191  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1656058977 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1285298191, ____identity_0)); }
	inline ServerIdentity_t1656058977 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1656058977 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1656058977 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_pinvoke
{
	ServerIdentity_t1656058977 * ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t1285298191_marshaled_com
{
	ServerIdentity_t1656058977 * ____identity_0;
};
#endif // MARSHALBYREFOBJECT_T1285298191_H
#ifndef CHAINVALIDATIONHELPER_T3893280544_H
#define CHAINVALIDATIONHELPER_T3893280544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ChainValidationHelper
struct  ChainValidationHelper_t3893280544  : public RuntimeObject
{
public:
	// System.Object Mono.Net.Security.ChainValidationHelper::sender
	RuntimeObject * ___sender_0;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.ChainValidationHelper::settings
	MonoTlsSettings_t302829305 * ___settings_1;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.ChainValidationHelper::provider
	MonoTlsProvider_t823784021 * ___provider_2;
	// System.Net.ServerCertValidationCallback Mono.Net.Security.ChainValidationHelper::certValidationCallback
	ServerCertValidationCallback_t2774612835 * ___certValidationCallback_3;
	// System.Net.Security.LocalCertSelectionCallback Mono.Net.Security.ChainValidationHelper::certSelectionCallback
	LocalCertSelectionCallback_t2279727292 * ___certSelectionCallback_4;
	// Mono.Net.Security.ServerCertValidationCallbackWrapper Mono.Net.Security.ChainValidationHelper::callbackWrapper
	ServerCertValidationCallbackWrapper_t93117366 * ___callbackWrapper_5;
	// Mono.Net.Security.MonoTlsStream Mono.Net.Security.ChainValidationHelper::tlsStream
	MonoTlsStream_t1249652258 * ___tlsStream_6;
	// System.Net.HttpWebRequest Mono.Net.Security.ChainValidationHelper::request
	HttpWebRequest_t1951404513 * ___request_7;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___settings_1)); }
	inline MonoTlsSettings_t302829305 * get_settings_1() const { return ___settings_1; }
	inline MonoTlsSettings_t302829305 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(MonoTlsSettings_t302829305 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}

	inline static int32_t get_offset_of_provider_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___provider_2)); }
	inline MonoTlsProvider_t823784021 * get_provider_2() const { return ___provider_2; }
	inline MonoTlsProvider_t823784021 ** get_address_of_provider_2() { return &___provider_2; }
	inline void set_provider_2(MonoTlsProvider_t823784021 * value)
	{
		___provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___provider_2), value);
	}

	inline static int32_t get_offset_of_certValidationCallback_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___certValidationCallback_3)); }
	inline ServerCertValidationCallback_t2774612835 * get_certValidationCallback_3() const { return ___certValidationCallback_3; }
	inline ServerCertValidationCallback_t2774612835 ** get_address_of_certValidationCallback_3() { return &___certValidationCallback_3; }
	inline void set_certValidationCallback_3(ServerCertValidationCallback_t2774612835 * value)
	{
		___certValidationCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___certValidationCallback_3), value);
	}

	inline static int32_t get_offset_of_certSelectionCallback_4() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___certSelectionCallback_4)); }
	inline LocalCertSelectionCallback_t2279727292 * get_certSelectionCallback_4() const { return ___certSelectionCallback_4; }
	inline LocalCertSelectionCallback_t2279727292 ** get_address_of_certSelectionCallback_4() { return &___certSelectionCallback_4; }
	inline void set_certSelectionCallback_4(LocalCertSelectionCallback_t2279727292 * value)
	{
		___certSelectionCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___certSelectionCallback_4), value);
	}

	inline static int32_t get_offset_of_callbackWrapper_5() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___callbackWrapper_5)); }
	inline ServerCertValidationCallbackWrapper_t93117366 * get_callbackWrapper_5() const { return ___callbackWrapper_5; }
	inline ServerCertValidationCallbackWrapper_t93117366 ** get_address_of_callbackWrapper_5() { return &___callbackWrapper_5; }
	inline void set_callbackWrapper_5(ServerCertValidationCallbackWrapper_t93117366 * value)
	{
		___callbackWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___callbackWrapper_5), value);
	}

	inline static int32_t get_offset_of_tlsStream_6() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___tlsStream_6)); }
	inline MonoTlsStream_t1249652258 * get_tlsStream_6() const { return ___tlsStream_6; }
	inline MonoTlsStream_t1249652258 ** get_address_of_tlsStream_6() { return &___tlsStream_6; }
	inline void set_tlsStream_6(MonoTlsStream_t1249652258 * value)
	{
		___tlsStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___tlsStream_6), value);
	}

	inline static int32_t get_offset_of_request_7() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t3893280544, ___request_7)); }
	inline HttpWebRequest_t1951404513 * get_request_7() const { return ___request_7; }
	inline HttpWebRequest_t1951404513 ** get_address_of_request_7() { return &___request_7; }
	inline void set_request_7(HttpWebRequest_t1951404513 * value)
	{
		___request_7 = value;
		Il2CppCodeGenWriteBarrier((&___request_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAINVALIDATIONHELPER_T3893280544_H
#ifndef CALLBACKHELPERS_T4210896810_H
#define CALLBACKHELPERS_T4210896810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers
struct  CallbackHelpers_t4210896810  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKHELPERS_T4210896810_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T183195720_H
#define U3CU3EC__DISPLAYCLASS8_0_T183195720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t183195720  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoLocalCertificateSelectionCallback Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass8_0::callback
	MonoLocalCertificateSelectionCallback_t4080334132 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t183195720, ___callback_0)); }
	inline MonoLocalCertificateSelectionCallback_t4080334132 * get_callback_0() const { return ___callback_0; }
	inline MonoLocalCertificateSelectionCallback_t4080334132 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoLocalCertificateSelectionCallback_t4080334132 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T183195720_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T573183817_H
#define U3CU3EC__DISPLAYCLASS5_0_T573183817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t573183817  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoRemoteCertificateValidationCallback Mono.Net.Security.Private.CallbackHelpers/<>c__DisplayClass5_0::callback
	MonoRemoteCertificateValidationCallback_t1929047274 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t573183817, ___callback_0)); }
	inline MonoRemoteCertificateValidationCallback_t1929047274 * get_callback_0() const { return ___callback_0; }
	inline MonoRemoteCertificateValidationCallback_t1929047274 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(MonoRemoteCertificateValidationCallback_t1929047274 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T573183817_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T3446885867_H
#define U3CU3EC__DISPLAYCLASS57_0_T3446885867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t3446885867  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0::clientCertificates
	X509CertificateCollection_t1197680765 * ___clientCertificates_0;
	// System.String Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0::targetHost
	String_t* ___targetHost_1;
	// Mono.Net.Security.Private.LegacySslStream Mono.Net.Security.Private.LegacySslStream/<>c__DisplayClass57_0::<>4__this
	LegacySslStream_t83997747 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_clientCertificates_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t3446885867, ___clientCertificates_0)); }
	inline X509CertificateCollection_t1197680765 * get_clientCertificates_0() const { return ___clientCertificates_0; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_clientCertificates_0() { return &___clientCertificates_0; }
	inline void set_clientCertificates_0(X509CertificateCollection_t1197680765 * value)
	{
		___clientCertificates_0 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificates_0), value);
	}

	inline static int32_t get_offset_of_targetHost_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t3446885867, ___targetHost_1)); }
	inline String_t* get_targetHost_1() const { return ___targetHost_1; }
	inline String_t** get_address_of_targetHost_1() { return &___targetHost_1; }
	inline void set_targetHost_1(String_t* value)
	{
		___targetHost_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetHost_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t3446885867, ___U3CU3E4__this_2)); }
	inline LegacySslStream_t83997747 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LegacySslStream_t83997747 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LegacySslStream_t83997747 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T3446885867_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T3879183210_H
#define U3CU3EC__DISPLAYCLASS13_0_T3879183210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t3879183210  : public RuntimeObject
{
public:
	// Mono.Net.CFProxy[] Mono.Net.CFNetwork/<>c__DisplayClass13_0::proxies
	CFProxyU5BU5D_t2453615604* ___proxies_0;
	// Mono.Net.CFRunLoop Mono.Net.CFNetwork/<>c__DisplayClass13_0::runLoop
	CFRunLoop_t1302886598 * ___runLoop_1;

public:
	inline static int32_t get_offset_of_proxies_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t3879183210, ___proxies_0)); }
	inline CFProxyU5BU5D_t2453615604* get_proxies_0() const { return ___proxies_0; }
	inline CFProxyU5BU5D_t2453615604** get_address_of_proxies_0() { return &___proxies_0; }
	inline void set_proxies_0(CFProxyU5BU5D_t2453615604* value)
	{
		___proxies_0 = value;
		Il2CppCodeGenWriteBarrier((&___proxies_0), value);
	}

	inline static int32_t get_offset_of_runLoop_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t3879183210, ___runLoop_1)); }
	inline CFRunLoop_t1302886598 * get_runLoop_1() const { return ___runLoop_1; }
	inline CFRunLoop_t1302886598 ** get_address_of_runLoop_1() { return &___runLoop_1; }
	inline void set_runLoop_1(CFRunLoop_t1302886598 * value)
	{
		___runLoop_1 = value;
		Il2CppCodeGenWriteBarrier((&___runLoop_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T3879183210_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305140_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305140  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305140_H
#ifndef PROVIDERCOLLECTION_T2548499159_H
#define PROVIDERCOLLECTION_T2548499159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderCollection
struct  ProviderCollection_t2548499159  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Configuration.Provider.ProviderCollection::lookup
	Hashtable_t909839986 * ___lookup_0;
	// System.Boolean System.Configuration.Provider.ProviderCollection::readOnly
	bool ___readOnly_1;
	// System.Collections.ArrayList System.Configuration.Provider.ProviderCollection::values
	ArrayList_t4252133567 * ___values_2;

public:
	inline static int32_t get_offset_of_lookup_0() { return static_cast<int32_t>(offsetof(ProviderCollection_t2548499159, ___lookup_0)); }
	inline Hashtable_t909839986 * get_lookup_0() const { return ___lookup_0; }
	inline Hashtable_t909839986 ** get_address_of_lookup_0() { return &___lookup_0; }
	inline void set_lookup_0(Hashtable_t909839986 * value)
	{
		___lookup_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_0), value);
	}

	inline static int32_t get_offset_of_readOnly_1() { return static_cast<int32_t>(offsetof(ProviderCollection_t2548499159, ___readOnly_1)); }
	inline bool get_readOnly_1() const { return ___readOnly_1; }
	inline bool* get_address_of_readOnly_1() { return &___readOnly_1; }
	inline void set_readOnly_1(bool value)
	{
		___readOnly_1 = value;
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(ProviderCollection_t2548499159, ___values_2)); }
	inline ArrayList_t4252133567 * get_values_2() const { return ___values_2; }
	inline ArrayList_t4252133567 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(ArrayList_t4252133567 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERCOLLECTION_T2548499159_H
#ifndef PROVIDERBASE_T2882126354_H
#define PROVIDERBASE_T2882126354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderBase
struct  ProviderBase_t2882126354  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.Provider.ProviderBase::alreadyInitialized
	bool ___alreadyInitialized_0;
	// System.String System.Configuration.Provider.ProviderBase::_description
	String_t* ____description_1;
	// System.String System.Configuration.Provider.ProviderBase::_name
	String_t* ____name_2;

public:
	inline static int32_t get_offset_of_alreadyInitialized_0() { return static_cast<int32_t>(offsetof(ProviderBase_t2882126354, ___alreadyInitialized_0)); }
	inline bool get_alreadyInitialized_0() const { return ___alreadyInitialized_0; }
	inline bool* get_address_of_alreadyInitialized_0() { return &___alreadyInitialized_0; }
	inline void set_alreadyInitialized_0(bool value)
	{
		___alreadyInitialized_0 = value;
	}

	inline static int32_t get_offset_of__description_1() { return static_cast<int32_t>(offsetof(ProviderBase_t2882126354, ____description_1)); }
	inline String_t* get__description_1() const { return ____description_1; }
	inline String_t** get_address_of__description_1() { return &____description_1; }
	inline void set__description_1(String_t* value)
	{
		____description_1 = value;
		Il2CppCodeGenWriteBarrier((&____description_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ProviderBase_t2882126354, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBASE_T2882126354_H
#ifndef CFWEBPROXY_T1432115055_H
#define CFWEBPROXY_T1432115055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/CFWebProxy
struct  CFWebProxy_t1432115055  : public RuntimeObject
{
public:
	// System.Net.ICredentials Mono.Net.CFNetwork/CFWebProxy::credentials
	RuntimeObject* ___credentials_0;
	// System.Boolean Mono.Net.CFNetwork/CFWebProxy::userSpecified
	bool ___userSpecified_1;

public:
	inline static int32_t get_offset_of_credentials_0() { return static_cast<int32_t>(offsetof(CFWebProxy_t1432115055, ___credentials_0)); }
	inline RuntimeObject* get_credentials_0() const { return ___credentials_0; }
	inline RuntimeObject** get_address_of_credentials_0() { return &___credentials_0; }
	inline void set_credentials_0(RuntimeObject* value)
	{
		___credentials_0 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_0), value);
	}

	inline static int32_t get_offset_of_userSpecified_1() { return static_cast<int32_t>(offsetof(CFWebProxy_t1432115055, ___userSpecified_1)); }
	inline bool get_userSpecified_1() const { return ___userSpecified_1; }
	inline bool* get_address_of_userSpecified_1() { return &___userSpecified_1; }
	inline void set_userSpecified_1(bool value)
	{
		___userSpecified_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFWEBPROXY_T1432115055_H
#ifndef CFNETWORK_T2805084167_H
#define CFNETWORK_T2805084167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork
struct  CFNetwork_t2805084167  : public RuntimeObject
{
public:

public:
};

struct CFNetwork_t2805084167_StaticFields
{
public:
	// System.Object Mono.Net.CFNetwork::lock_obj
	RuntimeObject * ___lock_obj_0;
	// System.Collections.Generic.Queue`1<Mono.Net.CFNetwork/GetProxyData> Mono.Net.CFNetwork::get_proxy_queue
	Queue_1_t1206146221 * ___get_proxy_queue_1;
	// System.Threading.AutoResetEvent Mono.Net.CFNetwork::proxy_event
	AutoResetEvent_t15112628 * ___proxy_event_2;

public:
	inline static int32_t get_offset_of_lock_obj_0() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___lock_obj_0)); }
	inline RuntimeObject * get_lock_obj_0() const { return ___lock_obj_0; }
	inline RuntimeObject ** get_address_of_lock_obj_0() { return &___lock_obj_0; }
	inline void set_lock_obj_0(RuntimeObject * value)
	{
		___lock_obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___lock_obj_0), value);
	}

	inline static int32_t get_offset_of_get_proxy_queue_1() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___get_proxy_queue_1)); }
	inline Queue_1_t1206146221 * get_get_proxy_queue_1() const { return ___get_proxy_queue_1; }
	inline Queue_1_t1206146221 ** get_address_of_get_proxy_queue_1() { return &___get_proxy_queue_1; }
	inline void set_get_proxy_queue_1(Queue_1_t1206146221 * value)
	{
		___get_proxy_queue_1 = value;
		Il2CppCodeGenWriteBarrier((&___get_proxy_queue_1), value);
	}

	inline static int32_t get_offset_of_proxy_event_2() { return static_cast<int32_t>(offsetof(CFNetwork_t2805084167_StaticFields, ___proxy_event_2)); }
	inline AutoResetEvent_t15112628 * get_proxy_event_2() const { return ___proxy_event_2; }
	inline AutoResetEvent_t15112628 ** get_address_of_proxy_event_2() { return &___proxy_event_2; }
	inline void set_proxy_event_2(AutoResetEvent_t15112628 * value)
	{
		___proxy_event_2 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_event_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFNETWORK_T2805084167_H
#ifndef SR_T2523137205_H
#define SR_T2523137205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t2523137205  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T2523137205_H
#ifndef MONOSSLSTREAMIMPL_T1549641033_H
#define MONOSSLSTREAMIMPL_T1549641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoSslStreamImpl
struct  MonoSslStreamImpl_t1549641033  : public RuntimeObject
{
public:
	// Mono.Net.Security.IMonoSslStream Mono.Net.Security.Private.MonoSslStreamImpl::impl
	RuntimeObject* ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(MonoSslStreamImpl_t1549641033, ___impl_0)); }
	inline RuntimeObject* get_impl_0() const { return ___impl_0; }
	inline RuntimeObject** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(RuntimeObject* value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLSTREAMIMPL_T1549641033_H
#ifndef CONFIGURATIONFILEMAP_T2625210096_H
#define CONFIGURATIONFILEMAP_T2625210096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationFileMap
struct  ConfigurationFileMap_t2625210096  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationFileMap::machineConfigFilename
	String_t* ___machineConfigFilename_0;

public:
	inline static int32_t get_offset_of_machineConfigFilename_0() { return static_cast<int32_t>(offsetof(ConfigurationFileMap_t2625210096, ___machineConfigFilename_0)); }
	inline String_t* get_machineConfigFilename_0() const { return ___machineConfigFilename_0; }
	inline String_t** get_address_of_machineConfigFilename_0() { return &___machineConfigFilename_0; }
	inline void set_machineConfigFilename_0(String_t* value)
	{
		___machineConfigFilename_0 = value;
		Il2CppCodeGenWriteBarrier((&___machineConfigFilename_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONFILEMAP_T2625210096_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#define NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2034248631  : public RuntimeObject
{
public:
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::_readOnly
	bool ____readOnly_0;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::_entriesArray
	ArrayList_t4252133567 * ____entriesArray_1;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::_keyComparer
	RuntimeObject* ____keyComparer_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_entriesTable
	Hashtable_t909839986 * ____entriesTable_3;
	// System.Collections.Specialized.NameObjectCollectionBase/NameObjectEntry modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Specialized.NameObjectCollectionBase::_nullKeyEntry
	NameObjectEntry_t4229094479 * ____nullKeyEntry_4;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::_keys
	KeysCollection_t633582367 * ____keys_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::_serializationInfo
	SerializationInfo_t228987430 * ____serializationInfo_6;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::_version
	int32_t ____version_7;
	// System.Object System.Collections.Specialized.NameObjectCollectionBase::_syncRoot
	RuntimeObject * ____syncRoot_8;

public:
	inline static int32_t get_offset_of__readOnly_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____readOnly_0)); }
	inline bool get__readOnly_0() const { return ____readOnly_0; }
	inline bool* get_address_of__readOnly_0() { return &____readOnly_0; }
	inline void set__readOnly_0(bool value)
	{
		____readOnly_0 = value;
	}

	inline static int32_t get_offset_of__entriesArray_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____entriesArray_1)); }
	inline ArrayList_t4252133567 * get__entriesArray_1() const { return ____entriesArray_1; }
	inline ArrayList_t4252133567 ** get_address_of__entriesArray_1() { return &____entriesArray_1; }
	inline void set__entriesArray_1(ArrayList_t4252133567 * value)
	{
		____entriesArray_1 = value;
		Il2CppCodeGenWriteBarrier((&____entriesArray_1), value);
	}

	inline static int32_t get_offset_of__keyComparer_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____keyComparer_2)); }
	inline RuntimeObject* get__keyComparer_2() const { return ____keyComparer_2; }
	inline RuntimeObject** get_address_of__keyComparer_2() { return &____keyComparer_2; }
	inline void set__keyComparer_2(RuntimeObject* value)
	{
		____keyComparer_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyComparer_2), value);
	}

	inline static int32_t get_offset_of__entriesTable_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____entriesTable_3)); }
	inline Hashtable_t909839986 * get__entriesTable_3() const { return ____entriesTable_3; }
	inline Hashtable_t909839986 ** get_address_of__entriesTable_3() { return &____entriesTable_3; }
	inline void set__entriesTable_3(Hashtable_t909839986 * value)
	{
		____entriesTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____entriesTable_3), value);
	}

	inline static int32_t get_offset_of__nullKeyEntry_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____nullKeyEntry_4)); }
	inline NameObjectEntry_t4229094479 * get__nullKeyEntry_4() const { return ____nullKeyEntry_4; }
	inline NameObjectEntry_t4229094479 ** get_address_of__nullKeyEntry_4() { return &____nullKeyEntry_4; }
	inline void set__nullKeyEntry_4(NameObjectEntry_t4229094479 * value)
	{
		____nullKeyEntry_4 = value;
		Il2CppCodeGenWriteBarrier((&____nullKeyEntry_4), value);
	}

	inline static int32_t get_offset_of__keys_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____keys_5)); }
	inline KeysCollection_t633582367 * get__keys_5() const { return ____keys_5; }
	inline KeysCollection_t633582367 ** get_address_of__keys_5() { return &____keys_5; }
	inline void set__keys_5(KeysCollection_t633582367 * value)
	{
		____keys_5 = value;
		Il2CppCodeGenWriteBarrier((&____keys_5), value);
	}

	inline static int32_t get_offset_of__serializationInfo_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____serializationInfo_6)); }
	inline SerializationInfo_t228987430 * get__serializationInfo_6() const { return ____serializationInfo_6; }
	inline SerializationInfo_t228987430 ** get_address_of__serializationInfo_6() { return &____serializationInfo_6; }
	inline void set__serializationInfo_6(SerializationInfo_t228987430 * value)
	{
		____serializationInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____serializationInfo_6), value);
	}

	inline static int32_t get_offset_of__version_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____version_7)); }
	inline int32_t get__version_7() const { return ____version_7; }
	inline int32_t* get_address_of__version_7() { return &____version_7; }
	inline void set__version_7(int32_t value)
	{
		____version_7 = value;
	}

	inline static int32_t get_offset_of__syncRoot_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ____syncRoot_8)); }
	inline RuntimeObject * get__syncRoot_8() const { return ____syncRoot_8; }
	inline RuntimeObject ** get_address_of__syncRoot_8() { return &____syncRoot_8; }
	inline void set__syncRoot_8(RuntimeObject * value)
	{
		____syncRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_8), value);
	}
};

struct NameObjectCollectionBase_t2034248631_StaticFields
{
public:
	// System.StringComparer System.Collections.Specialized.NameObjectCollectionBase::defaultComparer
	StringComparer_t1574862926 * ___defaultComparer_9;

public:
	inline static int32_t get_offset_of_defaultComparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631_StaticFields, ___defaultComparer_9)); }
	inline StringComparer_t1574862926 * get_defaultComparer_9() const { return ___defaultComparer_9; }
	inline StringComparer_t1574862926 ** get_address_of_defaultComparer_9() { return &___defaultComparer_9; }
	inline void set_defaultComparer_9(StringComparer_t1574862926 * value)
	{
		___defaultComparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultComparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#ifndef CONFIGINFO_T546730838_H
#define CONFIGINFO_T546730838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfo
struct  ConfigInfo_t546730838  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigInfo::Name
	String_t* ___Name_0;
	// System.String System.Configuration.ConfigInfo::TypeName
	String_t* ___TypeName_1;
	// System.Type System.Configuration.ConfigInfo::Type
	Type_t * ___Type_2;
	// System.String System.Configuration.ConfigInfo::streamName
	String_t* ___streamName_3;
	// System.Configuration.ConfigInfo System.Configuration.ConfigInfo::Parent
	ConfigInfo_t546730838 * ___Parent_4;
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.ConfigInfo::ConfigHost
	RuntimeObject* ___ConfigHost_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ConfigInfo_t546730838, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(ConfigInfo_t546730838, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(ConfigInfo_t546730838, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}

	inline static int32_t get_offset_of_streamName_3() { return static_cast<int32_t>(offsetof(ConfigInfo_t546730838, ___streamName_3)); }
	inline String_t* get_streamName_3() const { return ___streamName_3; }
	inline String_t** get_address_of_streamName_3() { return &___streamName_3; }
	inline void set_streamName_3(String_t* value)
	{
		___streamName_3 = value;
		Il2CppCodeGenWriteBarrier((&___streamName_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(ConfigInfo_t546730838, ___Parent_4)); }
	inline ConfigInfo_t546730838 * get_Parent_4() const { return ___Parent_4; }
	inline ConfigInfo_t546730838 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(ConfigInfo_t546730838 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_ConfigHost_5() { return static_cast<int32_t>(offsetof(ConfigInfo_t546730838, ___ConfigHost_5)); }
	inline RuntimeObject* get_ConfigHost_5() const { return ___ConfigHost_5; }
	inline RuntimeObject** get_address_of_ConfigHost_5() { return &___ConfigHost_5; }
	inline void set_ConfigHost_5(RuntimeObject* value)
	{
		___ConfigHost_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigHost_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFO_T546730838_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef MONOTLSPROVIDER_T823784021_H
#define MONOTLSPROVIDER_T823784021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoTlsProvider
struct  MonoTlsProvider_t823784021  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDER_T823784021_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef DNSPACKET_T413343383_H
#define DNSPACKET_T413343383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsPacket
struct  DnsPacket_t413343383  : public RuntimeObject
{
public:
	// System.Byte[] Mono.Net.Dns.DnsPacket::packet
	ByteU5BU5D_t3397334013* ___packet_0;
	// System.Int32 Mono.Net.Dns.DnsPacket::position
	int32_t ___position_1;
	// Mono.Net.Dns.DnsHeader Mono.Net.Dns.DnsPacket::header
	DnsHeader_t2481892084 * ___header_2;

public:
	inline static int32_t get_offset_of_packet_0() { return static_cast<int32_t>(offsetof(DnsPacket_t413343383, ___packet_0)); }
	inline ByteU5BU5D_t3397334013* get_packet_0() const { return ___packet_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_packet_0() { return &___packet_0; }
	inline void set_packet_0(ByteU5BU5D_t3397334013* value)
	{
		___packet_0 = value;
		Il2CppCodeGenWriteBarrier((&___packet_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(DnsPacket_t413343383, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_header_2() { return static_cast<int32_t>(offsetof(DnsPacket_t413343383, ___header_2)); }
	inline DnsHeader_t2481892084 * get_header_2() const { return ___header_2; }
	inline DnsHeader_t2481892084 ** get_address_of_header_2() { return &___header_2; }
	inline void set_header_2(DnsHeader_t2481892084 * value)
	{
		___header_2 = value;
		Il2CppCodeGenWriteBarrier((&___header_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSPACKET_T413343383_H
#ifndef MONOTLSPROVIDERWRAPPER_T210206708_H
#define MONOTLSPROVIDERWRAPPER_T210206708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoTlsProviderWrapper
struct  MonoTlsProviderWrapper_t210206708  : public RuntimeObject
{
public:
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.Private.MonoTlsProviderWrapper::provider
	MonoTlsProvider_t823784021 * ___provider_0;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderWrapper_t210206708, ___provider_0)); }
	inline MonoTlsProvider_t823784021 * get_provider_0() const { return ___provider_0; }
	inline MonoTlsProvider_t823784021 ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(MonoTlsProvider_t823784021 * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___provider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDERWRAPPER_T210206708_H
#ifndef MONOSSLSTREAMWRAPPER_T4202517786_H
#define MONOSSLSTREAMWRAPPER_T4202517786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.MonoSslStreamWrapper
struct  MonoSslStreamWrapper_t4202517786  : public RuntimeObject
{
public:
	// Mono.Security.Interface.IMonoSslStream Mono.Net.Security.Private.MonoSslStreamWrapper::impl
	RuntimeObject* ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(MonoSslStreamWrapper_t4202517786, ___impl_0)); }
	inline RuntimeObject* get_impl_0() const { return ___impl_0; }
	inline RuntimeObject** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(RuntimeObject* value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLSTREAMWRAPPER_T4202517786_H
#ifndef DNSUTIL_T3812046859_H
#define DNSUTIL_T3812046859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsUtil
struct  DnsUtil_t3812046859  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSUTIL_T3812046859_H
#ifndef CONFIGURATIONELEMENT_T1776195828_H
#define CONFIGURATIONELEMENT_T1776195828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_t1776195828  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationElement::rawXml
	String_t* ___rawXml_0;
	// System.Boolean System.Configuration.ConfigurationElement::modified
	bool ___modified_1;
	// System.Configuration.ElementMap System.Configuration.ConfigurationElement::map
	ElementMap_t997038224 * ___map_2;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::keyProps
	ConfigurationPropertyCollection_t3473514151 * ___keyProps_3;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElement::defaultCollection
	ConfigurationElementCollection_t1911180302 * ___defaultCollection_4;
	// System.Boolean System.Configuration.ConfigurationElement::readOnly
	bool ___readOnly_5;
	// System.Configuration.ElementInformation System.Configuration.ConfigurationElement::elementInfo
	ElementInformation_t3165583784 * ___elementInfo_6;
	// System.Configuration.Configuration System.Configuration.ConfigurationElement::_configuration
	Configuration_t3335372970 * ____configuration_7;
	// System.Boolean System.Configuration.ConfigurationElement::elementPresent
	bool ___elementPresent_8;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllAttributesExcept
	ConfigurationLockCollection_t1011762925 * ___lockAllAttributesExcept_9;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllElementsExcept
	ConfigurationLockCollection_t1011762925 * ___lockAllElementsExcept_10;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAttributes
	ConfigurationLockCollection_t1011762925 * ___lockAttributes_11;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockElements
	ConfigurationLockCollection_t1011762925 * ___lockElements_12;
	// System.Boolean System.Configuration.ConfigurationElement::lockItem
	bool ___lockItem_13;
	// System.Configuration.ConfigurationElement/SaveContext System.Configuration.ConfigurationElement::saveContext
	SaveContext_t3996373180 * ___saveContext_14;

public:
	inline static int32_t get_offset_of_rawXml_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___rawXml_0)); }
	inline String_t* get_rawXml_0() const { return ___rawXml_0; }
	inline String_t** get_address_of_rawXml_0() { return &___rawXml_0; }
	inline void set_rawXml_0(String_t* value)
	{
		___rawXml_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawXml_0), value);
	}

	inline static int32_t get_offset_of_modified_1() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___modified_1)); }
	inline bool get_modified_1() const { return ___modified_1; }
	inline bool* get_address_of_modified_1() { return &___modified_1; }
	inline void set_modified_1(bool value)
	{
		___modified_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___map_2)); }
	inline ElementMap_t997038224 * get_map_2() const { return ___map_2; }
	inline ElementMap_t997038224 ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(ElementMap_t997038224 * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_keyProps_3() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___keyProps_3)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_keyProps_3() const { return ___keyProps_3; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_keyProps_3() { return &___keyProps_3; }
	inline void set_keyProps_3(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___keyProps_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyProps_3), value);
	}

	inline static int32_t get_offset_of_defaultCollection_4() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___defaultCollection_4)); }
	inline ConfigurationElementCollection_t1911180302 * get_defaultCollection_4() const { return ___defaultCollection_4; }
	inline ConfigurationElementCollection_t1911180302 ** get_address_of_defaultCollection_4() { return &___defaultCollection_4; }
	inline void set_defaultCollection_4(ConfigurationElementCollection_t1911180302 * value)
	{
		___defaultCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollection_4), value);
	}

	inline static int32_t get_offset_of_readOnly_5() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___readOnly_5)); }
	inline bool get_readOnly_5() const { return ___readOnly_5; }
	inline bool* get_address_of_readOnly_5() { return &___readOnly_5; }
	inline void set_readOnly_5(bool value)
	{
		___readOnly_5 = value;
	}

	inline static int32_t get_offset_of_elementInfo_6() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___elementInfo_6)); }
	inline ElementInformation_t3165583784 * get_elementInfo_6() const { return ___elementInfo_6; }
	inline ElementInformation_t3165583784 ** get_address_of_elementInfo_6() { return &___elementInfo_6; }
	inline void set_elementInfo_6(ElementInformation_t3165583784 * value)
	{
		___elementInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___elementInfo_6), value);
	}

	inline static int32_t get_offset_of__configuration_7() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ____configuration_7)); }
	inline Configuration_t3335372970 * get__configuration_7() const { return ____configuration_7; }
	inline Configuration_t3335372970 ** get_address_of__configuration_7() { return &____configuration_7; }
	inline void set__configuration_7(Configuration_t3335372970 * value)
	{
		____configuration_7 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_7), value);
	}

	inline static int32_t get_offset_of_elementPresent_8() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___elementPresent_8)); }
	inline bool get_elementPresent_8() const { return ___elementPresent_8; }
	inline bool* get_address_of_elementPresent_8() { return &___elementPresent_8; }
	inline void set_elementPresent_8(bool value)
	{
		___elementPresent_8 = value;
	}

	inline static int32_t get_offset_of_lockAllAttributesExcept_9() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockAllAttributesExcept_9)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockAllAttributesExcept_9() const { return ___lockAllAttributesExcept_9; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockAllAttributesExcept_9() { return &___lockAllAttributesExcept_9; }
	inline void set_lockAllAttributesExcept_9(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockAllAttributesExcept_9 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllAttributesExcept_9), value);
	}

	inline static int32_t get_offset_of_lockAllElementsExcept_10() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockAllElementsExcept_10)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockAllElementsExcept_10() const { return ___lockAllElementsExcept_10; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockAllElementsExcept_10() { return &___lockAllElementsExcept_10; }
	inline void set_lockAllElementsExcept_10(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockAllElementsExcept_10 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllElementsExcept_10), value);
	}

	inline static int32_t get_offset_of_lockAttributes_11() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockAttributes_11)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockAttributes_11() const { return ___lockAttributes_11; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockAttributes_11() { return &___lockAttributes_11; }
	inline void set_lockAttributes_11(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockAttributes_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockAttributes_11), value);
	}

	inline static int32_t get_offset_of_lockElements_12() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockElements_12)); }
	inline ConfigurationLockCollection_t1011762925 * get_lockElements_12() const { return ___lockElements_12; }
	inline ConfigurationLockCollection_t1011762925 ** get_address_of_lockElements_12() { return &___lockElements_12; }
	inline void set_lockElements_12(ConfigurationLockCollection_t1011762925 * value)
	{
		___lockElements_12 = value;
		Il2CppCodeGenWriteBarrier((&___lockElements_12), value);
	}

	inline static int32_t get_offset_of_lockItem_13() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___lockItem_13)); }
	inline bool get_lockItem_13() const { return ___lockItem_13; }
	inline bool* get_address_of_lockItem_13() { return &___lockItem_13; }
	inline void set_lockItem_13(bool value)
	{
		___lockItem_13 = value;
	}

	inline static int32_t get_offset_of_saveContext_14() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1776195828, ___saveContext_14)); }
	inline SaveContext_t3996373180 * get_saveContext_14() const { return ___saveContext_14; }
	inline SaveContext_t3996373180 ** get_address_of_saveContext_14() { return &___saveContext_14; }
	inline void set_saveContext_14(SaveContext_t3996373180 * value)
	{
		___saveContext_14 = value;
		Il2CppCodeGenWriteBarrier((&___saveContext_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T1776195828_H
#ifndef THROWHELPER_T3666395979_H
#define THROWHELPER_T3666395979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ThrowHelper
struct  ThrowHelper_t3666395979  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROWHELPER_T3666395979_H
#ifndef SIMPLERESOLVER_T2933968162_H
#define SIMPLERESOLVER_T2933968162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.SimpleResolver
struct  SimpleResolver_t2933968162  : public RuntimeObject
{
public:
	// System.Net.IPEndPoint[] Mono.Net.Dns.SimpleResolver::endpoints
	IPEndPointU5BU5D_t294524195* ___endpoints_2;
	// System.Net.Sockets.Socket Mono.Net.Dns.SimpleResolver::client
	Socket_t3821512045 * ___client_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,Mono.Net.Dns.SimpleResolverEventArgs> Mono.Net.Dns.SimpleResolver::queries
	Dictionary_2_t3302389772 * ___queries_4;
	// System.AsyncCallback Mono.Net.Dns.SimpleResolver::receive_cb
	AsyncCallback_t163412349 * ___receive_cb_5;
	// System.Threading.TimerCallback Mono.Net.Dns.SimpleResolver::timeout_cb
	TimerCallback_t1684927372 * ___timeout_cb_6;
	// System.Boolean Mono.Net.Dns.SimpleResolver::disposed
	bool ___disposed_7;

public:
	inline static int32_t get_offset_of_endpoints_2() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___endpoints_2)); }
	inline IPEndPointU5BU5D_t294524195* get_endpoints_2() const { return ___endpoints_2; }
	inline IPEndPointU5BU5D_t294524195** get_address_of_endpoints_2() { return &___endpoints_2; }
	inline void set_endpoints_2(IPEndPointU5BU5D_t294524195* value)
	{
		___endpoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___endpoints_2), value);
	}

	inline static int32_t get_offset_of_client_3() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___client_3)); }
	inline Socket_t3821512045 * get_client_3() const { return ___client_3; }
	inline Socket_t3821512045 ** get_address_of_client_3() { return &___client_3; }
	inline void set_client_3(Socket_t3821512045 * value)
	{
		___client_3 = value;
		Il2CppCodeGenWriteBarrier((&___client_3), value);
	}

	inline static int32_t get_offset_of_queries_4() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___queries_4)); }
	inline Dictionary_2_t3302389772 * get_queries_4() const { return ___queries_4; }
	inline Dictionary_2_t3302389772 ** get_address_of_queries_4() { return &___queries_4; }
	inline void set_queries_4(Dictionary_2_t3302389772 * value)
	{
		___queries_4 = value;
		Il2CppCodeGenWriteBarrier((&___queries_4), value);
	}

	inline static int32_t get_offset_of_receive_cb_5() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___receive_cb_5)); }
	inline AsyncCallback_t163412349 * get_receive_cb_5() const { return ___receive_cb_5; }
	inline AsyncCallback_t163412349 ** get_address_of_receive_cb_5() { return &___receive_cb_5; }
	inline void set_receive_cb_5(AsyncCallback_t163412349 * value)
	{
		___receive_cb_5 = value;
		Il2CppCodeGenWriteBarrier((&___receive_cb_5), value);
	}

	inline static int32_t get_offset_of_timeout_cb_6() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___timeout_cb_6)); }
	inline TimerCallback_t1684927372 * get_timeout_cb_6() const { return ___timeout_cb_6; }
	inline TimerCallback_t1684927372 ** get_address_of_timeout_cb_6() { return &___timeout_cb_6; }
	inline void set_timeout_cb_6(TimerCallback_t1684927372 * value)
	{
		___timeout_cb_6 = value;
		Il2CppCodeGenWriteBarrier((&___timeout_cb_6), value);
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}
};

struct SimpleResolver_t2933968162_StaticFields
{
public:
	// System.String[] Mono.Net.Dns.SimpleResolver::EmptyStrings
	StringU5BU5D_t1642385972* ___EmptyStrings_0;
	// System.Net.IPAddress[] Mono.Net.Dns.SimpleResolver::EmptyAddresses
	IPAddressU5BU5D_t4087230954* ___EmptyAddresses_1;

public:
	inline static int32_t get_offset_of_EmptyStrings_0() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162_StaticFields, ___EmptyStrings_0)); }
	inline StringU5BU5D_t1642385972* get_EmptyStrings_0() const { return ___EmptyStrings_0; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyStrings_0() { return &___EmptyStrings_0; }
	inline void set_EmptyStrings_0(StringU5BU5D_t1642385972* value)
	{
		___EmptyStrings_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyStrings_0), value);
	}

	inline static int32_t get_offset_of_EmptyAddresses_1() { return static_cast<int32_t>(offsetof(SimpleResolver_t2933968162_StaticFields, ___EmptyAddresses_1)); }
	inline IPAddressU5BU5D_t4087230954* get_EmptyAddresses_1() const { return ___EmptyAddresses_1; }
	inline IPAddressU5BU5D_t4087230954** get_address_of_EmptyAddresses_1() { return &___EmptyAddresses_1; }
	inline void set_EmptyAddresses_1(IPAddressU5BU5D_t4087230954* value)
	{
		___EmptyAddresses_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAddresses_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLERESOLVER_T2933968162_H
#ifndef ELEMENTINFORMATION_T3165583784_H
#define ELEMENTINFORMATION_T3165583784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ElementInformation
struct  ElementInformation_t3165583784  : public RuntimeObject
{
public:
	// System.Configuration.PropertyInformation System.Configuration.ElementInformation::propertyInfo
	PropertyInformation_t2089433965 * ___propertyInfo_0;
	// System.Configuration.ConfigurationElement System.Configuration.ElementInformation::owner
	ConfigurationElement_t1776195828 * ___owner_1;
	// System.Configuration.PropertyInformationCollection System.Configuration.ElementInformation::properties
	PropertyInformationCollection_t954922393 * ___properties_2;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(ElementInformation_t3165583784, ___propertyInfo_0)); }
	inline PropertyInformation_t2089433965 * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInformation_t2089433965 ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInformation_t2089433965 * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(ElementInformation_t3165583784, ___owner_1)); }
	inline ConfigurationElement_t1776195828 * get_owner_1() const { return ___owner_1; }
	inline ConfigurationElement_t1776195828 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(ConfigurationElement_t1776195828 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier((&___owner_1), value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ElementInformation_t3165583784, ___properties_2)); }
	inline PropertyInformationCollection_t954922393 * get_properties_2() const { return ___properties_2; }
	inline PropertyInformationCollection_t954922393 ** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(PropertyInformationCollection_t954922393 * value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINFORMATION_T3165583784_H
#ifndef INTERNALCONFIGURATIONFACTORY_T3846641927_H
#define INTERNALCONFIGURATIONFACTORY_T3846641927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationFactory
struct  InternalConfigurationFactory_t3846641927  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONFACTORY_T3846641927_H
#ifndef CONFIGURATIONSECTIONGROUP_T2230982736_H
#define CONFIGURATIONSECTIONGROUP_T2230982736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroup
struct  ConfigurationSectionGroup_t2230982736  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionGroup::sections
	ConfigurationSectionCollection_t4261113299 * ___sections_0;
	// System.Configuration.ConfigurationSectionGroupCollection System.Configuration.ConfigurationSectionGroup::groups
	ConfigurationSectionGroupCollection_t575145286 * ___groups_1;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroup::config
	Configuration_t3335372970 * ___config_2;
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroup::group
	SectionGroupInfo_t2346323570 * ___group_3;
	// System.Boolean System.Configuration.ConfigurationSectionGroup::initialized
	bool ___initialized_4;

public:
	inline static int32_t get_offset_of_sections_0() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t2230982736, ___sections_0)); }
	inline ConfigurationSectionCollection_t4261113299 * get_sections_0() const { return ___sections_0; }
	inline ConfigurationSectionCollection_t4261113299 ** get_address_of_sections_0() { return &___sections_0; }
	inline void set_sections_0(ConfigurationSectionCollection_t4261113299 * value)
	{
		___sections_0 = value;
		Il2CppCodeGenWriteBarrier((&___sections_0), value);
	}

	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t2230982736, ___groups_1)); }
	inline ConfigurationSectionGroupCollection_t575145286 * get_groups_1() const { return ___groups_1; }
	inline ConfigurationSectionGroupCollection_t575145286 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(ConfigurationSectionGroupCollection_t575145286 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier((&___groups_1), value);
	}

	inline static int32_t get_offset_of_config_2() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t2230982736, ___config_2)); }
	inline Configuration_t3335372970 * get_config_2() const { return ___config_2; }
	inline Configuration_t3335372970 ** get_address_of_config_2() { return &___config_2; }
	inline void set_config_2(Configuration_t3335372970 * value)
	{
		___config_2 = value;
		Il2CppCodeGenWriteBarrier((&___config_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t2230982736, ___group_3)); }
	inline SectionGroupInfo_t2346323570 * get_group_3() const { return ___group_3; }
	inline SectionGroupInfo_t2346323570 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(SectionGroupInfo_t2346323570 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_initialized_4() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t2230982736, ___initialized_4)); }
	inline bool get_initialized_4() const { return ___initialized_4; }
	inline bool* get_address_of_initialized_4() { return &___initialized_4; }
	inline void set_initialized_4(bool value)
	{
		___initialized_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUP_T2230982736_H
#ifndef PROPERTYINFORMATIONENUMERATOR_T1453501302_H
#define PROPERTYINFORMATIONENUMERATOR_T1453501302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformationCollection/PropertyInformationEnumerator
struct  PropertyInformationEnumerator_t1453501302  : public RuntimeObject
{
public:
	// System.Configuration.PropertyInformationCollection System.Configuration.PropertyInformationCollection/PropertyInformationEnumerator::collection
	PropertyInformationCollection_t954922393 * ___collection_0;
	// System.Int32 System.Configuration.PropertyInformationCollection/PropertyInformationEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(PropertyInformationEnumerator_t1453501302, ___collection_0)); }
	inline PropertyInformationCollection_t954922393 * get_collection_0() const { return ___collection_0; }
	inline PropertyInformationCollection_t954922393 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(PropertyInformationCollection_t954922393 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(PropertyInformationEnumerator_t1453501302, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATIONENUMERATOR_T1453501302_H
#ifndef INTERNALCONFIGURATIONROOT_T547578517_H
#define INTERNALCONFIGURATIONROOT_T547578517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationRoot
struct  InternalConfigurationRoot_t547578517  : public RuntimeObject
{
public:
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.InternalConfigurationRoot::host
	RuntimeObject* ___host_0;
	// System.Boolean System.Configuration.InternalConfigurationRoot::isDesignTime
	bool ___isDesignTime_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(InternalConfigurationRoot_t547578517, ___host_0)); }
	inline RuntimeObject* get_host_0() const { return ___host_0; }
	inline RuntimeObject** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(RuntimeObject* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_isDesignTime_1() { return static_cast<int32_t>(offsetof(InternalConfigurationRoot_t547578517, ___isDesignTime_1)); }
	inline bool get_isDesignTime_1() const { return ___isDesignTime_1; }
	inline bool* get_address_of_isDesignTime_1() { return &___isDesignTime_1; }
	inline void set_isDesignTime_1(bool value)
	{
		___isDesignTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONROOT_T547578517_H
#ifndef PROTECTEDCONFIGURATION_T1807950812_H
#define PROTECTEDCONFIGURATION_T1807950812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfiguration
struct  ProtectedConfiguration_t1807950812  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATION_T1807950812_H
#ifndef INTERNALCONFIGURATIONSYSTEM_T2108740756_H
#define INTERNALCONFIGURATIONSYSTEM_T2108740756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationSystem
struct  InternalConfigurationSystem_t2108740756  : public RuntimeObject
{
public:
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.InternalConfigurationSystem::host
	RuntimeObject* ___host_0;
	// System.Configuration.Internal.IInternalConfigRoot System.Configuration.InternalConfigurationSystem::root
	RuntimeObject* ___root_1;
	// System.Object[] System.Configuration.InternalConfigurationSystem::hostInitParams
	ObjectU5BU5D_t3614634134* ___hostInitParams_2;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_t2108740756, ___host_0)); }
	inline RuntimeObject* get_host_0() const { return ___host_0; }
	inline RuntimeObject** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(RuntimeObject* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_t2108740756, ___root_1)); }
	inline RuntimeObject* get_root_1() const { return ___root_1; }
	inline RuntimeObject** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(RuntimeObject* value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_hostInitParams_2() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_t2108740756, ___hostInitParams_2)); }
	inline ObjectU5BU5D_t3614634134* get_hostInitParams_2() const { return ___hostInitParams_2; }
	inline ObjectU5BU5D_t3614634134** get_address_of_hostInitParams_2() { return &___hostInitParams_2; }
	inline void set_hostInitParams_2(ObjectU5BU5D_t3614634134* value)
	{
		___hostInitParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostInitParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONSYSTEM_T2108740756_H
#ifndef INTERNALCONFIGURATIONHOST_T547577555_H
#define INTERNALCONFIGURATIONHOST_T547577555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationHost
struct  InternalConfigurationHost_t547577555  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONHOST_T547577555_H
#ifndef U3CGETENUMERATORU3ED__17_T2724409453_H
#define U3CGETENUMERATORU3ED__17_T2724409453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionCollection/<GetEnumerator>d__17
struct  U3CGetEnumeratorU3Ed__17_t2724409453  : public RuntimeObject
{
public:
	// System.Int32 System.Configuration.ConfigurationSectionCollection/<GetEnumerator>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object System.Configuration.ConfigurationSectionCollection/<GetEnumerator>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionCollection/<GetEnumerator>d__17::<>4__this
	ConfigurationSectionCollection_t4261113299 * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator System.Configuration.ConfigurationSectionCollection/<GetEnumerator>d__17::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__17_t2724409453, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__17_t2724409453, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__17_t2724409453, ___U3CU3E4__this_2)); }
	inline ConfigurationSectionCollection_t4261113299 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ConfigurationSectionCollection_t4261113299 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ConfigurationSectionCollection_t4261113299 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__17_t2724409453, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__17_T2724409453_H
#ifndef MONOTLSPROVIDERFACTORY_T3928617369_H
#define MONOTLSPROVIDERFACTORY_T3928617369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsProviderFactory
struct  MonoTlsProviderFactory_t3928617369  : public RuntimeObject
{
public:

public:
};

struct MonoTlsProviderFactory_t3928617369_StaticFields
{
public:
	// System.Object Mono.Net.Security.MonoTlsProviderFactory::locker
	RuntimeObject * ___locker_0;
	// System.Boolean Mono.Net.Security.MonoTlsProviderFactory::initialized
	bool ___initialized_1;
	// Mono.Net.Security.IMonoTlsProvider Mono.Net.Security.MonoTlsProviderFactory::defaultProvider
	RuntimeObject* ___defaultProvider_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mono.Net.Security.MonoTlsProviderFactory::providerRegistration
	Dictionary_2_t3943999495 * ___providerRegistration_3;

public:
	inline static int32_t get_offset_of_locker_0() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___locker_0)); }
	inline RuntimeObject * get_locker_0() const { return ___locker_0; }
	inline RuntimeObject ** get_address_of_locker_0() { return &___locker_0; }
	inline void set_locker_0(RuntimeObject * value)
	{
		___locker_0 = value;
		Il2CppCodeGenWriteBarrier((&___locker_0), value);
	}

	inline static int32_t get_offset_of_initialized_1() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___initialized_1)); }
	inline bool get_initialized_1() const { return ___initialized_1; }
	inline bool* get_address_of_initialized_1() { return &___initialized_1; }
	inline void set_initialized_1(bool value)
	{
		___initialized_1 = value;
	}

	inline static int32_t get_offset_of_defaultProvider_2() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___defaultProvider_2)); }
	inline RuntimeObject* get_defaultProvider_2() const { return ___defaultProvider_2; }
	inline RuntimeObject** get_address_of_defaultProvider_2() { return &___defaultProvider_2; }
	inline void set_defaultProvider_2(RuntimeObject* value)
	{
		___defaultProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultProvider_2), value);
	}

	inline static int32_t get_offset_of_providerRegistration_3() { return static_cast<int32_t>(offsetof(MonoTlsProviderFactory_t3928617369_StaticFields, ___providerRegistration_3)); }
	inline Dictionary_2_t3943999495 * get_providerRegistration_3() const { return ___providerRegistration_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_providerRegistration_3() { return &___providerRegistration_3; }
	inline void set_providerRegistration_3(Dictionary_2_t3943999495 * value)
	{
		___providerRegistration_3 = value;
		Il2CppCodeGenWriteBarrier((&___providerRegistration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSPROVIDERFACTORY_T3928617369_H
#ifndef CONFIGURATIONVALIDATORBASE_T210547623_H
#define CONFIGURATIONVALIDATORBASE_T210547623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationValidatorBase
struct  ConfigurationValidatorBase_t210547623  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVALIDATORBASE_T210547623_H
#ifndef DNSQUERY_T3483920911_H
#define DNSQUERY_T3483920911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQuery
struct  DnsQuery_t3483920911  : public DnsPacket_t413343383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQUERY_T3483920911_H
#ifndef CONFIGURATIONSECTIONCOLLECTION_T4261113299_H
#define CONFIGURATIONSECTIONCOLLECTION_T4261113299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionCollection
struct  ConfigurationSectionCollection_t4261113299  : public NameObjectCollectionBase_t2034248631
{
public:
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionCollection::group
	SectionGroupInfo_t2346323570 * ___group_10;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionCollection::config
	Configuration_t3335372970 * ___config_11;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t4261113299, ___group_10)); }
	inline SectionGroupInfo_t2346323570 * get_group_10() const { return ___group_10; }
	inline SectionGroupInfo_t2346323570 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(SectionGroupInfo_t2346323570 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier((&___group_10), value);
	}

	inline static int32_t get_offset_of_config_11() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t4261113299, ___config_11)); }
	inline Configuration_t3335372970 * get_config_11() const { return ___config_11; }
	inline Configuration_t3335372970 ** get_address_of_config_11() { return &___config_11; }
	inline void set_config_11(Configuration_t3335372970 * value)
	{
		___config_11 = value;
		Il2CppCodeGenWriteBarrier((&___config_11), value);
	}
};

struct ConfigurationSectionCollection_t4261113299_StaticFields
{
public:
	// System.Object System.Configuration.ConfigurationSectionCollection::lockObject
	RuntimeObject * ___lockObject_12;

public:
	inline static int32_t get_offset_of_lockObject_12() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t4261113299_StaticFields, ___lockObject_12)); }
	inline RuntimeObject * get_lockObject_12() const { return ___lockObject_12; }
	inline RuntimeObject ** get_address_of_lockObject_12() { return &___lockObject_12; }
	inline void set_lockObject_12(RuntimeObject * value)
	{
		___lockObject_12 = value;
		Il2CppCodeGenWriteBarrier((&___lockObject_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONCOLLECTION_T4261113299_H
#ifndef CONFIGURATIONSECTION_T2600766927_H
#define CONFIGURATIONSECTION_T2600766927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSection
struct  ConfigurationSection_t2600766927  : public ConfigurationElement_t1776195828
{
public:
	// System.Configuration.SectionInformation System.Configuration.ConfigurationSection::sectionInformation
	SectionInformation_t2754609709 * ___sectionInformation_15;
	// System.Configuration.IConfigurationSectionHandler System.Configuration.ConfigurationSection::section_handler
	RuntimeObject* ___section_handler_16;
	// System.String System.Configuration.ConfigurationSection::externalDataXml
	String_t* ___externalDataXml_17;
	// System.Object System.Configuration.ConfigurationSection::_configContext
	RuntimeObject * ____configContext_18;

public:
	inline static int32_t get_offset_of_sectionInformation_15() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ___sectionInformation_15)); }
	inline SectionInformation_t2754609709 * get_sectionInformation_15() const { return ___sectionInformation_15; }
	inline SectionInformation_t2754609709 ** get_address_of_sectionInformation_15() { return &___sectionInformation_15; }
	inline void set_sectionInformation_15(SectionInformation_t2754609709 * value)
	{
		___sectionInformation_15 = value;
		Il2CppCodeGenWriteBarrier((&___sectionInformation_15), value);
	}

	inline static int32_t get_offset_of_section_handler_16() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ___section_handler_16)); }
	inline RuntimeObject* get_section_handler_16() const { return ___section_handler_16; }
	inline RuntimeObject** get_address_of_section_handler_16() { return &___section_handler_16; }
	inline void set_section_handler_16(RuntimeObject* value)
	{
		___section_handler_16 = value;
		Il2CppCodeGenWriteBarrier((&___section_handler_16), value);
	}

	inline static int32_t get_offset_of_externalDataXml_17() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ___externalDataXml_17)); }
	inline String_t* get_externalDataXml_17() const { return ___externalDataXml_17; }
	inline String_t** get_address_of_externalDataXml_17() { return &___externalDataXml_17; }
	inline void set_externalDataXml_17(String_t* value)
	{
		___externalDataXml_17 = value;
		Il2CppCodeGenWriteBarrier((&___externalDataXml_17), value);
	}

	inline static int32_t get_offset_of__configContext_18() { return static_cast<int32_t>(offsetof(ConfigurationSection_t2600766927, ____configContext_18)); }
	inline RuntimeObject * get__configContext_18() const { return ____configContext_18; }
	inline RuntimeObject ** get_address_of__configContext_18() { return &____configContext_18; }
	inline void set__configContext_18(RuntimeObject * value)
	{
		____configContext_18 = value;
		Il2CppCodeGenWriteBarrier((&____configContext_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTION_T2600766927_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_4;
	// System.Int16 System.Guid::_b
	int16_t ____b_5;
	// System.Int16 System.Guid::_c
	int16_t ____c_6;
	// System.Byte System.Guid::_d
	uint8_t ____d_7;
	// System.Byte System.Guid::_e
	uint8_t ____e_8;
	// System.Byte System.Guid::_f
	uint8_t ____f_9;
	// System.Byte System.Guid::_g
	uint8_t ____g_10;
	// System.Byte System.Guid::_h
	uint8_t ____h_11;
	// System.Byte System.Guid::_i
	uint8_t ____i_12;
	// System.Byte System.Guid::_j
	uint8_t ____j_13;
	// System.Byte System.Guid::_k
	uint8_t ____k_14;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Guid_t, ____a_4)); }
	inline int32_t get__a_4() const { return ____a_4; }
	inline int32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(int32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(Guid_t, ____b_5)); }
	inline int16_t get__b_5() const { return ____b_5; }
	inline int16_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(int16_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(Guid_t, ____c_6)); }
	inline int16_t get__c_6() const { return ____c_6; }
	inline int16_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(int16_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(Guid_t, ____d_7)); }
	inline uint8_t get__d_7() const { return ____d_7; }
	inline uint8_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint8_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__e_8() { return static_cast<int32_t>(offsetof(Guid_t, ____e_8)); }
	inline uint8_t get__e_8() const { return ____e_8; }
	inline uint8_t* get_address_of__e_8() { return &____e_8; }
	inline void set__e_8(uint8_t value)
	{
		____e_8 = value;
	}

	inline static int32_t get_offset_of__f_9() { return static_cast<int32_t>(offsetof(Guid_t, ____f_9)); }
	inline uint8_t get__f_9() const { return ____f_9; }
	inline uint8_t* get_address_of__f_9() { return &____f_9; }
	inline void set__f_9(uint8_t value)
	{
		____f_9 = value;
	}

	inline static int32_t get_offset_of__g_10() { return static_cast<int32_t>(offsetof(Guid_t, ____g_10)); }
	inline uint8_t get__g_10() const { return ____g_10; }
	inline uint8_t* get_address_of__g_10() { return &____g_10; }
	inline void set__g_10(uint8_t value)
	{
		____g_10 = value;
	}

	inline static int32_t get_offset_of__h_11() { return static_cast<int32_t>(offsetof(Guid_t, ____h_11)); }
	inline uint8_t get__h_11() const { return ____h_11; }
	inline uint8_t* get_address_of__h_11() { return &____h_11; }
	inline void set__h_11(uint8_t value)
	{
		____h_11 = value;
	}

	inline static int32_t get_offset_of__i_12() { return static_cast<int32_t>(offsetof(Guid_t, ____i_12)); }
	inline uint8_t get__i_12() const { return ____i_12; }
	inline uint8_t* get_address_of__i_12() { return &____i_12; }
	inline void set__i_12(uint8_t value)
	{
		____i_12 = value;
	}

	inline static int32_t get_offset_of__j_13() { return static_cast<int32_t>(offsetof(Guid_t, ____j_13)); }
	inline uint8_t get__j_13() const { return ____j_13; }
	inline uint8_t* get_address_of__j_13() { return &____j_13; }
	inline void set__j_13(uint8_t value)
	{
		____j_13 = value;
	}

	inline static int32_t get_offset_of__k_14() { return static_cast<int32_t>(offsetof(Guid_t, ____k_14)); }
	inline uint8_t get__k_14() const { return ____k_14; }
	inline uint8_t* get_address_of__k_14() { return &____k_14; }
	inline void set__k_14(uint8_t value)
	{
		____k_14 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_1;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2510243513 * ____fastRng_2;
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_3;

public:
	inline static int32_t get_offset_of__rngAccess_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_0)); }
	inline RuntimeObject * get__rngAccess_0() const { return ____rngAccess_0; }
	inline RuntimeObject ** get_address_of__rngAccess_0() { return &____rngAccess_0; }
	inline void set__rngAccess_0(RuntimeObject * value)
	{
		____rngAccess_0 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_0), value);
	}

	inline static int32_t get_offset_of__rng_1() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_1)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_1() const { return ____rng_1; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_1() { return &____rng_1; }
	inline void set__rng_1(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_1 = value;
		Il2CppCodeGenWriteBarrier((&____rng_1), value);
	}

	inline static int32_t get_offset_of__fastRng_2() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_2)); }
	inline RandomNumberGenerator_t2510243513 * get__fastRng_2() const { return ____fastRng_2; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__fastRng_2() { return &____fastRng_2; }
	inline void set__fastRng_2(RandomNumberGenerator_t2510243513 * value)
	{
		____fastRng_2 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_2), value);
	}

	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_3)); }
	inline Guid_t  get_Empty_3() const { return ___Empty_3; }
	inline Guid_t * get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(Guid_t  value)
	{
		___Empty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public MarshalByRefObject_t1285298191
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t2745753060 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t461808439 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t2745753060 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t2745753060 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t2745753060 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t3255436806, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t461808439 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t461808439 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t461808439 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_1)); }
	inline Stream_t3255436806 * get_Null_1() const { return ___Null_1; }
	inline Stream_t3255436806 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t3255436806 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef PROPERTYINFORMATIONCOLLECTION_T954922393_H
#define PROPERTYINFORMATIONCOLLECTION_T954922393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformationCollection
struct  PropertyInformationCollection_t954922393  : public NameObjectCollectionBase_t2034248631
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATIONCOLLECTION_T954922393_H
#ifndef ARRAYSEGMENT_1_T2594217482_H
#define ARRAYSEGMENT_1_T2594217482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArraySegment`1<System.Byte>
struct  ArraySegment_1_t2594217482 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_t3397334013* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t2594217482, ____array_0)); }
	inline ByteU5BU5D_t3397334013* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_t3397334013* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t2594217482, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t2594217482, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSEGMENT_1_T2594217482_H
#ifndef CONFIGURATIONSECTIONGROUPCOLLECTION_T575145286_H
#define CONFIGURATIONSECTIONGROUPCOLLECTION_T575145286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroupCollection
struct  ConfigurationSectionGroupCollection_t575145286  : public NameObjectCollectionBase_t2034248631
{
public:
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroupCollection::group
	SectionGroupInfo_t2346323570 * ___group_10;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroupCollection::config
	Configuration_t3335372970 * ___config_11;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroupCollection_t575145286, ___group_10)); }
	inline SectionGroupInfo_t2346323570 * get_group_10() const { return ___group_10; }
	inline SectionGroupInfo_t2346323570 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(SectionGroupInfo_t2346323570 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier((&___group_10), value);
	}

	inline static int32_t get_offset_of_config_11() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroupCollection_t575145286, ___config_11)); }
	inline Configuration_t3335372970 * get_config_11() const { return ___config_11; }
	inline Configuration_t3335372970 ** get_address_of_config_11() { return &___config_11; }
	inline void set_config_11(Configuration_t3335372970 * value)
	{
		___config_11 = value;
		Il2CppCodeGenWriteBarrier((&___config_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUPCOLLECTION_T575145286_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t1328083999* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t1328083999* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t1328083999** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t1328083999* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CONFIGURATIONELEMENTCOLLECTION_T1911180302_H
#define CONFIGURATIONELEMENTCOLLECTION_T1911180302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection
struct  ConfigurationElementCollection_t1911180302  : public ConfigurationElement_t1776195828
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::list
	ArrayList_t4252133567 * ___list_15;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::removed
	ArrayList_t4252133567 * ___removed_16;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::inherited
	ArrayList_t4252133567 * ___inherited_17;
	// System.Boolean System.Configuration.ConfigurationElementCollection::emitClear
	bool ___emitClear_18;
	// System.Boolean System.Configuration.ConfigurationElementCollection::modified
	bool ___modified_19;
	// System.Collections.IComparer System.Configuration.ConfigurationElementCollection::comparer
	RuntimeObject* ___comparer_20;
	// System.Int32 System.Configuration.ConfigurationElementCollection::inheritedLimitIndex
	int32_t ___inheritedLimitIndex_21;
	// System.String System.Configuration.ConfigurationElementCollection::addElementName
	String_t* ___addElementName_22;
	// System.String System.Configuration.ConfigurationElementCollection::clearElementName
	String_t* ___clearElementName_23;
	// System.String System.Configuration.ConfigurationElementCollection::removeElementName
	String_t* ___removeElementName_24;

public:
	inline static int32_t get_offset_of_list_15() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___list_15)); }
	inline ArrayList_t4252133567 * get_list_15() const { return ___list_15; }
	inline ArrayList_t4252133567 ** get_address_of_list_15() { return &___list_15; }
	inline void set_list_15(ArrayList_t4252133567 * value)
	{
		___list_15 = value;
		Il2CppCodeGenWriteBarrier((&___list_15), value);
	}

	inline static int32_t get_offset_of_removed_16() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___removed_16)); }
	inline ArrayList_t4252133567 * get_removed_16() const { return ___removed_16; }
	inline ArrayList_t4252133567 ** get_address_of_removed_16() { return &___removed_16; }
	inline void set_removed_16(ArrayList_t4252133567 * value)
	{
		___removed_16 = value;
		Il2CppCodeGenWriteBarrier((&___removed_16), value);
	}

	inline static int32_t get_offset_of_inherited_17() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___inherited_17)); }
	inline ArrayList_t4252133567 * get_inherited_17() const { return ___inherited_17; }
	inline ArrayList_t4252133567 ** get_address_of_inherited_17() { return &___inherited_17; }
	inline void set_inherited_17(ArrayList_t4252133567 * value)
	{
		___inherited_17 = value;
		Il2CppCodeGenWriteBarrier((&___inherited_17), value);
	}

	inline static int32_t get_offset_of_emitClear_18() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___emitClear_18)); }
	inline bool get_emitClear_18() const { return ___emitClear_18; }
	inline bool* get_address_of_emitClear_18() { return &___emitClear_18; }
	inline void set_emitClear_18(bool value)
	{
		___emitClear_18 = value;
	}

	inline static int32_t get_offset_of_modified_19() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___modified_19)); }
	inline bool get_modified_19() const { return ___modified_19; }
	inline bool* get_address_of_modified_19() { return &___modified_19; }
	inline void set_modified_19(bool value)
	{
		___modified_19 = value;
	}

	inline static int32_t get_offset_of_comparer_20() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___comparer_20)); }
	inline RuntimeObject* get_comparer_20() const { return ___comparer_20; }
	inline RuntimeObject** get_address_of_comparer_20() { return &___comparer_20; }
	inline void set_comparer_20(RuntimeObject* value)
	{
		___comparer_20 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_20), value);
	}

	inline static int32_t get_offset_of_inheritedLimitIndex_21() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___inheritedLimitIndex_21)); }
	inline int32_t get_inheritedLimitIndex_21() const { return ___inheritedLimitIndex_21; }
	inline int32_t* get_address_of_inheritedLimitIndex_21() { return &___inheritedLimitIndex_21; }
	inline void set_inheritedLimitIndex_21(int32_t value)
	{
		___inheritedLimitIndex_21 = value;
	}

	inline static int32_t get_offset_of_addElementName_22() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___addElementName_22)); }
	inline String_t* get_addElementName_22() const { return ___addElementName_22; }
	inline String_t** get_address_of_addElementName_22() { return &___addElementName_22; }
	inline void set_addElementName_22(String_t* value)
	{
		___addElementName_22 = value;
		Il2CppCodeGenWriteBarrier((&___addElementName_22), value);
	}

	inline static int32_t get_offset_of_clearElementName_23() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___clearElementName_23)); }
	inline String_t* get_clearElementName_23() const { return ___clearElementName_23; }
	inline String_t** get_address_of_clearElementName_23() { return &___clearElementName_23; }
	inline void set_clearElementName_23(String_t* value)
	{
		___clearElementName_23 = value;
		Il2CppCodeGenWriteBarrier((&___clearElementName_23), value);
	}

	inline static int32_t get_offset_of_removeElementName_24() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t1911180302, ___removeElementName_24)); }
	inline String_t* get_removeElementName_24() const { return ___removeElementName_24; }
	inline String_t** get_address_of_removeElementName_24() { return &___removeElementName_24; }
	inline void set_removeElementName_24(String_t* value)
	{
		___removeElementName_24 = value;
		Il2CppCodeGenWriteBarrier((&___removeElementName_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTION_T1911180302_H
#ifndef CONFIGURATIONVALIDATORATTRIBUTE_T1007519140_H
#define CONFIGURATIONVALIDATORATTRIBUTE_T1007519140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationValidatorAttribute
struct  ConfigurationValidatorAttribute_t1007519140  : public Attribute_t542643598
{
public:
	// System.Type System.Configuration.ConfigurationValidatorAttribute::validatorType
	Type_t * ___validatorType_0;
	// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationValidatorAttribute::instance
	ConfigurationValidatorBase_t210547623 * ___instance_1;

public:
	inline static int32_t get_offset_of_validatorType_0() { return static_cast<int32_t>(offsetof(ConfigurationValidatorAttribute_t1007519140, ___validatorType_0)); }
	inline Type_t * get_validatorType_0() const { return ___validatorType_0; }
	inline Type_t ** get_address_of_validatorType_0() { return &___validatorType_0; }
	inline void set_validatorType_0(Type_t * value)
	{
		___validatorType_0 = value;
		Il2CppCodeGenWriteBarrier((&___validatorType_0), value);
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(ConfigurationValidatorAttribute_t1007519140, ___instance_1)); }
	inline ConfigurationValidatorBase_t210547623 * get_instance_1() const { return ___instance_1; }
	inline ConfigurationValidatorBase_t210547623 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(ConfigurationValidatorBase_t210547623 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVALIDATORATTRIBUTE_T1007519140_H
#ifndef EXECONFIGURATIONFILEMAP_T1419586304_H
#define EXECONFIGURATIONFILEMAP_T1419586304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ExeConfigurationFileMap
struct  ExeConfigurationFileMap_t1419586304  : public ConfigurationFileMap_t2625210096
{
public:
	// System.String System.Configuration.ExeConfigurationFileMap::exeConfigFilename
	String_t* ___exeConfigFilename_1;
	// System.String System.Configuration.ExeConfigurationFileMap::localUserConfigFilename
	String_t* ___localUserConfigFilename_2;
	// System.String System.Configuration.ExeConfigurationFileMap::roamingUserConfigFilename
	String_t* ___roamingUserConfigFilename_3;

public:
	inline static int32_t get_offset_of_exeConfigFilename_1() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t1419586304, ___exeConfigFilename_1)); }
	inline String_t* get_exeConfigFilename_1() const { return ___exeConfigFilename_1; }
	inline String_t** get_address_of_exeConfigFilename_1() { return &___exeConfigFilename_1; }
	inline void set_exeConfigFilename_1(String_t* value)
	{
		___exeConfigFilename_1 = value;
		Il2CppCodeGenWriteBarrier((&___exeConfigFilename_1), value);
	}

	inline static int32_t get_offset_of_localUserConfigFilename_2() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t1419586304, ___localUserConfigFilename_2)); }
	inline String_t* get_localUserConfigFilename_2() const { return ___localUserConfigFilename_2; }
	inline String_t** get_address_of_localUserConfigFilename_2() { return &___localUserConfigFilename_2; }
	inline void set_localUserConfigFilename_2(String_t* value)
	{
		___localUserConfigFilename_2 = value;
		Il2CppCodeGenWriteBarrier((&___localUserConfigFilename_2), value);
	}

	inline static int32_t get_offset_of_roamingUserConfigFilename_3() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t1419586304, ___roamingUserConfigFilename_3)); }
	inline String_t* get_roamingUserConfigFilename_3() const { return ___roamingUserConfigFilename_3; }
	inline String_t** get_address_of_roamingUserConfigFilename_3() { return &___roamingUserConfigFilename_3; }
	inline void set_roamingUserConfigFilename_3(String_t* value)
	{
		___roamingUserConfigFilename_3 = value;
		Il2CppCodeGenWriteBarrier((&___roamingUserConfigFilename_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECONFIGURATIONFILEMAP_T1419586304_H
#ifndef DNSRESPONSE_T3880332876_H
#define DNSRESPONSE_T3880332876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResponse
struct  DnsResponse_t3880332876  : public DnsPacket_t413343383
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion> Mono.Net.Dns.DnsResponse::question
	ReadOnlyCollection_1_t3276628651 * ___question_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::answer
	ReadOnlyCollection_1_t3129240104 * ___answer_6;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::authority
	ReadOnlyCollection_1_t3129240104 * ___authority_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::additional
	ReadOnlyCollection_1_t3129240104 * ___additional_8;
	// System.Int32 Mono.Net.Dns.DnsResponse::offset
	int32_t ___offset_9;

public:
	inline static int32_t get_offset_of_question_5() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___question_5)); }
	inline ReadOnlyCollection_1_t3276628651 * get_question_5() const { return ___question_5; }
	inline ReadOnlyCollection_1_t3276628651 ** get_address_of_question_5() { return &___question_5; }
	inline void set_question_5(ReadOnlyCollection_1_t3276628651 * value)
	{
		___question_5 = value;
		Il2CppCodeGenWriteBarrier((&___question_5), value);
	}

	inline static int32_t get_offset_of_answer_6() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___answer_6)); }
	inline ReadOnlyCollection_1_t3129240104 * get_answer_6() const { return ___answer_6; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_answer_6() { return &___answer_6; }
	inline void set_answer_6(ReadOnlyCollection_1_t3129240104 * value)
	{
		___answer_6 = value;
		Il2CppCodeGenWriteBarrier((&___answer_6), value);
	}

	inline static int32_t get_offset_of_authority_7() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___authority_7)); }
	inline ReadOnlyCollection_1_t3129240104 * get_authority_7() const { return ___authority_7; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_authority_7() { return &___authority_7; }
	inline void set_authority_7(ReadOnlyCollection_1_t3129240104 * value)
	{
		___authority_7 = value;
		Il2CppCodeGenWriteBarrier((&___authority_7), value);
	}

	inline static int32_t get_offset_of_additional_8() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___additional_8)); }
	inline ReadOnlyCollection_1_t3129240104 * get_additional_8() const { return ___additional_8; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_additional_8() { return &___additional_8; }
	inline void set_additional_8(ReadOnlyCollection_1_t3129240104 * value)
	{
		___additional_8 = value;
		Il2CppCodeGenWriteBarrier((&___additional_8), value);
	}

	inline static int32_t get_offset_of_offset_9() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876, ___offset_9)); }
	inline int32_t get_offset_9() const { return ___offset_9; }
	inline int32_t* get_address_of_offset_9() { return &___offset_9; }
	inline void set_offset_9(int32_t value)
	{
		___offset_9 = value;
	}
};

struct DnsResponse_t3880332876_StaticFields
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsResourceRecord> Mono.Net.Dns.DnsResponse::EmptyRR
	ReadOnlyCollection_1_t3129240104 * ___EmptyRR_3;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<Mono.Net.Dns.DnsQuestion> Mono.Net.Dns.DnsResponse::EmptyQS
	ReadOnlyCollection_1_t3276628651 * ___EmptyQS_4;

public:
	inline static int32_t get_offset_of_EmptyRR_3() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876_StaticFields, ___EmptyRR_3)); }
	inline ReadOnlyCollection_1_t3129240104 * get_EmptyRR_3() const { return ___EmptyRR_3; }
	inline ReadOnlyCollection_1_t3129240104 ** get_address_of_EmptyRR_3() { return &___EmptyRR_3; }
	inline void set_EmptyRR_3(ReadOnlyCollection_1_t3129240104 * value)
	{
		___EmptyRR_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyRR_3), value);
	}

	inline static int32_t get_offset_of_EmptyQS_4() { return static_cast<int32_t>(offsetof(DnsResponse_t3880332876_StaticFields, ___EmptyQS_4)); }
	inline ReadOnlyCollection_1_t3276628651 * get_EmptyQS_4() const { return ___EmptyQS_4; }
	inline ReadOnlyCollection_1_t3276628651 ** get_address_of_EmptyQS_4() { return &___EmptyQS_4; }
	inline void set_EmptyQS_4(ReadOnlyCollection_1_t3276628651 * value)
	{
		___EmptyQS_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyQS_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESPONSE_T3880332876_H
#ifndef DEFAULTVALIDATOR_T300527515_H
#define DEFAULTVALIDATOR_T300527515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultValidator
struct  DefaultValidator_t300527515  : public ConfigurationValidatorBase_t210547623
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALIDATOR_T300527515_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1841601450__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef PROVIDERSETTINGS_T873049714_H
#define PROVIDERSETTINGS_T873049714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProviderSettings
struct  ProviderSettings_t873049714  : public ConfigurationElement_t1776195828
{
public:
	// System.Configuration.ConfigNameValueCollection System.Configuration.ProviderSettings::parameters
	ConfigNameValueCollection_t2395569530 * ___parameters_15;

public:
	inline static int32_t get_offset_of_parameters_15() { return static_cast<int32_t>(offsetof(ProviderSettings_t873049714, ___parameters_15)); }
	inline ConfigNameValueCollection_t2395569530 * get_parameters_15() const { return ___parameters_15; }
	inline ConfigNameValueCollection_t2395569530 ** get_address_of_parameters_15() { return &___parameters_15; }
	inline void set_parameters_15(ConfigNameValueCollection_t2395569530 * value)
	{
		___parameters_15 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_15), value);
	}
};

struct ProviderSettings_t873049714_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProviderSettings::nameProp
	ConfigurationProperty_t2048066811 * ___nameProp_16;
	// System.Configuration.ConfigurationProperty System.Configuration.ProviderSettings::typeProp
	ConfigurationProperty_t2048066811 * ___typeProp_17;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettings::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_18;

public:
	inline static int32_t get_offset_of_nameProp_16() { return static_cast<int32_t>(offsetof(ProviderSettings_t873049714_StaticFields, ___nameProp_16)); }
	inline ConfigurationProperty_t2048066811 * get_nameProp_16() const { return ___nameProp_16; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_nameProp_16() { return &___nameProp_16; }
	inline void set_nameProp_16(ConfigurationProperty_t2048066811 * value)
	{
		___nameProp_16 = value;
		Il2CppCodeGenWriteBarrier((&___nameProp_16), value);
	}

	inline static int32_t get_offset_of_typeProp_17() { return static_cast<int32_t>(offsetof(ProviderSettings_t873049714_StaticFields, ___typeProp_17)); }
	inline ConfigurationProperty_t2048066811 * get_typeProp_17() const { return ___typeProp_17; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_typeProp_17() { return &___typeProp_17; }
	inline void set_typeProp_17(ConfigurationProperty_t2048066811 * value)
	{
		___typeProp_17 = value;
		Il2CppCodeGenWriteBarrier((&___typeProp_17), value);
	}

	inline static int32_t get_offset_of_properties_18() { return static_cast<int32_t>(offsetof(ProviderSettings_t873049714_StaticFields, ___properties_18)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_18() const { return ___properties_18; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_18() { return &___properties_18; }
	inline void set_properties_18(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_18 = value;
		Il2CppCodeGenWriteBarrier((&___properties_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERSETTINGS_T873049714_H
#ifndef PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T388338823_H
#define PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T388338823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationProviderCollection
struct  ProtectedConfigurationProviderCollection_t388338823  : public ProviderCollection_t2548499159
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T388338823_H
#ifndef PROTECTEDCONFIGURATIONPROVIDER_T3971982415_H
#define PROTECTEDCONFIGURATIONPROVIDER_T3971982415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationProvider
struct  ProtectedConfigurationProvider_t3971982415  : public ProviderBase_t2882126354
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONPROVIDER_T3971982415_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T3420015604_H
#define MONOPINVOKECALLBACKATTRIBUTE_T3420015604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Util.MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t3420015604  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T3420015604_H
#ifndef CONFIGINFOCOLLECTION_T3264723080_H
#define CONFIGINFOCOLLECTION_T3264723080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfoCollection
struct  ConfigInfoCollection_t3264723080  : public NameObjectCollectionBase_t2034248631
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFOCOLLECTION_T3264723080_H
#ifndef SECTIONGROUPINFO_T2346323570_H
#define SECTIONGROUPINFO_T2346323570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionGroupInfo
struct  SectionGroupInfo_t2346323570  : public ConfigInfo_t546730838
{
public:
	// System.Boolean System.Configuration.SectionGroupInfo::modified
	bool ___modified_6;
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::sections
	ConfigInfoCollection_t3264723080 * ___sections_7;
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::groups
	ConfigInfoCollection_t3264723080 * ___groups_8;

public:
	inline static int32_t get_offset_of_modified_6() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t2346323570, ___modified_6)); }
	inline bool get_modified_6() const { return ___modified_6; }
	inline bool* get_address_of_modified_6() { return &___modified_6; }
	inline void set_modified_6(bool value)
	{
		___modified_6 = value;
	}

	inline static int32_t get_offset_of_sections_7() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t2346323570, ___sections_7)); }
	inline ConfigInfoCollection_t3264723080 * get_sections_7() const { return ___sections_7; }
	inline ConfigInfoCollection_t3264723080 ** get_address_of_sections_7() { return &___sections_7; }
	inline void set_sections_7(ConfigInfoCollection_t3264723080 * value)
	{
		___sections_7 = value;
		Il2CppCodeGenWriteBarrier((&___sections_7), value);
	}

	inline static int32_t get_offset_of_groups_8() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t2346323570, ___groups_8)); }
	inline ConfigInfoCollection_t3264723080 * get_groups_8() const { return ___groups_8; }
	inline ConfigInfoCollection_t3264723080 ** get_address_of_groups_8() { return &___groups_8; }
	inline void set_groups_8(ConfigInfoCollection_t3264723080 * value)
	{
		___groups_8 = value;
		Il2CppCodeGenWriteBarrier((&___groups_8), value);
	}
};

struct SectionGroupInfo_t2346323570_StaticFields
{
public:
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::emptyList
	ConfigInfoCollection_t3264723080 * ___emptyList_9;

public:
	inline static int32_t get_offset_of_emptyList_9() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t2346323570_StaticFields, ___emptyList_9)); }
	inline ConfigInfoCollection_t3264723080 * get_emptyList_9() const { return ___emptyList_9; }
	inline ConfigInfoCollection_t3264723080 ** get_address_of_emptyList_9() { return &___emptyList_9; }
	inline void set_emptyList_9(ConfigInfoCollection_t3264723080 * value)
	{
		___emptyList_9 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONGROUPINFO_T2346323570_H
#ifndef CFRANGE_T2990408436_H
#define CFRANGE_T2990408436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFRange
struct  CFRange_t2990408436 
{
public:
	// System.IntPtr Mono.Net.CFRange::Location
	intptr_t ___Location_0;
	// System.IntPtr Mono.Net.CFRange::Length
	intptr_t ___Length_1;

public:
	inline static int32_t get_offset_of_Location_0() { return static_cast<int32_t>(offsetof(CFRange_t2990408436, ___Location_0)); }
	inline intptr_t get_Location_0() const { return ___Location_0; }
	inline intptr_t* get_address_of_Location_0() { return &___Location_0; }
	inline void set_Location_0(intptr_t value)
	{
		___Location_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(CFRange_t2990408436, ___Length_1)); }
	inline intptr_t get_Length_1() const { return ___Length_1; }
	inline intptr_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(intptr_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRANGE_T2990408436_H
#ifndef CFOBJECT_T3730145744_H
#define CFOBJECT_T3730145744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFObject
struct  CFObject_t3730145744  : public RuntimeObject
{
public:
	// System.IntPtr Mono.Net.CFObject::<Handle>k__BackingField
	intptr_t ___U3CHandleU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CHandleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CFObject_t3730145744, ___U3CHandleU3Ek__BackingField_0)); }
	inline intptr_t get_U3CHandleU3Ek__BackingField_0() const { return ___U3CHandleU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CHandleU3Ek__BackingField_0() { return &___U3CHandleU3Ek__BackingField_0; }
	inline void set_U3CHandleU3Ek__BackingField_0(intptr_t value)
	{
		___U3CHandleU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFOBJECT_T3730145744_H
#ifndef CONFIGURATIONUSERLEVEL_T1204907851_H
#define CONFIGURATIONUSERLEVEL_T1204907851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationUserLevel
struct  ConfigurationUserLevel_t1204907851 
{
public:
	// System.Int32 System.Configuration.ConfigurationUserLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConfigurationUserLevel_t1204907851, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONUSERLEVEL_T1204907851_H
#ifndef DNSQCLASS_T2376709280_H
#define DNSQCLASS_T2376709280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQClass
struct  DnsQClass_t2376709280 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsQClass::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsQClass_t2376709280, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQCLASS_T2376709280_H
#ifndef DNSTYPE_T1822475631_H
#define DNSTYPE_T1822475631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsType
struct  DnsType_t1822475631 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsType_t1822475631, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSTYPE_T1822475631_H
#ifndef CFPROXYTYPE_T2264861409_H
#define CFPROXYTYPE_T2264861409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxyType
struct  CFProxyType_t2264861409 
{
public:
	// System.Int32 Mono.Net.CFProxyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CFProxyType_t2264861409, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXYTYPE_T2264861409_H
#ifndef CFSTREAMCLIENTCONTEXT_T3245641555_H
#define CFSTREAMCLIENTCONTEXT_T3245641555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFStreamClientContext
struct  CFStreamClientContext_t3245641555 
{
public:
	// System.IntPtr Mono.Net.CFStreamClientContext::Version
	intptr_t ___Version_0;
	// System.IntPtr Mono.Net.CFStreamClientContext::Info
	intptr_t ___Info_1;
	// System.IntPtr Mono.Net.CFStreamClientContext::Retain
	intptr_t ___Retain_2;
	// System.IntPtr Mono.Net.CFStreamClientContext::Release
	intptr_t ___Release_3;
	// System.IntPtr Mono.Net.CFStreamClientContext::CopyDescription
	intptr_t ___CopyDescription_4;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Version_0)); }
	inline intptr_t get_Version_0() const { return ___Version_0; }
	inline intptr_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(intptr_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Info_1() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Info_1)); }
	inline intptr_t get_Info_1() const { return ___Info_1; }
	inline intptr_t* get_address_of_Info_1() { return &___Info_1; }
	inline void set_Info_1(intptr_t value)
	{
		___Info_1 = value;
	}

	inline static int32_t get_offset_of_Retain_2() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Retain_2)); }
	inline intptr_t get_Retain_2() const { return ___Retain_2; }
	inline intptr_t* get_address_of_Retain_2() { return &___Retain_2; }
	inline void set_Retain_2(intptr_t value)
	{
		___Retain_2 = value;
	}

	inline static int32_t get_offset_of_Release_3() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___Release_3)); }
	inline intptr_t get_Release_3() const { return ___Release_3; }
	inline intptr_t* get_address_of_Release_3() { return &___Release_3; }
	inline void set_Release_3(intptr_t value)
	{
		___Release_3 = value;
	}

	inline static int32_t get_offset_of_CopyDescription_4() { return static_cast<int32_t>(offsetof(CFStreamClientContext_t3245641555, ___CopyDescription_4)); }
	inline intptr_t get_CopyDescription_4() const { return ___CopyDescription_4; }
	inline intptr_t* get_address_of_CopyDescription_4() { return &___CopyDescription_4; }
	inline void set_CopyDescription_4(intptr_t value)
	{
		___CopyDescription_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFSTREAMCLIENTCONTEXT_T3245641555_H
#ifndef DEFAULTSECTION_T3840532724_H
#define DEFAULTSECTION_T3840532724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultSection
struct  DefaultSection_t3840532724  : public ConfigurationSection_t2600766927
{
public:

public:
};

struct DefaultSection_t3840532724_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.DefaultSection::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_19;

public:
	inline static int32_t get_offset_of_properties_19() { return static_cast<int32_t>(offsetof(DefaultSection_t3840532724_StaticFields, ___properties_19)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_19() const { return ___properties_19; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_19() { return &___properties_19; }
	inline void set_properties_19(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_19 = value;
		Il2CppCodeGenWriteBarrier((&___properties_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSECTION_T3840532724_H
#ifndef RESOLVERERROR_T2494446112_H
#define RESOLVERERROR_T2494446112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.ResolverError
struct  ResolverError_t2494446112 
{
public:
	// System.Int32 Mono.Net.Dns.ResolverError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResolverError_t2494446112, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVERERROR_T2494446112_H
#ifndef CONFIGURATIONALLOWDEFINITION_T3250313246_H
#define CONFIGURATIONALLOWDEFINITION_T3250313246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowDefinition
struct  ConfigurationAllowDefinition_t3250313246 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowDefinition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConfigurationAllowDefinition_t3250313246, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWDEFINITION_T3250313246_H
#ifndef X509KEYUSAGEFLAGS_T2461349531_H
#define X509KEYUSAGEFLAGS_T2461349531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_t2461349531 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_t2461349531, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_T2461349531_H
#ifndef X509REVOCATIONMODE_T2065307963_H
#define X509REVOCATIONMODE_T2065307963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t2065307963 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(X509RevocationMode_t2065307963, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T2065307963_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_9)); }
	inline DelegateData_t1572802995 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1572802995 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1572802995 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t3022476291_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1572802995 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T3022476291_H
#ifndef AUTHENTICATEDSTREAM_T1183414097_H
#define AUTHENTICATEDSTREAM_T1183414097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticatedStream
struct  AuthenticatedStream_t1183414097  : public Stream_t3255436806
{
public:
	// System.IO.Stream System.Net.Security.AuthenticatedStream::_InnerStream
	Stream_t3255436806 * ____InnerStream_4;
	// System.Boolean System.Net.Security.AuthenticatedStream::_LeaveStreamOpen
	bool ____LeaveStreamOpen_5;

public:
	inline static int32_t get_offset_of__InnerStream_4() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t1183414097, ____InnerStream_4)); }
	inline Stream_t3255436806 * get__InnerStream_4() const { return ____InnerStream_4; }
	inline Stream_t3255436806 ** get_address_of__InnerStream_4() { return &____InnerStream_4; }
	inline void set__InnerStream_4(Stream_t3255436806 * value)
	{
		____InnerStream_4 = value;
		Il2CppCodeGenWriteBarrier((&____InnerStream_4), value);
	}

	inline static int32_t get_offset_of__LeaveStreamOpen_5() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t1183414097, ____LeaveStreamOpen_5)); }
	inline bool get__LeaveStreamOpen_5() const { return ____LeaveStreamOpen_5; }
	inline bool* get_address_of__LeaveStreamOpen_5() { return &____LeaveStreamOpen_5; }
	inline void set__LeaveStreamOpen_5(bool value)
	{
		____LeaveStreamOpen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDSTREAM_T1183414097_H
#ifndef WEBEXCEPTIONSTATUS_T1169373531_H
#define WEBEXCEPTIONSTATUS_T1169373531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_t1169373531 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebExceptionStatus_t1169373531, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_T1169373531_H
#ifndef PROTECTEDCONFIGURATIONSECTION_T3541826375_H
#define PROTECTEDCONFIGURATIONSECTION_T3541826375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationSection
struct  ProtectedConfigurationSection_t3541826375  : public ConfigurationSection_t2600766927
{
public:
	// System.Configuration.ProtectedConfigurationProviderCollection System.Configuration.ProtectedConfigurationSection::providers
	ProtectedConfigurationProviderCollection_t388338823 * ___providers_22;

public:
	inline static int32_t get_offset_of_providers_22() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375, ___providers_22)); }
	inline ProtectedConfigurationProviderCollection_t388338823 * get_providers_22() const { return ___providers_22; }
	inline ProtectedConfigurationProviderCollection_t388338823 ** get_address_of_providers_22() { return &___providers_22; }
	inline void set_providers_22(ProtectedConfigurationProviderCollection_t388338823 * value)
	{
		___providers_22 = value;
		Il2CppCodeGenWriteBarrier((&___providers_22), value);
	}
};

struct ProtectedConfigurationSection_t3541826375_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::defaultProviderProp
	ConfigurationProperty_t2048066811 * ___defaultProviderProp_19;
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::providersProp
	ConfigurationProperty_t2048066811 * ___providersProp_20;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProtectedConfigurationSection::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_21;

public:
	inline static int32_t get_offset_of_defaultProviderProp_19() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375_StaticFields, ___defaultProviderProp_19)); }
	inline ConfigurationProperty_t2048066811 * get_defaultProviderProp_19() const { return ___defaultProviderProp_19; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_defaultProviderProp_19() { return &___defaultProviderProp_19; }
	inline void set_defaultProviderProp_19(ConfigurationProperty_t2048066811 * value)
	{
		___defaultProviderProp_19 = value;
		Il2CppCodeGenWriteBarrier((&___defaultProviderProp_19), value);
	}

	inline static int32_t get_offset_of_providersProp_20() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375_StaticFields, ___providersProp_20)); }
	inline ConfigurationProperty_t2048066811 * get_providersProp_20() const { return ___providersProp_20; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_providersProp_20() { return &___providersProp_20; }
	inline void set_providersProp_20(ConfigurationProperty_t2048066811 * value)
	{
		___providersProp_20 = value;
		Il2CppCodeGenWriteBarrier((&___providersProp_20), value);
	}

	inline static int32_t get_offset_of_properties_21() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t3541826375_StaticFields, ___properties_21)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_21() const { return ___properties_21; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_21() { return &___properties_21; }
	inline void set_properties_21(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_21 = value;
		Il2CppCodeGenWriteBarrier((&___properties_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONSECTION_T3541826375_H
#ifndef CONFIGURATIONALLOWEXEDEFINITION_T3860111898_H
#define CONFIGURATIONALLOWEXEDEFINITION_T3860111898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowExeDefinition
struct  ConfigurationAllowExeDefinition_t3860111898 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowExeDefinition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConfigurationAllowExeDefinition_t3860111898, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWEXEDEFINITION_T3860111898_H
#ifndef PROVIDERSETTINGSCOLLECTION_T585304908_H
#define PROVIDERSETTINGSCOLLECTION_T585304908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProviderSettingsCollection
struct  ProviderSettingsCollection_t585304908  : public ConfigurationElementCollection_t1911180302
{
public:

public:
};

struct ProviderSettingsCollection_t585304908_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettingsCollection::props
	ConfigurationPropertyCollection_t3473514151 * ___props_25;

public:
	inline static int32_t get_offset_of_props_25() { return static_cast<int32_t>(offsetof(ProviderSettingsCollection_t585304908_StaticFields, ___props_25)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_props_25() const { return ___props_25; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_props_25() { return &___props_25; }
	inline void set_props_25(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___props_25 = value;
		Il2CppCodeGenWriteBarrier((&___props_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERSETTINGSCOLLECTION_T585304908_H
#ifndef MONOSSLPOLICYERRORS_T621534536_H
#define MONOSSLPOLICYERRORS_T621534536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Interface.MonoSslPolicyErrors
struct  MonoSslPolicyErrors_t621534536 
{
public:
	// System.Int32 Mono.Security.Interface.MonoSslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonoSslPolicyErrors_t621534536, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSSLPOLICYERRORS_T621534536_H
#ifndef CFPROXY_T537977545_H
#define CFPROXY_T537977545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxy
struct  CFProxy_t537977545  : public RuntimeObject
{
public:
	// Mono.Net.CFDictionary Mono.Net.CFProxy::settings
	CFDictionary_t3548969133 * ___settings_13;

public:
	inline static int32_t get_offset_of_settings_13() { return static_cast<int32_t>(offsetof(CFProxy_t537977545, ___settings_13)); }
	inline CFDictionary_t3548969133 * get_settings_13() const { return ___settings_13; }
	inline CFDictionary_t3548969133 ** get_address_of_settings_13() { return &___settings_13; }
	inline void set_settings_13(CFDictionary_t3548969133 * value)
	{
		___settings_13 = value;
		Il2CppCodeGenWriteBarrier((&___settings_13), value);
	}
};

struct CFProxy_t537977545_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFProxy::kCFProxyAutoConfigurationJavaScriptKey
	intptr_t ___kCFProxyAutoConfigurationJavaScriptKey_0;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyAutoConfigurationURLKey
	intptr_t ___kCFProxyAutoConfigurationURLKey_1;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyHostNameKey
	intptr_t ___kCFProxyHostNameKey_2;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyPasswordKey
	intptr_t ___kCFProxyPasswordKey_3;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyPortNumberKey
	intptr_t ___kCFProxyPortNumberKey_4;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeKey
	intptr_t ___kCFProxyTypeKey_5;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyUsernameKey
	intptr_t ___kCFProxyUsernameKey_6;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeAutoConfigurationURL
	intptr_t ___kCFProxyTypeAutoConfigurationURL_7;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeAutoConfigurationJavaScript
	intptr_t ___kCFProxyTypeAutoConfigurationJavaScript_8;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeFTP
	intptr_t ___kCFProxyTypeFTP_9;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeHTTP
	intptr_t ___kCFProxyTypeHTTP_10;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeHTTPS
	intptr_t ___kCFProxyTypeHTTPS_11;
	// System.IntPtr Mono.Net.CFProxy::kCFProxyTypeSOCKS
	intptr_t ___kCFProxyTypeSOCKS_12;

public:
	inline static int32_t get_offset_of_kCFProxyAutoConfigurationJavaScriptKey_0() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyAutoConfigurationJavaScriptKey_0)); }
	inline intptr_t get_kCFProxyAutoConfigurationJavaScriptKey_0() const { return ___kCFProxyAutoConfigurationJavaScriptKey_0; }
	inline intptr_t* get_address_of_kCFProxyAutoConfigurationJavaScriptKey_0() { return &___kCFProxyAutoConfigurationJavaScriptKey_0; }
	inline void set_kCFProxyAutoConfigurationJavaScriptKey_0(intptr_t value)
	{
		___kCFProxyAutoConfigurationJavaScriptKey_0 = value;
	}

	inline static int32_t get_offset_of_kCFProxyAutoConfigurationURLKey_1() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyAutoConfigurationURLKey_1)); }
	inline intptr_t get_kCFProxyAutoConfigurationURLKey_1() const { return ___kCFProxyAutoConfigurationURLKey_1; }
	inline intptr_t* get_address_of_kCFProxyAutoConfigurationURLKey_1() { return &___kCFProxyAutoConfigurationURLKey_1; }
	inline void set_kCFProxyAutoConfigurationURLKey_1(intptr_t value)
	{
		___kCFProxyAutoConfigurationURLKey_1 = value;
	}

	inline static int32_t get_offset_of_kCFProxyHostNameKey_2() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyHostNameKey_2)); }
	inline intptr_t get_kCFProxyHostNameKey_2() const { return ___kCFProxyHostNameKey_2; }
	inline intptr_t* get_address_of_kCFProxyHostNameKey_2() { return &___kCFProxyHostNameKey_2; }
	inline void set_kCFProxyHostNameKey_2(intptr_t value)
	{
		___kCFProxyHostNameKey_2 = value;
	}

	inline static int32_t get_offset_of_kCFProxyPasswordKey_3() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyPasswordKey_3)); }
	inline intptr_t get_kCFProxyPasswordKey_3() const { return ___kCFProxyPasswordKey_3; }
	inline intptr_t* get_address_of_kCFProxyPasswordKey_3() { return &___kCFProxyPasswordKey_3; }
	inline void set_kCFProxyPasswordKey_3(intptr_t value)
	{
		___kCFProxyPasswordKey_3 = value;
	}

	inline static int32_t get_offset_of_kCFProxyPortNumberKey_4() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyPortNumberKey_4)); }
	inline intptr_t get_kCFProxyPortNumberKey_4() const { return ___kCFProxyPortNumberKey_4; }
	inline intptr_t* get_address_of_kCFProxyPortNumberKey_4() { return &___kCFProxyPortNumberKey_4; }
	inline void set_kCFProxyPortNumberKey_4(intptr_t value)
	{
		___kCFProxyPortNumberKey_4 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeKey_5() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeKey_5)); }
	inline intptr_t get_kCFProxyTypeKey_5() const { return ___kCFProxyTypeKey_5; }
	inline intptr_t* get_address_of_kCFProxyTypeKey_5() { return &___kCFProxyTypeKey_5; }
	inline void set_kCFProxyTypeKey_5(intptr_t value)
	{
		___kCFProxyTypeKey_5 = value;
	}

	inline static int32_t get_offset_of_kCFProxyUsernameKey_6() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyUsernameKey_6)); }
	inline intptr_t get_kCFProxyUsernameKey_6() const { return ___kCFProxyUsernameKey_6; }
	inline intptr_t* get_address_of_kCFProxyUsernameKey_6() { return &___kCFProxyUsernameKey_6; }
	inline void set_kCFProxyUsernameKey_6(intptr_t value)
	{
		___kCFProxyUsernameKey_6 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeAutoConfigurationURL_7() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeAutoConfigurationURL_7)); }
	inline intptr_t get_kCFProxyTypeAutoConfigurationURL_7() const { return ___kCFProxyTypeAutoConfigurationURL_7; }
	inline intptr_t* get_address_of_kCFProxyTypeAutoConfigurationURL_7() { return &___kCFProxyTypeAutoConfigurationURL_7; }
	inline void set_kCFProxyTypeAutoConfigurationURL_7(intptr_t value)
	{
		___kCFProxyTypeAutoConfigurationURL_7 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeAutoConfigurationJavaScript_8() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeAutoConfigurationJavaScript_8)); }
	inline intptr_t get_kCFProxyTypeAutoConfigurationJavaScript_8() const { return ___kCFProxyTypeAutoConfigurationJavaScript_8; }
	inline intptr_t* get_address_of_kCFProxyTypeAutoConfigurationJavaScript_8() { return &___kCFProxyTypeAutoConfigurationJavaScript_8; }
	inline void set_kCFProxyTypeAutoConfigurationJavaScript_8(intptr_t value)
	{
		___kCFProxyTypeAutoConfigurationJavaScript_8 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeFTP_9() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeFTP_9)); }
	inline intptr_t get_kCFProxyTypeFTP_9() const { return ___kCFProxyTypeFTP_9; }
	inline intptr_t* get_address_of_kCFProxyTypeFTP_9() { return &___kCFProxyTypeFTP_9; }
	inline void set_kCFProxyTypeFTP_9(intptr_t value)
	{
		___kCFProxyTypeFTP_9 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeHTTP_10() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeHTTP_10)); }
	inline intptr_t get_kCFProxyTypeHTTP_10() const { return ___kCFProxyTypeHTTP_10; }
	inline intptr_t* get_address_of_kCFProxyTypeHTTP_10() { return &___kCFProxyTypeHTTP_10; }
	inline void set_kCFProxyTypeHTTP_10(intptr_t value)
	{
		___kCFProxyTypeHTTP_10 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeHTTPS_11() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeHTTPS_11)); }
	inline intptr_t get_kCFProxyTypeHTTPS_11() const { return ___kCFProxyTypeHTTPS_11; }
	inline intptr_t* get_address_of_kCFProxyTypeHTTPS_11() { return &___kCFProxyTypeHTTPS_11; }
	inline void set_kCFProxyTypeHTTPS_11(intptr_t value)
	{
		___kCFProxyTypeHTTPS_11 = value;
	}

	inline static int32_t get_offset_of_kCFProxyTypeSOCKS_12() { return static_cast<int32_t>(offsetof(CFProxy_t537977545_StaticFields, ___kCFProxyTypeSOCKS_12)); }
	inline intptr_t get_kCFProxyTypeSOCKS_12() const { return ___kCFProxyTypeSOCKS_12; }
	inline intptr_t* get_address_of_kCFProxyTypeSOCKS_12() { return &___kCFProxyTypeSOCKS_12; }
	inline void set_kCFProxyTypeSOCKS_12(intptr_t value)
	{
		___kCFProxyTypeSOCKS_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXY_T537977545_H
#ifndef DNSQTYPE_T1279797630_H
#define DNSQTYPE_T1279797630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQType
struct  DnsQType_t1279797630 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsQType::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsQType_t1279797630, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQTYPE_T1279797630_H
#ifndef DNSRCODE_T977533636_H
#define DNSRCODE_T977533636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsRCode
struct  DnsRCode_t977533636 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsRCode::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsRCode_t977533636, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRCODE_T977533636_H
#ifndef GETPROXYDATA_T1386489386_H
#define GETPROXYDATA_T1386489386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/GetProxyData
struct  GetProxyData_t1386489386  : public RuntimeObject
{
public:
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::script
	intptr_t ___script_0;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::targetUri
	intptr_t ___targetUri_1;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::error
	intptr_t ___error_2;
	// System.IntPtr Mono.Net.CFNetwork/GetProxyData::result
	intptr_t ___result_3;
	// System.Threading.ManualResetEvent Mono.Net.CFNetwork/GetProxyData::evt
	ManualResetEvent_t926074657 * ___evt_4;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___script_0)); }
	inline intptr_t get_script_0() const { return ___script_0; }
	inline intptr_t* get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(intptr_t value)
	{
		___script_0 = value;
	}

	inline static int32_t get_offset_of_targetUri_1() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___targetUri_1)); }
	inline intptr_t get_targetUri_1() const { return ___targetUri_1; }
	inline intptr_t* get_address_of_targetUri_1() { return &___targetUri_1; }
	inline void set_targetUri_1(intptr_t value)
	{
		___targetUri_1 = value;
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___error_2)); }
	inline intptr_t get_error_2() const { return ___error_2; }
	inline intptr_t* get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(intptr_t value)
	{
		___error_2 = value;
	}

	inline static int32_t get_offset_of_result_3() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___result_3)); }
	inline intptr_t get_result_3() const { return ___result_3; }
	inline intptr_t* get_address_of_result_3() { return &___result_3; }
	inline void set_result_3(intptr_t value)
	{
		___result_3 = value;
	}

	inline static int32_t get_offset_of_evt_4() { return static_cast<int32_t>(offsetof(GetProxyData_t1386489386, ___evt_4)); }
	inline ManualResetEvent_t926074657 * get_evt_4() const { return ___evt_4; }
	inline ManualResetEvent_t926074657 ** get_address_of_evt_4() { return &___evt_4; }
	inline void set_evt_4(ManualResetEvent_t926074657 * value)
	{
		___evt_4 = value;
		Il2CppCodeGenWriteBarrier((&___evt_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPROXYDATA_T1386489386_H
#ifndef DNSHEADER_T2481892084_H
#define DNSHEADER_T2481892084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsHeader
struct  DnsHeader_t2481892084  : public RuntimeObject
{
public:
	// System.ArraySegment`1<System.Byte> Mono.Net.Dns.DnsHeader::bytes
	ArraySegment_1_t2594217482  ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(DnsHeader_t2481892084, ___bytes_0)); }
	inline ArraySegment_1_t2594217482  get_bytes_0() const { return ___bytes_0; }
	inline ArraySegment_1_t2594217482 * get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ArraySegment_1_t2594217482  value)
	{
		___bytes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSHEADER_T2481892084_H
#ifndef DNSOPCODE_T1353552885_H
#define DNSOPCODE_T1353552885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsOpCode
struct  DnsOpCode_t1353552885 
{
public:
	// System.Byte Mono.Net.Dns.DnsOpCode::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsOpCode_t1353552885, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSOPCODE_T1353552885_H
#ifndef DNSCLASS_T3168085651_H
#define DNSCLASS_T3168085651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsClass
struct  DnsClass_t3168085651 
{
public:
	// System.UInt16 Mono.Net.Dns.DnsClass::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DnsClass_t3168085651, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSCLASS_T3168085651_H
#ifndef CFPROXYSETTINGS_T4092929580_H
#define CFPROXYSETTINGS_T4092929580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFProxySettings
struct  CFProxySettings_t4092929580  : public RuntimeObject
{
public:
	// Mono.Net.CFDictionary Mono.Net.CFProxySettings::settings
	CFDictionary_t3548969133 * ___settings_6;

public:
	inline static int32_t get_offset_of_settings_6() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580, ___settings_6)); }
	inline CFDictionary_t3548969133 * get_settings_6() const { return ___settings_6; }
	inline CFDictionary_t3548969133 ** get_address_of_settings_6() { return &___settings_6; }
	inline void set_settings_6(CFDictionary_t3548969133 * value)
	{
		___settings_6 = value;
		Il2CppCodeGenWriteBarrier((&___settings_6), value);
	}
};

struct CFProxySettings_t4092929580_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPEnable
	intptr_t ___kCFNetworkProxiesHTTPEnable_0;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPPort
	intptr_t ___kCFNetworkProxiesHTTPPort_1;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesHTTPProxy
	intptr_t ___kCFNetworkProxiesHTTPProxy_2;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigEnable
	intptr_t ___kCFNetworkProxiesProxyAutoConfigEnable_3;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigJavaScript
	intptr_t ___kCFNetworkProxiesProxyAutoConfigJavaScript_4;
	// System.IntPtr Mono.Net.CFProxySettings::kCFNetworkProxiesProxyAutoConfigURLString
	intptr_t ___kCFNetworkProxiesProxyAutoConfigURLString_5;

public:
	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPEnable_0() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesHTTPEnable_0)); }
	inline intptr_t get_kCFNetworkProxiesHTTPEnable_0() const { return ___kCFNetworkProxiesHTTPEnable_0; }
	inline intptr_t* get_address_of_kCFNetworkProxiesHTTPEnable_0() { return &___kCFNetworkProxiesHTTPEnable_0; }
	inline void set_kCFNetworkProxiesHTTPEnable_0(intptr_t value)
	{
		___kCFNetworkProxiesHTTPEnable_0 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPPort_1() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesHTTPPort_1)); }
	inline intptr_t get_kCFNetworkProxiesHTTPPort_1() const { return ___kCFNetworkProxiesHTTPPort_1; }
	inline intptr_t* get_address_of_kCFNetworkProxiesHTTPPort_1() { return &___kCFNetworkProxiesHTTPPort_1; }
	inline void set_kCFNetworkProxiesHTTPPort_1(intptr_t value)
	{
		___kCFNetworkProxiesHTTPPort_1 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesHTTPProxy_2() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesHTTPProxy_2)); }
	inline intptr_t get_kCFNetworkProxiesHTTPProxy_2() const { return ___kCFNetworkProxiesHTTPProxy_2; }
	inline intptr_t* get_address_of_kCFNetworkProxiesHTTPProxy_2() { return &___kCFNetworkProxiesHTTPProxy_2; }
	inline void set_kCFNetworkProxiesHTTPProxy_2(intptr_t value)
	{
		___kCFNetworkProxiesHTTPProxy_2 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigEnable_3() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesProxyAutoConfigEnable_3)); }
	inline intptr_t get_kCFNetworkProxiesProxyAutoConfigEnable_3() const { return ___kCFNetworkProxiesProxyAutoConfigEnable_3; }
	inline intptr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigEnable_3() { return &___kCFNetworkProxiesProxyAutoConfigEnable_3; }
	inline void set_kCFNetworkProxiesProxyAutoConfigEnable_3(intptr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigEnable_3 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesProxyAutoConfigJavaScript_4)); }
	inline intptr_t get_kCFNetworkProxiesProxyAutoConfigJavaScript_4() const { return ___kCFNetworkProxiesProxyAutoConfigJavaScript_4; }
	inline intptr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4() { return &___kCFNetworkProxiesProxyAutoConfigJavaScript_4; }
	inline void set_kCFNetworkProxiesProxyAutoConfigJavaScript_4(intptr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigJavaScript_4 = value;
	}

	inline static int32_t get_offset_of_kCFNetworkProxiesProxyAutoConfigURLString_5() { return static_cast<int32_t>(offsetof(CFProxySettings_t4092929580_StaticFields, ___kCFNetworkProxiesProxyAutoConfigURLString_5)); }
	inline intptr_t get_kCFNetworkProxiesProxyAutoConfigURLString_5() const { return ___kCFNetworkProxiesProxyAutoConfigURLString_5; }
	inline intptr_t* get_address_of_kCFNetworkProxiesProxyAutoConfigURLString_5() { return &___kCFNetworkProxiesProxyAutoConfigURLString_5; }
	inline void set_kCFNetworkProxiesProxyAutoConfigURLString_5(intptr_t value)
	{
		___kCFNetworkProxiesProxyAutoConfigURLString_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXYSETTINGS_T4092929580_H
#ifndef LEGACYTLSPROVIDER_T3024743_H
#define LEGACYTLSPROVIDER_T3024743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.LegacyTlsProvider
struct  LegacyTlsProvider_t3024743  : public MonoTlsProvider_t823784021
{
public:

public:
};

struct LegacyTlsProvider_t3024743_StaticFields
{
public:
	// System.Guid Mono.Net.Security.LegacyTlsProvider::id
	Guid_t  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LegacyTlsProvider_t3024743_StaticFields, ___id_0)); }
	inline Guid_t  get_id_0() const { return ___id_0; }
	inline Guid_t * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(Guid_t  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYTLSPROVIDER_T3024743_H
#ifndef PROPERTYVALUEORIGIN_T1217826846_H
#define PROPERTYVALUEORIGIN_T1217826846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyValueOrigin
struct  PropertyValueOrigin_t1217826846 
{
public:
	// System.Int32 System.Configuration.PropertyValueOrigin::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyValueOrigin_t1217826846, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYVALUEORIGIN_T1217826846_H
#ifndef IGNORESECTION_T681509237_H
#define IGNORESECTION_T681509237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.IgnoreSection
struct  IgnoreSection_t681509237  : public ConfigurationSection_t2600766927
{
public:
	// System.String System.Configuration.IgnoreSection::xml
	String_t* ___xml_19;

public:
	inline static int32_t get_offset_of_xml_19() { return static_cast<int32_t>(offsetof(IgnoreSection_t681509237, ___xml_19)); }
	inline String_t* get_xml_19() const { return ___xml_19; }
	inline String_t** get_address_of_xml_19() { return &___xml_19; }
	inline void set_xml_19(String_t* value)
	{
		___xml_19 = value;
		Il2CppCodeGenWriteBarrier((&___xml_19), value);
	}
};

struct IgnoreSection_t681509237_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.IgnoreSection::properties
	ConfigurationPropertyCollection_t3473514151 * ___properties_20;

public:
	inline static int32_t get_offset_of_properties_20() { return static_cast<int32_t>(offsetof(IgnoreSection_t681509237_StaticFields, ___properties_20)); }
	inline ConfigurationPropertyCollection_t3473514151 * get_properties_20() const { return ___properties_20; }
	inline ConfigurationPropertyCollection_t3473514151 ** get_address_of_properties_20() { return &___properties_20; }
	inline void set_properties_20(ConfigurationPropertyCollection_t3473514151 * value)
	{
		___properties_20 = value;
		Il2CppCodeGenWriteBarrier((&___properties_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORESECTION_T681509237_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1606206610* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___delegates_11)); }
	inline DelegateU5BU5D_t1606206610* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1606206610** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1606206610* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_pinvoke : public Delegate_t3022476291_marshaled_pinvoke
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t3201952435_marshaled_com : public Delegate_t3022476291_marshaled_com
{
	DelegateU5BU5D_t1606206610* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef SECTIONINFORMATION_T2754609709_H
#define SECTIONINFORMATION_T2754609709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionInformation
struct  SectionInformation_t2754609709  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSection System.Configuration.SectionInformation::parent
	ConfigurationSection_t2600766927 * ___parent_0;
	// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInformation::allow_definition
	int32_t ___allow_definition_1;
	// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInformation::allow_exe_definition
	int32_t ___allow_exe_definition_2;
	// System.Boolean System.Configuration.SectionInformation::allow_location
	bool ___allow_location_3;
	// System.Boolean System.Configuration.SectionInformation::allow_override
	bool ___allow_override_4;
	// System.Boolean System.Configuration.SectionInformation::inherit_on_child_apps
	bool ___inherit_on_child_apps_5;
	// System.Boolean System.Configuration.SectionInformation::restart_on_external_changes
	bool ___restart_on_external_changes_6;
	// System.Boolean System.Configuration.SectionInformation::require_permission
	bool ___require_permission_7;
	// System.String System.Configuration.SectionInformation::config_source
	String_t* ___config_source_8;
	// System.String System.Configuration.SectionInformation::name
	String_t* ___name_9;
	// System.String System.Configuration.SectionInformation::raw_xml
	String_t* ___raw_xml_10;
	// System.Configuration.ProtectedConfigurationProvider System.Configuration.SectionInformation::protection_provider
	ProtectedConfigurationProvider_t3971982415 * ___protection_provider_11;
	// System.String System.Configuration.SectionInformation::<ConfigFilePath>k__BackingField
	String_t* ___U3CConfigFilePathU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___parent_0)); }
	inline ConfigurationSection_t2600766927 * get_parent_0() const { return ___parent_0; }
	inline ConfigurationSection_t2600766927 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ConfigurationSection_t2600766927 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_allow_definition_1() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___allow_definition_1)); }
	inline int32_t get_allow_definition_1() const { return ___allow_definition_1; }
	inline int32_t* get_address_of_allow_definition_1() { return &___allow_definition_1; }
	inline void set_allow_definition_1(int32_t value)
	{
		___allow_definition_1 = value;
	}

	inline static int32_t get_offset_of_allow_exe_definition_2() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___allow_exe_definition_2)); }
	inline int32_t get_allow_exe_definition_2() const { return ___allow_exe_definition_2; }
	inline int32_t* get_address_of_allow_exe_definition_2() { return &___allow_exe_definition_2; }
	inline void set_allow_exe_definition_2(int32_t value)
	{
		___allow_exe_definition_2 = value;
	}

	inline static int32_t get_offset_of_allow_location_3() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___allow_location_3)); }
	inline bool get_allow_location_3() const { return ___allow_location_3; }
	inline bool* get_address_of_allow_location_3() { return &___allow_location_3; }
	inline void set_allow_location_3(bool value)
	{
		___allow_location_3 = value;
	}

	inline static int32_t get_offset_of_allow_override_4() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___allow_override_4)); }
	inline bool get_allow_override_4() const { return ___allow_override_4; }
	inline bool* get_address_of_allow_override_4() { return &___allow_override_4; }
	inline void set_allow_override_4(bool value)
	{
		___allow_override_4 = value;
	}

	inline static int32_t get_offset_of_inherit_on_child_apps_5() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___inherit_on_child_apps_5)); }
	inline bool get_inherit_on_child_apps_5() const { return ___inherit_on_child_apps_5; }
	inline bool* get_address_of_inherit_on_child_apps_5() { return &___inherit_on_child_apps_5; }
	inline void set_inherit_on_child_apps_5(bool value)
	{
		___inherit_on_child_apps_5 = value;
	}

	inline static int32_t get_offset_of_restart_on_external_changes_6() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___restart_on_external_changes_6)); }
	inline bool get_restart_on_external_changes_6() const { return ___restart_on_external_changes_6; }
	inline bool* get_address_of_restart_on_external_changes_6() { return &___restart_on_external_changes_6; }
	inline void set_restart_on_external_changes_6(bool value)
	{
		___restart_on_external_changes_6 = value;
	}

	inline static int32_t get_offset_of_require_permission_7() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___require_permission_7)); }
	inline bool get_require_permission_7() const { return ___require_permission_7; }
	inline bool* get_address_of_require_permission_7() { return &___require_permission_7; }
	inline void set_require_permission_7(bool value)
	{
		___require_permission_7 = value;
	}

	inline static int32_t get_offset_of_config_source_8() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___config_source_8)); }
	inline String_t* get_config_source_8() const { return ___config_source_8; }
	inline String_t** get_address_of_config_source_8() { return &___config_source_8; }
	inline void set_config_source_8(String_t* value)
	{
		___config_source_8 = value;
		Il2CppCodeGenWriteBarrier((&___config_source_8), value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_raw_xml_10() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___raw_xml_10)); }
	inline String_t* get_raw_xml_10() const { return ___raw_xml_10; }
	inline String_t** get_address_of_raw_xml_10() { return &___raw_xml_10; }
	inline void set_raw_xml_10(String_t* value)
	{
		___raw_xml_10 = value;
		Il2CppCodeGenWriteBarrier((&___raw_xml_10), value);
	}

	inline static int32_t get_offset_of_protection_provider_11() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___protection_provider_11)); }
	inline ProtectedConfigurationProvider_t3971982415 * get_protection_provider_11() const { return ___protection_provider_11; }
	inline ProtectedConfigurationProvider_t3971982415 ** get_address_of_protection_provider_11() { return &___protection_provider_11; }
	inline void set_protection_provider_11(ProtectedConfigurationProvider_t3971982415 * value)
	{
		___protection_provider_11 = value;
		Il2CppCodeGenWriteBarrier((&___protection_provider_11), value);
	}

	inline static int32_t get_offset_of_U3CConfigFilePathU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SectionInformation_t2754609709, ___U3CConfigFilePathU3Ek__BackingField_12)); }
	inline String_t* get_U3CConfigFilePathU3Ek__BackingField_12() const { return ___U3CConfigFilePathU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CConfigFilePathU3Ek__BackingField_12() { return &___U3CConfigFilePathU3Ek__BackingField_12; }
	inline void set_U3CConfigFilePathU3Ek__BackingField_12(String_t* value)
	{
		___U3CConfigFilePathU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigFilePathU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONINFORMATION_T2754609709_H
#ifndef SECTIONINFO_T1739019515_H
#define SECTIONINFO_T1739019515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionInfo
struct  SectionInfo_t1739019515  : public ConfigInfo_t546730838
{
public:
	// System.Boolean System.Configuration.SectionInfo::allowLocation
	bool ___allowLocation_6;
	// System.Boolean System.Configuration.SectionInfo::requirePermission
	bool ___requirePermission_7;
	// System.Boolean System.Configuration.SectionInfo::restartOnExternalChanges
	bool ___restartOnExternalChanges_8;
	// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInfo::allowDefinition
	int32_t ___allowDefinition_9;
	// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInfo::allowExeDefinition
	int32_t ___allowExeDefinition_10;

public:
	inline static int32_t get_offset_of_allowLocation_6() { return static_cast<int32_t>(offsetof(SectionInfo_t1739019515, ___allowLocation_6)); }
	inline bool get_allowLocation_6() const { return ___allowLocation_6; }
	inline bool* get_address_of_allowLocation_6() { return &___allowLocation_6; }
	inline void set_allowLocation_6(bool value)
	{
		___allowLocation_6 = value;
	}

	inline static int32_t get_offset_of_requirePermission_7() { return static_cast<int32_t>(offsetof(SectionInfo_t1739019515, ___requirePermission_7)); }
	inline bool get_requirePermission_7() const { return ___requirePermission_7; }
	inline bool* get_address_of_requirePermission_7() { return &___requirePermission_7; }
	inline void set_requirePermission_7(bool value)
	{
		___requirePermission_7 = value;
	}

	inline static int32_t get_offset_of_restartOnExternalChanges_8() { return static_cast<int32_t>(offsetof(SectionInfo_t1739019515, ___restartOnExternalChanges_8)); }
	inline bool get_restartOnExternalChanges_8() const { return ___restartOnExternalChanges_8; }
	inline bool* get_address_of_restartOnExternalChanges_8() { return &___restartOnExternalChanges_8; }
	inline void set_restartOnExternalChanges_8(bool value)
	{
		___restartOnExternalChanges_8 = value;
	}

	inline static int32_t get_offset_of_allowDefinition_9() { return static_cast<int32_t>(offsetof(SectionInfo_t1739019515, ___allowDefinition_9)); }
	inline int32_t get_allowDefinition_9() const { return ___allowDefinition_9; }
	inline int32_t* get_address_of_allowDefinition_9() { return &___allowDefinition_9; }
	inline void set_allowDefinition_9(int32_t value)
	{
		___allowDefinition_9 = value;
	}

	inline static int32_t get_offset_of_allowExeDefinition_10() { return static_cast<int32_t>(offsetof(SectionInfo_t1739019515, ___allowExeDefinition_10)); }
	inline int32_t get_allowExeDefinition_10() const { return ___allowExeDefinition_10; }
	inline int32_t* get_address_of_allowExeDefinition_10() { return &___allowExeDefinition_10; }
	inline void set_allowExeDefinition_10(int32_t value)
	{
		___allowExeDefinition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONINFO_T1739019515_H
#ifndef EXECONFIGURATIONHOST_T2778769322_H
#define EXECONFIGURATIONHOST_T2778769322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ExeConfigurationHost
struct  ExeConfigurationHost_t2778769322  : public InternalConfigurationHost_t547577555
{
public:
	// System.Configuration.ExeConfigurationFileMap System.Configuration.ExeConfigurationHost::map
	ExeConfigurationFileMap_t1419586304 * ___map_0;
	// System.Configuration.ConfigurationUserLevel System.Configuration.ExeConfigurationHost::level
	int32_t ___level_1;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t2778769322, ___map_0)); }
	inline ExeConfigurationFileMap_t1419586304 * get_map_0() const { return ___map_0; }
	inline ExeConfigurationFileMap_t1419586304 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(ExeConfigurationFileMap_t1419586304 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier((&___map_0), value);
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t2778769322, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECONFIGURATIONHOST_T2778769322_H
#ifndef LEGACYSSLSTREAM_T83997747_H
#define LEGACYSSLSTREAM_T83997747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.Private.LegacySslStream
struct  LegacySslStream_t83997747  : public AuthenticatedStream_t1183414097
{
public:
	// Mono.Security.Protocol.Tls.SslStreamBase Mono.Net.Security.Private.LegacySslStream::ssl_stream
	SslStreamBase_t934199321 * ___ssl_stream_6;
	// Mono.Security.Interface.ICertificateValidator Mono.Net.Security.Private.LegacySslStream::certificateValidator
	RuntimeObject* ___certificateValidator_7;
	// Mono.Security.Interface.MonoTlsProvider Mono.Net.Security.Private.LegacySslStream::provider
	MonoTlsProvider_t823784021 * ___provider_8;

public:
	inline static int32_t get_offset_of_ssl_stream_6() { return static_cast<int32_t>(offsetof(LegacySslStream_t83997747, ___ssl_stream_6)); }
	inline SslStreamBase_t934199321 * get_ssl_stream_6() const { return ___ssl_stream_6; }
	inline SslStreamBase_t934199321 ** get_address_of_ssl_stream_6() { return &___ssl_stream_6; }
	inline void set_ssl_stream_6(SslStreamBase_t934199321 * value)
	{
		___ssl_stream_6 = value;
		Il2CppCodeGenWriteBarrier((&___ssl_stream_6), value);
	}

	inline static int32_t get_offset_of_certificateValidator_7() { return static_cast<int32_t>(offsetof(LegacySslStream_t83997747, ___certificateValidator_7)); }
	inline RuntimeObject* get_certificateValidator_7() const { return ___certificateValidator_7; }
	inline RuntimeObject** get_address_of_certificateValidator_7() { return &___certificateValidator_7; }
	inline void set_certificateValidator_7(RuntimeObject* value)
	{
		___certificateValidator_7 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValidator_7), value);
	}

	inline static int32_t get_offset_of_provider_8() { return static_cast<int32_t>(offsetof(LegacySslStream_t83997747, ___provider_8)); }
	inline MonoTlsProvider_t823784021 * get_provider_8() const { return ___provider_8; }
	inline MonoTlsProvider_t823784021 ** get_address_of_provider_8() { return &___provider_8; }
	inline void set_provider_8(MonoTlsProvider_t823784021 * value)
	{
		___provider_8 = value;
		Il2CppCodeGenWriteBarrier((&___provider_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYSSLSTREAM_T83997747_H
#ifndef PROPERTYINFORMATION_T2089433965_H
#define PROPERTYINFORMATION_T2089433965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformation
struct  PropertyInformation_t2089433965  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.PropertyInformation::isModified
	bool ___isModified_0;
	// System.Int32 System.Configuration.PropertyInformation::lineNumber
	int32_t ___lineNumber_1;
	// System.String System.Configuration.PropertyInformation::source
	String_t* ___source_2;
	// System.Object System.Configuration.PropertyInformation::val
	RuntimeObject * ___val_3;
	// System.Configuration.PropertyValueOrigin System.Configuration.PropertyInformation::origin
	int32_t ___origin_4;
	// System.Configuration.ConfigurationElement System.Configuration.PropertyInformation::owner
	ConfigurationElement_t1776195828 * ___owner_5;
	// System.Configuration.ConfigurationProperty System.Configuration.PropertyInformation::property
	ConfigurationProperty_t2048066811 * ___property_6;

public:
	inline static int32_t get_offset_of_isModified_0() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___isModified_0)); }
	inline bool get_isModified_0() const { return ___isModified_0; }
	inline bool* get_address_of_isModified_0() { return &___isModified_0; }
	inline void set_isModified_0(bool value)
	{
		___isModified_0 = value;
	}

	inline static int32_t get_offset_of_lineNumber_1() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___lineNumber_1)); }
	inline int32_t get_lineNumber_1() const { return ___lineNumber_1; }
	inline int32_t* get_address_of_lineNumber_1() { return &___lineNumber_1; }
	inline void set_lineNumber_1(int32_t value)
	{
		___lineNumber_1 = value;
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___source_2)); }
	inline String_t* get_source_2() const { return ___source_2; }
	inline String_t** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(String_t* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_val_3() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___val_3)); }
	inline RuntimeObject * get_val_3() const { return ___val_3; }
	inline RuntimeObject ** get_address_of_val_3() { return &___val_3; }
	inline void set_val_3(RuntimeObject * value)
	{
		___val_3 = value;
		Il2CppCodeGenWriteBarrier((&___val_3), value);
	}

	inline static int32_t get_offset_of_origin_4() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___origin_4)); }
	inline int32_t get_origin_4() const { return ___origin_4; }
	inline int32_t* get_address_of_origin_4() { return &___origin_4; }
	inline void set_origin_4(int32_t value)
	{
		___origin_4 = value;
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___owner_5)); }
	inline ConfigurationElement_t1776195828 * get_owner_5() const { return ___owner_5; }
	inline ConfigurationElement_t1776195828 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(ConfigurationElement_t1776195828 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier((&___owner_5), value);
	}

	inline static int32_t get_offset_of_property_6() { return static_cast<int32_t>(offsetof(PropertyInformation_t2089433965, ___property_6)); }
	inline ConfigurationProperty_t2048066811 * get_property_6() const { return ___property_6; }
	inline ConfigurationProperty_t2048066811 ** get_address_of_property_6() { return &___property_6; }
	inline void set_property_6(ConfigurationProperty_t2048066811 * value)
	{
		___property_6 = value;
		Il2CppCodeGenWriteBarrier((&___property_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATION_T2089433965_H
#ifndef MONOTLSSTREAM_T1249652258_H
#define MONOTLSSTREAM_T1249652258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.MonoTlsStream
struct  MonoTlsStream_t1249652258  : public RuntimeObject
{
public:
	// Mono.Net.Security.IMonoTlsProvider Mono.Net.Security.MonoTlsStream::provider
	RuntimeObject* ___provider_0;
	// System.Net.Sockets.NetworkStream Mono.Net.Security.MonoTlsStream::networkStream
	NetworkStream_t581172200 * ___networkStream_1;
	// System.Net.HttpWebRequest Mono.Net.Security.MonoTlsStream::request
	HttpWebRequest_t1951404513 * ___request_2;
	// Mono.Net.Security.IMonoSslStream Mono.Net.Security.MonoTlsStream::sslStream
	RuntimeObject* ___sslStream_3;
	// System.Net.WebExceptionStatus Mono.Net.Security.MonoTlsStream::status
	int32_t ___status_4;
	// System.Boolean Mono.Net.Security.MonoTlsStream::<CertificateValidationFailed>k__BackingField
	bool ___U3CCertificateValidationFailedU3Ek__BackingField_5;
	// Mono.Security.Interface.MonoTlsSettings Mono.Net.Security.MonoTlsStream::settings
	MonoTlsSettings_t302829305 * ___settings_6;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___provider_0)); }
	inline RuntimeObject* get_provider_0() const { return ___provider_0; }
	inline RuntimeObject** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(RuntimeObject* value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___provider_0), value);
	}

	inline static int32_t get_offset_of_networkStream_1() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___networkStream_1)); }
	inline NetworkStream_t581172200 * get_networkStream_1() const { return ___networkStream_1; }
	inline NetworkStream_t581172200 ** get_address_of_networkStream_1() { return &___networkStream_1; }
	inline void set_networkStream_1(NetworkStream_t581172200 * value)
	{
		___networkStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___networkStream_1), value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___request_2)); }
	inline HttpWebRequest_t1951404513 * get_request_2() const { return ___request_2; }
	inline HttpWebRequest_t1951404513 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(HttpWebRequest_t1951404513 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}

	inline static int32_t get_offset_of_sslStream_3() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___sslStream_3)); }
	inline RuntimeObject* get_sslStream_3() const { return ___sslStream_3; }
	inline RuntimeObject** get_address_of_sslStream_3() { return &___sslStream_3; }
	inline void set_sslStream_3(RuntimeObject* value)
	{
		___sslStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___U3CCertificateValidationFailedU3Ek__BackingField_5)); }
	inline bool get_U3CCertificateValidationFailedU3Ek__BackingField_5() const { return ___U3CCertificateValidationFailedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CCertificateValidationFailedU3Ek__BackingField_5() { return &___U3CCertificateValidationFailedU3Ek__BackingField_5; }
	inline void set_U3CCertificateValidationFailedU3Ek__BackingField_5(bool value)
	{
		___U3CCertificateValidationFailedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_settings_6() { return static_cast<int32_t>(offsetof(MonoTlsStream_t1249652258, ___settings_6)); }
	inline MonoTlsSettings_t302829305 * get_settings_6() const { return ___settings_6; }
	inline MonoTlsSettings_t302829305 ** get_address_of_settings_6() { return &___settings_6; }
	inline void set_settings_6(MonoTlsSettings_t302829305 * value)
	{
		___settings_6 = value;
		Il2CppCodeGenWriteBarrier((&___settings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTLSSTREAM_T1249652258_H
#ifndef SYSTEMCERTIFICATEVALIDATOR_T2641375168_H
#define SYSTEMCERTIFICATEVALIDATOR_T2641375168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.SystemCertificateValidator
struct  SystemCertificateValidator_t2641375168  : public RuntimeObject
{
public:

public:
};

struct SystemCertificateValidator_t2641375168_StaticFields
{
public:
	// System.Boolean Mono.Net.Security.SystemCertificateValidator::is_macosx
	bool ___is_macosx_0;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode Mono.Net.Security.SystemCertificateValidator::revocation_mode
	int32_t ___revocation_mode_1;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags Mono.Net.Security.SystemCertificateValidator::s_flags
	int32_t ___s_flags_2;

public:
	inline static int32_t get_offset_of_is_macosx_0() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t2641375168_StaticFields, ___is_macosx_0)); }
	inline bool get_is_macosx_0() const { return ___is_macosx_0; }
	inline bool* get_address_of_is_macosx_0() { return &___is_macosx_0; }
	inline void set_is_macosx_0(bool value)
	{
		___is_macosx_0 = value;
	}

	inline static int32_t get_offset_of_revocation_mode_1() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t2641375168_StaticFields, ___revocation_mode_1)); }
	inline int32_t get_revocation_mode_1() const { return ___revocation_mode_1; }
	inline int32_t* get_address_of_revocation_mode_1() { return &___revocation_mode_1; }
	inline void set_revocation_mode_1(int32_t value)
	{
		___revocation_mode_1 = value;
	}

	inline static int32_t get_offset_of_s_flags_2() { return static_cast<int32_t>(offsetof(SystemCertificateValidator_t2641375168_StaticFields, ___s_flags_2)); }
	inline int32_t get_s_flags_2() const { return ___s_flags_2; }
	inline int32_t* get_address_of_s_flags_2() { return &___s_flags_2; }
	inline void set_s_flags_2(int32_t value)
	{
		___s_flags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMCERTIFICATEVALIDATOR_T2641375168_H
#ifndef CFDICTIONARY_T3548969133_H
#define CFDICTIONARY_T3548969133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFDictionary
struct  CFDictionary_t3548969133  : public CFObject_t3730145744
{
public:

public:
};

struct CFDictionary_t3548969133_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFDictionary::KeyCallbacks
	intptr_t ___KeyCallbacks_1;
	// System.IntPtr Mono.Net.CFDictionary::ValueCallbacks
	intptr_t ___ValueCallbacks_2;

public:
	inline static int32_t get_offset_of_KeyCallbacks_1() { return static_cast<int32_t>(offsetof(CFDictionary_t3548969133_StaticFields, ___KeyCallbacks_1)); }
	inline intptr_t get_KeyCallbacks_1() const { return ___KeyCallbacks_1; }
	inline intptr_t* get_address_of_KeyCallbacks_1() { return &___KeyCallbacks_1; }
	inline void set_KeyCallbacks_1(intptr_t value)
	{
		___KeyCallbacks_1 = value;
	}

	inline static int32_t get_offset_of_ValueCallbacks_2() { return static_cast<int32_t>(offsetof(CFDictionary_t3548969133_StaticFields, ___ValueCallbacks_2)); }
	inline intptr_t get_ValueCallbacks_2() const { return ___ValueCallbacks_2; }
	inline intptr_t* get_address_of_ValueCallbacks_2() { return &___ValueCallbacks_2; }
	inline void set_ValueCallbacks_2(intptr_t value)
	{
		___ValueCallbacks_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFDICTIONARY_T3548969133_H
#ifndef CFSTRING_T3358817686_H
#define CFSTRING_T3358817686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFString
struct  CFString_t3358817686  : public CFObject_t3730145744
{
public:
	// System.String Mono.Net.CFString::str
	String_t* ___str_1;

public:
	inline static int32_t get_offset_of_str_1() { return static_cast<int32_t>(offsetof(CFString_t3358817686, ___str_1)); }
	inline String_t* get_str_1() const { return ___str_1; }
	inline String_t** get_address_of_str_1() { return &___str_1; }
	inline void set_str_1(String_t* value)
	{
		___str_1 = value;
		Il2CppCodeGenWriteBarrier((&___str_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFSTRING_T3358817686_H
#ifndef SIMPLERESOLVEREVENTARGS_T4294564137_H
#define SIMPLERESOLVEREVENTARGS_T4294564137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.SimpleResolverEventArgs
struct  SimpleResolverEventArgs_t4294564137  : public EventArgs_t3289624707
{
public:
	// System.EventHandler`1<Mono.Net.Dns.SimpleResolverEventArgs> Mono.Net.Dns.SimpleResolverEventArgs::Completed
	EventHandler_1_t2885871309 * ___Completed_1;
	// Mono.Net.Dns.ResolverError Mono.Net.Dns.SimpleResolverEventArgs::<ResolverError>k__BackingField
	int32_t ___U3CResolverErrorU3Ek__BackingField_2;
	// System.String Mono.Net.Dns.SimpleResolverEventArgs::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_3;
	// System.String Mono.Net.Dns.SimpleResolverEventArgs::<HostName>k__BackingField
	String_t* ___U3CHostNameU3Ek__BackingField_4;
	// System.Net.IPHostEntry Mono.Net.Dns.SimpleResolverEventArgs::<HostEntry>k__BackingField
	IPHostEntry_t994738509 * ___U3CHostEntryU3Ek__BackingField_5;
	// System.UInt16 Mono.Net.Dns.SimpleResolverEventArgs::QueryID
	uint16_t ___QueryID_6;
	// System.UInt16 Mono.Net.Dns.SimpleResolverEventArgs::Retries
	uint16_t ___Retries_7;
	// System.Threading.Timer Mono.Net.Dns.SimpleResolverEventArgs::Timer
	Timer_t791717973 * ___Timer_8;
	// System.Net.IPAddress Mono.Net.Dns.SimpleResolverEventArgs::PTRAddress
	IPAddress_t1399971723 * ___PTRAddress_9;

public:
	inline static int32_t get_offset_of_Completed_1() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___Completed_1)); }
	inline EventHandler_1_t2885871309 * get_Completed_1() const { return ___Completed_1; }
	inline EventHandler_1_t2885871309 ** get_address_of_Completed_1() { return &___Completed_1; }
	inline void set_Completed_1(EventHandler_1_t2885871309 * value)
	{
		___Completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_1), value);
	}

	inline static int32_t get_offset_of_U3CResolverErrorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CResolverErrorU3Ek__BackingField_2)); }
	inline int32_t get_U3CResolverErrorU3Ek__BackingField_2() const { return ___U3CResolverErrorU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CResolverErrorU3Ek__BackingField_2() { return &___U3CResolverErrorU3Ek__BackingField_2; }
	inline void set_U3CResolverErrorU3Ek__BackingField_2(int32_t value)
	{
		___U3CResolverErrorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CErrorMessageU3Ek__BackingField_3)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_3() const { return ___U3CErrorMessageU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_3() { return &___U3CErrorMessageU3Ek__BackingField_3; }
	inline void set_U3CErrorMessageU3Ek__BackingField_3(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorMessageU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CHostNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CHostNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CHostNameU3Ek__BackingField_4() const { return ___U3CHostNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CHostNameU3Ek__BackingField_4() { return &___U3CHostNameU3Ek__BackingField_4; }
	inline void set_U3CHostNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CHostNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHostEntryU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___U3CHostEntryU3Ek__BackingField_5)); }
	inline IPHostEntry_t994738509 * get_U3CHostEntryU3Ek__BackingField_5() const { return ___U3CHostEntryU3Ek__BackingField_5; }
	inline IPHostEntry_t994738509 ** get_address_of_U3CHostEntryU3Ek__BackingField_5() { return &___U3CHostEntryU3Ek__BackingField_5; }
	inline void set_U3CHostEntryU3Ek__BackingField_5(IPHostEntry_t994738509 * value)
	{
		___U3CHostEntryU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostEntryU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_QueryID_6() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___QueryID_6)); }
	inline uint16_t get_QueryID_6() const { return ___QueryID_6; }
	inline uint16_t* get_address_of_QueryID_6() { return &___QueryID_6; }
	inline void set_QueryID_6(uint16_t value)
	{
		___QueryID_6 = value;
	}

	inline static int32_t get_offset_of_Retries_7() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___Retries_7)); }
	inline uint16_t get_Retries_7() const { return ___Retries_7; }
	inline uint16_t* get_address_of_Retries_7() { return &___Retries_7; }
	inline void set_Retries_7(uint16_t value)
	{
		___Retries_7 = value;
	}

	inline static int32_t get_offset_of_Timer_8() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___Timer_8)); }
	inline Timer_t791717973 * get_Timer_8() const { return ___Timer_8; }
	inline Timer_t791717973 ** get_address_of_Timer_8() { return &___Timer_8; }
	inline void set_Timer_8(Timer_t791717973 * value)
	{
		___Timer_8 = value;
		Il2CppCodeGenWriteBarrier((&___Timer_8), value);
	}

	inline static int32_t get_offset_of_PTRAddress_9() { return static_cast<int32_t>(offsetof(SimpleResolverEventArgs_t4294564137, ___PTRAddress_9)); }
	inline IPAddress_t1399971723 * get_PTRAddress_9() const { return ___PTRAddress_9; }
	inline IPAddress_t1399971723 ** get_address_of_PTRAddress_9() { return &___PTRAddress_9; }
	inline void set_PTRAddress_9(IPAddress_t1399971723 * value)
	{
		___PTRAddress_9 = value;
		Il2CppCodeGenWriteBarrier((&___PTRAddress_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLERESOLVEREVENTARGS_T4294564137_H
#ifndef DNSRESOURCERECORD_T2943454412_H
#define DNSRESOURCERECORD_T2943454412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecord
struct  DnsResourceRecord_t2943454412  : public RuntimeObject
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecord::name
	String_t* ___name_0;
	// Mono.Net.Dns.DnsType Mono.Net.Dns.DnsResourceRecord::type
	uint16_t ___type_1;
	// Mono.Net.Dns.DnsClass Mono.Net.Dns.DnsResourceRecord::klass
	uint16_t ___klass_2;
	// System.Int32 Mono.Net.Dns.DnsResourceRecord::ttl
	int32_t ___ttl_3;
	// System.UInt16 Mono.Net.Dns.DnsResourceRecord::rdlength
	uint16_t ___rdlength_4;
	// System.ArraySegment`1<System.Byte> Mono.Net.Dns.DnsResourceRecord::m_rdata
	ArraySegment_1_t2594217482  ___m_rdata_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___type_1)); }
	inline uint16_t get_type_1() const { return ___type_1; }
	inline uint16_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint16_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_klass_2() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___klass_2)); }
	inline uint16_t get_klass_2() const { return ___klass_2; }
	inline uint16_t* get_address_of_klass_2() { return &___klass_2; }
	inline void set_klass_2(uint16_t value)
	{
		___klass_2 = value;
	}

	inline static int32_t get_offset_of_ttl_3() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___ttl_3)); }
	inline int32_t get_ttl_3() const { return ___ttl_3; }
	inline int32_t* get_address_of_ttl_3() { return &___ttl_3; }
	inline void set_ttl_3(int32_t value)
	{
		___ttl_3 = value;
	}

	inline static int32_t get_offset_of_rdlength_4() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___rdlength_4)); }
	inline uint16_t get_rdlength_4() const { return ___rdlength_4; }
	inline uint16_t* get_address_of_rdlength_4() { return &___rdlength_4; }
	inline void set_rdlength_4(uint16_t value)
	{
		___rdlength_4 = value;
	}

	inline static int32_t get_offset_of_m_rdata_5() { return static_cast<int32_t>(offsetof(DnsResourceRecord_t2943454412, ___m_rdata_5)); }
	inline ArraySegment_1_t2594217482  get_m_rdata_5() const { return ___m_rdata_5; }
	inline ArraySegment_1_t2594217482 * get_address_of_m_rdata_5() { return &___m_rdata_5; }
	inline void set_m_rdata_5(ArraySegment_1_t2594217482  value)
	{
		___m_rdata_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORD_T2943454412_H
#ifndef CFURL_T2608898728_H
#define CFURL_T2608898728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFUrl
struct  CFUrl_t2608898728  : public CFObject_t3730145744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFURL_T2608898728_H
#ifndef CFNUMBER_T2174524538_H
#define CFNUMBER_T2174524538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNumber
struct  CFNumber_t2174524538  : public CFObject_t3730145744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFNUMBER_T2174524538_H
#ifndef CFARRAY_T3428036444_H
#define CFARRAY_T3428036444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFArray
struct  CFArray_t3428036444  : public CFObject_t3730145744
{
public:

public:
};

struct CFArray_t3428036444_StaticFields
{
public:
	// System.IntPtr Mono.Net.CFArray::kCFTypeArrayCallbacks
	intptr_t ___kCFTypeArrayCallbacks_1;

public:
	inline static int32_t get_offset_of_kCFTypeArrayCallbacks_1() { return static_cast<int32_t>(offsetof(CFArray_t3428036444_StaticFields, ___kCFTypeArrayCallbacks_1)); }
	inline intptr_t get_kCFTypeArrayCallbacks_1() const { return ___kCFTypeArrayCallbacks_1; }
	inline intptr_t* get_address_of_kCFTypeArrayCallbacks_1() { return &___kCFTypeArrayCallbacks_1; }
	inline void set_kCFTypeArrayCallbacks_1(intptr_t value)
	{
		___kCFTypeArrayCallbacks_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFARRAY_T3428036444_H
#ifndef CFRUNLOOP_T1302886598_H
#define CFRUNLOOP_T1302886598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFRunLoop
struct  CFRunLoop_t1302886598  : public CFObject_t3730145744
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRUNLOOP_T1302886598_H
#ifndef DNSQUESTION_T3090842959_H
#define DNSQUESTION_T3090842959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsQuestion
struct  DnsQuestion_t3090842959  : public RuntimeObject
{
public:
	// System.String Mono.Net.Dns.DnsQuestion::name
	String_t* ___name_0;
	// Mono.Net.Dns.DnsQType Mono.Net.Dns.DnsQuestion::type
	uint16_t ___type_1;
	// Mono.Net.Dns.DnsQClass Mono.Net.Dns.DnsQuestion::_class
	uint16_t ____class_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DnsQuestion_t3090842959, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DnsQuestion_t3090842959, ___type_1)); }
	inline uint16_t get_type_1() const { return ___type_1; }
	inline uint16_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(uint16_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of__class_2() { return static_cast<int32_t>(offsetof(DnsQuestion_t3090842959, ____class_2)); }
	inline uint16_t get__class_2() const { return ____class_2; }
	inline uint16_t* get_address_of__class_2() { return &____class_2; }
	inline void set__class_2(uint16_t value)
	{
		____class_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSQUESTION_T3090842959_H
#ifndef DNSRESOURCERECORDCNAME_T595248992_H
#define DNSRESOURCERECORDCNAME_T595248992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordCName
struct  DnsResourceRecordCName_t595248992  : public DnsResourceRecord_t2943454412
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecordCName::cname
	String_t* ___cname_6;

public:
	inline static int32_t get_offset_of_cname_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordCName_t595248992, ___cname_6)); }
	inline String_t* get_cname_6() const { return ___cname_6; }
	inline String_t** get_address_of_cname_6() { return &___cname_6; }
	inline void set_cname_6(String_t* value)
	{
		___cname_6 = value;
		Il2CppCodeGenWriteBarrier((&___cname_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDCNAME_T595248992_H
#ifndef DNSRESOURCERECORDPTR_T1722196618_H
#define DNSRESOURCERECORDPTR_T1722196618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordPTR
struct  DnsResourceRecordPTR_t1722196618  : public DnsResourceRecord_t2943454412
{
public:
	// System.String Mono.Net.Dns.DnsResourceRecordPTR::dname
	String_t* ___dname_6;

public:
	inline static int32_t get_offset_of_dname_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordPTR_t1722196618, ___dname_6)); }
	inline String_t* get_dname_6() const { return ___dname_6; }
	inline String_t** get_address_of_dname_6() { return &___dname_6; }
	inline void set_dname_6(String_t* value)
	{
		___dname_6 = value;
		Il2CppCodeGenWriteBarrier((&___dname_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDPTR_T1722196618_H
#ifndef DNSRESOURCERECORDIPADDRESS_T2549622903_H
#define DNSRESOURCERECORDIPADDRESS_T2549622903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordIPAddress
struct  DnsResourceRecordIPAddress_t2549622903  : public DnsResourceRecord_t2943454412
{
public:
	// System.Net.IPAddress Mono.Net.Dns.DnsResourceRecordIPAddress::address
	IPAddress_t1399971723 * ___address_6;

public:
	inline static int32_t get_offset_of_address_6() { return static_cast<int32_t>(offsetof(DnsResourceRecordIPAddress_t2549622903, ___address_6)); }
	inline IPAddress_t1399971723 * get_address_6() const { return ___address_6; }
	inline IPAddress_t1399971723 ** get_address_of_address_6() { return &___address_6; }
	inline void set_address_6(IPAddress_t1399971723 * value)
	{
		___address_6 = value;
		Il2CppCodeGenWriteBarrier((&___address_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDIPADDRESS_T2549622903_H
#ifndef CFPROXYAUTOCONFIGURATIONRESULTCALLBACK_T3537096558_H
#define CFPROXYAUTOCONFIGURATIONRESULTCALLBACK_T3537096558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.CFNetwork/CFProxyAutoConfigurationResultCallback
struct  CFProxyAutoConfigurationResultCallback_t3537096558  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFPROXYAUTOCONFIGURATIONRESULTCALLBACK_T3537096558_H
#ifndef SERVERCERTVALIDATIONCALLBACKWRAPPER_T93117366_H
#define SERVERCERTVALIDATIONCALLBACKWRAPPER_T93117366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Security.ServerCertValidationCallbackWrapper
struct  ServerCertValidationCallbackWrapper_t93117366  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCERTVALIDATIONCALLBACKWRAPPER_T93117366_H
#ifndef DNSRESOURCERECORDAAAA_T4028376332_H
#define DNSRESOURCERECORDAAAA_T4028376332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordAAAA
struct  DnsResourceRecordAAAA_t4028376332  : public DnsResourceRecordIPAddress_t2549622903
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDAAAA_T4028376332_H
#ifndef DNSRESOURCERECORDA_T2533778347_H
#define DNSRESOURCERECORDA_T2533778347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Net.Dns.DnsResourceRecordA
struct  DnsResourceRecordA_t2533778347  : public DnsResourceRecordIPAddress_t2549622903
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNSRESOURCERECORDA_T2533778347_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (ConfigurationSection_t2600766927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	ConfigurationSection_t2600766927::get_offset_of_sectionInformation_15(),
	ConfigurationSection_t2600766927::get_offset_of_section_handler_16(),
	ConfigurationSection_t2600766927::get_offset_of_externalDataXml_17(),
	ConfigurationSection_t2600766927::get_offset_of__configContext_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (ConfigurationSectionCollection_t4261113299), -1, sizeof(ConfigurationSectionCollection_t4261113299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2301[3] = 
{
	ConfigurationSectionCollection_t4261113299::get_offset_of_group_10(),
	ConfigurationSectionCollection_t4261113299::get_offset_of_config_11(),
	ConfigurationSectionCollection_t4261113299_StaticFields::get_offset_of_lockObject_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CGetEnumeratorU3Ed__17_t2724409453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[4] = 
{
	U3CGetEnumeratorU3Ed__17_t2724409453::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__17_t2724409453::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__17_t2724409453::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__17_t2724409453::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (ConfigurationSectionGroup_t2230982736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[5] = 
{
	ConfigurationSectionGroup_t2230982736::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_config_2(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_group_3(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (ConfigurationSectionGroupCollection_t575145286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[2] = 
{
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (ConfigurationUserLevel_t1204907851)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2305[4] = 
{
	ConfigurationUserLevel_t1204907851::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (ConfigurationValidatorAttribute_t1007519140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[2] = 
{
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (ConfigurationValidatorBase_t210547623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (DefaultSection_t3840532724), -1, sizeof(DefaultSection_t3840532724_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	DefaultSection_t3840532724_StaticFields::get_offset_of_properties_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (DefaultValidator_t300527515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (ElementInformation_t3165583784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[3] = 
{
	ElementInformation_t3165583784::get_offset_of_propertyInfo_0(),
	ElementInformation_t3165583784::get_offset_of_owner_1(),
	ElementInformation_t3165583784::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (ExeConfigurationFileMap_t1419586304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[3] = 
{
	ExeConfigurationFileMap_t1419586304::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (IgnoreSection_t681509237), -1, sizeof(IgnoreSection_t681509237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[2] = 
{
	IgnoreSection_t681509237::get_offset_of_xml_19(),
	IgnoreSection_t681509237_StaticFields::get_offset_of_properties_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (InternalConfigurationFactory_t3846641927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (InternalConfigurationSystem_t2108740756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[3] = 
{
	InternalConfigurationSystem_t2108740756::get_offset_of_host_0(),
	InternalConfigurationSystem_t2108740756::get_offset_of_root_1(),
	InternalConfigurationSystem_t2108740756::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (InternalConfigurationHost_t547577555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ExeConfigurationHost_t2778769322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[2] = 
{
	ExeConfigurationHost_t2778769322::get_offset_of_map_0(),
	ExeConfigurationHost_t2778769322::get_offset_of_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (InternalConfigurationRoot_t547578517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[2] = 
{
	InternalConfigurationRoot_t547578517::get_offset_of_host_0(),
	InternalConfigurationRoot_t547578517::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (PropertyInformation_t2089433965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[7] = 
{
	PropertyInformation_t2089433965::get_offset_of_isModified_0(),
	PropertyInformation_t2089433965::get_offset_of_lineNumber_1(),
	PropertyInformation_t2089433965::get_offset_of_source_2(),
	PropertyInformation_t2089433965::get_offset_of_val_3(),
	PropertyInformation_t2089433965::get_offset_of_origin_4(),
	PropertyInformation_t2089433965::get_offset_of_owner_5(),
	PropertyInformation_t2089433965::get_offset_of_property_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (PropertyInformationCollection_t954922393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (PropertyInformationEnumerator_t1453501302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[2] = 
{
	PropertyInformationEnumerator_t1453501302::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t1453501302::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (PropertyValueOrigin_t1217826846)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2321[4] = 
{
	PropertyValueOrigin_t1217826846::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (ProtectedConfiguration_t1807950812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ProtectedConfigurationProvider_t3971982415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ProtectedConfigurationProviderCollection_t388338823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (ProtectedConfigurationSection_t3541826375), -1, sizeof(ProtectedConfigurationSection_t3541826375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2325[4] = 
{
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_defaultProviderProp_19(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_providersProp_20(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_properties_21(),
	ProtectedConfigurationSection_t3541826375::get_offset_of_providers_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (ProviderSettings_t873049714), -1, sizeof(ProviderSettings_t873049714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2326[4] = 
{
	ProviderSettings_t873049714::get_offset_of_parameters_15(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_nameProp_16(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_typeProp_17(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (ProviderSettingsCollection_t585304908), -1, sizeof(ProviderSettingsCollection_t585304908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2327[1] = 
{
	ProviderSettingsCollection_t585304908_StaticFields::get_offset_of_props_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (SectionInfo_t1739019515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[5] = 
{
	SectionInfo_t1739019515::get_offset_of_allowLocation_6(),
	SectionInfo_t1739019515::get_offset_of_requirePermission_7(),
	SectionInfo_t1739019515::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t1739019515::get_offset_of_allowDefinition_9(),
	SectionInfo_t1739019515::get_offset_of_allowExeDefinition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (SectionGroupInfo_t2346323570), -1, sizeof(SectionGroupInfo_t2346323570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2329[4] = 
{
	SectionGroupInfo_t2346323570::get_offset_of_modified_6(),
	SectionGroupInfo_t2346323570::get_offset_of_sections_7(),
	SectionGroupInfo_t2346323570::get_offset_of_groups_8(),
	SectionGroupInfo_t2346323570_StaticFields::get_offset_of_emptyList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (ConfigInfoCollection_t3264723080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (SectionInformation_t2754609709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[13] = 
{
	SectionInformation_t2754609709::get_offset_of_parent_0(),
	SectionInformation_t2754609709::get_offset_of_allow_definition_1(),
	SectionInformation_t2754609709::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t2754609709::get_offset_of_allow_location_3(),
	SectionInformation_t2754609709::get_offset_of_allow_override_4(),
	SectionInformation_t2754609709::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t2754609709::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t2754609709::get_offset_of_require_permission_7(),
	SectionInformation_t2754609709::get_offset_of_config_source_8(),
	SectionInformation_t2754609709::get_offset_of_name_9(),
	SectionInformation_t2754609709::get_offset_of_raw_xml_10(),
	SectionInformation_t2754609709::get_offset_of_protection_provider_11(),
	SectionInformation_t2754609709::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (ProviderBase_t2882126354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[3] = 
{
	ProviderBase_t2882126354::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2882126354::get_offset_of__description_1(),
	ProviderBase_t2882126354::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (ProviderCollection_t2548499159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[3] = 
{
	ProviderCollection_t2548499159::get_offset_of_lookup_0(),
	ProviderCollection_t2548499159::get_offset_of_readOnly_1(),
	ProviderCollection_t2548499159::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (SR_t2523137205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (MonoPInvokeCallbackAttribute_t3420015604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (CFObject_t3730145744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[1] = 
{
	CFObject_t3730145744::get_offset_of_U3CHandleU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (CFArray_t3428036444), -1, sizeof(CFArray_t3428036444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2346[1] = 
{
	CFArray_t3428036444_StaticFields::get_offset_of_kCFTypeArrayCallbacks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (CFNumber_t2174524538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (CFRange_t2990408436)+ sizeof (RuntimeObject), sizeof(CFRange_t2990408436 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2348[2] = 
{
	CFRange_t2990408436::get_offset_of_Location_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFRange_t2990408436::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (CFStreamClientContext_t3245641555)+ sizeof (RuntimeObject), sizeof(CFStreamClientContext_t3245641555 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[5] = 
{
	CFStreamClientContext_t3245641555::get_offset_of_Version_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3245641555::get_offset_of_Info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3245641555::get_offset_of_Retain_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3245641555::get_offset_of_Release_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CFStreamClientContext_t3245641555::get_offset_of_CopyDescription_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (CFString_t3358817686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[1] = 
{
	CFString_t3358817686::get_offset_of_str_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (CFDictionary_t3548969133), -1, sizeof(CFDictionary_t3548969133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2351[2] = 
{
	CFDictionary_t3548969133_StaticFields::get_offset_of_KeyCallbacks_1(),
	CFDictionary_t3548969133_StaticFields::get_offset_of_ValueCallbacks_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (CFUrl_t2608898728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (CFRunLoop_t1302886598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (CFProxyType_t2264861409)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[8] = 
{
	CFProxyType_t2264861409::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (CFProxy_t537977545), -1, sizeof(CFProxy_t537977545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[14] = 
{
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyAutoConfigurationJavaScriptKey_0(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyAutoConfigurationURLKey_1(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyHostNameKey_2(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyPasswordKey_3(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyPortNumberKey_4(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeKey_5(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyUsernameKey_6(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeAutoConfigurationURL_7(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeAutoConfigurationJavaScript_8(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeFTP_9(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeHTTP_10(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeHTTPS_11(),
	CFProxy_t537977545_StaticFields::get_offset_of_kCFProxyTypeSOCKS_12(),
	CFProxy_t537977545::get_offset_of_settings_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (CFProxySettings_t4092929580), -1, sizeof(CFProxySettings_t4092929580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2356[7] = 
{
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesHTTPEnable_0(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesHTTPPort_1(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesHTTPProxy_2(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigEnable_3(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigJavaScript_4(),
	CFProxySettings_t4092929580_StaticFields::get_offset_of_kCFNetworkProxiesProxyAutoConfigURLString_5(),
	CFProxySettings_t4092929580::get_offset_of_settings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (CFNetwork_t2805084167), -1, sizeof(CFNetwork_t2805084167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2357[3] = 
{
	CFNetwork_t2805084167_StaticFields::get_offset_of_lock_obj_0(),
	CFNetwork_t2805084167_StaticFields::get_offset_of_get_proxy_queue_1(),
	CFNetwork_t2805084167_StaticFields::get_offset_of_proxy_event_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (GetProxyData_t1386489386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[5] = 
{
	GetProxyData_t1386489386::get_offset_of_script_0(),
	GetProxyData_t1386489386::get_offset_of_targetUri_1(),
	GetProxyData_t1386489386::get_offset_of_error_2(),
	GetProxyData_t1386489386::get_offset_of_result_3(),
	GetProxyData_t1386489386::get_offset_of_evt_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (CFProxyAutoConfigurationResultCallback_t3537096558), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (CFWebProxy_t1432115055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[2] = 
{
	CFWebProxy_t1432115055::get_offset_of_credentials_0(),
	CFWebProxy_t1432115055::get_offset_of_userSpecified_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (U3CU3Ec__DisplayClass13_0_t3879183210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[2] = 
{
	U3CU3Ec__DisplayClass13_0_t3879183210::get_offset_of_proxies_0(),
	U3CU3Ec__DisplayClass13_0_t3879183210::get_offset_of_runLoop_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (ServerCertValidationCallbackWrapper_t93117366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (ChainValidationHelper_t3893280544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[8] = 
{
	ChainValidationHelper_t3893280544::get_offset_of_sender_0(),
	ChainValidationHelper_t3893280544::get_offset_of_settings_1(),
	ChainValidationHelper_t3893280544::get_offset_of_provider_2(),
	ChainValidationHelper_t3893280544::get_offset_of_certValidationCallback_3(),
	ChainValidationHelper_t3893280544::get_offset_of_certSelectionCallback_4(),
	ChainValidationHelper_t3893280544::get_offset_of_callbackWrapper_5(),
	ChainValidationHelper_t3893280544::get_offset_of_tlsStream_6(),
	ChainValidationHelper_t3893280544::get_offset_of_request_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (LegacyTlsProvider_t3024743), -1, sizeof(LegacyTlsProvider_t3024743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2366[1] = 
{
	LegacyTlsProvider_t3024743_StaticFields::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (MonoTlsProviderFactory_t3928617369), -1, sizeof(MonoTlsProviderFactory_t3928617369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[4] = 
{
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_locker_0(),
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_initialized_1(),
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_defaultProvider_2(),
	MonoTlsProviderFactory_t3928617369_StaticFields::get_offset_of_providerRegistration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (MonoTlsStream_t1249652258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[7] = 
{
	MonoTlsStream_t1249652258::get_offset_of_provider_0(),
	MonoTlsStream_t1249652258::get_offset_of_networkStream_1(),
	MonoTlsStream_t1249652258::get_offset_of_request_2(),
	MonoTlsStream_t1249652258::get_offset_of_sslStream_3(),
	MonoTlsStream_t1249652258::get_offset_of_status_4(),
	MonoTlsStream_t1249652258::get_offset_of_U3CCertificateValidationFailedU3Ek__BackingField_5(),
	MonoTlsStream_t1249652258::get_offset_of_settings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (SystemCertificateValidator_t2641375168), -1, sizeof(SystemCertificateValidator_t2641375168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2369[3] = 
{
	SystemCertificateValidator_t2641375168_StaticFields::get_offset_of_is_macosx_0(),
	SystemCertificateValidator_t2641375168_StaticFields::get_offset_of_revocation_mode_1(),
	SystemCertificateValidator_t2641375168_StaticFields::get_offset_of_s_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (CallbackHelpers_t4210896810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (U3CU3Ec__DisplayClass5_0_t573183817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[1] = 
{
	U3CU3Ec__DisplayClass5_0_t573183817::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (U3CU3Ec__DisplayClass8_0_t183195720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[1] = 
{
	U3CU3Ec__DisplayClass8_0_t183195720::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (LegacySslStream_t83997747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	LegacySslStream_t83997747::get_offset_of_ssl_stream_6(),
	LegacySslStream_t83997747::get_offset_of_certificateValidator_7(),
	LegacySslStream_t83997747::get_offset_of_provider_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (U3CU3Ec__DisplayClass57_0_t3446885867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[3] = 
{
	U3CU3Ec__DisplayClass57_0_t3446885867::get_offset_of_clientCertificates_0(),
	U3CU3Ec__DisplayClass57_0_t3446885867::get_offset_of_targetHost_1(),
	U3CU3Ec__DisplayClass57_0_t3446885867::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (MonoSslStreamImpl_t1549641033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[1] = 
{
	MonoSslStreamImpl_t1549641033::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (MonoSslStreamWrapper_t4202517786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[1] = 
{
	MonoSslStreamWrapper_t4202517786::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (MonoTlsProviderWrapper_t210206708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[1] = 
{
	MonoTlsProviderWrapper_t210206708::get_offset_of_provider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (DnsClass_t3168085651)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2378[9] = 
{
	DnsClass_t3168085651::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (DnsHeader_t2481892084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	DnsHeader_t2481892084::get_offset_of_bytes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (DnsOpCode_t1353552885)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2380[6] = 
{
	DnsOpCode_t1353552885::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (DnsPacket_t413343383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[3] = 
{
	DnsPacket_t413343383::get_offset_of_packet_0(),
	DnsPacket_t413343383::get_offset_of_position_1(),
	DnsPacket_t413343383::get_offset_of_header_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (DnsQClass_t2376709280)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2382[11] = 
{
	DnsQClass_t2376709280::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (DnsQType_t1279797630)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2383[71] = 
{
	DnsQType_t1279797630::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (DnsQuery_t3483920911), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (DnsQuestion_t3090842959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[3] = 
{
	DnsQuestion_t3090842959::get_offset_of_name_0(),
	DnsQuestion_t3090842959::get_offset_of_type_1(),
	DnsQuestion_t3090842959::get_offset_of__class_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (DnsRCode_t977533636)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2386[20] = 
{
	DnsRCode_t977533636::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (DnsResourceRecord_t2943454412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[6] = 
{
	DnsResourceRecord_t2943454412::get_offset_of_name_0(),
	DnsResourceRecord_t2943454412::get_offset_of_type_1(),
	DnsResourceRecord_t2943454412::get_offset_of_klass_2(),
	DnsResourceRecord_t2943454412::get_offset_of_ttl_3(),
	DnsResourceRecord_t2943454412::get_offset_of_rdlength_4(),
	DnsResourceRecord_t2943454412::get_offset_of_m_rdata_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (DnsResourceRecordA_t2533778347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (DnsResourceRecordAAAA_t4028376332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (DnsResourceRecordCName_t595248992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	DnsResourceRecordCName_t595248992::get_offset_of_cname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (DnsResourceRecordIPAddress_t2549622903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[1] = 
{
	DnsResourceRecordIPAddress_t2549622903::get_offset_of_address_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (DnsResourceRecordPTR_t1722196618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	DnsResourceRecordPTR_t1722196618::get_offset_of_dname_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (DnsResponse_t3880332876), -1, sizeof(DnsResponse_t3880332876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2393[7] = 
{
	DnsResponse_t3880332876_StaticFields::get_offset_of_EmptyRR_3(),
	DnsResponse_t3880332876_StaticFields::get_offset_of_EmptyQS_4(),
	DnsResponse_t3880332876::get_offset_of_question_5(),
	DnsResponse_t3880332876::get_offset_of_answer_6(),
	DnsResponse_t3880332876::get_offset_of_authority_7(),
	DnsResponse_t3880332876::get_offset_of_additional_8(),
	DnsResponse_t3880332876::get_offset_of_offset_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (DnsType_t1822475631)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2394[70] = 
{
	DnsType_t1822475631::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (DnsUtil_t3812046859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (ResolverError_t2494446112)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[10] = 
{
	ResolverError_t2494446112::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (SimpleResolver_t2933968162), -1, sizeof(SimpleResolver_t2933968162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2397[8] = 
{
	SimpleResolver_t2933968162_StaticFields::get_offset_of_EmptyStrings_0(),
	SimpleResolver_t2933968162_StaticFields::get_offset_of_EmptyAddresses_1(),
	SimpleResolver_t2933968162::get_offset_of_endpoints_2(),
	SimpleResolver_t2933968162::get_offset_of_client_3(),
	SimpleResolver_t2933968162::get_offset_of_queries_4(),
	SimpleResolver_t2933968162::get_offset_of_receive_cb_5(),
	SimpleResolver_t2933968162::get_offset_of_timeout_cb_6(),
	SimpleResolver_t2933968162::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (SimpleResolverEventArgs_t4294564137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[9] = 
{
	SimpleResolverEventArgs_t4294564137::get_offset_of_Completed_1(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CResolverErrorU3Ek__BackingField_2(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CErrorMessageU3Ek__BackingField_3(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CHostNameU3Ek__BackingField_4(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_U3CHostEntryU3Ek__BackingField_5(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_QueryID_6(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_Retries_7(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_Timer_8(),
	SimpleResolverEventArgs_t4294564137::get_offset_of_PTRAddress_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (ThrowHelper_t3666395979), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
