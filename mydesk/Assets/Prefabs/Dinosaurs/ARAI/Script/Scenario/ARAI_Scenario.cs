﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 모든 시나리오 스크립트의 부모 클래스.
/// </summary>
public class ARAI_Scenario : MonoBehaviour
{
    public ARAI_IntelligenEntity intelligentEntity;



    // 시나리오의 초기화 및 실행 코드.
    // 새로운 시나리오 스크립트에서 상속하여 사용한다.
	protected virtual void Start()
    {
	
	}

    // 시나리오의 지속 코드.
    // 상속하여 사용.
    protected virtual void Update()
    {
		  
	}
}
