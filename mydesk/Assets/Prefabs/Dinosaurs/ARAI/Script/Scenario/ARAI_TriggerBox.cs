﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_TriggerBox : MonoBehaviour
{
	[HideInInspector]
	public ARAI_Scenario scenario;

	private Camera cam;
	private BoxCollider box;



	private void Start()
	{
		cam = Camera.main;
		box = this.GetComponent<BoxCollider>();
	}

	private void Update()
	{
		if (box.bounds.Contains(cam.transform.position))
		{
			// 박스가 카메라와 충돌했을 경우.
			// 시나리오에서 특정한 메서드를 실행시켜 관리한다.
			// scenario.PlayScenario();
			// ...
		}
	}
}
