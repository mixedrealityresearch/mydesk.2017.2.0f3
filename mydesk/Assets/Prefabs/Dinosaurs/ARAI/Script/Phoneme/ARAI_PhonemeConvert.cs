﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARAI_PhonemeConvert : MonoBehaviour
{
    private static ARAI_PhonemeConvert _instance;
    public static ARAI_PhonemeConvert Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = (ARAI_PhonemeConvert)GameObject.FindObjectOfType(typeof(ARAI_PhonemeConvert));
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "ARAI_PhonemeConvert";
                    _instance = container.AddComponent(typeof(ARAI_PhonemeConvert)) as ARAI_PhonemeConvert;
                }
            }
            return _instance;
        }
    }
    


    public void ConvertTTPWJ(ARAI_IntelligenEntity _targetObj)
    {
        if ( _targetObj != null)
        {
            string result = ARAI_KoreanToPhoneme.Seperate(_targetObj.m_originText);
            result = ARAI_EnglishToPhoneme.Seperate(result);
            CreateLipSyncData(_targetObj , result);
        }
    }

    public void CreateLipSyncData(ARAI_IntelligenEntity _targetObj , string _PhonemeResult, float wordsPerMinute = 120.0f)
    {
        if(_targetObj.m_PhonemeList.Count > 0)
        {
            _targetObj.m_PhonemeList.Clear();
        }
        string[] words = _PhonemeResult.Split(' ');

        float timePerWord = (60f / wordsPerMinute);
        float duration = timePerWord * words.Length;

        for (int wordIndex = 0; wordIndex < words.Length; ++wordIndex)
        {
            string word = words[wordIndex];
            char[] chars = word.ToCharArray();
            
            for (int i = 0; i < chars.Length; ++i)
            {
                char c = chars[i];
                if (c == '>')
                    continue;

                float phonemeDuration = 0.110f;
                
                int index = ARAI_MappingTable.Instance.phonemeMapper.FindIndex((x) => x.character == c);
                if (index >= 0)
                {
                    Phoneme phonemeData = ARAI_MappingTable.Instance.phonemeMapper[index];
                    string phoneme = phonemeData.phoneme;

                    PhonemeComponent phonemeComp = new PhonemeComponent(phoneme, phonemeDuration, phonemeData.weight);
                    _targetObj.m_PhonemeList.Add(phonemeComp);
                }
            }

            PhonemeComponent blankPhoneme = new PhonemeComponent(" ", 0.110f, 1.0f);
            _targetObj.m_PhonemeList.Add(blankPhoneme);
        }

        // 연음('AI'가 연속으로 두 번 이상 나오는 경우 등)의 지속시간 처리. 
        for (int i = 0; i < _targetObj.m_PhonemeList.Count; ++i)
        {
            PhonemeComponent p = _targetObj.m_PhonemeList[i];
            if (i + 1 < _targetObj.m_PhonemeList.Count &&
               p.phoneme.Equals(_targetObj.m_PhonemeList[i + 1].phoneme))
            {
                p.duration += 0.055f;
                _targetObj.m_PhonemeList.RemoveAt(i + 1);
                --i;
            }
        }
    }
}