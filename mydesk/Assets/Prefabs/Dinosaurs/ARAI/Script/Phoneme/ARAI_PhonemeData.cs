﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_PhonemeData
{
    [SerializeField]
    public List<PhonemeComponent> phonemes;



    public ARAI_PhonemeData(string phonemeResult)
    {

    }
    
    public void SetPhonemeData(string phoneme , float time)
    {

    }
    
    [System.Serializable]
    public struct PhonemeComponent
    {
        public string phoneme;
        public float weight;

        public PhonemeComponent(string phoneme, float weight)
        {
            this.phoneme = phoneme;
            this.weight = weight;
        }
    }
}
