﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ARAI_EnglishToPhoneme
{
    // 미리 변경할 문자들.
    private static Dictionary<string, string> replaces = new Dictionary<string, string>()
    {
        {"oo"   , "u"},
        {"th"   , "u"},
        {"gh"   , " "},
        {"ch"   , "i"},
    };
    


    public static string Seperate(string data)
    {
        // 문장을 소문자로.
        string result = data.ToLower();

        // replaces에 정의된 문자는 미리 변경.
        var iter = replaces.GetEnumerator();
        while (iter.MoveNext())
        {
            result = result.Replace(iter.Current.Key, iter.Current.Value);
        }
        
        return result;
    }
}
