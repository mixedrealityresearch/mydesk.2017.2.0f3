﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Naviworks_Util
{
    public class MultiSortedDictionary<TKey, TValue>
    {
        public MultiSortedDictionary()
        {
            dic = new SortedDictionary<TKey, List<TValue>>();
        }

        public MultiSortedDictionary(IComparer<TKey> comparer)
        {
            dic = new SortedDictionary<TKey, List<TValue>>(comparer);
        }

        public void Add(TKey key, TValue value)
        {
            List<TValue> list;

            if (dic.TryGetValue(key, out list))
            {
                list.Add(value);
            }
            else
            {
                list = new List<TValue>();
                list.Add(value);

                dic.Add(key, list);
            }
        }

        public IEnumerable<TKey> Keys
        {
            get
            {
                return this.dic.Keys;
            }
        }

        public List<TValue> this[TKey key]
        {
            get
            {
                List<TValue> list;

                if (this.dic.TryGetValue(key, out list))
                {
                    return list;
                }
                else
                {
                    return new List<TValue>();
                }
            }
        }

        private SortedDictionary<TKey, List<TValue>> dic = null;
    }

    public static class ARAI_Utill
    {
        public static string ConvertStringArrayToString(string[] array)
        {
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append('.');
            }
            return builder.ToString();
        }

        public static string IPA_PhonemeParse(string _html , string _item , string _endstring)
        {
            int firstIndex = _html.IndexOf(_item);
            if (firstIndex < 0)
                return null;
            _html = _html.Substring(firstIndex);
            int lastIndex = _html.IndexOf(_endstring);
            _html = _html.Substring(0, lastIndex);
            
            string[] ssefs = _html.Split('>');

            string parseResult = "";

            for (int parseIndex = 1; parseIndex < ssefs.Length; parseIndex += 2)
            {
                string split = ssefs[parseIndex];
                int tokenfirstIndex = split.IndexOf("</");
                split = split.Substring(0, tokenfirstIndex);
                parseResult += split;
            }
            return parseResult;
        }

        public static string IPA_PhonemeToKorean(string _html, string _item, string _endstring)
        {
            int firstIndex = _html.LastIndexOf(_item);
            _html = _html.Substring(firstIndex);
            int middleTorkenIndex = _html.IndexOf('/');
            int lastIndex = _html.IndexOf(_endstring);

            _html = _html.Substring(middleTorkenIndex, lastIndex - middleTorkenIndex);
            
            return _html;
        }
    }
}