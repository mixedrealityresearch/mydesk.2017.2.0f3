﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 타겟을 따라 추적하는 트레일 이펙트
/// </summary>
public class ARAI_TraceTrail : MonoBehaviour
{
    [HideInInspector]
    public Transform TargetTransform;
    [HideInInspector]
    public Vector3 TragetPosition;

    public float moveSpeed = 1.0f;          // controls character move speed
    public float rotateSpeed = 180f;        // speed at which character will rotate
    public float stopDistance = 0.1f;       // stop moving when closer to stopDistance
    public float stopRoationAngle = 1.0f;

    private float destinationDistance;      //distance to destination
    private Quaternion targetRotation;



    // Update is called once per frame
    private void Update()
    {
        Vector3 destDir = TragetPosition - transform.position;
        // destDir.y = 0;                           // make it strictly horizontal to avoid object tilting
        destinationDistance = destDir.magnitude;    // get the horizontal distance

        if (destinationDistance >= stopDistance)
        {
            targetRotation = Quaternion.LookRotation(destDir);
            // if (Quaternion.Angle(transform.rotation, targetRotation) >= stopRoationAngle)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
            }
            // else
            {
                transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            }
        }
    }

}
