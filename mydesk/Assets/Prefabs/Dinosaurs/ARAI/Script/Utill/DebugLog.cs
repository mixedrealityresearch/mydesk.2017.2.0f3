﻿// using UnityEngine;
// using System.Collections;
// using System.Collections.Generic;
// using System.IO;
// using System;
   
// public class DebugLog : MonoBehaviour
// {
//     public TextMesh _RuntimeLog;
     
   
   
//     private void OnEnable()
//     {
//         _RuntimeLog.text = "";
   
//         // Debug.Log를 중간에 Hooking할 수 있도록 Callback을 등록한다.
//         Application.logMessageReceived += HandleLog;
//     }
   
//     private void OnDisable()
//     {
//         // GameObject가 disable될 때, 등록한 Callback을 삭제한다.
//         Application.logMessageReceived -= HandleLog;
//     }
     
//     // Debug.Log를 중간에 Hooking해서 처리하는 함수
//     // - 파일에 Log를 기록하거나..등등...
//     private void HandleLog(string logString, string stackTrace, LogType type)
//     {
//         // logString에 debug 문자열
//         // stackTrace에 현재 call stack
//         // type에 로그의 종류
//         _RuntimeLog.text += logString + "\n";
//     }
// }