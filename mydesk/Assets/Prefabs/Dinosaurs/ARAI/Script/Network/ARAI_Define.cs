﻿using System;
using System.Collections.Generic;

namespace ARAI_Define
{
    public enum PacketType
    {
        ARAI_INTELLIGENT_NONE = -1,
        ARAI_INTELLIGENT_TALK = 100,
        ARAI_INTELLIGENT_MESSAGE = 200,
        ARAI_INTELLIGENT_TARGETINFO = 300,
    }

    [Serializable]
    public class PACKET_BASE
    {
        public int PacketType;         // 패킷 타입
    }

    [Serializable]
    public class PACKET_INTELLIGENT_TALK : PACKET_BASE
    {
        public string Emotion;
        public Dictionary<string, int> EmotionValue;
        public string TalkBody;
    }

}