﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Phoneme))]
public class PhonemeDrawer : PropertyDrawer
{



    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        int oldIndentLevel = EditorGUI.indentLevel;

        label = EditorGUI.BeginProperty(position, label, property);
        Rect contentPosition = EditorGUI.PrefixLabel(position, label);
        float width = contentPosition.width;

        if (position.height > 16f)
        {
            position.height = 16f;
            EditorGUI.indentLevel += 1;
            contentPosition = EditorGUI.IndentedRect(position);
            contentPosition.y += 18f;
        }

        EditorGUI.indentLevel = 0;
        contentPosition.width = width * 0.12f;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("character"), GUIContent.none);

        EditorGUIUtility.labelWidth = 15f;
        contentPosition.x += contentPosition.width;
        contentPosition.width = width * 0.68f;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("phoneme"), new GUIContent("→"));
        
        contentPosition.x += contentPosition.width;
        contentPosition.width = width * 0.20f;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("weight"), GUIContent.none);

        EditorGUI.EndProperty();
        EditorGUI.indentLevel = oldIndentLevel;
    }
}
