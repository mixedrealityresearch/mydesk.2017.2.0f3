﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ARAI_MappingTable))]
public class MappingTableInspector : Editor
{
    private static GUILayoutOption miniButtonWidth = GUILayout.Width(20f);
    private static GUIContent phonemeDeleteBtn = new GUIContent("X", "Delete Phoneme.");
    private static GUIContent phonemeAddBtn = new GUIContent("Add Phoneme", "Add new Phoneme item.");
    private static GUIContent emotionDeleteBtn = new GUIContent("X", "Delete Emotion.");
    private static GUIContent emotionAddBtn = new GUIContent("Add Emotion", "Add new Emotion item.");

    public SerializedProperty phonemeList;
    public SerializedProperty emotionList;
    public SerializedProperty talkActionList;



    private void OnEnable()
    {
        phonemeList = serializedObject.FindProperty("phonemeMapper");
        emotionList = serializedObject.FindProperty("emotionMapper");
        talkActionList = serializedObject.FindProperty("talkActionMapper");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        // PhonemeMapper : 커스텀 인스펙터 구조를 사용.
        EditorGUILayout.PropertyField(phonemeList);

        EditorGUI.indentLevel += 1;
        if (phonemeList.isExpanded)
        {
            EditorGUILayout.PropertyField(phonemeList.FindPropertyRelative("Array.size"));
            if (GUILayout.Button(phonemeAddBtn, EditorStyles.miniButton))
            {
                phonemeList.arraySize += 1;
            }

            for (int i = 0; i < phonemeList.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.PropertyField(phonemeList.GetArrayElementAtIndex(i));
                
                if (GUILayout.Button(phonemeDeleteBtn, EditorStyles.miniButton, miniButtonWidth))
                {
                    int oldSize = phonemeList.arraySize;
                    phonemeList.DeleteArrayElementAtIndex(i);
                    if (phonemeList.arraySize == oldSize)
                        phonemeList.DeleteArrayElementAtIndex(i);
                }

                EditorGUILayout.EndHorizontal();
            }
        }
        EditorGUI.indentLevel -= 1;

        // EmotionMapper
        EditorGUILayout.PropertyField(emotionList, true);

        serializedObject.ApplyModifiedProperties();
    }
}
