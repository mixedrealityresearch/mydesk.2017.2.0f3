﻿using UnityEngine;
using System.Collections;

public class ARAI_ClickToMove : MonoBehaviour
{
    private const float stopDistance = 0.1f;
    private const float stopRotation = 1.0f;

    // Controls character move speed.
    public float moveSpeed = 5.0f;
    // Speed at which character will rotate.
    public float rotateSpeed = 90f;
    // 캐릭터가 이동중인지의 여부.
    public bool isMoving = false;
    
    private Transform cam;
    private Animator animator;
    private ARAI_WanderAI wanderAI;
    private Coroutine walkCoroutine;
    private float destinationDistance;
    private Vector3 destinationPosition;



    private void Start()
    {
        destinationPosition = this.transform.position;

        cam = Camera.main.transform;
        wanderAI = this.GetComponent<ARAI_WanderAI>();
        animator = this.GetComponentInChildren<Animator>();
    }
    
    public void MoveToPosition(Vector3 _position)
    {
        if (isMoving)
            StopCoroutine(walkCoroutine);

        destinationPosition = _position;
        walkCoroutine = StartCoroutine(WalkCoroutine());
        isMoving = true;
    }

    private IEnumerator WalkCoroutine()
    {
        Vector3 direction = destinationPosition - this.transform.position;
        direction.y = 0;
        destinationDistance = direction.magnitude;
        direction.Normalize();

        SetWalkAnimationState(true);
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        while (Quaternion.Angle(this.transform.rotation, targetRotation) >= stopRotation)
        {
            this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
            yield return null;   
        }
        
        while (destinationDistance >= stopDistance)
        {
            this.transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            direction = destinationPosition - this.transform.position;
            direction.y = 0;
            destinationDistance = direction.magnitude;
            direction.Normalize();
            yield return null;
        }

        direction = cam.position - destinationPosition;
        direction.y = 0;
        direction.Normalize();
        targetRotation = Quaternion.LookRotation(direction);
        while (Quaternion.Angle(this.transform.rotation, targetRotation) >= stopRotation)
        {
            this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
            yield return null;
        }

        SetWalkAnimationState(false);
        isMoving = false;
    }

    private void SetWalkAnimationState(bool _state)
    {
        animator.SetBool("Walking", _state);
    }
}




/*
private Quaternion targetRotation;
private Transform myTransform;          // this transform
private Vector3 destinationPosition;    // destination position
private float destinationDistance;      // distance to destination



private void Start()
{
    if(null == WatchCamera)
    {
        WatchCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    myTransform = this.transform;
    destinationPosition = myTransform.position; // initialize destinationPosition

    animator = this.GetComponentInChildren<Animator>();

    wanderAI = this.GetComponent<ARAI_WanderAI>();
}

private void Update()
{
    Vector3 destDir = destinationPosition - myTransform.position;
    destDir.y = 0;                              // make it strictly horizontal to avoid object tilting
    destinationDistance = destDir.magnitude;    // get the horizontal distance

    if (destinationDistance >= stopDistance)
    {
        animator.SetBool("Walking", true);
        targetRotation = Quaternion.LookRotation(destDir);
        if( Quaternion.Angle(myTransform.rotation, targetRotation) >= stopRoationAngle)
        {
            myTransform.rotation = Quaternion.RotateTowards(myTransform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
        }
        else
        {
            myTransform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }
        IsMoving = true;
    }
    else
    {
        animator.SetBool("Walking", false);

        Vector3 cameraDir = WatchCamera.position - destinationPosition;
        cameraDir.y = 0;                        // make it strictly horizontal to avoid object tilting

        targetRotation = Quaternion.LookRotation(cameraDir);

        if (Quaternion.Angle(myTransform.rotation, targetRotation) >= stopRoationAngle)
        {
            myTransform.rotation = Quaternion.RotateTowards(myTransform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
        }
        IsMoving = false;
    }
}
*/
