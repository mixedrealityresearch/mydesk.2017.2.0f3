﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ARAI_TestIntelligenManager : MonoBehaviour
{
    public ARAI_IntelligenManager IntelligentManager;
    public List<Slider> sliders;

    private Dictionary<string, float> emotionIntensity;
    


    private void Start()
    {
        emotionIntensity = new Dictionary<string, float>();
        emotionIntensity["Angry"]       = 0.0f;
        emotionIntensity["Happy"]       = 0.0f;
        emotionIntensity["Hate"]        = 0.0f;
        emotionIntensity["Sad"]         = 0.0f;
        emotionIntensity["Surprisal"]   = 0.0f;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && GUIUtility.hotControl == 0)
        {
            // 대화창 선택시 움직임을 방지.
            int bottomLimit = (int)(Screen.height * 0.15f);
            int topLimit = (int)(Screen.height * 0.50f);
            if (Input.mousePosition.y >= bottomLimit && Input.mousePosition.y <= topLimit)
            {
                Plane playerPlane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                float hitdist = 0.0f;
                if (playerPlane.Raycast(ray, out hitdist))
                {
                    IntelligentManager.MoveToPosition(ray.GetPoint(hitdist));
                }
            }
        }

        // 아바타 스왑 테스트용.
        for (int i = 0; i < 10; ++i)
        {
            if (Input.GetKeyDown(i.ToString()))
            {
                IntelligentManager.SwapIntelligentEntity(i);
                break;
            }
        }

        // 카메라를 아바타의 얼굴로.
        //Camera.main.transform.LookAt(IntelligentManager.intelligentEntity.headTransform);
        float wheel = Input.GetAxis("Mouse ScrollWheel");
        if (wheel != 0f)
        {
            Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView - wheel * 20f, 10f, 80f);
        }
    }



    // 이하는 TestScene 관리용.

    public void SetTalkMessage(Text _text)
    {
        IntelligentManager.IntelligentTalk(_text.text);
    }

    public void OnEmotionButtonRelease(string _emotionName)
    {
        IntelligentManager.ResetEmotion();
        IntelligentManager.PlayEmotion(_emotionName, 1.0f, 4f);
    }

    public void OnEmotionSliderChange(string _emotionName)
    {
        Slider slider = sliders.Find((s) => (s.name == _emotionName));
        if (slider == null)
            return;

        emotionIntensity[_emotionName] = slider.value;
        BalanceEmotionIntensity(_emotionName);
        float total = 0f;
        foreach (Slider s in sliders)
        {
            s.value = emotionIntensity[s.name];
            total += emotionIntensity[s.name];
        }
    }

    public void OnEmotionMixedPlayRelease()
    {
        IntelligentManager.ResetEmotion();
        foreach (Slider s in sliders)
        {
            IntelligentManager.PlayEmotion(s.name, emotionIntensity[s.name], 4f);
        }
    }

    private void BalanceEmotionIntensity(string _mainEmotion)
    {
        float totalValue = 0f;
        foreach (var pair in emotionIntensity)
        {
            totalValue += pair.Value;
        }
        if (totalValue <= 1f)
            return;

        float overValue = totalValue - 1f;
        float mainValue = emotionIntensity[_mainEmotion];
        float elseValue = totalValue - mainValue;
        float calcValue = elseValue - overValue;
        foreach (var pair in emotionIntensity.ToList())
        {
            if (pair.Key.Equals(_mainEmotion))
                continue;

            float percentage = pair.Value / elseValue;
            emotionIntensity[pair.Key] = calcValue * percentage;
        }
    }
}